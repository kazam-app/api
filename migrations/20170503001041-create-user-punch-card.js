'use strict'

const fk = require('../lib/umzug/fk')
const uuid = require('../lib/umzug/uuid')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('user_punch_cards', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      user_id: fk(Sequelize, 'users', true, true),
      punch_card_id: fk(Sequelize, 'punch_cards'),
      identifier: uuid(Sequelize),
      identity: identity(Sequelize),
      punch_count: {
        defaultValue: 0,
        allowNull: false,
        type: Sequelize.INTEGER
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('user_punch_cards')
  }
}
