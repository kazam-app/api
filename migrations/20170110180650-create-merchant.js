'use strict'

const fk = require('../lib/umzug/fk')
const uuid = require('../lib/umzug/uuid')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('merchants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      organization_id: fk(Sequelize, 'organizations', true, true),
      category_id: fk(Sequelize, 'categories', true, true),
      is_public: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      identifier: {
        allowNull: true,
        type: Sequelize.STRING
      },
      identifier_sk: uuid(Sequelize),
      identifier_pk: uuid(Sequelize),
      color: {
        allowNull: true,
        type: Sequelize.STRING
      },
      invite_code: {
        unique: true,
        allowNull: true,
        type: Sequelize.STRING
      },
      has_bank: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      has_code: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      has_company_name: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      budget_rating: {
        defaultValue: 0,
        allowNull: false,
        type: Sequelize.INTEGER
      },
      image_url: {
        allowNull: false,
        type: Sequelize.STRING
      },
      request_tip: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('merchants')
  }
}
