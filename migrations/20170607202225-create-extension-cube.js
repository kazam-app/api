'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('CREATE EXTENSION cube;')
      .catch(function (err) {
        console.log(err.message)
      })
  },
  down: function (queryInterface, Sequelize) {
    /*
      Non reversible
    */
  }
}
