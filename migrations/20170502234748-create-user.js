'use strict'

const uuid = require('../lib/umzug/uuid')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      is_valid: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      is_verified: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      last_names: {
        allowNull: false,
        type: Sequelize.STRING
      },
      email: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING
      },
      validate_email: {
        unique: true,
        allowNull: true,
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.INTEGER
      },
      birthdate: {
        type: Sequelize.DATEONLY
      },
      image_url: {
        type: Sequelize.STRING
      },
      fb_id: {
        unique: true,
        type: Sequelize.TEXT
      },
      fb_token: {
        type: Sequelize.TEXT
      },
      fb_expires_at: {
        type: Sequelize.DATE
      },
      identifier: uuid(Sequelize),
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('users')
  }
}
