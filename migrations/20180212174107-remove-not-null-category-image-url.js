'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('ALTER TABLE categories ALTER COLUMN image_url DROP NOT NULL')
      .catch(function (err) {
        console.log(err.message)
      })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('ALTER TABLE categories ALTER COLUMN image_url SET NOT NULL')
  }
}
