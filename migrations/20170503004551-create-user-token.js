'use strict'

const fk = require('../lib/umzug/fk')
const tokenPair = require('../lib/umzug/token')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('user_tokens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      user_id: fk(Sequelize, 'users', true, true),
      identity: identity(Sequelize),
      token: tokenPair.token(Sequelize, false),
      secret: tokenPair.secret(Sequelize, false),
      expires_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('user_tokens')
  }
}
