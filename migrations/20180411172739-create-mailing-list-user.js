'use strict'

const fk = require('../lib/umzug/fk')

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('mailing_list_users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      mailing_list_id: fk(Sequelize, 'mailing_lists', true, true),
      user_id: fk(Sequelize, 'users', true, true),
      mailchimp_id: {
        allowNull: false,
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('mailing_list_users')
  }
}
