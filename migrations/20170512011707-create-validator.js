'use strict'

const fk = require('../lib/umzug/fk')
const uuid = require('../lib/umzug/uuid')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('validators', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      merchant_id: fk(Sequelize, 'merchants', true, true),
      shop_id: fk(Sequelize, 'shops'),
      is_active: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      identity: identity(Sequelize),
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      uid: {
        type: Sequelize.STRING
      },
      major: {
        type: Sequelize.INTEGER
      },
      minor: {
        type: Sequelize.INTEGER
      },
      token: {
        unique: true,
        type: Sequelize.TEXT
      },
      image_url: {
        type: Sequelize.STRING
      },
      identifier: uuid(Sequelize),
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('validators')
  }
}
