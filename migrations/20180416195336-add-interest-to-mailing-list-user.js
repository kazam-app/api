'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('mailing_list_users', 'interest', {
      type: Sequelize.STRING
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('mailing_list_users', 'interest')
  }
}
