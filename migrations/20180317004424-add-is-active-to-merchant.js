'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('merchants', 'is_active', { type: Sequelize.BOOLEAN, defaultValue: true })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('merchants', 'is_active')
  }
}
