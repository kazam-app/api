'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('organization_users', 'mailchimp_id', {
      type: Sequelize.STRING
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('organization_users', 'mailchimp_id')
  }
}
