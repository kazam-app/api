'use strict'

const fk = require('../lib/umzug/fk')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('punch_cards', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      merchant_id: fk(Sequelize, 'merchants', true, true),
      identity: identity(Sequelize),
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      prize: {
        allowNull: false,
        type: Sequelize.STRING
      },
      rules: {
        allowNull: false,
        type: Sequelize.STRING
      },
      terms: {
        allowNull: false,
        type: Sequelize.TEXT
      },
      redeem_code: {
        unique: true,
        allowNull: false,
        type: Sequelize.STRING
      },
      punch_limit: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      expires_at: {
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('punch_cards')
  }
}
