'use strict'

const fk = require('../lib/umzug/fk')
const tokenPair = require('../lib/umzug/token')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('organization_user_tokens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      organization_user_id: fk(Sequelize, 'organization_users', true, true),
      identity: identity(Sequelize),
      token: tokenPair.token(Sequelize, false),
      secret: tokenPair.secret(Sequelize, false),
      expires_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('organization_user_tokens')
  }
}
