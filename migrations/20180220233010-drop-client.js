'use strict'

const uuid = require('../lib/umzug/uuid')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('clients')
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.createTable('clients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      identifier: uuid(Sequelize),
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      is_active: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      deleted_at: {
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  }
}
