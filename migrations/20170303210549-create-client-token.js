'use strict'

const fk = require('../lib/umzug/fk')
const tokenPair = require('../lib/umzug/token')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('client_tokens', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      client_id: fk(Sequelize, 'clients', true, true),
      token: tokenPair.token(Sequelize, false),
      secret: tokenPair.secret(Sequelize, false),
      deleted_at: {
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('client_tokens')
  }
}
