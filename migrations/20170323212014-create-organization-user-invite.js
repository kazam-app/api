'use strict'

const fk = require('../lib/umzug/fk')
const tokenPair = require('../lib/umzug/token')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('organization_user_invites', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      organization_user_id: fk(Sequelize, 'organization_users', true, true),
      email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      token: tokenPair.token(Sequelize, false),
      secret: tokenPair.secret(Sequelize, false),
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('organization_user_invites')
  }
}
