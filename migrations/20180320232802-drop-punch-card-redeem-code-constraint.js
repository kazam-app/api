'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.removeConstraint('punch_cards', 'punch_cards_redeem_code_key').catch(function (err) {
      console.log(err.message)
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.addConstraint('punch_cards', ['redeem_code'], {
      type: 'unique',
      name: 'punch_cards_redeem_code_key'
    }).catch(function (err) {
      console.log(err.message)
    })
  }
}
