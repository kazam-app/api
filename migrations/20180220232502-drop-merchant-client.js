'use strict'

const fk = require('../lib/umzug/fk')
const uuid = require('../lib/umzug/uuid')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('merchant_clients')
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.createTable('merchant_clients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      merchant_id: fk(Sequelize, 'merchants', true, true),
      client_id: fk(Sequelize, 'clients', true, true),
      identity: identity(Sequelize),
      identifier: uuid(Sequelize),
      name: Sequelize.STRING,
      fee: {
        allowNull: false,
        defaultValue: 0.0,
        type: Sequelize.NUMERIC(5, 2)
      },
      deleted_at: {
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  }
}
