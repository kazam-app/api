'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('users', 'has_expiry_notify_permission', {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('users', 'has_expiry_notify_permission')
  }
}
