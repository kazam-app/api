'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('punch_cards', 'validation_limit_type',
      {
        defaultValue: 0,
        type: Sequelize.INTEGER
      }
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('punch_cards', 'validation_limit_type')
  }
}
