'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('merchants', 'background_image_url', { type: Sequelize.STRING })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('merchants', 'background_image_url')
  }
}
