'use strict'

const fk = require('../lib/umzug/fk')
const uuid = require('../lib/umzug/uuid')
const tokenPair = require('../lib/umzug/token')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('terminals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      shop_id: fk(Sequelize, 'shops', true, true),
      identity: identity(Sequelize),
      token: tokenPair.token(Sequelize, false),
      secret: tokenPair.secret(Sequelize, false),
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      identifier: uuid(Sequelize),
      password: {
        allowNull: true,
        type: Sequelize.STRING
      },
      lat: {
        allowNull: true,
        type: Sequelize.DOUBLE
      },
      lng: {
        allowNull: true,
        type: Sequelize.DOUBLE
      },
      logged_in_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('terminals')
  }
}
