'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query(
      'ALTER TABLE user_punch_cards ALTER COLUMN alert_notifcation_sent DROP DEFAULT'
    ).catch(function (err) {
      console.log(err.message)
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query(
      'ALTER TABLE user_punch_cards ALTER COLUMN alert_notifcation_sent SET DEFAULT 0'
    )
  }
}
