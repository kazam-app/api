'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('ALTER TABLE shops ALTER COLUMN city DROP NOT NULL')
      .catch(function (err) {
        console.log(err.message)
      })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.sequelize.query('ALTER TABLE shops ALTER COLUMN city SET NOT NULL')
  }
}
