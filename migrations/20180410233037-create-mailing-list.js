'use strict'

const identity = require('../lib/umzug/identity')

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('mailing_lists', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      identity: identity(Sequelize),
      mailchimp_id: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('mailing_lists')
  }
}
