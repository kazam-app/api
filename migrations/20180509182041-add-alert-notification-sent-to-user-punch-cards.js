'use strict'

const field = 'alert_notifcation_sent'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('user_punch_cards', field, {
      type: Sequelize.INTEGER,
      defaultValue: 0
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('user_punch_cards', field)
  }
}
