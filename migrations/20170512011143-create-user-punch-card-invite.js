'use strict'

const fk = require('../lib/umzug/fk')
const uuid = require('../lib/umzug/uuid')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('user_punch_card_invites', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      merchant_id: fk(Sequelize, 'merchants', true, true),
      user_punch_card_id: fk(Sequelize, 'user_punch_cards', true, true),
      identifier: uuid(Sequelize),
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('user_punch_card_invites')
  }
}
