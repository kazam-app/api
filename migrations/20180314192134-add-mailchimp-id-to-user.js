'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'mailchimp_id', {
      type: Sequelize.STRING
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('users', 'mailchimp_id')
  }
}
