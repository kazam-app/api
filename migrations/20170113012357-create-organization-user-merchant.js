'use strict'

const fk = require('../lib/umzug/fk')
const identity = require('../lib/umzug/identity')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('organization_user_merchants', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      organization_user_id: fk(Sequelize, 'organization_users', true, true),
      merchant_id: fk(Sequelize, 'merchants', true, true),
      role: identity(Sequelize),
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('organization_user_merchants')
  }
}
