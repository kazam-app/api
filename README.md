# Kazam API

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

[![build status](https://git.urbanity.xyz/kazam/api/badges/master/build.svg)](https://git.urbanity.xyz/kazam/api/commits/master)

![coverage](https://git.urbanity.xyz/kazam/api/badges/master/coverage.svg?job=mocha-coverage)

## Install

### Postgres

```sql
CREATE DATABASE kazam_api;
```

### Config

Create file `.env` file in `config/environments/`

```sh
vi config/environments/.env
```
Paste the following content:

```sh
# Application
NODE_APP=kazamapi
NODE_PORT=5333

# Keys
OU_KEY=OU_KEY_PLACEHOLDER
AR_KEY=AR_KEY_PLACEHOLDER
TR_KEY=TR_KEY_PLACEHOLDER
CT_KEY=CT_KEY_PLACEHOLDER
OI_KEY=OI_KEY_PLACEHOLDER
UT_KEY=UT_KEY_PLACEHOLDER

# Database
DB_NAME=merchant_api
DB_USER=<USER_NAME>
DB_HOST=localhost
DB_DIALECT=postgres
DB_LOGGING=false

# Mailing
NO_MAILS=true

# Mailgun
MAILGUN_KEY=ABC
MAILGUN_DOMAIN=TEST
MAILGUN_FROM=testing@u360.mx
MAILGUN_AUTHORIZED=joel.cano.avila@gmail.com
```
Replace the `<USER_NAME>` with your postgres user.

### Dependencies

To install run the following commands in the projects root directory.

```sh
# Globals
npm install -g sequelize sequelize-cli pg standard mocha
# project dependencies
npm install
# migrate
npm run migrate
```

### Run

```sh
npm start
```

## Stage

### Testing

- sequelize
- sequelize-cli
- pg
- standard
- mocha
- chai

### Deploy

- shipit-cli

#### Database

```sql
CREATE USER kazam_api WITH PASSWORD '<passwd>';

CREATE DATABASE kazam_api_stage;

ALTER DATABASE kazam_api_stage OWNER TO merchant_api;
```

#### Server

`https://api.urbanity.xyz:3500/`

## Documentation

```sh
npm run doc
```

## Production

### Deploy

Gitlab deploys a new version into the EBS environment on AWS. If all tests pass on the production branch.

### CD

The following env vars are needed:

#### AWS

AWS User with S3 and EBS roles.

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY

#### EBS

- EBS_APPLICATION: Application name
- EBS_BUCKET: S3 Version bucket
- EBS_ENV: Environment name

#### Generated

The following vars are generated in the CD process.

- TS
- BUNDLE_NAME
- EBS_VERSION


### EBS Structure

Load balanced EC2 instances with single RDS instance.
