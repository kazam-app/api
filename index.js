/**
 * Kazam Api
 * @name Restify Server
 * @file index.js
 * @author Joel Cano
 */

'use strict'

// load environment config
require('./config')

const fs = require('fs')
const i18n = require('i18n')
const path = require('path')
const xss = require('xss-clean')
const helmet = require('helmet')
const restify = require('restify')
const cors = require('./lib/cors')
const tasks = require('./lib/tasks')
const sentry = require('./lib/sentry')
const logger = require('./lib/logger')
const response = require('./lib/response')

// initialize server
const env = process.env
const name = env.NODE_APP
const log = logger(name)
// error logging
if (env.NODE_ENV === 'production') {
  log.info({ server: 'restify' }, 'statring sentry')
  sentry.install()
}
// server options
let opts = { log: log, name: name }
let port = env.NODE_PORT
if (env.SSL_KEY && env.SSL_CERT) {
  opts.key = fs.readFileSync(env.SSL_KEY)
  port = env.SSL_PORT || 443
  opts.certificate = fs.readFileSync(env.SSL_CERT)
  log.info('ssl on')
} else log.info('ssl off')
log.debug({ server: 'restify' }, { config: opts })
log.info({ server: 'restify' }, { env: env.NODE_ENV })
let server = restify.createServer(opts)

const plugins = restify.plugins
server.use(plugins.pre.sanitizePath())
server.use(plugins.fullResponse())
// Apply CORS logic
cors(server, log)
// Clean headers
server.use(helmet())
server.use(plugins.queryParser({
  mapParams: true
}))
server.use(plugins.bodyParser({
  mapParams: true,
  requestBodyOnGet: true
}))
server.use(plugins.authorizationParser())
// Clean req.body, req.query, req.params
server.use(xss())
server.use(plugins.gzipResponse())
server.use(plugins.requestLogger())

// translation config
const lang = {
  locales: ['es'],
  defaultLocale: 'es',
  updateFiles: !process.env.NODE_ENV,
  directory: path.join(__dirname, '/config/locales')
}
log.debug({ server: 'i18n' }, { i18n: lang })
i18n.configure(lang)

server.use(i18n.init)
log.info('i18n set')

// models
const db = require('./models')(log)
const models = db.sequelize.models

// response config
log.debug('log responses:', env.RES_LOG)
server.use(function (req, res, next) {
  req.models = models
  req.ValidationError = db.Sequelize.ValidationError
  res.charSet('utf-8')
  // Custom Success
  res.success = response.success(res, req.log) // => 200
  res.created = response.created(res, req.log) // => 201
  // Custom Errors
  res.gone = response.gone(res, req.log) // => 410
  res.tooMany = response.tooMany(res, req.log) // => 429
  res.notFound = response.notFound(res, req.log) // => 404
  res.forbidden = response.forbidden(res, req.log) // => 403
  res.validation = response.validation(res, req.log) // 400
  res.badRequest = response.badRequest(res, req.log) // 400
  res.mailerError = response.mailerError(res, req.log) // just error parsing
  res.unauthorized = response.unauthorized(res, req.log) // => 401
  next()
})

// server models
server.models = models

/**
 * Task configuration. Only run tasks before
 */
const runTasks = (env.NODE_ENV === 'stage' || env.NODE_ENV === 'production')
let jobs = null
log.info({ provider: 'tasks' }, { tasks: runTasks })
if (runTasks) {
  jobs = tasks.configure(models, log)
  tasks.start(jobs, log)
}

// cleanup
server.on('close', function () {
  log.debug({ server: 'restify' }, 'closing')
  if (runTasks) {
    tasks.stop(jobs, log)
  }
  // clean db connection pool
  db.sequelize.close()
  log.info({ server: 'restify' }, 'closed')
})

// routes
require('./routes')(server)

// start server
log.info({ provider: 'AWS S3' }, { cdn: process.env.CDN_URL })
log.debug(
  { provider: 'Mailgun' },
  { no_mails: process.env.NO_MAILS, authorized: process.env.MAILGUN_AUTHORIZED }
)
if (process.env.NO_MAILS !== 'false') log.debug('mailers disabled')
else log.debug('mailers enabled')
log.info('listening port:', port)
server.listen(port)

module.exports = server
