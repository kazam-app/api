/**
 * Merchant Api
 * @name Mailer Class Definition
 * @file index.js
 * @author Joel Cano
 */

'use strict'

const fs = require('fs')
const path = require('path')
const _ = require('lodash/object')
const Mailgun = require('./mailgun')
const Mustache = require('mustache')

/**
 * Mailer Constructor
 * @constructs Mailer
 */
function Mailer (logger) {
  return this
}

_.assign(Mailer.prototype, {
  /**
   * @method getTemplatePath
   * @description Retuns a full path to a mustache template.
   * @param {string}  name - file name
   * @return {string} returns full path needed to open the file
   *
   * @instance
   */
  getTemplatePath: function (name) {
    return path.join(__dirname, '/templates/', name + '.mustache')
  },
  /**
   * @method getTemplate
   * @description Reads the file and applies the data to the mustache template.
   * @param {path}  path - file path to the view
   * @param {object}  data - information to replace within the view.
   * @return {string} returns rendered html view
   *
   * @instance
   */
  getTemplate: function (path, data) {
    let file = fs.readFileSync(path, 'utf-8')
    Mustache.parse(file)
    return Mustache.render(file, data)
  },
  /**
   * @method send
   * @description Reads the file and applies the data to the mustache template.
   * @param {path}  path - file path to the view
   * @param {object}  data - information to replace within the view.
   * @return {Promise} returns rendered html view
   *
   * @instance
   */
  send: function (to, name, data, mailer, log) {
    const view = this.getTemplate(this.getTemplatePath(name), data)
    return mailer.send(to, data.subject, view, log)
  },
  /**
   * @method mailgunSend
   * @description Sends template to email via mailgun api.
   * @param {string}  to - recipient email
   * @param {string}  name - template file name
   * @param {object}  data - template data
   * @return {Promise} returns api call response
   *
   * @instance
   */
  mailgunSend: function (to, name, data, logger) {
    let log = logger.child({ components: 'mailer', mailer: 'Mailgun' })
    return this.send(to, name, data, Mailgun, log)
  }
})

module.exports = new Mailer()
