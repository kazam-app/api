/**
 * Kazam Api
 * @name Mailgun Class Definition
 * @file mailer/mailgun.js
 * @author Joel Cano
 */

'use strict'

const Promise = require('bluebird')
const from = process.env.MAILGUN_FROM
const mailgun =
  require('mailgun-js')({
    apiKey: process.env.MAILGUN_KEY,
    domain: process.env.MAILGUN_DOMAIN
  })

module.exports = {
  /**
   * @method send
   * @description Retuns a full path to a mustache template.
   * @param {string} to - send to email
   * @param {string} subject - email subject
   * @param {string} view - email content
   * @return {Promise} return sent mail promise
   *
   * @static
   */
  send: function (to, subject, view, log) {
    return new Promise(function (resolve, reject) {
      log.info({ to: to, from: from, subject: subject, no_mails: process.env.NO_MAILS !== 'false' })
      if (process.env.NO_MAILS !== 'false') resolve('NO_MAILS')
      else {
        to = process.env.MAILGUN_AUTHORIZED ? process.env.MAILGUN_AUTHORIZED : to
        const data = {
          from: from,
          to: to,
          subject: subject,
          html: view
        }
        mailgun.messages().send(data, function (error, body) {
          if (error) {
            log.error(error)
            reject(error)
          } else {
            log.debug(body)
            resolve(body)
          }
        })
      }
    })
  }
}
