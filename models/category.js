/**
 * Kazam Api
 * @name Sequelize Category Definition
 * @file models/category.js
 * @author Joel Cano
 */

'use strict'

const Aws = require('../lib/aws')
const _ = require('lodash/object')
const decorator = require('../lib/decorate')
const imageUrl = require('../lib/sequelize/imageurl')
const stringField = require('../lib/sequelize/string')

module.exports = function (sequelize, DataTypes) {
  let Category = sequelize.define(
    // Model name
    'Category',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      name: stringField(DataTypes, false),
      imageUrl: _.merge({}, imageUrl(DataTypes), {
        get () {
          return Aws.getUrl(this.getDataValue('imageUrl'))
        }
      })
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'categories',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method name
         * @description name query template
         * @returns {object} - query structure
         *
         * @static
         */
        name: { attributes: ['name'] }
      },
      /**
       * hooks
       * @description Model hooks
       * @type {Object}
       *
       */
      hooks: {
        /**
         * @method beforeDestroy
         * @description Before destroy hook
         * @param {Category} instance - Removed instance
         *
         * @static
         */
        beforeDestroy: function (instance) {
          return Aws.remove(instance.imageUrl, Category.logger)
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  Category.associate = function (models) {
    Category.hasMany(models.Merchant, { foreignKey: 'categoryId' })
  }
  /**
   * @method toList
   * @description Returns apprpriate list data
   * @returns {Promise<array>} - Category array promise
   *
   * @static
   */
  Category.toList = 'toShow'
  /**
   * @method toShow
   * @description Returns apprpriate show data
   * @returns {Promise<Category>} - Category promise
   *
   * @static
   */
  Category.toShow = 'toShow'
  Category.decorate = decorator
  /**
   * @method list
   * @description Returns apprpriate list data
   * @returns {Promise<array>} - Category array promise
   *
   * @static
   */
  Category.list = function () {
    return this.findAll({ attributes: ['id', 'name', 'imageUrl'] })
  }
  /**
   * @method show
   * @description Returns a Category by id
   * @param {integer} id - Category id
   * @returns {Promise<Category>} - Returns a Category promise or not found
   *
   * @static
   */
  Category.show = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.findById(id, { transaction: t }).then(function (result) {
          if (result) return result
          else throw new Error('not_found')
        })
      }
    })
  }
  /**
   * @method uploadImages
   * @description Returns a string Promise with the resulting upload url.
   * @return {Promise<string>} Returns a string Promise
   *
   * @static
   */
  Category.uploadImages = function (data) {
    return Aws.upload(data.image, null, this.logger)
  }
  /**
   * @method createFromData
   * @description Creates a new Category with request data
   * @param {object} params - request parameters
   * @return {Promise<Category>} Returns a Category instance
   *
   * @static
   */
  Category.createFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.category
      if (data) {
        return this.uploadImages(data).then((url) => {
          return this.create(
            { name: data.name, imageUrl: url },
            { transaction: t }
          )
        })
      } else throw new Error('missing_category_data')
    })
  }
  /**
   * @method updateFromData
   * @description Updates an Category via id and request params
   * @param {integer} id - Category identifier
   * @param {object} params - request parameters
   * @return {Promise<Category>} Returns a Category instance
   *
   * @static
   */
  Category.updateFromData = function (id, params) {
    return sequelize.transaction((t) => {
      const data = params.category
      if (!isNaN(id) && data) {
        return this.uploadImages(data).then((url) => {
          let update = {}
          if (data.name) update.name = data.name
          if (url) update.imageUrl = url
          return this.update(update, {
            where: { id: id },
            returning: true,
            transaction: t
          }).spread(function (updated, result) {
            if (updated > 0) return result[0]
            else throw new Error('not_found')
          })
        })
      } else throw new Error('missing_category_data')
    })
  }
  /**
   * @method delete
   * @description Removes a Category by id
   * @param {integer} id - Category id
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  Category.delete = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.destroy({ where: { id: id }, transaction: t })
          .then(function (result) {
            if (result > 0) return true
            else throw new Error('not_found')
          })
      }
    })
  }
  /**
   * @method breakdownForUser
   * @description Returns the percentage of categories a user has interactions
   *  with
   * @param {User} user - User instance
   * @returns {Promise<array>} - Category percentage array
   *
   * @static
   */
  Category.breakdownForUser = function (user) {
    const joins = `INNER JOIN merchants ON categories.id = merchants.category_id
      INNER JOIN validators ON merchants.id = validators.merchant_id
      INNER JOIN user_punch_card_validations ON validators.id = user_punch_card_validations.validator_id
      INNER JOIN user_punch_cards ON user_punch_card_validations.user_punch_card_id = user_punch_cards.id`
    const where = `WHERE user_punch_cards.user_id = :userId`
    const subQuery = `SELECT COUNT(categories.id) FROM categories ${joins} ${where}`
    const query = `
      SELECT ROUND((COUNT(categories.id)::numeric / (${subQuery})::numeric) * 100, 2) AS value, categories.name
      FROM categories
      ${joins}
      ${where}
      GROUP BY categories.id, categories.name`
    return sequelize.query(query, { type: sequelize.QueryTypes.SELECT, replacements: { userId: user.id } })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toShow
   * @description Return show apprpriate data
   * @returns {object} Category show data
   *
   * @instance
   */
  Category.prototype.toShow = function () {
    return { id: this.id, name: this.name, image_url: this.imageUrl }
  }
  return Category
}
