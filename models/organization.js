/**
 * Kazam Api
 * @name Sequelize Organization Definition
 * @file models/organization.js
 * @author Joel Cano
 */

'use strict'

const Promise = require('bluebird')

module.exports = function (sequelize, DataTypes) {
  let Organization = sequelize.define(
    // Model name
    'Organization',
    {
      name: {
        unique: true,
        allowNull: false,
        type: DataTypes.STRING
      }
    },
    {
      underscored: true,
      tableName: 'organizations'
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  Organization.associate = function (models) {
    Organization.hasMany(models.Merchant, { foreignKey: 'organizationId' })
    Organization.hasMany(models.OrganizationUser, {
      foreignKey: 'organizationId'
    })
  }
  /**
   * @method list
   * @description Returns all Organizations as an array
   * @return {array} Organization array
   *
   * @static
   */
  Organization.list = function () {
    return this.findAll({ attributes: ['id', 'name'] })
  }
  /**
   * @method adminShow
   * @description Returns a merchant instance with all admin values
   * @return {Promise<Organization>} Returns a Merchant instance promise
   *
   * @static
   */
  Organization.show = function (id) {
    return new Promise((resolve, reject) => {
      const err = new Error('not_found')
      if (isNaN(id)) reject(err)
      else {
        this.findById(id).then(function (result) {
          if (result) resolve(result)
          else reject(err)
        })
      }
    })
  }
  /**
   * @method createFromData
   * @description Creates a new Organization with request data
   * @param {object} params - request parameters
   * @return {Promise<Organization>} Returns a Organization instance
   *
   * @static
   */
  Organization.createFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.organization
      if (data) {
        return this.create({ name: data.name }, { transaction: t })
      } else throw new Error('missing_organization_data')
    })
  }
  /**
   * @method updateFromData
   * @description Updates an Organization via id from request params
   * @param {integer} id - Organization id
   * @param {object} params - request parameters
   * @return {Promise<Organization>} Returns a Organization instance
   *
   * @static
   */
  Organization.updateFromData = function (id, params) {
    return sequelize.transaction((t) => {
      const data = params.organization
      if (!isNaN(id) && data) {
        let update = {}
        if (data.name) update.name = data.name
        let query = { where: { id: id }, returning: true, transaction: t }
        return this.update(update, query).spread(function (updated, result) {
          if (updated > 0) return result[0]
          else throw new Error('not_found')
        })
      } else throw new Error('missing_organization_data')
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toAdmin
   * @description Returns admin apprpriate data for an organization
   * @returns {object} - Organization data
   *
   * @instance
   */
  Organization.prototype.toAdmin = function () {
    return { id: this.id, name: this.name }
  }
  return Organization
}
