/**
 * Kazam Api
 * @name Sequelize Organization User Invite Definition
 * @file models/organizationuser.js
 * @author Joel Cano
 */

'use strict'

const pair = require('pair')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const Promise = require('bluebird')
const encryptionKey = process.env.OI_KEY
const tokenPair = require('../lib/sequelize/token')
const emailField = require('../lib/sequelize/email')

module.exports = function (sequelize, DataTypes) {
  let OrganizationUserInvite = sequelize.define(
    // Model name
    'OrganizationUserInvite',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      organizationUserId: {
        allowNull: false,
        type: DataTypes.BIGINT,
        field: 'organization_user_id'
      },
      email: emailField(DataTypes, false),
      token: tokenPair.token(DataTypes, false),
      secret: tokenPair.secret(DataTypes, false)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'organization_user_invites',
      /**
       * hooks
       * @description Model hooks
       * @type {Object}
       *
       */
      hooks: {
        /**
         * @method beforeValidate
         * @description Sets token and secret values before the validations are
         *  run.
         * @param  {OrganizationUserToken} instance - current model instance
         *
         * @static
         */
        beforeValidate: function (instance) {
          if (!instance.token || !instance.secret) {
            const token = instance.email + '-' + instance.created_at.getTime()
            const keys = pair.generate(token, encryptionKey)
            instance.token = keys.token
            instance.secret = keys.secret
          }
        }
      }
    }
  )
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  OrganizationUserInvite.associate = function (models) {
    OrganizationUserInvite.belongsTo(models.OrganizationUser, {
      foreignKey: 'organizationUserId'
    })
  }
  /**
   * @method findValidInvite
   * @description Finds an invitation via a jwt
   * @param {string} token - Json Web Token
   * @param {object} t - sequelize transaction instance
   * @return {Promise<OrganizationUserInvite>} finds an invite instance
   *
   * @static
   */
  OrganizationUserInvite.findValidInvite = function (token, t) {
    const decoded = jwt.decode(token)
    if (decoded) {
      const opts = {
        where: { token: decoded.iss },
        include: [{ model: sequelize.models.OrganizationUser.scope('active') }],
        transaction: t
      }
      return this.findOne(opts).then(function (result) {
        if (result) return [result.verify(token), result]
        else throw new Error('invalid_token')
      })
    } else throw new Error('invalid_token')
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method usableSecret
   * @description Decrypts the secret attribute stored in the database.
   * @returns {string} returns the decrypted secret
   *
   * @instance
   */
  OrganizationUserInvite.prototype.usableSecret = function () {
    return pair.decrypt(this.secret, encryptionKey)
  }
  /**
   * @method jwt
   * @description Signs the jwt with the secret and adds the rel attr if any.
   * @returns {string} returns a signed jwt string
   *
   * @instance
   */
  OrganizationUserInvite.prototype.jwt = function () {
    const exp = moment().add(4, 'days')
    return jwt.sign({
      iss: this.token,
      exp: exp.unix()
    }, this.usableSecret())
  }
  /**
   * @method verify
   * @description Verifies the provided jwt with the this instance's secret
   * @param  {string} token unverified jwt
   * @return {Promise<string>} returns a verfied jwt promise
   *
   * @instance
   */
  OrganizationUserInvite.prototype.verify = function (token) {
    return new Promise((resolve, reject) => {
      try {
        resolve(jwt.verify(token, this.usableSecret()))
      } catch (err) {
        reject(new Error('invalid_token'))
      }
    })
  }
  return OrganizationUserInvite
}
