/**
 * Kazam Api
 * @name Sequelize Mailing List Organization User Definition
 * @file models/mailinglistorganizationuser.js
 * @author Joel Cano
 */

'use strict'

const fkField = require('../lib/sequelize/fk')
const stringField = require('../lib/sequelize/string')

module.exports = function (sequelize, DataTypes) {
  let MailingListOrganizationUser = sequelize.define(
    // Model name
    'MailingListOrganizationUser',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      mailingListId: fkField(DataTypes, false, 'mailing_list_id'),
      organizationUserId: fkField(DataTypes, false, 'organization_user_id'),
      mailchimpId: stringField(DataTypes, true, 'mailchimp_id')
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'mailing_list_organization_users'
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  MailingListOrganizationUser.associate = function (models) {
    MailingListOrganizationUser.belongsTo(models.MailingList, {
      foreignKey: 'mailingListId'
    })
    MailingListOrganizationUser.belongsTo(models.OrganizationUser, {
      foreignKey: 'organizationUserId'
    })
  }
  return MailingListOrganizationUser
}
