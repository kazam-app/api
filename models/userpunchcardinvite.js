/**
 * Kazam Api
 * @name Sequelize UserPunchCardInvite Definition
 * @file models/userpunchcardinvite.js
 * @author Joel Cano
 */

'use strict'

const uuidField = require('../lib/sequelize/uuid')
const toChar = require('../lib/sequelize/to-char')

module.exports = function (sequelize, DataTypes) {
  let UserPunchCardInvite = sequelize.define(
    // Model name
    'UserPunchCardInvite',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      merchantId: {
        allowNull: false,
        field: 'merchant_id',
        type: DataTypes.BIGINT
      },
      userPunchCardId: {
        allowNull: false,
        type: DataTypes.BIGINT,
        field: 'user_punch_card_id'
      },
      identifier: uuidField(DataTypes)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'user_punch_card_invites'
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  UserPunchCardInvite.associate = function (models) {
    UserPunchCardInvite.belongsTo(models.Merchant, { foreignKey: 'merchantId' })
    UserPunchCardInvite.belongsTo(models.UserPunchCard, {
      foreignKey: 'userPunchCardId'
    })
  }
  /**
   * @method countByMonthForMerchant
   * @description Total invites redeemed for a merchant by month
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<array>} Returns an array with total values
   *
   * @static
   */
  UserPunchCardInvite.countByMonthForMerchant = function (merchant) {
    const monthField = 'month'
    const yearField = 'year'
    const dateField = 'created_at'
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCardInvite.id')),
          'total'
        ],
        toChar(sequelize, dateField, monthField, 'MM'),
        toChar(sequelize, dateField, yearField, 'YYYY')
      ],
      where: { merchantId: merchant.id },
      group: [monthField, yearField],
      order: [
        [sequelize.literal(yearField), 'DESC'],
        [sequelize.literal(monthField), 'DESC']
      ],
      raw: true
    })
  }
  return UserPunchCardInvite
}
