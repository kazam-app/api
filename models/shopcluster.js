/**
 * Kazam Api
 * @name Sequelize Shop Cluster Definition
 * @file models/shopcluster.js
 * @author Joel Cano
 */

'use strict'

const geo = require('../lib/sequelize/geo')
const stringField = require('../lib/sequelize/string')

module.exports = function (sequelize, DataTypes) {
  let ShopCluster = sequelize.define(
    'ShopCluster',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      name: stringField(DataTypes, false),
      location: stringField(DataTypes, false),
      city: stringField(DataTypes, false),
      postalCode: stringField(DataTypes, false, 'postal_code'),
      state: stringField(DataTypes, false),
      country: {
        allowNull: false,
        defaultValue: 'MEX',
        type: DataTypes.STRING
      },
      lat: geo.lat(DataTypes),
      lng: geo.lng(DataTypes)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'shop_clusters',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method name
         * @description name query template
         * @returns {object} - query structure
         *
         * @static
         */
        name: { attributes: ['name'] }
      }
    }
  )
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  ShopCluster.associate = function (models) {
    ShopCluster.hasMany(models.Shop, { foreignKey: 'shopClusterId' })
  }
  /**
   * @method list
   * @description Returns apprpriate list data
   * @returns {Promise<array>} - ShopCluster array promise
   *
   * @static
   */
  ShopCluster.list = function () {
    return this.findAll({
      attributes: ['id', 'name', 'location'],
      order: [['name', 'ASC']]
    })
  }
  /**
   * @method show
   * @description Returns a shop cluster instance
   * @return {Promise<ShopCluster>} Returns a ShopCluster instance promise
   *
   * @static
   */
  ShopCluster.show = function (id) {
    return sequelize.transaction((t) => {
      const err = new Error('not_found')
      if (isNaN(id)) throw err
      else {
        return this.findById(id, { transaction: t }).then(function (result) {
          if (result) return result
          else throw err
        })
      }
    })
  }
  /**
   * @method delete
   * @description Removes a ShopCluster by id
   * @param {integer} id - ShopCluster id
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  ShopCluster.delete = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.destroy({ where: { id: id }, transaction: t })
          .then(function (result) {
            if (result > 0) return true
            else throw new Error('not_found')
          })
      }
    })
  }
  /**
   * @method updateFromData
   * @description Updates an ShopCluster via id and request params
   * @param {integer} id - ShopCluster identifier
   * @param {object} params - request parameters
   * @return {Promise<ShopCluster>} Returns a ShopCluster instance
   *
   * @static
   */
  ShopCluster.updateFromData = function (id, params) {
    return sequelize.transaction((t) => {
      const data = params.shop_cluster
      if (!isNaN(id) && data) {
        let update = {}
        if (data.name) update.name = data.name
        if (data.location) update.location = data.location
        if (data.postal_code) update.postalCode = data.postal_code
        if (data.city) update.city = data.city
        if (data.state) update.state = data.state
        if (data.country) update.country = data.country
        if (data.lat) update.lat = data.lat
        if (data.lng) update.lng = data.lng
        return this.update(update, {
          where: { id: id },
          returning: true,
          transaction: t
        }).spread(function (updated, result) {
          if (updated > 0) return result[0]
          else throw new Error('not_found')
        })
      } else throw new Error('missing_shop_cluster_data')
    })
  }
  /**
   * @method createFromData
   * @description Creates a new ShopCluster with request data
   * @param {object} params - request parameters
   * @return {Promise<ShopCluster>} Returns a ShopCluster instance
   *
   * @static
   */
  ShopCluster.createFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.shop_cluster
      if (data) {
        const insert = {
          name: data.name,
          location: data.location,
          postalCode: data.postal_code,
          city: data.city,
          state: data.state,
          country: data.country,
          lat: data.lat,
          lng: data.lng
        }
        return this.create(insert, { transaction: t })
      } else throw new Error('missing_shop_cluster_data')
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toShow
   * @description Returns show apprpriate data for a shop cluster
   * @returns {object} - shop cluster information
   *
   * @instance
   */
  ShopCluster.prototype.toShow = function () {
    return {
      id: this.id,
      name: this.name,
      location: this.location,
      postal_code: this.postalCode,
      city: this.city,
      state: this.state,
      country: this.country,
      lat: this.lat,
      lng: this.lng
    }
  }
  return ShopCluster
}
