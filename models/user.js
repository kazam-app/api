/**
 * Kazam Api
 * @name Sequelize User Definition
 * @file models/user.js
 * @author Joel Cano
 */

'use strict'

const salts = 11
const FB = require('fb')
const bcrypt = require('bcrypt')
const moment = require('moment')
const Aws = require('../lib/aws')
const each = require('async/each')
const date = require('../lib/date')
const Promise = require('bluebird')
const _ = require('lodash/collection')
const validator = require('validator')
const isBlank = require('../lib/blank')
const decorator = require('../lib/decorate')
const OneSignal = require('../lib/one-signal')
const rawDecorator = require('../lib/rawdecorate')
const uuidField = require('../lib/sequelize/uuid')
const toChar = require('../lib/sequelize/to-char')
const emailField = require('../lib/sequelize/email')
const imageUrl = require('../lib/sequelize/imageurl')
const notEmpty = require('../lib/sequelize/notempty')
const stringField = require('../lib/sequelize/string')
const expiryNotifications = require('../lib/expiry-notification')

const Gender = { Female: 0, Male: 1 }
const Genders = [Gender.Female, Gender.Male]
FB.options({ appId: process.env.FB_APP, appSecret: process.env.FB_SECRET })

const HistoryEvent = { Validation: 0, Card: 1 }
const HistoryCardEvent = { Expired: 0, Completed: 1 }

module.exports = function (sequelize, DataTypes) {
  let User = sequelize.define(
    // Model name
    'User',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      isValid: {
        allowNull: false,
        field: 'is_valid',
        defaultValue: true,
        type: DataTypes.BOOLEAN
      },
      // email verified
      isVerified: {
        allowNull: false,
        defaultValue: false,
        field: 'is_verified',
        type: DataTypes.BOOLEAN
      },
      name: stringField(DataTypes, false),
      lastNames: stringField(DataTypes, false, 'last_names'),
      email: emailField(DataTypes, false),
      validateEmail: emailField(DataTypes, true, 'validate_email'),
      password: stringField(DataTypes),
      gender: {
        type: DataTypes.INTEGER,
        validate: {
          isIn: { args: [Genders], msg: 'invalid_value' }
        }
      },
      birthdate: {
        type: DataTypes.DATEONLY,
        validate: notEmpty
      },
      imageUrl: imageUrl(DataTypes),
      identifier: uuidField(DataTypes),
      // facebook fields
      fbId: {
        unique: true,
        field: 'fb_id',
        validate: notEmpty,
        type: DataTypes.TEXT
      },
      fbToken: {
        field: 'fb_token',
        validate: notEmpty,
        type: DataTypes.TEXT
      },
      fbExpiresAt: {
        validate: notEmpty,
        type: DataTypes.DATE,
        field: 'fb_expires_at'
      },
      // mailchimp fields
      mailchimpId: stringField(DataTypes, true, 'mailchimp_id'),
      // onesignal fields
      hasExpiryNotifyPermission: {
        allowNull: false,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'has_expiry_notify_permission'
      },
      playerId: stringField(DataTypes, true, 'player_id')
    },
    {
      underscored: true,
      tableName: 'users',
      /**
       * scopes
       * @description Model scopes
       * @type {object}
       *
       */
      scopes: {
        /**
         * @method active
         * @description Returns query for active users
         * @returns {object} - query structure
         *
         * @static
         */
        active: { where: { isValid: true } },
        /**
         * @method validated
         * @description Returns query for valid users
         * @returns {object} - query structure
         *
         * @static
         */
        validated: { where: { isValid: true } },
        /**
         * @method verified
         * @description Returns query for verified users
         * @returns {object} - query structure
         *
         * @static
         */
        verified: { where: { isValid: true, isVerified: true } },
        /**
         * @method gender
         * @description Returns query for gender values
         * @returns {object} - query structure
         *
         * @static
         */
        gender: { attributes: ['gender'] },
        activeForMerchant: function (merchantId) {
          const Op = sequelize.Op
          const models = sequelize.models
          const UserPunchCard = models.UserPunchCard
          return {
            where: { isValid: true },
            include: [{
              attributes: [],
              required: true,
              model: UserPunchCard,
              where: {
                identity: {
                  [Op.or]: [
                    UserPunchCard.Identity.InProgress,
                    UserPunchCard.Identity.Ready
                  ]
                },
                punchCount: { [Op.gte]: 1 }
              },
              include: [models.PunchCard.activeForMerchantInclude(merchantId)]
            }],
            raw: true
          }
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  User.associate = function (models) {
    User.hasMany(models.UserToken, { foreignKey: 'userId' })
    User.hasMany(models.Suggestion, { foreignKey: 'userId' })
    User.hasMany(models.UserPunchCard, { foreignKey: 'userId' })
    User.hasMany(models.MailingListUser, { foreignKey: 'userId' })
  }
  /**
   * Gender Enum
   * @type {Object}
   */
  User.Gender = Gender
  /**
   * @method toAdminList
   * @description Returns apprpriate admin list data
   * @returns {Promise<object[]>} - User data array promise
   *
   * @static
   */
  User.toAdminList = 'toAdminList'
  /**
   * @method toAdmin
   * @description Returns apprpriate admin data
   * @returns {Promise<object[]>} - User data array promise
   *
   * @static
   */
  User.toAdmin = 'toAdmin'
  /**
   * @method toExpiryNotification
   * @description Returns apprpriate expiry notification
   * @returns {Promise<object[]>} - User data array promise
   *
   * @static
   */
  User.toExpiryNotification = 'toExpiryNotification'
  /**
   * @method decorate
   * @description Decorates items from a sequelize query
   * @returns {function} - Returns the processing function
   *
   * @static
   */
  User.decorate = decorator
  /**
   * @method toApiHistory
   * @description Return UserPunchCardValidation or UserPunchCard data as a more
   *  generic chronological event data type.
   * @param {object} - UserPunchCardValidation data or UserPunchCard data
   * @returns {object} - Event data
   *
   * @static
   */
  User.toApiHistory = function (item) {
    let result = { id: item.identifier }
    if (item['Validator.Shop.name']) {
      // validation events
      Object.assign(result, User.fromApiHistoryValidation(item))
    } else {
      // card events
      Object.assign(result, User.fromApiHistoryCard(item))
    }
    return result
  }
  /**
   * @method toApiHistoryValidation
   * @description UserPunchCardValidation history apprpriate data
   * @returns {object} - Event data
   *
   * @static
   */
  User.fromApiHistoryValidation = function (item) {
    let result = {
      // event data
      event_at: item.created_at,
      event_type: HistoryEvent.Validation,
      // punch validation or redeem validation
      validation: { identity: item.identity },
      // association data
      punch_card: {
        id: item['UserPunchCard.identifier'],
        identity: User.eventIdentity(
          item['UserPunchCard.identity'],
          item['UserPunchCard.PunchCard.expiresAt']
        )
      },
      merchant: {
        name: item['Validator.Merchant.name'],
        image_url: Aws.getUrl(item['Validator.Merchant.imageUrl'])
      },
      shop: { name: item['Validator.Shop.name'] }
    }
    return result
  }
  /**
   * @method fromApiHistoryCard
   * @description UserPunchCard history apprpriate data
   * @returns {object} - Event data
   *
   * @static
   */
  User.fromApiHistoryCard = function (item) {
    // event data
    let result = { event_type: HistoryEvent.Card }
    if (item['PunchCard.expiresAt']) { // expiry event
      Object.assign(result, {
        // expired card
        event_at: item['PunchCard.expiresAt'],
        card: { identity: HistoryCardEvent.Expired },
        merchant: {
          name: item['PunchCard.Merchant.name'],
          image_url: Aws.getUrl(item['PunchCard.Merchant.imageUrl'])
        },
        punch_card: {
          id: item.identifier,
          identity: User.eventIdentity(
            item.identity,
            item['PunchCard.expiresAt']
          )
        }
      })
    } else { // completion event
      Object.assign(result, {
        // completed card
        event_at: item.created_at,
        card: { identity: HistoryCardEvent.Completed },
        merchant: {
          name: item.name,
          image_url: Aws.getUrl(item.image_url)
        },
        punch_card: {
          id: item.identifier,
          identity: User.eventIdentity(
            item.identity,
            item.expires_at
          )
        }
      })
    }
    return result
  }
  /**
   * @method eventIdentity
   * @description Parses the punch card status and returns the event
   *  identity
   * @param {integer} identity -  PunchCard identity
   * @param {Date} expiresAt - PunchCard expiresAt
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  User.eventIdentity = function (identity, expiresAt) {
    const Identities = sequelize.models.UserPunchCard.Identity
    if (moment().isAfter(expiresAt) && identity !== Identities.Redeemed) {
      return Identities.Expired
    } else return identity
  }
  /**
   * @method rawDecorate
   * @description Decorates items from a sequelize raw query
   * @returns {function} - Returns the processing function
   *
   * @static
   */
  User.rawDecorate = rawDecorator
  /**
   * @name findActive
   * @description find all active users
   * @param {object} value - plain text string
   * @returns {Promise} encrypted string
   *
   * @static
   */
  User.findActive = function (t) {
    return this.scope('validated').findAll({ transaction: t })
  }
  /**
   * @name hash
   * @description Returns a bcrypt encrypted string of the plain value.
   * @param {string} value - plain text string
   * @returns {string} encrypted string
   *
   * @static
   */
  User.hash = function (value) {
    return bcrypt.hashSync(value, salts)
  }
  /**
   * @method login
   * @description Compares the password and retruns a jwt if valid
   * @param {object} data - incoming request parameters
   * @returns {Promise<object>} returns a user promise with a jwt
   *
   * @static
   */
  User.login = function (data) {
    return sequelize.transaction((t) => {
      let userPromise = this.scope('validated')
      if (data.email && data.password) {
        userPromise = userPromise.findOne({
          where: { email: data.email },
          transaction: t
        }).then(function (result) {
          if (result) {
            return result.checkAuth(data.password, t).then(function (token) {
              return [token, result]
            })
          } else throw new Error('login_fail')
        })
      } else if (data.fb_id && data.fb_token && data.fb_expires_at) {
        userPromise = userPromise.findOne({
          where: { fbId: data.fb_id },
          transaction: t
        }).then(function (result) {
          if (result) {
            // TODO: update fb_token and expires at
            return result.fbMe(data.fb_token).then(function (res) {
              return [result, res]
            })
          }
        }).spread(function (user, fb) {
          return sequelize.models.UserToken.refreshSession(user, t)
            .then(function (token) {
              return [token, user]
            })
        })
      } else throw new Error('login_fail')
      return userPromise.spread(function (token, user) {
        let data = user.toShow()
        data.token = token.jwt()
        return data
      })
    })
  }
  /**
   * @method signUp
   * @description Creates a new User via request params
   * @param {object} params - request params
   * @returns {Promise<User>} returns a user promise with a jwt
   *
   * @static
   */
  User.signUp = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      if (data) {
        let userPromise
        if (data.fb_id && data.fb_token && data.fb_expires_at) {
          userPromise = this.generateFbUser(data, t)
        } else userPromise = this.generateUser(data, t)
        const models = sequelize.models
        const UserToken = models.UserToken
        return userPromise.then(function (user) {
          if (data.invite_code) {
            return models.Merchant.findByInviteCode(data.invite_code, t)
              .then(function (merchant) {
                if (merchant) return [user, merchant]
                else throw new Error('invalid_invite_code')
              })
          } else return [user, null]
        }).spread((user, merchant) => {
          return this.generatePunchCards(user, merchant, t).then(function () {
            return [user, merchant]
          })
        }).spread(function (user, merchant) {
          return UserToken.verificationToken(user, t).then((newToken) => {
            return [user, newToken, merchant]
          })
        }).spread(function (user, verificationToken, merchant) {
          return UserToken.sessionToken(user, t).then(function (token) {
            return user.mailchimpSignUp(t).then(function () {
              return token
            })
          }).then(function (token) {
            let data = user.toShow()
            data.token = token.jwt()
            if (merchant) data.merchant = merchant.toApiSignUp()
            return [data, verificationToken.jwt()]
          })
        })
      } else throw new Error('missing_user_data')
    })
  }
  /**
   * @method generatePunchCards
   * @description Creates a PunchCard for every active PunchCard
   * @param {User} user - User instance
   * @param {Merchant} merchant - Merchant instance
   * @param {object} t - sequelize transaction
   * @returns {Promise<User>} returns an User promise
   *
   * @static
   */
  User.generatePunchCards = function (user, merchant, t) {
    const models = sequelize.models
    const userId = user.id
    let merchantId
    if (merchant) merchantId = merchant.id
    return models.PunchCard.findActive(t).then(function (cards) {
      let invite
      let bulk = []
      for (let i = 0; i < cards.length; ++i) {
        let card = cards[i]
        let cardId = card.id
        let conexion = { userId: userId, punchCardId: cardId }
        if (!invite && merchant && parseInt(card.merchantId) === parseInt(merchantId)) invite = cardId
        bulk.push(conexion)
      }
      return models.UserPunchCard.bulkCreate(bulk, { transaction: t })
        .then(function () {
          return invite
        })
    }).then(function (invite) {
      if (invite) {
        return models.UserPunchCard.findOne({
          where: { userId: userId, punchCardId: invite },
          transaction: t
        })
      }
    }).then(function (card) {
      if (card) {
        return models.UserPunchCardInvite.create({
          merchantId: merchantId,
          userPunchCardId: card.id
        }, { transaction: t }).then(function () {
          ++card.punchCount
          return card.save({ transaction: t })
        })
      }
    })
  }
  /**
   * @method generateFbUser
   * @description Returns a new User via fb params
   * @param {object} data - Request parameters
   * @param {object} t - sequelize transaction
   * @returns {Promise<User>} returns an User promise
   *
   * @static
   */
  User.generateFbUser = function (data, t) {
    let dateVal
    let expiresVal
    let d = data.birthdate
    if (d && date.validate(d)) dateVal = date.parse(d)
    d = data.fb_expires_at
    if (date.validate(d)) expiresVal = date.parse(d)
    let gender = parseInt(data.gender)
    if (isNaN(gender) || isBlank(gender)) gender = null
    const insert = {
      name: data.name,
      lastNames: data.last_names,
      email: data.email,
      gender: gender,
      birthdate: dateVal,
      fbId: data.fb_id,
      fbToken: data.fb_token,
      fbExpiresAt: expiresVal,
      playerId: data.player_id
    }
    return this.fbMe(data.fb_token).then((res) => {
      insert.fbId = res.id
      return this.create(insert, { transaction: t })
    })
  }
  /**
   * @method fbMe
   * @description Get the id and name from facebook
   * @param {string} accessToken - facebook token
   * @returns {Promise<object>} returns an fb response object
   *
   * @instance
   */
  User.fbMe = function (accessToken) {
    return new Promise((resolve, reject) => {
      FB.api(
        'me',
        { fields: 'id,name', access_token: accessToken },
        (res) => {
          if (!res || res.error) {
            this.logger.error(res)
            reject(new Error('fb_error'))
          } else {
            this.logger.debug(res)
            resolve(res)
          }
        }
      )
    })
  }
  /**
   * @method generateUser
   * @description Returns a new User via params
   * @param {object} data - Request parameters
   * @param {object} t - sequelize transaction
   * @returns {Promise<User>} returns an User promise
   *
   * @static
   */
  User.generateUser = function (data, t) {
    let hash = null
    if ((data.password && data.password_confirmation)) {
      if (data.password === data.password_confirmation) {
        hash = this.hash(data.password)
      } else throw new Error('password_does_not_match')
    } else throw new Error('missing_password_confirmation')
    let dateVal
    const d = data.birthdate
    if (d && date.validate(d)) dateVal = date.parse(d)
    let gender = parseInt(data.gender)
    if (isNaN(gender) || isBlank(gender)) gender = null
    const insert = {
      name: data.name,
      lastNames: data.last_names,
      email: data.email,
      gender: gender,
      birthdate: dateVal,
      password: hash,
      playerId: data.player_id
    }
    return this.create(insert, { transaction: t })
  }
  /**
   * @method checkFields
   * @description Returns a new User via params
   * @param {object} data - Request parameters
   * @returns {Promise<boolean>} returns an User promise
   *
   * @static
   */
  User.checkFields = function (params) {
    const reject = sequelize.Promise.reject
    const data = params.user
    if (data) {
      let dateVal
      let d = data.birthdate
      if (d) {
        if (date.validate(d)) dateVal = date.parse(d)
        else return reject(new Error('invalid_birthdate_date'))
      }
      let gender = parseInt(data.gender)
      if (isNaN(gender) || isBlank(gender)) gender = null
      let values = {
        name: data.name,
        lastNames: data.last_names,
        email: data.email,
        gender: gender,
        birthdate: dateVal
      }
      if (data.fb_id && data.fb_token && data.fb_expires_at) {
        d = data.fb_expires_at
        if (date.validate(d)) values.fbId = date.parse(d)
        else return reject(new Error('invalid_facebook_date'))
        values.fbId = data.fb_id
        values.fbToken = data.fb_token
      } else {
        if ((data.password && data.password_confirmation)) {
          if (data.password === data.password_confirmation) {
            values.password = this.hash(data.password)
          } else return reject(new Error('password_does_not_match'))
        } else return reject(new Error('missing_password_confirmation'))
      }
      let user = this.build(values)
      return user.validate().then(function () {
        return true
      })
    } else return reject(new Error('missing_user_data'))
  }
  /**
   * @method validate
   * @description Validates an incoming user token and set their password
   * @param {object} params - Request parameters
   * @returns {Promise<User>} returns a user promise with a session jwt
   *
   * @static
   */
  User.validate = function (params) {
    return sequelize.transaction(function (t) {
      const token = params.token
      if (token) {
        const UserToken = sequelize.models.UserToken
        return UserToken.findValidationToken(token, t)
          .spread(function (verfied, token) {
            const email = token.User.validateEmail
            if (email) {
              token.User.email = email
              token.User.validateEmail = null
              return token.User.save({ transaction: t }).then(function (user) {
                return [user, token]
              })
            } else throw new Error('invalid_token') // should never happen
          })
          .spread(function (user, token) {
            return token.destroy({ transaction: t }).then(function () {
              return user
            })
          })
          .then(function () {
            return true
          })
      } else throw new Error('invalid_token')
    })
  }
  /**
   * @method validate
   * @description Validates an incoming user token and set their password
   * @param {object} params - Request parameters
   * @returns {Promise<User>} returns a user promise with a session jwt
   *
   * @static
   */
  User.validate = function (params) {
    return sequelize.transaction(function (t) {
      const token = params.token
      if (token) {
        const UserToken = sequelize.models.UserToken
        return UserToken.findValidationToken(token, t)
          .spread(function (verfied, token) {
            const email = token.User.validateEmail
            if (email) {
              token.User.email = email
              token.User.validateEmail = null
              return token.User.save({ transaction: t }).then(function (user) {
                return [user, token]
              })
            } else throw new Error('invalid_token') // should never happen
          })
          .spread(function (user, token) {
            return token.destroy({ transaction: t }).then(function () {
              return user
            })
          })
          .then(function () {
            return true
          })
      } else throw new Error('invalid_token')
    })
  }
  /**
   * @method verify
   * @description Verifies an incoming user
   * @param {object} params - Request parameters
   * @returns {Promise<boolean>} returns a boolean promise if verified
   *
   * @static
   */
  User.verify = function (params) {
    return sequelize.transaction(function (t) {
      const token = params.token
      const err = new Error('invalid_token')
      if (token) {
        const UserToken = sequelize.models.UserToken
        return UserToken.findVerificationToken(token, t)
          .spread(function (verified, userToken) {
            if (verified && userToken) {
              userToken.User.isVerified = true
              return userToken.User.save({ transaction: t })
                .then(function (user) {
                  return [user, userToken]
                })
            } else throw err
          })
          .spread(function (user, token) {
            return token.destroy({ transaction: t }).then(function () {
              return user
            })
          })
          .then(function () {
            return true
          })
      } else throw err
    })
  }
  /**
   * @method recovery
   * @description Initiates the recovery process for an email if it exists
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser>} returns a user promise with a recovery
   *  jwt
   *
   * @static
   */
  User.recovery = function (email) {
    return sequelize.transaction((t) => {
      if (email) {
        const UserToken = sequelize.models.UserToken
        return this.findOne({
          attributes: ['id', 'email'],
          where: { email: email, isValid: true },
          transaction: t
        }).then(function (user) {
          if (user) {
            return UserToken.findRecoveryTokenByUser(user.id, t)
              .then(function (token) {
                return [user, token]
              })
          } else throw new Error('not_found')
        }).spread(function (user, token) {
          if (token) throw new Error('already_sent')
          else {
            return UserToken.recoveryToken(user, t).then(function (result) {
              return [user, result.jwt()]
            })
          }
        })
      } else throw new Error('missing_email')
    })
  }
  /**
   * @method recover
   * @description Sets the new password with a valid token
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser>} returns a user promise with a recovery
   *  jwt
   *
   * @static
   */
  User.recover = function (params) {
    return sequelize.transaction((t) => {
      const token = params.token
      const password = params.password
      const passwordConfirmation = params.password_confirmation
      if (token) {
        return sequelize.models.UserToken.findRecoveryToken(token, t)
          .spread((verified, token) => {
            if (
              (password && passwordConfirmation) &&
              (password === passwordConfirmation)
            ) {
              token.User.password = this.hash(password)
              return token.User.save({ transaction: t })
                .then(function (user) {
                  return [user, token]
                })
            } else throw new Error('password_error')
          })
          .spread(function (user, token) {
            return token.destroy({ transaction: t }).then(function () {
              return user
            })
          })
          .then(function (user) {
            return sequelize.models.UserToken.refreshSession(user, t)
              .then(function (token) {
                return [user, token]
              })
          })
          .spread(function (user, token) {
            let data = user.toShow()
            data.token = token.jwt()
            return data
          })
      } else throw new Error('invalid_token')
    })
  }
  /**
   * @method activeForMerchant
   * @description Counts the total number of active users punch cards for a
   *  merchant
   * @return {Promise<array>} Returns an array with total values
   *
   * @static
   */
  User.activeForMerchant = function (merchant) {
    // console.log('User.activeForMerchant')
    return this.scope({ method: ['activeForMerchant', merchant.id] }).findAll({
      attributes: [
        [sequelize.fn('COUNT', sequelize.col('User.id')), 'total']
      ]
    })
  }
  /**
   * @method activeForMerchantByMonth
   * @description Counts the total number of active users punch cards for a
   *  merchant per month
   * @return {Promise<array>} Returns an array with total values
   *
   * @static
   */
  User.activeForMerchantByMonth = function (merchant) {
    const monthField = 'month'
    const yearField = 'year'
    const dateField = 'UserPunchCards.created_at'
    return this.scope({ method: ['activeForMerchant', merchant.id] }).findAll({
      attributes: [
        [sequelize.fn('COUNT', sequelize.col('User.id')), 'total'],
        toChar(sequelize, dateField, monthField, 'MM'),
        toChar(sequelize, dateField, yearField, 'YYYY')
      ],
      group: [monthField, yearField],
      order: [
        [sequelize.literal(yearField), 'DESC'],
        [sequelize.literal(monthField), 'DESC']
      ]
    })
  }
  /**
   * @method mailchimpSignUpSync
   * @description Syncs existing Users with Mailchimp
   * @returns {Promise} returns a user promise with a session jwt
   *
   * @static
   */
  User.mailchimpSignUpSync = function () {
    return this.findAll({ where: { mailchimpId: null } })
      .then(function (result) {
        let edits = []
        for (let i = 0; i < result.length; ++i) {
          let element = result[i]
          edits.push(
            element.mailchimpSignUp().then(function () {
              return element.save()
            })
          )
        }
        return Promise.all(edits)
      })
  }
  /**
   * @method adminList
   * @description Get a User list
   * @return {Promise<array>} Returns an array with User instances
   *
   * @static
   */
  User.adminList = function () {
    return this.findAll({
      attributes: ['identifier', 'name', 'lastNames', 'email', 'isVerified']
    })
  }
  /**
   * @method adminShow
   * @description Returns a single user via identifier if any
   * @return {Promise<User>} Returns a User promise
   *
   * @static
   */
  User.adminShow = function (id) {
    const err = new Error('not_found')
    if (validator.isUUID(id)) {
      return this.findOne({ where: { identifier: id } })
        .then(function (result) {
          if (result) return result
          else throw err
        })
    } else return Promise.reject(err)
  }
  /**
   * @method show
   * @description Returns a single user via identifier if any
   * @return {Promise<User>} Returns a User promise
   *
   * @static
   */
  User.show = function (id) {
    const err = new Error('not_found')
    if (validator.isUUID(id)) {
      return this.findOne({ where: { identifier: id } })
        .then(function (result) {
          if (result) return result
          else throw err
        })
    } else return Promise.reject(err)
  }
  /**
   * @method newChurned
   * @description Uncategoriesed churned users
   * @param {array<Date,Date>} range - date array
   * @return {Promise<User[]>} Returns a User array promise
   *
   * @static
   */
  User.newChurned = function (startAt, endAt) {
    const subQuery = `
      SELECT DISTINCT users.id
      FROM users
      INNER JOIN user_punch_cards ON user_punch_cards.user_id = users.id
      INNER JOIN user_punch_card_validations
        ON user_punch_card_validations.user_punch_card_id = user_punch_cards.id
      WHERE user_punch_card_validations.created_at
        BETWEEN :startAt AND :endAt`
    const query = `SELECT users.id,users.email FROM users
      INNER JOIN mailing_list_users
        ON
          mailing_list_users.user_id = users.id
        AND
          mailing_list_users.interest = :interest
      WHERE users.id NOT IN(${subQuery})`
    return sequelize.query(query, {
      model: User,
      replacements: {
        interest: sequelize.models.MailingListUser.Interests.Active,
        startAt: startAt,
        endAt: endAt
      },
      logging: console.log
    })
  }
  /**
   * @method forFirstExpiryNotification
   * @description Users with a punchcard at least 30 days notified.
   * @return {Promise<User[]>} Returns a User array promise
   *
   * @static
   */
  User.forFirstExpiryNotification = function () {
    const expiresAt = moment().add(
      process.env.USER_FIRST_EXPIRY_WARNING_AT,
      'days'
    )
    return this.forExpiryNotification(
      expiresAt,
      models.UserPunchCard.AlertNotificationSent.First
    )
  }
  /**
   * @method forSecondExpiryWarningNotification
   * @description Users with a punchcard at least 15 days notified.
   * @return {Promise<User[]>} Returns a User array promise
   *
   * @static
   */
  User.forSecondExpiryNotification = function () {
    const expiresAt = moment().add(
      process.env.USER_SECOND_EXPIRY_WARNING_AT,
      'days'
    )
    return this.forExpiryNotification(
      expiresAt,
      models.UserPunchCard.AlertNotificationSent.Second
    )
  }
  /**
   * @method forExpiryNotification
   * @description Users with notify permission a punchcard at least +expiresAt+
   *  from expiring.
   * @param {moment} expiresAt - moment date instance
   * @param {integer} notificationFlag - Notification being sent
   * @return {Promise<User[]>} Returns a User array promise
   *
   * @static
   */
  User.forExpiryNotification = function (expiresAt, notificationFlag) {
    const Op = sequelize.Op
    const models = sequelize.models
    const PunchCard = models.PunchCard
    return this.findAll({
      attributes: ['id', 'playerId'],
      where: {
        playerId: { [Op.not]: null },
        hasExpiryNotifyPermission: true
      },
      include: [{
        required: true,
        attributes: ['id', 'alertNotifcationSent', 'punchCount'],
        model: models.UserPunchCard,
        where: {
          punchCount: { [Op.gt]: 0 },
          alertNotifcationSent: {
            [Op.or]: [{ [Op.eq]: null }, { [Op.lt]: notificationFlag }]
          }
        },
        include: [{
          required: true,
          attributes: ['expiresAt', 'punchLimit'],
          model: PunchCard,
          where: {
            expiresAt: { [Op.lte]: expiresAt },
            identity: PunchCard.Identity.Published
          },
          include: [{
            required: true,
            attributes: ['id', 'name'],
            model: models.Merchant.scope('active')
          }]
        }]
      }]
    })
  }
  /**
   * @method expiryNotify
   * @description Notify only once for each user. And return a Merchant count.
   * @param {integer} notificationFlag - Notification being sent
   * @return {object[]} Returns a object array promise
   *
   * @static
   */
  User.expiryNotify = function (notificationFlag) {
    return function (users) {
      return new Promise((resolve, reject) => {
        const notifications = expiryNotifications[notificationFlag]
        each(
          users,
          function (user, cbk) {
            // only notify once
            const notification = user.toExpiryNotification(notifications)
            // notify each user with custom
            OneSignal.notify.player(user.playerId, notification)
              .then(function (result) {
                User.logger.debug('notification sent:', result)
                return User.updateNotificationSent(
                  user.UserPunchCards,
                  notificationFlag
                )
              })
              .then(function () {
                cbk()
              })
          },
          function (err) {
            if (err) reject(err)
            else resolve(users)
          }
        )
      })
    }
  }
  User.updateNotificationSent = function (userPunchCards, flag) {
    const ids = _.map(userPunchCards, 'id')
    return sequelize.models.UserPunchCard.update(
      { alertNotifcationSent: flag },
      { where: { id: ids } }
    )
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toShow
   * @description Returns api apprpriate data for a User
   * @returns {object} - User information
   *
   * @instance
   */
  User.prototype.toShow = function () {
    return {
      id: this.id,
      identifier: this.identifier,
      is_verified: this.isVerified,
      name: this.name,
      last_names: this.lastNames,
      email: this.email,
      gender: this.gender,
      birthdate: this.birthdate,
      created_at: this.created_at,
      has_expiry_notify_permission: this.hasExpiryNotifyPermission
    }
  }
  /**
   * @method toShowPermissions
   * @description Returns api apprpriate permission data for a User
   * @returns {object} - User information
   *
   * @instance
   */
  User.prototype.toShowPermissions = function () {
    return {
      has_expiry_notify_permission: this.hasExpiryNotifyPermission
    }
  }
  /**
   * @method toVerified
   * @description Returns verification state
   * @returns {object} - User information
   *
   * @instance
   */
  User.prototype.toVerified = function () {
    return { is_verified: this.isVerified }
  }
  /**
   * @method toPermissions
   * @description Returns permissions state
   * @returns {object} - User information
   *
   * @instance
   */
  User.prototype.toPermissions = function () {
    return { has_expiry_notify_permission: this.hasExpiryNotifyPermission }
  }
  /**
   * @method verification
   * @description Returns verification token for user if any
   * @returns {object} - sequelize transaction
   *
   * @instance
   */
  User.prototype.verification = function () {
    return sequelize.transaction((t) => {
      if (!this.isVerified) {
        const UserToken = sequelize.models.UserToken
        return UserToken.findVerificationTokenByUser(this.id, t)
          .then((token) => {
            if (token) return [this, token]
            else {
              return UserToken.verificationToken(this, t).then((newToken) => {
                return [this, newToken]
              })
            }
          })
          .spread(function (user, token) {
            return [user, token.jwt()]
          })
      } else throw Error('user_verified')
    })
  }
  /**
   * @method checkAuth
   * @description Compares if the password is the one saved in the db.
   * @param {string} plain - password in plain text
   * @returns {Promise<UserToken>} - return an OrganizationUserToken promise if
   *  correct
   *
   * @instance
   */
  User.prototype.checkAuth = function (plain, t) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(plain, this.password, (err, res) => {
        if (err || res === false) reject(new Error('login_fail'))
        else {
          sequelize.models.UserToken.sessionToken(this, t)
            .then(resolve)
            .catch(reject)
        }
      })
    })
  }
  /**
   * @method checkPassword
   * @description Compares if the password is the one saved in the db.
   * @param {string} plain - password in plain text
   * @returns {Promise} - return an promise
   *
   * @instance
   */
  User.prototype.checkPassword = function (plain) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(plain, this.password, function (err, res) {
        if (err || res === false) reject(new Error('incorrect_password'))
        else resolve()
      })
    })
  }
  /**
   * @method fbMe
   * @description Compares the password and retruns a jwt if valid
   * @param {string} fbToken - facebook token
   * @returns {Promise<object>} returns an fb response object
   *
   * @instance
   */
  User.prototype.fbMe = function () {
    return new Promise((resolve, reject) => {
      FB.api(
        'me',
        { fields: 'id,name', access_token: this.fbToken },
        function (res) {
          if (!res || res.error) reject(res)
          else resolve(res)
        }
      )
    })
  }
  /**
   * @method logout
   * @description Removes a user's session token
   * @param {UserToken} token - UserToken instance
   * @returns {object} returns an sequelize transaction object
   *
   * @instance
   */
  User.prototype.logout = function (token) {
    return sequelize.transaction(function (t) {
      return token.destroy({ transaction: t })
    })
  }
  /**
   * @method updateFromData
   * @description Updates the current user via request params
   * @param {object} params - request params
   * @returns {object} returns an sequelize transaction object
   *
   * @instance
   */
  User.prototype.updateFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      if (data) {
        if (data.name) this.name = data.name
        if (data.last_names) this.lastNames = data.last_names
        if (data.gender !== undefined) {
          if (isNaN(data.gender) || isBlank(data.gender)) this.gender = null
          else this.gender = data.gender
        }
        const d = data.birthdate
        if (d && date.validate(d)) this.birthdate = d
        return this.save({ transaction: t })
      } else throw new Error('missing_user_data')
    })
  }
  /**
   * @method updatePlayerFromData
   * @description Updates the current user's player data via request params
   * @param {object} params - request params
   * @returns {object} returns an sequelize transaction promise
   *
   * @instance
   */
  User.prototype.updatePlayerFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      if (data) {
        if (data.player_id) this.playerId = data.player_id
        return this.save({ transaction: t })
      } else throw new Error('missing_user_data')
    })
  }
  /**
   * @method updateEmail
   * @description Sets the new email up for validation
   * @param {string} email - new email
   * @returns {object} returns an sequelize transaction object
   *
   * @instance
   */
  User.prototype.updateEmail = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      if (data && data.email && data.password) {
        const email = data.email
        const UserToken = sequelize.models.UserToken
        const query = {
          attributes: ['id'],
          where: { email: email },
          transaction: t
        }
        return User.findOne(query).then((user) => {
          if (user) throw new Error('email_exists')
          else return this.checkPassword(data.password)
        }).then(() => {
          return UserToken.findValidationTokenForUser(this.id, t)
        }).then((token) => {
          this.validateEmail = email
          return this.save({ transaction: t }).then(function (user) {
            return [user, token]
          })
        }).spread((user, token) => {
          if (token) return [user, token]
          else {
            return UserToken.validateToken(this, t).then(function (token) {
              return [user, token]
            })
          }
        }).spread(function (user, token) {
          return [user, token.jwt()]
        })
      } else throw new Error('missing_email')
    })
  }
  /**
   * @method updatePassword
   * @description Updates the current password
   * @param {string} old - old password
   * @param {string} newPassword - new password
   * @param {string} confirmation - new password confirmation
   * @returns {object} returns an sequelize transaction object
   *
   * @instance
   */
  User.prototype.updatePassword = function (old, newPassword, confirmation) {
    return sequelize.transaction((t) => {
      if (old && newPassword && confirmation) {
        return this.checkPassword(old).then(() => {
          if (newPassword === confirmation) {
            this.password = User.hash(newPassword)
            return this.save({ transaction: t })
          } else throw new Error('password_does_not_match')
        })
      } else throw new Error('missing_password_data')
    })
  }
  /**
   * @method updatePermissions
   * @description Updates the current permission
   * @param {object} params - incomming request parameters
   * @returns {object} returns an sequelize transaction object
   *
   * @instance
   */
  User.prototype.updatePermissions = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      if (data) {
        if (data.has_expiry_notify_permission !== undefined) {
          this.hasExpiryNotifyPermission = data.has_expiry_notify_permission
        }
        return this.save({ transaction: t })
      } else throw new Error('missing_user_data')
    })
  }
  /**
   * @method toMailchimp
   * @description Returns mailchimp data
   * @returns {object} - User data
   *
   * @instance
   */
  User.prototype.toMailchimp = function () {
    let mergeFields = { FNAME: this.name, LNAME: this.lastNames }
    if (this.gender) mergeFields['GENDER'] = this.gender
    if (this.birthdate) mergeFields['BIRTHDATE'] = this.birthdate
    return {
      email_address: this.email,
      status: 'subscribed',
      merge_fields: mergeFields
    }
  }
  /**
   * @method mailchimpSignUp
   * @description Subscribe User to mailing list
   * @returns {Promise} returns subscribe results promise
   *
   * @static
   */
  User.prototype.mailchimpSignUp = function (t) {
    return sequelize.models.MailingList.scope('users').findAll()
      .then((lists) => {
        let subscribes = []
        for (let i = 0; i < lists.length; ++i) {
          subscribes.push(lists[i].subscribeUser(this, t))
        }
        return Promise.all(subscribes)
      }).then((results) => {
        if (results.length > 0) {
          const firstResult = results[0]
          this.mailchimpId = firstResult[0].unique_email_id
          return this.save({ transaction: t })
        }
      })
  }
  /**
   * @method churn
   * @description Subscribe User to mailing list
   * @returns {Promise} returns subscribe results promise
   *
   * @static
   */
  User.prototype.churn = function () {
    const MailingListUser = sequelize.models.MailingListUser
    return MailingListUser.findAll({
      where: { userId: this.id },
      include: [{
        required: true,
        model: sequelize.models.MailingList
      }]
    }).then(function (lists) {
      let updates = []
      for (let i = 0; i < lists.length; ++i) {
        updates.push(lists[i].updateInterest(MailingListUser.Interests.Churned))
      }
      return Promise.all(updates)
    })
  }
  /**
   * @method toAdminList
   * @description Return admin list apprpriate data
   * @returns {object} User admin list data
   *
   * @instance
   */
  User.prototype.toAdminList = function () {
    return {
      id: this.identifier,
      name: this.name,
      last_names: this.lastNames,
      email: this.email,
      is_verified: this.isVerified
    }
  }
  /**
   * @method signUpType
   * @description Return if user registred via facebook or email
   * @returns {string} Signup type
   *
   * @instance
   */
  User.prototype.signUpType = function () {
    return (this.fbId && this.fbToken) ? 'facebook' : 'email'
  }
  /**
   * @method topMerchants
   * @description Return top merchant interaction for a User
   * @returns {array} top merchants for user
   *
   * @instance
   */
  User.prototype.topMerchants = function () {
    return sequelize.models.UserPunchCardValidation.topMerchantsForUser(this)
  }
  /**
   * @method topShops
   * @description Return top shop interaction for a User
   * @returns {array} top shops for user
   *
   * @instance
   */
  User.prototype.topShops = function () {
    return sequelize.models.UserPunchCardValidation.topShopsForUser(this)
  }
  /**
   * @method topShopsByMerchant
   * @description Return top shop interaction for a User filtered by a Merchant
   * @param {Merchant} merchant - Merchant instance
   * @returns {array} top shops for user
   *
   * @instance
   */
  User.prototype.topShopsByMerchant = function (merchant) {
    return sequelize.models.UserPunchCardValidation.topShopsForUserByMerchant(
      this,
      merchant
    )
  }
  /**
   * @method weeklyPunches
   * @description Return top shop interaction for a User
   * @returns {array} top shops for user
   *
   * @instance
   */
  User.prototype.weeklyPunches = function () {
    return sequelize.models.UserPunchCardValidation.weeklyPunchesForUser(this)
  }
  /**
   * @method monthlyPunches
   * @description Return top shop interaction for a User
   * @returns {array} top shops for user
   *
   * @instance
   */
  User.prototype.monthlyPunches = function () {
    return sequelize.models.UserPunchCardValidation.weeklyPunchesForUser(this)
  }
  /**
   * @method history
   * @description Return validation history for a User
   * @returns {array} validation history for user
   *
   * @instance
   */
  User.prototype.history = function () {
    const UserPunchCardValidation = sequelize.models.UserPunchCardValidation
    return UserPunchCardValidation.historyForUser(this)
      .then(
        UserPunchCardValidation.rawDecorator(
          UserPunchCardValidation.toAdminUserHistory
        )
      )
  }
  /**
   * @method historyByMerchant
   * @description Return validation history for a User filtered by Merchant
   * @param {Merchant} merchant - Merchant instance
   * @returns {array} validation history for user
   *
   * @instance
   */
  User.prototype.historyByMerchant = function (merchant) {
    const UserPunchCardValidation = sequelize.models.UserPunchCardValidation
    return UserPunchCardValidation.historyForUserByMerchant(this, merchant)
      .then(
        UserPunchCardValidation.rawDecorator(
          UserPunchCardValidation.toAdminUserHistory
        )
      )
  }
  /**
   * @method categoryBreakdown
   * @description Return category percentages for a User
   * @returns {array} category percentages for user
   *
   * @instance
   */
  User.prototype.categoryBreakdown = function () {
    return sequelize.models.Category.breakdownForUser(this)
  }
  /**
   * @method averageBreakdown
   * @description Return average values for each identity for a User
   * @returns {array} average data
   *
   * @instance
   */
  User.prototype.averageBreakdown = function () {
    const UserPunchCardValidation = sequelize.models.UserPunchCardValidation
    return UserPunchCardValidation.averageBreakdownForUser(this)
      .then(UserPunchCardValidation.toAverageBreakdown)
  }
  /**
   * @method weekAverageByMerchant
   * @description Return average values for each identity for a User filtered by
   *  a Merchant
   * @param {Merchant} merchant - Merchant instance
   * @returns {array} average data
   *
   * @instance
   */
  User.prototype.weeklyAverageByMerchant = function (merchant) {
    return sequelize.models.UserPunchCardValidation
      .weeklyAverageForUserByMerchant(this, merchant)
  }
  /**
   * @method monthAverageByMerchant
   * @description Return average values for each identity for a User filtered by
   *  a Merchant
   * @param {Merchant} merchant - Merchant instance
   * @returns {array} average data
   *
   * @instance
   */
  User.prototype.monthAverageByMerchant = function (merchant) {
    return sequelize.models.UserPunchCardValidation
      .monthlyAverageForUserByMerchant(this, merchant)
  }
  /**
   * @method totalVisitsByMerchant
   * @description Returns the total visits for a Merchant
   * @param {Merchant} merchant - Merchant instance
   * @returns {array} visit total data
   *
   * @instance
   */
  User.prototype.totalVisitsByMerchant = function (merchant) {
    return sequelize.models.UserPunchCardValidation.merchantTotalForUser(
      merchant,
      this
    )
  }
  /**
   * @method toAdmin
   * @description Return admin apprpriate data
   * @returns {object} User admin data
   *
   * @instance
   */
  User.prototype.toAdmin = function () {
    return Promise.join(
      this.topMerchants(),
      this.topShops(),
      this.history(),
      this.categoryBreakdown(),
      this.averageBreakdown(),
      (merchants, shops, history, categories, averages) => {
        return {
          id: this.identifier,
          name: this.name,
          last_names: this.lastNames,
          email: this.email,
          is_verified: this.isVerified,
          sign_up_type: this.signUpType(),
          gender: this.gender,
          birthdate: this.birthdate,
          created_at: this.created_at,
          top_merchants: merchants,
          top_shops: shops,
          history: history,
          categories: categories,
          averages: averages
        }
      }
    )
  }
  /**
   * @method toAdminMerchant
   * @description Return admin merchant apprpriate data
   * @returns {object} User detail admin data
   *
   * @instance
   */
  User.prototype.toAdminMerchant = function (merchant) {
    return Promise.join(
      this.totalVisitsByMerchant(merchant),
      this.topShopsByMerchant(merchant),
      this.historyByMerchant(merchant),
      this.weeklyAverageByMerchant(merchant),
      this.monthAverageByMerchant(merchant),
      (merchantTotal, shops, history, weekly, monthly) => {
        let weeklyAverage = weekly[0].average
        if (weeklyAverage === null) weeklyAverage = 0
        let monthlyAverage = monthly[0].average
        if (monthlyAverage === null) monthlyAverage = 0
        return {
          id: this.identifier,
          name: this.name,
          last_names: this.lastNames,
          email: this.email,
          birthdate: this.birthdate,
          merchant_total: parseInt(merchantTotal[0].total),
          top_shops: shops,
          history: history,
          weekly_average: parseInt(weeklyAverage),
          monthly_average: parseInt(monthlyAverage)
        }
      }
    )
  }
  /**
   * @method toMerchant
   * @description Return merchant apprpriate data
   * @returns {object} User detail merchant data
   *
   * @instance
   */
  User.prototype.toMerchant = function (merchant) {
    return Promise.join(
      this.totalVisitsByMerchant(merchant),
      this.topShopsByMerchant(merchant),
      this.historyByMerchant(merchant),
      this.weeklyAverageByMerchant(merchant),
      this.monthAverageByMerchant(merchant),
      (merchantTotal, shops, history, weekly, monthly) => {
        let weeklyAverage = weekly[0].average
        if (weeklyAverage === null) weeklyAverage = 0
        let monthlyAverage = monthly[0].average
        if (monthlyAverage === null) monthlyAverage = 0
        return {
          id: this.identifier,
          name: this.name,
          last_names: this.lastNames[0],
          email: this.email,
          birthdate: this.birthdate,
          merchant_total: parseInt(merchantTotal[0].total),
          top_shops: shops,
          history: history,
          weekly_average: parseInt(weeklyAverage),
          monthly_average: parseInt(monthlyAverage)
        }
      }
    )
  }
  /**
   * @method apiHistory
   * @description merges all events into a single ordered array
   * @param {object} params - Incoming request params
   * @returns {array} User history event array
   *
   * @instance
   */
  User.prototype.apiHistory = function (params) {
    const models = sequelize.models
    return Promise.join(
      models.UserPunchCardValidation.apiHistoryForUser(this),
      models.UserPunchCard.expired(this),
      models.UserPunchCard.completed(this),
      function (validations, expired, completed) {
        // NOTE: check if sorting can be optimized
        // Evaluate using an Events table or nosql service and consuming from an
        // external source.
        return expired
          .concat(completed)
          .concat(validations)
          .sort(function (a, b) {
            const expiresAt = 'PunchCard.expiresAt'
            let compareA = a.created_at
            let compareAIsComplete = true
            if (a[expiresAt]) {
              compareA = a[expiresAt]
              compareAIsComplete = false
            }
            compareA = new Date(compareA).getTime()
            let compareB = b.created_at
            let compareBIsComplete = true
            if (b[expiresAt]) {
              compareB = b[expiresAt]
              compareBIsComplete = false
            }
            compareB = new Date(compareB).getTime()
            // complete events take should be first
            if (compareA === compareB && compareAIsComplete) return -1
            else if (compareA === compareB && compareBIsComplete) return 1
            return compareB - compareA
          })
      }
    )
  }
  /**
   * @method toExpiryNotification
   * @description Returns expiry notification data for a User
   * @returns {object} - User information
   *
   * @instance
   */
  User.prototype.toExpiryNotification = function (notifications) {
    let result = {}
    // association included from query
    const userPunchCards = this.UserPunchCards
    const cardCount = userPunchCards.length
    if (cardCount >= 3) {
      result = notifications[2]
      result.contents.en = result.contents.en.replace(':cardCount', cardCount)
    } else if (cardCount === 2) {
      result = notifications[1]
      const firstUserPunchCard = userPunchCards[0]
      const secondUserPunchCard = userPunchCards[1]
      const firstMerchant = firstUserPunchCard.PunchCard.Merchant
      const secondMerchant = secondUserPunchCard.PunchCard.Merchant
      result.contents.en = result.contents.en.replace(
        ':firstMerchant',
        firstMerchant.name
      )
      result.contents.en = result.contents.en.replace(
        ':secondMerchant',
        secondMerchant.name
      )
    } else if (cardCount === 1) {
      result = notifications[0]
      const userPunchCard = userPunchCards[0]
      const punchCard = userPunchCard.PunchCard
      const merchant = punchCard.Merchant
      result.contents.en = result.contents.en.replace(
        ':merchant',
        merchant.name
      )
    }
    return result
  }
  return User
}
