/**
 * Kazam Api
 * @name Sequelize Suggestion Definition
 * @file models/suggestion.js
 * @author Joel Cano
 */

'use strict'

const decorator = require('../lib/decorate')
const stringField = require('../lib/sequelize/string')

module.exports = function (sequelize, DataTypes) {
  let Suggestion = sequelize.define(
    // Model name
    'Suggestion',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      userId: {
        allowNull: false,
        field: 'user_id',
        type: DataTypes.BIGINT
      },
      name: stringField(DataTypes, false)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'suggestions'
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  Suggestion.associate = function (models) {
    Suggestion.belongsTo(models.User, { foreignKey: 'userId' })
  }
  /**
   * @method toApi
   * @description Returns apprpriate api data
   * @returns {Promise<Suggestion>} - Suggestion promise
   *
   * @static
   */
  Suggestion.toApi = 'toApi'
  /**
   * @method toAdmin
   * @description Returns apprpriate admin data
   * @returns {Promise<array>} - Suggestion array promise
   *
   * @static
   */
  Suggestion.toAdmin = 'toAdmin'
  Suggestion.decorate = decorator
  /**
   * @method adminList
   * @description List suggestions for admin orderd by latest
   * @return {Promise<Suggestion[]>}      Returns a Suggestion array promise
   */
  Suggestion.adminList = function () {
    const query = {
      attributes: ['id', 'name', 'created_at'],
      include: [{
        attributes: ['id', 'identifier', 'name', 'email'],
        model: sequelize.models.User
      }],
      order: [['created_at', 'DESC']]
    }
    return this.findAll(query)
  }
  /**
   * @method createFromData
   * @description Create a new Suggestion with request data
   * @param  {Object} params            request parameters
   * @return {Promise<Suggestion>}      Returns a Suggestion instance promise
   */
  Suggestion.createFromData = function (params, user) {
    return sequelize.transaction((t) => {
      const data = params.suggestion
      if (data && user) {
        const insert = { userId: user.id, name: data.name }
        return this.create(insert, { transaction: t })
      } else throw new Error('missing_suggestion_data')
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toApi
   * @description Returns api apprpriate data for a suggestion
   * @returns {object} - shop cluster information
   *
   * @instance
   */
  Suggestion.prototype.toApi = function () {
    return { id: this.id, name: this.name }
  }
  /**
   * @method toAdmin
   * @description Returns admin apprpriate data for a suggestion
   * @returns {object} - shop cluster information
   *
   * @instance
   */
  Suggestion.prototype.toAdmin = function () {
    return {
      id: this.id,
      name: this.name,
      created_at: this.created_at,
      user: {
        identifier: this.User.identifier,
        name: this.User.name,
        email: this.User.email
      }
    }
  }
  return Suggestion
}
