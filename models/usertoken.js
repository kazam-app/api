/**
 * Kazam Api
 * @name Sequelize User Token Definition
 * @file models/usertoken.js
 * @author Joel Cano
 */

'use strict'

const pair = require('pair')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const Promise = require('bluebird')
const errors = require('../lib/error')
const encryptionKey = process.env.UT_KEY
const InvalidError = errors.InvalidError
const ExpiredError = errors.ExpiredError
const NotFoundError = errors.NotFoundError
const tokenPair = require('../lib/sequelize/token')
const identityField = require('../lib/sequelize/identity')

const Identity = { Login: 0, Validate: 1, Recovery: 2, Verify: 3 }
const Identities = [
  Identity.Login,
  Identity.Validate,
  Identity.Recovery,
  Identity.Verify
]
const scopeAttributes = ['id', 'userId', 'token', 'secret', 'expiresAt']

module.exports = function (sequelize, DataTypes) {
  let UserToken = sequelize.define(
    // Model name
    'UserToken',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      userId: {
        field: 'user_id',
        allowNull: false,
        type: DataTypes.BIGINT
      },
      identity: identityField(DataTypes, Identity.Login, Identities),
      token: tokenPair.token(DataTypes, false),
      secret: tokenPair.secret(DataTypes, false),
      expiresAt: {
        allowNull: true,
        field: 'expires_at',
        type: DataTypes.DATE
      }
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'user_tokens',
      /**
       * hooks
       * @description Model hooks
       * @type {Object}
       *
       */
      hooks: {
        /**
         * @method beforeValidate
         * @description Sets token and secret values before the validations are
         *  run.
         * @param  {OrganizationUserToken} instance - current model instance
         *
         * @static
         */
        beforeValidate: function (instance) {
          if (!instance.token || !instance.secret) {
            const token = instance.userId + '-' + instance.created_at.getTime()
            const keys = pair.generate(token, encryptionKey)
            instance.token = keys.token
            instance.secret = keys.secret
          }
        }
      },
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method activeForUserAndIdentity
         * @description Find active element for a user by identity
         * @returns {object} - query structure for scope
         *
         * @static
         */
        activeForUserAndIdentity: function (userId, identity) {
          const Op = sequelize.Op
          return {
            attributes: scopeAttributes,
            where: {
              [Op.or]: [
                { identity: identity, userId: userId, expiresAt: null },
                {
                  identity: identity,
                  userId: userId,
                  expiresAt: { [Op.gt]: new Date() }
                }
              ]
            }
          }
        },
        /**
         * @method activeForTokenAndIdentity
         * @description Find active element for a token by identity
         * @returns {object} - query structure for scope
         *
         * @static
         */
        activeForTokenAndIdentity: function (token, identity) {
          return {
            attributes: scopeAttributes,
            where: tokenPair.activeForTokenAndIdentity(sequelize, token, identity),
            include: [{ model: sequelize.models.User }]
          }
        },
        /**
         * @method expiredForTokenAndIdentity
         * @description Find and expired token for a token by identity
         * @returns {object} - query structure for scope
         *
         * @static
         */
        expiredForTokenAndIdentity: function (token, identity) {
          return {
            attributes: scopeAttributes,
            where: tokenPair.expiredForTokenAndIdentity(sequelize, token, identity),
            include: [{ model: sequelize.models.User }]
          }
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * Identity Enum
   * @type {Object}
   */
  UserToken.Identity = Identity
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  UserToken.associate = function (models) {
    UserToken.belongsTo(models.User, { foreignKey: 'userId' })
  }
  /**
   * @method findUserToken
   * @description Finds the latest active session token expires it and creates a
   *  new instance.
   * @param {User} user - Organization User instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<UserToken>} returns a organization UserToken promise
   *
   * @static
   */
  UserToken.findUserToken = function (user, t) {
    return this.findOne({
      where: { userId: user.id, identity: Identity.Login },
      transaction: t
    })
  }
  /**
   * @method sessionToken
   * @description Creates an UserToken
   * @param {User} user - organization user instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<UserToken>} returns a UserToken promise
   *
   * @static
   */
  UserToken.sessionToken = function (user, t) {
    return this.create({ userId: user.id }, { transaction: t })
  }
  /**
   * @method findSessionToken
   * @description Finds the latest active session token expires it and creates a
   *  new instance.
   * @param {User} user - Organization User instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<UserToken>} returns a organization user token promise
   *
   * @static
   */
  UserToken.findSessionToken = function (token) {
    return this.findOne({
      where: { token: token, identity: Identity.Login },
      include: [{ model: sequelize.models.User }]
    })
  }
  /**
   * @method refreshSession
   * @description Finds the latest active session token expires it and creates a
   *  new instance.
   * @param {User} user - User instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<UserToken>} returns a organization user token promise
   *
   * @static
   */
  UserToken.refreshSession = function (user, t) {
    return this.findUserToken(user.id, t)
      .then((last) => {
        const token = this.sessionToken(user, t)
        if (last) {
          return last.destroy({ transaction: t }).then(function () {
            return token
          })
        } else return token
      })
  }
  /**
   * @method validateToken
   * @description Creates an validation UserToken
   * @param {User} user - User instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<UserToken>} returns a UserToken promise
   *
   * @static
   */
  UserToken.validateToken = function (user, t) {
    return this.newExpireToken(user, Identity.Validate, t)
  }
  /**
   * @method findValidationToken
   * @description Finds a user's validation token if any
   * @param {string} token - jwt token
   * @param {object} t - sequelize transaction
   * @returns {Promise<UserToken>} returns a user token promise
   *
   * @static
   */
  UserToken.findValidationToken = function (token, t) {
    return this.findByJwtAndVerify(token, Identity.Validate, t)
  }
  /**
   * @method findValidationTokenForUser
   * @description Finds a user's validation token if any
   * @param {integer} userId - User id
   * @param {object} t - sequelize transaction
   * @returns {Promise<UserToken>} returns a UserToken promise
   *
   * @static
   */
  UserToken.findValidationTokenForUser = function (userId, t) {
    const Op = sequelize.Op
    return this.findOne({
      where: {
        userId: userId,
        identity: Identity.Validate,
        expiresAt: { [Op.gt]: new Date() }
      },
      transaction: t
    })
  }
  /**
   * @method verificationToken
   * @description Creates an verification UserToken
   * @param {User} user - User instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<UserToken>} returns a UserToken promise
   *
   * @static
   */
  UserToken.verificationToken = function (user, t) {
    return this.newExpireToken(user, Identity.Verify, t)
  }
  /**
   * @method findVerificationToken
   * @description Finds a user's verification token if any. Decodes, verifies it
   *  and returns the element.
   * @param {string} token - jwt token
   * @returns {Promise<UserToken>} returns a user token promise
   *
   * @static
   */
  UserToken.findVerificationToken = function (token, t) {
    return this.findByJwtAndVerify(token, Identity.Verify, t)
  }
  /**
   * @method findVerificationTokenByUser
   * @description Finds a user's verification token if any
   * @param {integer} userId - User id
   * @param {object} t - sequelize transaction
   * @returns {Promise<OrganizationUserToken>} returns a UserToken promise
   *
   * @static
   */
  UserToken.findVerificationTokenByUser = function (userId, t) {
    let opts = { include: [{ model: sequelize.models.User.scope('active') }] }
    if (t) opts.transaction = t
    return this.scope({
      method: ['activeForUserAndIdentity', userId, this.Identity.Verify]
    }).findOne(opts)
  }
  /**
   * @method recoveryToken
   * @description Creates an recovery UserToken
   * @param {User} user - User instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<UserToken>} returns a UserToken promise
   *
   * @static
   */
  UserToken.recoveryToken = function (user, t) {
    return this.newExpireToken(user, Identity.Recovery, t)
  }
  /**
   * @method findRecoveryToken
   * @description Finds a user's recovery token if any
   * @param {string} token - iss value from the jwt
   * @param {object} t - sequelize transaction
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  UserToken.findRecoveryToken = function (token, t) {
    const opts = { include: [{ model: sequelize.models.User.scope('active') }] }
    return this.findByJwtAndVerify(token, Identity.Recovery, t, opts)
  }
  /**
   * @method findRecoveryTokenByUser
   * @description Finds a user's recovery token if any
   * @param {integer} userId - User id
   * @param {object} t - sequelize transaction
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  UserToken.findRecoveryTokenByUser = function (userId, t) {
    let opts = { include: [{ model: sequelize.models.User.scope('active') }] }
    if (t) opts.transaction = t
    return this.scope({
      method: ['activeForUserAndIdentity', userId, this.Identity.Recovery]
    }).findOne(opts)
  }
  /**
   * @method findByJwtAndVerify
   * @description Finds a user's token if any. Decodes, verifies it and returns
   *  the element.
   * @param {string} token - jwt token
   * @param {integer} identity - UserToken identity
   * @param {object} t - sequelize transaction
   * @param {object} options - extra query options
   * @returns {Promise<UserToken>} returns a user token promise
   *
   * @static
   */
  UserToken.findByJwtAndVerify = function (token, identity, t, options) {
    let opts = {}
    if (options) opts = options
    if (t) opts.transaction = t
    const decoded = jwt.decode(token)
    if (decoded) {
      // Check for active tokens
      return this.scope({
        method: ['activeForTokenAndIdentity', decoded.iss, identity]
      }).findOne(opts).then((result) => {
        if (result) {
          // active token found
          return result.verify(token).then(function (verified) {
            return [verified, result]
          })
        } else {
          // If no active tokens are found check for expired tokens
          return this.scope({
            method: ['expiredForTokenAndIdentity', decoded.iss, identity]
          }).findOne(opts).then(function (expired) {
            if (expired) {
              // expired token found
              return expired.verify(token).then(function (verified) {
                return Promise.reject(new ExpiredError('expired_token'))
              })
            } else return Promise.reject(new NotFoundError('token_not_found'))
          })
        }
      })
    } else return Promise.reject(new InvalidError('invalid_token'))
  }
  /**
   * @method newExpireToken
   * @description Create a new token with a future expire date
   * @param {object} user - User instance
   * @param {integer} identity - UserToken identity
   * @param {object} t - sequelize transaction
   * @returns {Promise<UserToken>} returns a new UserToken promise
   *
   * @static
   */
  UserToken.newExpireToken = function (user, identity, t) {
    return this.create(
      {
        userId: user.id,
        identity: identity,
        expiresAt: moment().add(1, 'days')
      },
      { transaction: t }
    )
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method usableSecret
   * @description Decrypts the secret attribute stored in the database.
   * @returns {string} returns the decrypted secret
   *
   * @instance
   */
  UserToken.prototype.usableSecret = function () {
    return pair.decrypt(this.secret, encryptionKey)
  }
  /**
   * @method jwt
   * @description Signs the jwt with the secret and adds the rel attr if any.
   * @param {integer} rel - relationship atribute
   * @returns {string} returns a signed jwt string
   *
   * @instance
   */
  UserToken.prototype.jwt = function (rel) {
    const exp = moment().add(1, 'years')
    let data = {
      iss: this.token,
      exp: exp.unix()
    }
    if (rel) data.rel = rel
    return jwt.sign(data, this.usableSecret())
  }
  /**
   * @method verify
   * @description Verifies the provided jwt with the this instance's secret
   * @param  {string} token unverified jwt
   * @return {Promise<string>} returns a verfied jwt promise
   *
   * @instance
   */
  UserToken.prototype.verify = function (token) {
    return new Promise((resolve, reject) => {
      try {
        resolve(jwt.verify(token, this.usableSecret()))
      } catch (err) {
        reject(new InvalidError('invalid_token'))
      }
    })
  }
  return UserToken
}
