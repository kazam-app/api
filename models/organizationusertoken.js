/**
 * Kazam Api
 * @name Sequelize OrganizationUserToken Definition
 * @file models/organizationusertoken.js
 * @author Joel Cano
 */

'use strict'

const pair = require('pair')
const moment = require('moment')
const jwt = require('jsonwebtoken')
const Promise = require('bluebird')
const errors = require('../lib/error')
const encryptionKey = process.env.OU_KEY
const InvalidError = errors.InvalidError
const ExpiredError = errors.ExpiredError
const NotFoundError = errors.NotFoundError
const tokenPair = require('../lib/sequelize/token')
const identityField = require('../lib/sequelize/identity')

const Identity = { Login: 0, Validate: 1, Recovery: 2, Invite: 3, SignUp: 4 }
const Identities = [
  Identity.Login,
  Identity.Validate,
  Identity.Recovery,
  Identity.Invite,
  Identity.SignUp
]
const scopeAttributes = [
  'id',
  'organizationUserId',
  'token',
  'secret',
  'expiresAt'
]

module.exports = function (sequelize, DataTypes) {
  let OrganizationUserToken = sequelize.define(
    // Model name
    'OrganizationUserToken',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      organizationUserId: {
        allowNull: false,
        type: DataTypes.BIGINT,
        field: 'organization_user_id'
      },
      identity: identityField(DataTypes, Identity.Login, Identities),
      token: tokenPair.token(DataTypes, false),
      secret: tokenPair.secret(DataTypes, false),
      expiresAt: {
        allowNull: true,
        field: 'expires_at',
        type: DataTypes.DATE
      }
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'organization_user_tokens',
      /**
       * hooks
       * @description Model sequelize hooks
       * @type {Object}
       *
       */
      hooks: {
        /**
         * @method beforeValidate
         * @description Sets token and secret values before the validations are
         *  run.
         * @param {OrganizationUserToken} instance - current model instance
         *
         * @static
         */
        beforeValidate: function (instance) {
          if (!instance.token || !instance.secret) {
            const token = instance.organizationUserId + '-' + instance.identity + '-' + instance.created_at.getTime()
            const keys = pair.generate(token, encryptionKey)
            instance.token = keys.token
            instance.secret = keys.secret
          }
        }
      },
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method activeForUserAndIdentity
         * @description Find active element for a user by identity
         * @returns {object} - query structure for scope
         *
         * @static
         */
        activeForUserAndIdentity: function (userId, identity) {
          const Op = sequelize.Op
          return {
            attributes: scopeAttributes,
            where: {
              [Op.or]: [
                {
                  identity: identity,
                  organizationUserId: userId,
                  expiresAt: null
                },
                {
                  identity: identity,
                  organizationUserId: userId,
                  expiresAt: { [Op.gt]: new Date() }
                }
              ]
            }
          }
        },
        /**
         * @method activeForTokenAndIdentity
         * @description Find active element for a token by identity
         * @returns {object} - query structure for scope
         *
         * @static
         */
        activeForTokenAndIdentity: function (token, identity) {
          return {
            attributes: scopeAttributes,
            where: tokenPair.activeForTokenAndIdentity(sequelize, token, identity),
            include: [{ model: sequelize.models.OrganizationUser }]
          }
        },
        /**
         * @method expiredForTokenAndIdentity
         * @description Find and expired token for a token by identity
         * @returns {object} - query structure for scope
         *
         * @static
         */
        expiredForTokenAndIdentity: function (token, identity) {
          return {
            attributes: scopeAttributes,
            where: tokenPair.expiredForTokenAndIdentity(sequelize, token, identity),
            include: [{ model: sequelize.models.OrganizationUser }]
          }
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * Identity Enum
   * @type {Object}
   */
  OrganizationUserToken.Identity = Identity
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  OrganizationUserToken.associate = function (models) {
    OrganizationUserToken.belongsTo(models.OrganizationUser, {
      foreignKey: 'organizationUserId'
    })
  }
  /**
   * @method findByJwtAndVerify
   * @description Finds a user's token if any. Decodes, verifies it and returns
   *  the element.
   * @param {string} token - jwt token
   * @param {integer} identity - UserToken identity
   * @param {object} t - sequelize transaction
   * @param {object} options - extra query options
   * @returns {Promise<UserToken>} returns a user token promise
   *
   * @static
   */
  OrganizationUserToken.findByJwtAndVerify = function (token, identity, t, options) {
    let opts = {}
    if (options) opts = options
    if (t) opts.transaction = t
    const decoded = jwt.decode(token)
    if (decoded) {
      // Check for active tokens
      return this.scope({ method: ['activeForTokenAndIdentity', decoded.iss, identity] })
        .findOne(opts)
        .then((result) => {
          if (result) {
            return result.verify(token).then(function (verified) {
              return [verified, result]
            })
          } else {
            // If no active tokens are found check for expired tokens
            return this.scope({ method: ['expiredForTokenAndIdentity', decoded.iss, identity] })
              .findOne(opts)
              .then(function (expired) {
                if (expired) {
                  return expired.verify(token).then(function (verified) {
                    return Promise.reject(new ExpiredError('expired_token'))
                  })
                } else {
                  return Promise.reject(new NotFoundError('token_not_found'))
                }
              })
          }
        })
    } else return Promise.reject(new InvalidError('invalid_token'))
  }
  /**
   * @method refreshSession
   * @description Finds the latest active session token expires it and creates a
   *  new instance.
   * @param {OrganizationUser} user - Organization User instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.refreshSession = function (user, t) {
    return this.scope({
      method: ['activeForUserAndIdentity', user.id, this.Identity.Login]
    }).findOne({ transaction: t }).then((last) => {
      const token = this.sessionToken(user, t)
      if (last) return last.logout(t).then(function () { return token })
      else return token
    })
  }
  /**
   * @method findSessionToken
   * @description Finds the latest active session token.
   * @param {string} issuer - iss value from the jwt
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.findSessionToken = function (issuer, t) {
    let opts = {}
    if (t) opts.transaction = t
    return this.scope({
      method: ['activeForTokenAndIdentity', issuer, Identity.Login]
    }).findOne(opts)
  }
  /**
   * @method ationToken
   * @description Finds a user's validation token if any
   * @param {string} issuer - iss value from the jwt
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.findValidationToken = function (token, t) {
    const opts = {}
    return this.findByJwtAndVerify(token, Identity.Validate, t, opts)
  }
  /**
   * @method findValidationToken
   * @description Finds a user's validation token if any
   * @param {string} issuer - iss value from the jwt
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.findSignUpToken = function (token, t) {
    const opts = {}
    return this.findByJwtAndVerify(token, Identity.SignUp, t, opts)
  }
  /**
   * @method findRecoveryTokenByUser
   * @description Finds a user's recovery token if any
   * @param {string} issuer - iss value from the jwt
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.findValidationTokenByUser = function (userId, t) {
    let opts = { include: [{ model: sequelize.models.OrganizationUser }] }
    if (t) opts.transaction = t
    return this.scope({
      method: ['activeForUserAndIdentity', userId, Identity.Validate]
    }).findOne(opts)
  }
  /**
   * @method findExpiredValidationToken
   * @description Finds a user's expired validation token if any
   * @param {string} issuer - iss value from the jwt
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.findExpiredValidationToken = function (token, t) {
    const Op = sequelize.Op
    const decoded = jwt.decode(token)
    if (decoded) {
      let opts = {
        where: {
          token: decoded.iss,
          expiresAt: { [Op.lt]: new Date() },
          identity: this.Identity.Validate
        },
        include: [{
          model: sequelize.models.OrganizationUser.scope('active')
        }],
        transaction: t
      }
      return this.findOne(opts)
        .then(function (result) {
          if (result) return [result.verify(token), result]
          else return sequelize.Promise.reject('invalid_token')
        })
    } else return sequelize.Promise.reject('invalid_token')
  }
  /**
   * @method findRecoveryToken
   * @description Finds a user's recovery token if any
   * @param {string} token - jwt token
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.findRecoveryToken = function (token, t) {
    const opts = {
      include: [{ model: sequelize.models.OrganizationUser.scope('active') }]
    }
    return this.findByJwtAndVerify(token, Identity.Recovery, t, opts)
  }
  /**
   * @method findRecoveryTokenByUser
   * @description Finds a user's recovery token if any
   * @param {string} issuer - iss value from the jwt
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.findRecoveryTokenByUser = function (userId, t) {
    let opts = {
      include: [{ model: sequelize.models.OrganizationUser.scope('active') }]
    }
    if (t) opts.transaction = t
    return this.scope({
      method: ['activeForUserAndIdentity', userId, Identity.Recovery]
    }).findOne(opts)
  }
  /**
   * @method sessionToken
   * @description Creates an organization user token with the Login identity
   * @param {OrganizationUser} user - organization user instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.sessionToken = function (user, t) {
    return this.create({
      identity: Identity.Login, organizationUserId: user.id
    }, { transaction: t })
  }
  /**
   * @method validationToken
   * @description Creates an organization user token with the Validate identity
   * @param {OrganizationUser} user - organization user instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.validationToken = function (user, t) {
    const exp = moment().add(1, 'day')
    return this.create({
      identity: Identity.Validate, organizationUserId: user.id, expiresAt: exp
    }, { transaction: t })
  }
  /**
   * @method signUpToken
   * @description Creates an organization user token with the Validate identity
   * @param {OrganizationUser} user - organization user instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.signUpToken = function (user, t) {
    const exp = moment().add(30, 'minutes')
    return this.create({
      identity: Identity.SignUp,
      organizationUserId: user.id,
      expiresAt: exp
    }, { transaction: t })
  }
  /**
   * @method recoveryToken
   * @description Creates an organization user token with the Recovery identity
   * @param {OrganizationUser} user - organization user instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<OrganizationUserToken>} returns a organization user token
   *  promise
   *
   * @static
   */
  OrganizationUserToken.recoveryToken = function (user, t) {
    const exp = moment().add(2, 'hours')
    return this.create({
      identity: this.Identity.Recovery,
      organizationUserId: user.id,
      expiresAt: exp
    }, { transaction: t })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method logout
   * @description Sets the expiresAt field with the current date
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<OrganizationUserToken>} returns an organization user
   *  token promise
   *
   * @instance
   */
  OrganizationUserToken.prototype.logout = function (t) {
    let opts
    if (t) opts = { transaction: t }
    this.expiresAt = new Date()
    return this.save(opts)
  }
  /**
   * @method usableSecret
   * @description Decrypts the secret attribute stored in the database.
   * @returns {string} returns the decrypted secret
   *
   * @instance
   */
  OrganizationUserToken.prototype.usableSecret = function () {
    return pair.decrypt(this.secret, encryptionKey)
  }
  /**
   * @method jwt
   * @description Signs the jwt with the secret and adds the rel attr if any.
   * @param {integer} rel - relationship atribute
   * @returns {string} returns a signed jwt string
   *
   * @instance
   */
  OrganizationUserToken.prototype.jwt = function (rel) {
    const exp = moment().add(1, 'years')
    let data = {
      iss: this.token,
      exp: exp.unix()
    }
    if (rel) data.rel = rel
    return jwt.sign(data, this.usableSecret())
  }
  /**
   * @method verify
   * @description Verifies the provided jwt with the this instance's secret
   * @param  {string} token unverified jwt
   * @return {Promise<string>} returns a verfied jwt promise
   *
   * @instance
   */
  OrganizationUserToken.prototype.verify = function (token) {
    return new Promise((resolve, reject) => {
      try {
        resolve(jwt.verify(token, this.usableSecret()))
      } catch (err) {
        reject(new Error('invalid_token'))
      }
    })
  }
  return OrganizationUserToken
}
