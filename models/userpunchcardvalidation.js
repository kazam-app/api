/**
 * Kazam Api
 * @name Sequelize UserPunchCardValidation Definition
 * @file models/userpunchcardvalidation.js
 * @author Joel Cano
 */

'use strict'

const moment = require('moment')
const Promise = require('bluebird')
const _ = require('lodash/collection')
const decorator = require('../lib/decorate')
const page = require('../lib/sequelize/page')
const uuidField = require('../lib/sequelize/uuid')
const rawDecorator = require('../lib/rawdecorate')
const identityField = require('../lib/sequelize/identity')

const Identity = { Punch: 0, Redeem: 1 }
const Identities = [Identity.Punch, Identity.Redeem]

module.exports = function (sequelize, DataTypes) {
  let UserPunchCardValidation = sequelize.define(
    // Model name
    'UserPunchCardValidation',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      validatorId: {
        allowNull: false,
        field: 'validator_id',
        type: DataTypes.BIGINT
      },
      userPunchCardId: {
        allowNull: false,
        type: DataTypes.BIGINT,
        field: 'user_punch_card_id'
      },
      identifier: uuidField(DataTypes),
      identity: identityField(DataTypes, Identity.Punch, Identities)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'user_punch_card_validations'
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  UserPunchCardValidation.associate = function (models) {
    UserPunchCardValidation.belongsTo(models.Validator, {
      foreignKey: 'validatorId'
    })
    UserPunchCardValidation.belongsTo(models.UserPunchCard, {
      foreignKey: 'userPunchCardId'
    })
  }
  /**
   * Identity Enum
   * @type {Object}
   */
  UserPunchCardValidation.Identity = Identity
  UserPunchCardValidation.decorate = decorator
  /**
   * @method toHistory
   * @description UserPunchCardValidation history apprpriate data
   * @returns {object} - UserPunchCardValidation information
   *
   * @static
   */
  UserPunchCardValidation.toHistory = function (item) {
    let result = {
      id: item.identifier,
      identity: item.identity,
      created_at: item.created_at,
      shop: item['Validator.Shop.name'] || '-',
      user: item['UserPunchCard.User.identifier']
    }
    const name = item['UserPunchCard.User.name']
    const lastNames = item['UserPunchCard.User.lastNames']
    const email = item['UserPunchCard.User.email']
    if (name) result.name = name
    if (lastNames) result.last_names = lastNames
    if (email) result.email = email
    return result
  }
  /**
   * @method toAdminUserHistory
   * @description UserPunchCardValidation history apprpriate data
   * @returns {object} - UserPunchCardValidation information
   *
   * @static
   */
  UserPunchCardValidation.toAdminUserHistory = function (item) {
    let result = {
      id: item.identifier,
      identity: item.identity,
      created_at: item.created_at,
      shop: item['Validator.Shop.name'] || '-',
      merchant: item['Validator.Shop.Merchant.name'] || '-'
    }
    return result
  }
  /**
   * @method rawDecorate
   * @description Decorates items from a sequelize raw query
   * @returns {function} - Returns the processing function
   *
   * @static
   */
  UserPunchCardValidation.rawDecorator = rawDecorator
  /**
   * @method total
   * @description Returns total records
   * @returns {Integer} - UserPunchCardValidation total count
   *
   * @static
   */
  UserPunchCardValidation.total = page.total(sequelize)
  /**
   * @method merchantTotal
   * @description Returns total validations for merchant
   * @param {Merchant} merchant  Merchant instance
   * @returns {Promise} - UserPunchCardValidation total for merchant promise
   *
   * @static
   */
  UserPunchCardValidation.merchantTotal = function (merchant) {
    return this.findAll({
      raw: true,
      attributes: [[sequelize.fn('COUNT', 'id'), 'total']],
      include: [{
        required: true,
        attributes: [],
        model: sequelize.models.Validator,
        where: { merchantId: merchant.id }
      }]
    })
  }
  /**
   * @method merchantTotalForUser
   * @description Returns total validations for merchant
   * @param {Merchant} merchant  Merchant instance
   * @param {User} user  User instance
   * @returns {Promise} - UserPunchCardValidation total for merchant promise
   *
   * @static
   */
  UserPunchCardValidation.merchantTotalForUser = function (merchant, user) {
    const models = sequelize.models
    return this.findAll({
      raw: true,
      attributes: [[sequelize.fn('COUNT', 'id'), 'total']],
      include: [
        {
          required: true,
          attributes: [],
          model: models.UserPunchCard,
          where: { userId: user.id }
        },
        {
          required: true,
          attributes: [],
          model: models.Validator,
          where: { merchantId: merchant.id }
        }
      ]
    })
  }
  /**
   * @method history
   * @description Returns all UserPunchCardValidation for a merchant
   * @returns {Promise} - UserPunchCardValidation array promise
   *
   * @static
   */
  UserPunchCardValidation.history = function (merchant, urbanity, params) {
    const models = sequelize.models
    let userAttrs = ['identifier']
    if (urbanity) userAttrs.push('name', 'lastNames', 'email')
    return this.findAll({
      attributes: [
        [
          sequelize.fn('DISTINCT', sequelize.col('UserPunchCardValidation.id')),
          'id'
        ],
        'identifier',
        'identity',
        'created_at'
      ],
      include: [
        {
          required: true,
          attributes: ['id'],
          model: models.Validator,
          where: { merchantId: merchant.id },
          include: [{ required: false, model: models.Shop.scope('name') }]
        },
        {
          required: true,
          attributes: ['id'],
          model: models.UserPunchCard,
          include: [
            { required: true, model: models.User, attributes: userAttrs }
          ]
        }
      ],
      order: [['created_at', 'DESC']],
      raw: true
    })
  }
  /**
   * @method punch
   * @description Creates a validation for a punch action
   * @param {Validator} validator - Validator instance
   * @param {UserPunchCard} userPunchCard - UserPunchCard instance
   * @param {object} t - sequelize trasaction instance
   * @return {Promise<UserPunchCardValidation>} Returns a
   *  UserPunchCardValidation promise
   *
   * @static
   */
  UserPunchCardValidation.punch = function (validator, userPunchCard, t) {
    return this.create({
      validatorId: validator.id,
      userPunchCardId: userPunchCard.id,
      identity: Identity.Punch
    }, { transaction: t })
  }
  /**
   * @method canValidate
   * @description Creates a validation for a punch action
   * @param {Validator} validator - Validator instance
   * @param {UserPunchCard} userPunchCard - UserPunchCard instance
   * @param {object} t - sequelize trasaction instance
   * @return {Promise<UserPunchCardValidation>} Returns a
   *  UserPunchCardValidation promise
   *
   * @static
   */
  UserPunchCardValidation.canValidate = function (userPunchCard, t) {
    const Op = sequelize.Op
    const models = sequelize.models
    const card = userPunchCard.PunchCard
    const range = card.validationRange()
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCardValidation.id')),
          'total'
        ]
      ],
      where: { identity: Identity.Punch, created_at: { [Op.between]: range } },
      include: [{
        required: true,
        attributes: [],
        model: models.UserPunchCard,
        where: {
          userId: userPunchCard.userId,
          punchCardId: userPunchCard.punchCardId
        }
      }],
      transaction: t,
      raw: true
    }).then(function (result) {
      const total = result[0].total
      if (card.validationLimit !== null && card.validationLimit <= total) {
        return false
      } else return true
    })
  }
  /**
   * @method redeem
   * @description Creates a validation for a redeem action
   * @param {Validator} validator - Validator instance
   * @param {UserPunchCard} userPunchCard - UserPunchCard instance
   * @param {object} t - sequelize trasaction instance
   * @return {Promise<UserPunchCardValidation>} Returns a
   *  UserPunchCardValidation promise
   *
   * @static
   */
  UserPunchCardValidation.redeem = function (validator, userPunchCard, t) {
    return this.create({
      validatorId: validator.id,
      userPunchCardId: userPunchCard.id,
      identity: Identity.Redeem
    }, { transaction: t })
  }
  /**
   * @method redeemsForMerchant
   * @description Redeems filtered by merchant
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<array>} Returns an array with total values
   *
   * @static
   */
  UserPunchCardValidation.redeemsForMerchant = function (merchant) {
    const models = sequelize.models
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCardValidation.id')),
          'total'
        ]
      ], // Total UserPunchCard
      where: { identity: Identity.Redeem },
      include: [{
        required: true,
        attributes: [],
        model: models.UserPunchCard,
        include: [{
          required: true,
          attributes: [],
          model: models.PunchCard,
          where: { merchantId: merchant.id }
        }]
      }],
      raw: true
    })
  }
  /**
   * @method activeForMerchantPerMonth
   * @description Active UserPunchCards per month
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<array>}     Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.activeForMerchantPerMonth = function (merchant) {
    const groupField = 'month'
    const models = sequelize.models
    const timeLimit = moment().subtract(3, 'M')
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCard.id')),
          'total'
        ], // Total UserPunchCard
        [
          sequelize.fn(
            'TO_CHAR',
            sequelize.col('UserPunchCardValidation.created_at'),
            'MM'
          ),
          groupField
        ] // GroupField created_at
      ],
      where: {
        identity: Identity.Punch,
        created_at: { [sequelize.Op.gt]: timeLimit }
      },
      include: [{
        required: true,
        attributes: [],
        model: models.UserPunchCard,
        include: [{
          required: true,
          attributes: [],
          model: models.PunchCard,
          where: { merchantId: merchant.id }
        }]
      }],
      raw: true,
      group: [groupField]
    })
  }
  /**
   * @method topMerchantsForUser
   * @description Top Merchants for a User
   * @param {User} user - User instance
   * @return {Promise<array>} Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.topMerchantsForUser = function (user) {
    const totalField = 'total'
    const models = sequelize.models
    const merchantIdField = sequelize.col('Validator->Merchant.id')
    const merchantNameField = sequelize.col('Validator->Merchant.name')
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCardValidation.id')),
          totalField
        ], // Total UserPunchCardValidation
        merchantIdField,
        merchantNameField
      ],
      include: [
        {
          required: true,
          attributes: [],
          model: models.Validator,
          include: [{ required: true, attributes: [], model: models.Merchant }]
        },
        {
          required: true,
          attributes: [],
          model: models.UserPunchCard,
          where: { userId: user.id }
        }
      ],
      group: [merchantIdField, merchantNameField],
      order: [[sequelize.col(totalField), 'DESC']],
      limit: 10,
      raw: true
    })
  }
  /**
   * @method topShopsForUser
   * @description Top Shops for a User
   * @param {User} user - User instance
   * @return {Promise<array>} Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.topShopsForUser = function (user) {
    const totalField = 'total'
    const models = sequelize.models
    const shopIdField = sequelize.col('Validator->Shop.id')
    const shopNameField = sequelize.col('Validator->Shop.name')
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCardValidation.id')),
          totalField
        ], // Total UserPunchCardValidation
        shopIdField,
        shopNameField
      ],
      include: [
        {
          required: true,
          attributes: [],
          model: models.Validator,
          include: [{ required: true, attributes: [], model: models.Shop }]
        },
        {
          required: true,
          attributes: [],
          model: models.UserPunchCard,
          where: { userId: user.id }
        }
      ],
      group: [shopIdField, shopNameField],
      order: [[sequelize.col(totalField), 'DESC']],
      limit: 10,
      raw: true
    })
  }
  /**
   * @method topShopsForUserByMerchant
   * @description Top Shops for a User filtered by Merchant
   * @param {User} user - User instance
   * @return {Promise<array>} Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.topShopsForUserByMerchant = function (user, merchant) {
    const totalField = 'total'
    const models = sequelize.models
    const shopIdField = sequelize.col('Validator->Shop.id')
    const shopNameField = sequelize.col('Validator->Shop.name')
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCardValidation.id')),
          totalField
        ], // Total UserPunchCardValidation
        shopIdField,
        shopNameField
      ],
      include: [
        {
          required: true,
          attributes: [],
          model: models.Validator,
          where: { merchantId: merchant.id },
          include: [{ required: true, attributes: [], model: models.Shop }]
        },
        {
          required: true,
          attributes: [],
          model: models.UserPunchCard,
          where: { userId: user.id }
        }
      ],
      group: [shopIdField, shopNameField],
      order: [[sequelize.col(totalField), 'DESC']],
      limit: 10,
      raw: true
    })
  }
  /**
   * @method historyForUser
   * @description Complete validation history for a User
   * @param {User} user - User instance
   * @return {Promise<array>} Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.historyForUser = function (user) {
    const models = sequelize.models
    const orderField = 'created_at'
    return this.findAll({
      attributes: [
        [
          sequelize.fn('DISTINCT', sequelize.col('UserPunchCardValidation.id')),
          'id'
        ],
        'identifier',
        'identity',
        orderField
      ],
      include: [
        {
          required: true,
          attributes: [],
          model: models.Validator,
          include: [{
            required: true,
            model: models.Shop.scope('name'),
            include: [
              { required: true, attributes: ['name'], model: models.Merchant }
            ]
          }]
        },
        {
          required: true,
          attributes: [],
          model: models.UserPunchCard,
          where: { userId: user.id }
        }
      ],
      order: [[orderField, 'DESC']],
      raw: true
    })
  }
  /**
   * @method historyForUserByMerchant
   * @description Complete validation history fitltered by a Merchant
   * @param {User} user - User instance
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<array>} Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.historyForUserByMerchant = function (user, merchant) {
    const models = sequelize.models
    const orderField = 'created_at'
    return this.findAll({
      attributes: [
        [
          sequelize.fn('DISTINCT', sequelize.col('UserPunchCardValidation.id')),
          'id'
        ],
        'identifier',
        'identity',
        orderField
      ],
      include: [
        {
          required: true,
          attributes: [],
          model: models.Validator,
          where: { merchantId: merchant.id },
          include: [{ required: true, model: models.Shop.scope('name') }]
        },
        {
          required: true,
          attributes: [],
          model: models.UserPunchCard,
          where: { userId: user.id }
        }
      ],
      order: [[orderField, 'DESC']],
      raw: true
    })
  }
  /**
   * @method averageBreakdownForUser
   * @description Returns weekly and monthly averages for each validation
   *  identity
   * @param {User} user - User instance
   * @returns {Promise<array>} - Returns an array of averages
   *
   * @static
   */
  UserPunchCardValidation.averageBreakdownForUser = function (user) {
    /**
     * NOTE: Averages for timescales in postgres
     * https://stackoverflow.com/questions/38226788/postgresql-getting-daily-weekly-and-monthly-averages-of-the-occurrences-of-an/38227758
     */
    const validationData = `WITH validation_data AS (
      SELECT user_punch_card_validations.id, user_punch_card_validations.identity, user_punch_card_validations.created_at
      FROM user_punch_card_validations
      INNER JOIN user_punch_cards ON user_punch_card_validations.user_punch_card_id = user_punch_cards.id
      WHERE user_punch_cards.user_id = :userId
    )`
    const select = `COUNT(id) AS total_count, identity`
    // week
    const weekly = `SELECT ${select} FROM validation_data
      GROUP BY date_trunc('week', created_at), identity`
    const weeklyAvg = `SELECT identity, ROUND(AVG(total_count),2) AS weekly_avg
      FROM (${weekly}) AS week_data
      GROUP BY identity`
    // months
    const month = `SELECT ${select} FROM validation_data
      GROUP BY date_trunc('month', created_at), identity`
    const monthAvg = `SELECT identity, ROUND(AVG(total_count),2) AS monthly_avg
      FROM (${month}) AS monthly_data
      GROUP BY identity`
    // averages query
    const query = `${validationData}
      SELECT identity, weekly_avg, monthly_avg
      FROM (
        ${weeklyAvg}
      ) AS week_data
      JOIN (
        ${monthAvg}
      ) AS month_data USING(identity)`
    return sequelize.query(
      query,
      { type: sequelize.QueryTypes.SELECT, replacements: { userId: user.id } }
    )
  }
  /**
   * @method dateAverageForUserByMerchant
   * @description Returns weekly and monthly validation averages filtered by
   *  Merchant
   * @returns {string} - query string
   *
   * @static
   */
  UserPunchCardValidation.dateAverageForUserByMerchant = function () {
    const query = `
      SELECT ROUND(AVG(total_count), 2) AS average FROM (
        SELECT COUNT(user_punch_card_validations.id) AS total_count
        FROM user_punch_card_validations
        INNER JOIN user_punch_cards ON user_punch_card_validations.user_punch_card_id = user_punch_cards.id
        INNER JOIN validators ON user_punch_card_validations.validator_id = validators.id
        WHERE user_punch_cards.user_id = :userId AND validators.merchant_id = :merchantId
        GROUP BY date_trunc(:interval, user_punch_card_validations.created_at)
      ) AS interval_data`
    return query
  }
  /**
   * @method weeklyAverageForUserByMerchant
   * @description Returns weekly validation average filtered by Merchant
   * @param {User} user - User instance
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<array>} - Returns an array of averages
   *
   * @static
   */
  UserPunchCardValidation.weeklyAverageForUserByMerchant = function (user, merchant) {
    const query = this.dateAverageForUserByMerchant()
    return sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        userId: user.id,
        merchantId: merchant.id,
        interval: 'week'
      }
    })
  }
  /**
   * @method monthlyAverageForUserByMerchant
   * @description Returns monthly validation average filtered by Merchant
   * @param {User} user - User instance
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<array>} - Returns an array of averages
   *
   * @static
   */
  UserPunchCardValidation.monthlyAverageForUserByMerchant = function (user, merchant) {
    const query = this.dateAverageForUserByMerchant()
    return sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        userId: user.id,
        merchantId: merchant.id,
        interval: 'month'
      }
    })
  }
  /**
   * @method toAverageBreakdown
   * @description Group punch and redeem breakdown data by identity key
   * @param {array} results - Punch and Redeem breakdown data
   * @returns {Promise<object>} - Punch and Redeem data grouped by identity
   *
   * @static
   */
  UserPunchCardValidation.toAverageBreakdown = function (results) {
    return _.groupBy(results, 'identity')
  }
  /**
   * @method breakdown
   * @description UserPunchCardValidation breakdown apprpriate data
   * @returns {Promise<array>} - UserPunchCardValidation shop breakdown
   *
   * @static
   */
  UserPunchCardValidation.breakdown = function (merchant, startAt, endsAt) {
    /**
     * FIXME: should be solved with less iterations and/or queries
     */
    if (merchant) {
      return sequelize.models.Shop.toBreakdown(merchant).then((shops) => {
        return this.monthValidationBreakdownForMerchant(
          _.map(shops, 'id'),
          startAt,
          endsAt
        ).spread(function (results, length) {
          return [shops, results, length]
        })
      }).spread(function (shops, breakdown, length) {
        breakdown = _.map(breakdown, function (element) {
          return _.groupBy(element, 'id')
        })
        return [shops, breakdown, length]
      }).spread(function (shops, breakdowns, length) {
        shops = _.map(shops, function (shop) {
          let punchTotal = 0
          let redeemTotal = 0
          shop.breakdown = _.map(breakdowns, function (element) {
            let breakdown = element[shop.id]
            if (breakdown) {
              _.each(breakdown, function (identityData) {
                if (identityData.identity === 0) {
                  punchTotal += parseInt(identityData.total)
                } else redeemTotal += parseInt(identityData.total)
              })
            }
            return breakdown
          })
          shop.punch_total = punchTotal
          shop.redeem_total = redeemTotal
          return shop
        })
        return { shops: shops, intervals: length }
      })
    } else return Promise.reject(new Error('missing_merchant'))
  }
  /**
   * @method rangeValidationBreakdownForMerchant
   * @description Active UserPunchCards per month
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<array>}     Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.monthValidationBreakdownForMerchant = function (shops, startsAt, endsAt, range) {
    if (range === undefined) range = 'days'
    if (startsAt && endsAt) {
      let queries = []
      this.logger.debug(
        { action: 'monthValidationBreakdownForMerchant' },
        { startsAt: startsAt, endsAt: endsAt, range: range }
      )
      startsAt = moment(startsAt).startOf('month')
      endsAt = moment(endsAt).endOf('day')
      this.logger.info(
        { action: 'monthValidationBreakdownForMerchant' },
        { startsAt: startsAt, endsAt: endsAt, range: range }
      )
      let pointer = startsAt.clone()
      // 8 day intervals
      while (!pointer.isSameOrAfter(endsAt)) {
        let current = pointer.clone()
        pointer.add(8, range).endOf('day')
        if (pointer.isAfter(endsAt)) pointer = endsAt
        this.logger.debug(
          { action: 'monthValidationBreakdownForMerchant' },
          { startsAt: current, endsAt: pointer }
        )
        queries.push(
          this.intervalValidationBreakdownForMerchant(
            shops,
            current.clone(),
            pointer.clone()
          )
        )
      }
      const length = queries.length
      return Promise.all(queries).then((results) => {
        this.logger.debug(
          { action: 'monthValidationBreakdownForMerchant' },
          { intervals: length }
        )
        return [results, length]
      })
    } else return Promise.reject(new Error('missing_date_range'))
  }
  /**
   * @method intervalValidationBreakdownForMerchant
   * @description Total UserPunchCardValidations for a given time interval and
   *  Merchant
   * @param {Merchant} merchant   Merchant instance
   * @param {Date} startsAt       start date
   * @param {Date} endsAt         end date
   * @return {Promise<array>}     Returns an array with total values per month
   *
   * @static
   */
  UserPunchCardValidation.intervalValidationBreakdownForMerchant = function (shops, startsAt, endsAt) {
    const models = sequelize.models
    const identityField = sequelize.col('UserPunchCardValidation.identity')
    const shopIdField = sequelize.col('Validator->Shop.id')
    const shopNameField = sequelize.col('Validator->Shop.name')
    const range = [
      sequelize.fn('DATE', startsAt.toDate()),
      sequelize.fn('DATE', endsAt.toDate())
    ]
    return this.findAll({
      attributes: [
        [
          sequelize.fn('COUNT', sequelize.col('UserPunchCardValidation.id')),
          'total'
        ], // Total UserPunchCard
        identityField,
        shopIdField,
        shopNameField
      ],
      where: { created_at: { [sequelize.Op.between]: range } },
      include: [{
        required: false,
        attributes: [],
        model: models.Validator,
        where: { shopId: shops },
        include: [{ required: false, attributes: [], model: models.Shop }]
      }],
      group: [identityField, shopIdField, shopNameField],
      order: [[identityField, 'ASC']],
      raw: true
    })
  }
  /**
   * @method apiHistoryForUser
   * @description Finds all UserPunchCard validations in cronological order for
   *  a User
   * @param {User} user           User instance
   * @return {Promise<array>}     Returns an array of UserPunchCardValidations
   *  in chronological order
   *
   * @static
   */
  UserPunchCardValidation.apiHistoryForUser = function (user) {
    const models = sequelize.models
    const orderField = 'created_at'
    const limit = 25
    return this.findAll({
      attributes: [
        [
          sequelize.fn('DISTINCT', sequelize.col('UserPunchCardValidation.id')),
          'id'
        ],
        'identifier',
        'identity',
        orderField
      ],
      include: [
        {
          required: true,
          attributes: [],
          model: models.Validator,
          include: [
            { required: true, model: models.Shop.scope('name') },
            { required: true, model: models.Merchant.scope('apiEvent') }
          ]
        },
        {
          required: true,
          attributes: ['identity', 'identifier'],
          where: { userId: user.id },
          model: models.UserPunchCard,
          include: [
            {
              required: true,
              attributes: ['expiresAt'],
              model: models.PunchCard
            }
          ]
        }
      ],
      order: [[orderField, 'DESC']],
      limit: limit,
      raw: true
    })
  }
  return UserPunchCardValidation
}
