/**
 * Kazam Api
 * @name Sequelize Organization User Definition
 * @file /models/organizationuser.js
 * @author Joel Cano
 */

'use strict'

const salts = 11
const bcrypt = require('bcrypt')
const Promise = require('bluebird')
const emailField = require('../lib/sequelize/email')
const stringField = require('../lib/sequelize/string')

module.exports = function (sequelize, DataTypes) {
  let OrganizationUser = sequelize.define(
    // Model name
    'OrganizationUser',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      organizationId: {
        allowNull: false,
        type: DataTypes.BIGINT,
        field: 'organization_id'
      },
      isValid: {
        allowNull: false,
        field: 'is_valid',
        defaultValue: false,
        type: DataTypes.BOOLEAN
      },
      name: stringField(DataTypes, false),
      lastNames: stringField(DataTypes, false, 'last_names'),
      email: emailField(DataTypes, false),
      password: stringField(DataTypes, false),
      mailchimpId: stringField(DataTypes, true, 'mailchimp_id')
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      paranoid: true,
      underscored: true,
      tableName: 'organization_users',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method active
         * @description Returns query for active users
         * @returns {object} - query structure
         *
         * @static
         */
        active: { where: { isValid: true } }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  OrganizationUser.associate = function (models) {
    OrganizationUser.belongsTo(models.Organization, {
      foreignKey: 'organizationId'
    })
    OrganizationUser.hasMany(models.OrganizationUserToken, {
      foreignKey: 'organizationUserId'
    })
    OrganizationUser.hasMany(models.OrganizationUserMerchant, {
      foreignKey: 'organizationUserId'
    })
    OrganizationUser.hasMany(models.MailingListOrganizationUser, {
      foreignKey: 'organizationUserId'
    })
  }
  /**
   * @name hash
   * @description Returns a bcrypt encrypted string of the plain value.
   * @param {string} value - plain text string
   * @returns {string} encrypted string
   *
   * @static
   */
  OrganizationUser.hash = function (value) {
    return bcrypt.hashSync(value, salts)
  }
  /**
   * @method adminLogin
   * @description Compares the password and retruns a jwt if valid
   * @param {string} email - plain text email string
   * @param {string} password - plain text password string
   * @returns {Promise<Object>} returns a user promise with a jwt
   *
   * @static
   */
  OrganizationUser.adminLogin = function (email, password) {
    return sequelize.transaction((t) => {
      if (email && password) {
        return this.scope('active')
          .findOne({
            where: { organizationId: null, email: email },
            transaction: t
          })
          .then(function (result) {
            if (result) {
              return result.checkAuth(password, t).then(function (token) {
                return [token, result]
              })
            } else throw new Error('login_fail')
          }).spread(function (token, user) {
            let data = user.toShow()
            data.token = token.jwt()
            return data
          })
      } else throw new Error('login_fail')
    })
  }
  /**
   * @method login
   * @description Compares the password and retruns a jwt if valid
   * @param {string} email - plain text email string
   * @param {string} password - plain text password string
   * @returns {Promise<object>} returns a user promise with a jwt
   *
   * @static
   */
  OrganizationUser.login = function (email, password) {
    const Op = sequelize.Op
    return sequelize.transaction((t) => {
      if (email && password) {
        return this.scope('active')
          .findOne({
            where: { organizationId: { [Op.ne]: null }, email: email },
            transaction: t
          })
          .then(function (result) {
            if (result) {
              return result.checkAuth(password, t).then(function (token) {
                return [token, result]
              })
            } else throw new Error('login_fail')
          }).spread(function (token, user) {
            let data = user.toShow()
            data.token = token.jwt()
            return data
          })
      } else throw new Error('login_fail')
    })
  }
  /**
   * @method signUp
   * @description Creates a new Organization User via request params
   * @param {object} params - request params
   * @returns {Promise<OrganizationUser, ValidationJWT, SignUpJWT>}
   *  returns a user promise with validation and signup JWTs
   *
   * @static
   */
  OrganizationUser.signUp = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      const token = params.token
      const models = sequelize.models
      if (data) {
        let userPromise
        if (token) {
          userPromise = models.OrganizationUserInvite.findValidInvite(token, t)
            .spread((verified, invite) => {
              return this.generateUser(data,
                invite.email,
                invite.OrganizationUser.organizationId,
                t
              )
            })
        } else {
          userPromise = this.organizationPromise(params, t)
            .then((organization) => {
              return this.generateUser(data, data.email, organization.id, t)
            })
        }
        return userPromise.then(function (user) {
          return Promise.join(
            models.OrganizationUserToken.validationToken(user, t),
            models.OrganizationUserToken.signUpToken(user, t),
            function (validation, signup) {
              return [validation, signup, user]
            }
          )
        }).spread(function (validation, signup, user) {
          return [user.toShow(), validation.jwt(), signup.jwt()]
        })
      } else throw new Error('missing_user_data')
    })
  }
  /**
   * @method generateUser
   * @description Returns a new organization user via params
   * @param {object} data - Request parameters
   * @param {string} email - email string
   * @param {integer} organizationId - organization identifier
   * @param {object} t - sequelize transaction
   * @returns {Promise<OrganizationUser>} returns an OrganizationUser promise
   *
   * @static
   */
  OrganizationUser.generateUser = function (data, email, organizationId, t) {
    let hash = null
    if ((data.password && data.password_confirmation)) {
      if (data.password === data.password_confirmation) {
        hash = this.hash(data.password)
      } else throw new Error('password_does_not_match')
    } else throw new Error('missing_password_confirmation')
    const insert = {
      organizationId: organizationId,
      name: data.name,
      lastNames: data.last_names,
      email: email,
      password: hash
    }
    return this.create(insert, { transaction: t })
  }
  /**
   * @method organizationPromise
   * @description Returns a new organization via params
   * @param {object} params - Request parameters
   * @param {object} t - sequelize transaction
   * @returns {Promise<Organization>} returns an organization
   *
   * @static
   */
  OrganizationUser.organizationPromise = function (params, t) {
    const org = params.organization
    if (org && org.name) {
      return sequelize.models.Organization.create({
        name: org.name
      }, { transaction: t })
    } else throw new Error('missing_organization_data')
  }
  /**
   * @method resend
   * @description Resend a validation token
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser, ValidationJWT>} returns a user promise
   *  with a validation JWT
   *
   * @static
   */
  OrganizationUser.resend = function (params) {
    return sequelize.transaction(function (t) {
      const models = sequelize.models
      const OrganizationUserToken = models.OrganizationUserToken
      const token = params.token
      if (token) {
        return OrganizationUserToken.findSignUpToken(token, t)
          .spread(function (verified, token) {
            if (verified) {
              const user = token.OrganizationUser
              return OrganizationUserToken.findValidationTokenByUser(user.id, t)
                .then(function (result) {
                  if (result) return [user.toShow(), result.jwt()]
                  else throw new Error('invalid_token')
                })
            } else throw new Error('invalid_token')
          })
      } else throw new Error('invalid_token')
    })
  }
  /**
   * @method refresh
   * @description Validates an incoming user token and set their password
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser>} returns a user promise with a
   *  validation jwt
   *
   * @static
   */
  OrganizationUser.refresh = function (params) {
    return sequelize.transaction(function (t) {
      const models = sequelize.models
      const OrganizationUserToken = models.OrganizationUserToken
      const token = params.token
      if (token) {
        return OrganizationUserToken.findExpiredValidationToken(token, t)
          .spread(function (verified, token) {
            const user = token.OrganizationUser
            return models.OrganizationUserToken.validationToken(user, t)
              .then(function (result) {
                return [result, user]
              })
          })
      } else throw new Error('invalid_token')
    })
  }
  /**
   * @method validate
   * @description Validates an incoming user token and set their password
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser>} returns a user promise with a session
   *  jwt
   *
   * @static
   */
  OrganizationUser.validate = function (params) {
    return sequelize.transaction(function (t) {
      const OrganizationUserToken = sequelize.models.OrganizationUserToken
      const token = params.token
      if (token) {
        return OrganizationUserToken.findValidationToken(token, t)
          .spread(function (verified, token) {
            token.OrganizationUser.isValid = true
            return token.OrganizationUser.save({ transaction: t })
              .then(function (user) {
                return [user, token]
              })
          })
          .spread(function (user, token) {
            return token.destroy({ transaction: t }).then(function () {
              return user
            })
          })
          .then(function (user) {
            return OrganizationUserToken.sessionToken(user, t)
              .then(function (token) {
                return [user, token]
              })
          })
          .spread(function (user, token) {
            let data = user.toShow()
            data.token = token.jwt()
            return data
          })
      } else throw new Error('invalid_token')
    })
  }
  /**
   * @method request
   * @description Initiates the new password process for an email if it exists
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser>} returns a user promise with a recovery
   *  jwt
   *
   * @static
   */
  OrganizationUser.request = function (email) {
    return sequelize.transaction((t) => {
      if (email) {
        const OrganizationUserToken = sequelize.models.OrganizationUserToken
        return this.findOne({
          attributes: ['id', 'email'],
          where: { email: email, isValid: true },
          transaction: t
        }).then(function (user) {
          if (user) {
            return OrganizationUserToken.findRecoveryTokenByUser(user.id, t)
              .then(function (token) {
                return [user, token]
              })
          } else throw new Error('not_found')
        }).spread(function (user, token) {
          if (token) throw new Error('already_sent')
          else {
            return OrganizationUserToken.recoveryToken(user, t)
              .then(function (result) {
                return [user, result.jwt()]
              })
          }
        })
      } else throw new Error('missing_email')
    })
  }
  /**
   * @method recover
   * @description Sets the new password with a valid token
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser>} returns a user promise with a recovery
   *  jwt
   *
   * @static
   */
  OrganizationUser.recover = function (params) {
    return sequelize.transaction((t) => {
      const token = params.token
      const password = params.password
      const passwordConfirmation = params.password_confirmation
      if (token) {
        return sequelize.models.OrganizationUserToken.findRecoveryToken(token, t)
          .spread((verified, token) => {
            if ((password && passwordConfirmation) && (password === passwordConfirmation)) {
              token.OrganizationUser.password = this.hash(password)
              return token.OrganizationUser.save({ transaction: t })
                .then(function (user) {
                  return [user, token]
                })
            } else throw new Error('password_error')
          })
          .spread(function (user, token) {
            return token.destroy({ transaction: t }).then(function () {
              return user
            })
          })
          .then(function (user) {
            return sequelize.models.OrganizationUserToken
              .refreshSession(user, t)
              .then(function (token) {
                return [user, token]
              })
          })
          .spread(function (user, token) {
            let data = user.toShow()
            data.token = token.jwt()
            return data
          })
      } else throw new Error('invalid_token')
    })
  }
  /**
   * @method delete
   * @description Sets the new password with a valid token
   * @param {object} params - Request parameters
   * @returns {Promise<OrganizationUser>} returns a user promise with a recovery
   *  jwt
   *
   * @static
   */
  OrganizationUser.delete = function (id, user) {
    const Op = sequelize.Op
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      return this.destroy({
        where: {
          id: { [Op.and]: { [Op.eq]: id, [Op.not]: user.id } },
          organizationId: user.organizationId
        },
        transaction: t
      }).then(function (result) {
        if (result > 0) return true
        else throw new Error('not_found')
      })
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toShow
   * @description Returns list apprpriate data for a user
   * @returns {object} - user information
   *
   * @instance
   */
  OrganizationUser.prototype.toShow = function () {
    return {
      id: this.id,
      name: this.name,
      last_names: this.lastNames,
      email: this.email
    }
  }
  /**
   * @method toProfile
   * @description Returns list apprpriate data for a user
   * @returns {object} - user information
   *
   * @instance
   */
  OrganizationUser.prototype.toProfile = function (org) {
    return sequelize.models.OrganizationUserMerchant.findForUser(this)
      .then(sequelize.models.OrganizationUserMerchant.toList)
      .then((merchants) => {
        return {
          id: this.id,
          name: this.name,
          last_names: this.lastNames,
          email: this.email,
          organization: org.name,
          merchants: merchants
        }
      })
  }
  /**
   * @method checkAuth
   * @description Compares if the password is the one saved in the db.
   * @param {string} plain - password in plain text
   * @returns {Promise<OrganizationUserToken>} - return an OrganizationUserToken
   *  promise if correct
   *
   * @instance
   */
  OrganizationUser.prototype.checkAuth = function (plain, t) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(plain, this.password, (err, res) => {
        if (err || res === false) reject(new Error('login_fail'))
        else {
          sequelize.models.OrganizationUserToken.refreshSession(this, t)
            .then(resolve).catch(reject)
        }
      })
    })
  }
  /**
   * @name getPermissions
   * @description Returns a signed jwt with a valid rel attribute
   * @param {integer} rel - relationship atribute
   * @returns {string} returns a signed jwt string
   *
   * @instance
   */
  OrganizationUser.prototype.getPermissions = function (rel) {
    return new Promise((resolve, reject) => {
      if (rel) {
        const models = sequelize.models
        const OrganizationUserMerchant = models.OrganizationUserMerchant
        OrganizationUserMerchant.findForAuth(this.organizationId, this.id, rel)
          .then((userMerchant) => {
            if (userMerchant) {
              return models.OrganizationUserToken.refreshSession(this)
                .then(function (token) {
                  return [userMerchant, token]
                })
            } else reject(new Error('login_fail'))
          })
          .spread((userMerchant, token) => {
            let data = this.toShow()
            data.token = token.jwt(userMerchant.merchantId)
            resolve(data)
          })
          .catch(reject)
      } else reject(new Error('login_fail'))
    })
  }
  /**
   * @method invite
   * @description Returns a signed jwt with a valid rel attribute
   * @param {string} email - relationship atribute
   * @returns {boolean} returns a signed jwt string
   *
   * @instance
   */
  OrganizationUser.prototype.invite = function (email) {
    return sequelize.transaction((t) => {
      return sequelize.models.OrganizationUserInvite.findOrCreate({
        where: { email: email },
        include: [{
          model: sequelize.models.OrganizationUser,
          where: { organizationId: this.organizationId }
        }],
        defaults: { organizationUserId: this.id },
        transaction: t
      }).catch(sequelize.ValidationError, function () {
        throw new Error('email_error')
      })
    })
  }
  /**
   * @method logout
   * @description Removes a user's session token
   * @returns {object} returns an sequelize transaction object
   *
   * @instance
   */
  OrganizationUser.prototype.logout = function () {
    return sequelize.transaction((t) => {
      return sequelize.models.OrganizationUserToken.findSessionToken(this.id, t)
        .then(function (token) {
          if (token) return token.destroy({ transaction: t })
          else return true
        })
    })
  }
  /**
   * @method updateFromData
   * @description Updates a User via request params
   * @param {object} params - request parameters
   * @return {Promise<OrganizationUser>} Returns an updated OrganizationUser
   *  instance
   *
   * @instance
   */
  OrganizationUser.prototype.updateFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      if (data) {
        if (data.name) this.name = data.name
        if (data.last_names) this.lastNames = data.last_names
        if (data.email) this.email = data.email
        return this.save({ transaction: t })
      } else throw new Error('missing_user_data')
    })
  }
  /**
   * @method updatePassword
   * @description Updates a User's password via request params
   * @param {object} params - request parameters
   * @return {Promise<OrganizationUser>} Returns an updated OrganizationUser
   *  instance
   *
   * @instance
   */
  OrganizationUser.prototype.updatePassword = function (params) {
    return sequelize.transaction((t) => {
      const data = params.user
      if (data) {
        if ((data.password && data.password_confirmation)) {
          if (data.password === data.password_confirmation) {
            this.password = OrganizationUser.hash(data.password)
          } else throw new Error('password_does_not_match')
        }
        return this.save({ transaction: t })
      } else throw new Error('missing_user_data')
    })
  }
  /**
   * @method toMailchimp
   * @description Returns mailchimp data
   * @returns {object} - User data
   *
   * @instance
   */
  OrganizationUser.prototype.toMailchimp = function () {
    return {
      email_address: this.email,
      status: 'subscribed',
      merge_fields: {
        FNAME: this.name,
        LNAME: this.lastNames
      }
    }
  }
  /**
   * @method mailchimpSubscribe
   * @description Subscribe OrganizationUser to mailing list
   * @returns {Promise} returns subscribe results promise
   *
   * @static
   */
  OrganizationUser.prototype.mailchimpSignUp = function () {
    return sequelize.models.MailingList.scope('SignUpOrganizationUsers')
      .then((lists) => {
        let subscribes = []
        for (let i = 0; i < lists.length; ++i) {
          subscribes.push(lists[i].subscribeUser(this))
        }
        return Promise.all(subscribes)
      }).then(function (results) {
        if (results.length > 0) {
          const firstResult = results[0]
          this.mailchimp_id = firstResult[0].unique_email_id
        }
      })
  }
  return OrganizationUser
}
