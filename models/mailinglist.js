/**
 * Kazam Api
 * @name Sequelize Mailing List Definition
 * @file models/mailinglist.js
 * @author Joel Cano
 */

'use strict'

const mailchimp = require('../lib/mailchimp')
const stringField = require('../lib/sequelize/string')
const identityField = require('../lib/sequelize/identity')

const Identity = { Users: 0, OrganizationUsers: 2 }
const Identities = [Identity.Users, Identity.OrganizationUsers]

module.exports = function (sequelize, DataTypes) {
  let MailingList = sequelize.define(
    // Model name
    'MailingList',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      name: stringField(DataTypes, false),
      identity: identityField(DataTypes, Identity.Users, Identities),
      mailchimpId: stringField(DataTypes, true, 'mailchimp_id')
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'mailing_lists',
      /**
       * scopes
       * @description Model scopes
       * @type {object}
       *
       */
      scopes: {
        /**
         * @method users
         * @description Returns query for mailing lists
         * @returns {object} - query structure
         *
         * @static
         */
        users: { where: { identity: Identity.Users } },
        /**
         * @method organizationUsers
         * @description Returns query for mailing lists
         * @returns {object} - query structure
         *
         * @static
         */
        organizationUsers: { where: { identity: Identity.OrganizationUsers } }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  MailingList.associate = function (models) {
    MailingList.hasMany(models.MailingListUser, {
      foreignKey: 'mailingListId'
    })
    MailingList.hasMany(models.MailingListOrganizationUser, {
      foreignKey: 'mailingListId'
    })
  }
  /**
   * @method list
   * @description Returns all MailingLists as an array
   * @return {array} MailingList array
   *
   * @static
   */
  MailingList.adminList = function () {
    return this.findAll({ attributes: ['id', 'name'] })
  }
  /**
   * @method createFromData
   * @description Creates a new MailingList with request data
   * @param {object} params - request parameters
   * @return {Promise<MailingList>} Returns a MailingList instance
   *
   * @static
   */
  MailingList.createFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.mailing_list
      if (data) {
        const insert = {
          name: data.name,
          identity: data.identity,
          mailchimpId: data.mailchimp_id
        }
        return this.create(insert, { transaction: t })
      } else throw new Error('missing_mailinglist_data')
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method subscribe
   * @description Subscribes a user to the mailing list via the Mailchimp API
   * @param {object} user to subscribe via the api
   * @returns {Promise<object>} - Mailchimp API response promise
   *
   * @instance
   */
  MailingList.prototype.subscribe = function (user) {
    return mailchimp.subscribe(this.mailchimpId, user)
  }
  /**
   * @method subscribeUser
   * @description Subscribes a User to the mailing list via the Mailchimp API
   * @param {User} User to subscribe via the api
   * @returns {Promise<object, MailingListUser>} - subscription promise
   *
   * @instance
   */
  MailingList.prototype.subscribeUser = function (user, t) {
    return this.subscribe(user.toMailchimp()).then((subscription) => {
      return sequelize.models.MailingListUser.createActive(
        user.id,
        this.id,
        subscription.id,
        t
      ).then(function (result) {
        return [subscription, result]
      })
    })
  }
  /**
   * @method subscribeOrganizationUser
   * @description Subscribes an OrganizationUser to the mailing list via the
   *  Mailchimp API
   * @param {OrganizationUser} OrganizationUser to subscribe via the api
   * @returns {Promise<object, MailingListOrganizationUser>} - subscription
   *  promise
   *
   * @instance
   */
  MailingList.prototype.subscribeOrganizationUser = function (user) {
    return this.subscribe(user.toMailchimp())
      .then((subscription) => {
        return sequelize.models.MailingListOrganizationUser.create({
          organizationUserId: user.id,
          mailingListId: this.id,
          mailchimpId: subscription.id
        }).then(function (result) {
          return [subscription, result]
        })
      })
  }
  return MailingList
}
