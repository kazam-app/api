/**
 * Kazam Api
 * @name Sequelize Organization User Merchant Definition
 * @file models/organizationusermerchant.js
 * @author Joel Cano
 */

'use strict'

const decorator = require('../lib/decorate')
const identityField = require('../lib/sequelize/identity')

const Role = { Read: 0, ReadWrite: 1 }
const Roles = [Role.Read, Role.ReadWrite]

module.exports = function (sequelize, DataTypes) {
  let OrganizationUserMerchant = sequelize.define(
    // Model name
    'OrganizationUserMerchant',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      organizationUserId: {
        allowNull: false,
        type: DataTypes.BIGINT,
        field: 'organization_user_id'
      },
      merchantId: {
        allowNull: false,
        field: 'merchant_id',
        type: DataTypes.BIGINT
      },
      role: identityField(DataTypes, Role.Read, Roles)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'organization_user_merchants'
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * Role Enum
   * @type {Object}
   */
  OrganizationUserMerchant.Role = Role
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  OrganizationUserMerchant.associate = function (models) {
    OrganizationUserMerchant.belongsTo(models.OrganizationUser, {
      foreignKey: 'organizationUserId'
    })
    OrganizationUserMerchant.belongsTo(models.Merchant, {
      foreignKey: 'merchantId'
    })
  }
  /**
   * @method toAdminList
   * @description Returns apprpriate admin list data
   * @returns {Promise<array>} - OrganizationUserMerchant array promise
   *
   * @static
   */
  OrganizationUserMerchant.toAdminList = 'toAdminList'
  OrganizationUserMerchant.decorate = decorator
  /**
   * @method createFromData
   * @description Create a new OrganizationUserMerchant instance
   * @param {integer} id - organization user id
   * @param {object} params - incoming request params
   * @param {OrganizationUser} authUser - OrganizationUser instance
   * @param {Merchant} merchant - Merchant instance
   * @return {object} Sequelize transaction instance
   *
   * @static
   */
  OrganizationUserMerchant.createFromData = function (id, params, authUser, merchant) {
    return sequelize.transaction((t) => {
      const data = params.organization_user_merchant
      if (data) {
        return sequelize.models.OrganizationUser.findOne({
          where: { id: id, organizationId: authUser.organizationId },
          transaction: t
        }).then((otherUser) => {
          if (otherUser) {
            return this.create({
              organizationUserId: otherUser.id,
              merchantId: merchant.id,
              role: data.role
            }, { transaction: t })
          } else throw new Error('missing_organization_user_merchant_data')
        })
      } else throw new Error('missing_organization_user_merchant_data')
    })
  }
  /**
   * @method findForUser
   * @description Find all related merchants
   * @param {OrganizationUser} user - OrganizationUser instance
   *
   * @static
   */
  OrganizationUserMerchant.findForUser = function (user) {
    return this.findAll({
      attributes: [],
      where: { organizationUserId: user.id },
      include: [{ model: sequelize.models.Merchant.scope('name') }]
    })
  }
  /**
   * @method findForUserById
   * @description Find all related merchants
   * @param {OrganizationUser} user - OrganizationUser instance
   *
   * @static
   */
  OrganizationUserMerchant.findForUserById = function (id, user) {
    return this.findOne({
      attributes: [],
      where: { organizationUserId: user.id, merchantId: id },
      include: [{
        attributes: [
          'id',
          'categoryId',
          'name',
          'imageUrl',
          'backgroundImageUrl',
          'inviteCode',
          'color'
        ],
        model: sequelize.models.Merchant,
        include: [{ model: sequelize.models.Category.scope('name') }]
      }]
    })
  }
  /**
   * @method findForAuth
   * @description Sequelize association initialze
   * @param {integer} organizationId - Organization identifier
   *
   * @static
   */
  OrganizationUserMerchant.findForAuth = function (organizationId, userId, merchantId, role) {
    let query = {
      attributes: ['id', 'merchantId', 'role'],
      where: { organizationUserId: userId, merchantId: merchantId },
      include: [{
        model: sequelize.models.Merchant,
        where: { organizationId: organizationId }
      }]
    }
    if (role) query.where.role = role
    return this.findOne(query)
  }
  /**
   * @method generateRead
   * @description Returns a Read role OrganizationUserMerchant instance
   * @param {integer} userId - OrganizationUser id
   * @param {integer} merchantId - Merchant id
   * @param {object} t - sequelize transaction instance
   * @return {Promise<OrganizationUserMerchant>} Returns a
   *  OrganizationUserMerchant promise
   *
   * @static
   */
  OrganizationUserMerchant.generateRead = function (userId, merchantId, t) {
    return this.create({
      organizationUserId: userId,
      merchantId: merchantId,
      role: Role.Read
    }, { transaction: t })
  }
  /**
   * @method generateReadWrite
   * @description Returns a ReadWrite role OrganizationUserMerchant instance
   * @param {integer} userId - OrganizationUser id
   * @param {integer} merchantId - Merchant id
   * @param {object} t - sequelize transaction instance
   * @return {Promise<OrganizationUserMerchant>} Returns a
   *  OrganizationUserMerchant promise
   *
   * @static
   */
  OrganizationUserMerchant.generateReadWrite = function (userId, merchantId, t) {
    return this.create({
      organizationUserId: userId,
      merchantId: merchantId,
      role: Role.ReadWrite
    }, { transaction: t })
  }
  /**
   * @method adminList
   * @description Lists the users asigned to the provided Merchant id
   * @param {integer} merchantId - Merchant id
   * @return {Promise<OrganizationUserMerchant[]>} Returns a
   *  OrganizationUserMerchant array promise
   *
   * @static
   */
  OrganizationUserMerchant.adminList = function (merchantId) {
    return this.findAll({
      attributes: ['id', 'role'],
      where: { merchantId: merchantId },
      include: [{ model: sequelize.models.OrganizationUser }]
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toShow
   * @description Returns show apprpriate data for a organization user merchant
   * @returns {Object} - organization user merchant information
   *
   * @instance
   */
  OrganizationUserMerchant.prototype.toShow = function () {
    return { id: this.id, role: this.role }
  }
  /**
   * @method toList
   * @description Returns list apprpriate data for a organization user merchant
   * @returns {Object} - organization user merchant information
   *
   * @instance
   */
  OrganizationUserMerchant.prototype.toList = function () {
    return { name: this.Merchant.name }
  }
  /**
   * @method toShow
   * @description Returns show apprpriate data for a organization user merchant
   * @returns {Object} - organization user merchant information
   *
   * @instance
   */
  OrganizationUserMerchant.prototype.toAdminList = function () {
    return {
      id: this.id,
      role: this.role,
      organization_user: this.OrganizationUser.toShow()
    }
  }
  return OrganizationUserMerchant
}
