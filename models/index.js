/**
 * Kazam Api
 * @name Sequelize Initialize
 * @file models/index.js
 * @author Joel Cano
 */

'use strict'

const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const config = require('../config').db

module.exports = function (logger) {
  // Create Sequelize
  let sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  )
  let db = {}
  fs.readdirSync(__dirname).filter(function (file) {
    return (file.indexOf('.') !== 0) &&
      (file !== 'index.js') &&
      (file.slice(-3) === '.js')
  }).forEach(function (file) {
    let model = sequelize['import'](path.join(__dirname, file))
    model.logger = logger.child({ components: 'sequelize', model: model.name })
    db[model.name] = model
  })
  Object.keys(db).forEach(function (modelName) {
    if (db[modelName].associate) db[modelName].associate(db)
  })
  db.sequelize = sequelize
  db.Sequelize = Sequelize
  sequelize
    .authenticate()
    .then(() => {
      logger.debug(
        { database: config.database, username: config.username },
        'Authenticated'
      )
      logger.info('Connection has been established successfully.')
    })
    .catch(err => {
      logger.fatal('Unable to connect to the database:', err)
    })
  return db
}
