/**
 * Kazam Api
 * @name Sequelize Merchant Definition
 * @file models/merchant.js
 * @author Joel Cano
 */

'use strict'

const Aws = require('../lib/aws')
const _ = require('lodash/object')
const Promise = require('bluebird')
const decorator = require('../lib/decorate')
const uuidField = require('../lib/sequelize/uuid')
const imageUrl = require('../lib/sequelize/imageurl')
const stringField = require('../lib/sequelize/string')
const NotFoundError = require('../lib/error').NotFoundError

const LocationDistanceRadius = 3000.0
const distanceFieldName = 'distance'

module.exports = function (sequelize, DataTypes) {
  const Merchant = sequelize.define(
    // Model name
    'Merchant',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      organizationId: {
        allowNull: false,
        type: DataTypes.BIGINT,
        field: 'organization_id'
      },
      categoryId: {
        allowNull: false,
        field: 'category_id',
        type: DataTypes.BIGINT
      },
      identifier: stringField(DataTypes),
      identifierSk: uuidField(DataTypes, 'identifier_sk'),
      identifierPk: uuidField(DataTypes, 'identifier_pk'),
      isPublic: {
        allowNull: false,
        field: 'is_public',
        defaultValue: true,
        type: DataTypes.BOOLEAN
      },
      isActive: {
        allowNull: false,
        field: 'is_active',
        defaultValue: true,
        type: DataTypes.BOOLEAN
      },
      name: stringField(DataTypes, false),
      budgetRating: {
        defaultValue: 1,
        allowNull: true,
        field: 'budget_rating',
        type: DataTypes.INTEGER,
        validate: {
          isInt: { msg: 'invalid_int' },
          min: { args: [0], msg: 'less_than' },
          max: { args: [5], msg: 'greater_than' }
        }
      },
      imageUrl: _.merge({}, imageUrl(DataTypes, false), {
        get () {
          return Aws.getUrl(this.getDataValue('imageUrl'))
        }
      }),
      backgroundImageUrl: _.merge(
        {},
        imageUrl(DataTypes, false, 'background_image_url'), {
          get () {
            return Aws.getUrl(this.getDataValue('backgroundImageUrl'))
          }
        }
      ),
      color: stringField(DataTypes, false),
      inviteCode: {
        unique: true,
        allowNull: true,
        field: 'invite_code',
        type: DataTypes.STRING
      },
      hasBank: {
        allowNull: false,
        field: 'has_bank',
        defaultValue: false,
        type: DataTypes.BOOLEAN
      },
      hasCode: {
        allowNull: false,
        field: 'has_code',
        defaultValue: false,
        type: DataTypes.BOOLEAN
      },
      hasCompanyName: {
        allowNull: false,
        defaultValue: false,
        type: DataTypes.BOOLEAN,
        field: 'has_company_name'
      },
      requestTip: {
        allowNull: false,
        defaultValue: false,
        field: 'request_tip',
        type: DataTypes.BOOLEAN
      }
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'merchants',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method name
         * @description name query template
         * @returns {object} - query structure
         *
         * @static
         */
        name: { attributes: ['name'] },
        /**
         * @method apiEvent
         * @description apiEvent query template
         * @returns {object} - query structure
         *
         * @static
         */
        apiEvent: { attributes: ['name', 'imageUrl'] },
        /**
         * @method active
         * @description Returns query for only active shops
         * @returns {object} - query structure
         *
         * @static
         */
        active: { where: { isActive: true } },
        /**
         * @method api
         * @description api values query template
         * @returns {object} - query structure
         *
         * @static
         */
        api: { attributes: ['name', 'budgetRating', 'imageUrl'] },
        /**
         * @method punchCard
         * @description UserPunchCard scope
         * @returns {object} - query structure
         *
         * @static
         */
        punchCard: function () {
          return {
            attributes: ['id', 'name', 'imageUrl', 'color'],
            where: { isActive: true },
            include: [{ model: sequelize.models.Category.scope('name') }]
          }
        },
        /**
         * @method adminInclude
         * @description admin query template
         * @returns {object} - query structure
         *
         * @static
         */
        adminInclude: function () {
          return {
            include: [
              { model: sequelize.models.Category.scope('name') },
              { attributes: ['name'], model: sequelize.models.Organization }
            ]
          }
        }
      },
      /**
       * hooks
       * @description Model hooks
       * @type {Object}
       *
       */
      hooks: {
        /**
         * @method beforeDestroy
         * @description Before destroy hook
         * @param {Merchant} instance - Removed instance
         *
         * @static
         */
        beforeDestroy: function (instance) {
          return Promise.join(
            Aws.remove(instance.imageUrl, Merchant.logger),
            Aws.remove(instance.backgroundImageUrl, Merchant.logger)
          )
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  Merchant.associate = function (models) {
    // belongsTo associations
    Merchant.belongsTo(models.Category, { foreignKey: 'categoryId' })
    Merchant.belongsTo(models.Organization, { foreignKey: 'organizationId' })
    // hasMany associations
    Merchant.hasMany(models.Shop, { foreignKey: 'merchantId' })
    Merchant.hasMany(models.Validator, { foreignKey: 'merchantId' })
    Merchant.hasMany(models.UserPunchCardInvite, { foreignKey: 'merchantId' })
    Merchant.hasMany(models.OrganizationUserMerchant, {
      foreignKey: 'merchantId'
    })
  }
  /**
   * @method toList
   * @description Returns apprpriate list data
   * @returns {Promise<array>} - Merchant array promise
   *
   * @static
   */
  Merchant.toList = 'toList'
  /**
   * @method toView
   * @description Returns apprpriate view data
   * @returns {Promise<Merchant>} - Merchant promise
   *
   * @static
   */
  Merchant.toView = 'toView'
  /**
   * @method toApiNear
   * @description Returns apprpriate api data
   * @returns {Promise<array>} - Merchant array promise
   *
   * @static
   */
  Merchant.toApiNear = 'toApiNear'
  /**
   * @method toApi
   * @description Returns apprpriate api data
   * @returns {Promise<array>} - Merchant array promise
   *
   * @static
   */
  Merchant.toApi = 'toApi'
  /**
   * @method toAdmin
   * @description Returns apprpriate admin data
   * @returns {Promise<Merchant>} - Merchant promise
   *
   * @static
   */
  Merchant.toAdmin = 'toAdmin'
  /**
   * @method toAdminList
   * @description Returns apprpriate admin list data
   * @returns {Promise<array>} - Merchant array promise
   *
   * @static
   */
  Merchant.toAdminList = 'toAdminList'
  Merchant.decorate = decorator
  /**
   * @method apiShow
   * @description Searches for the apprpriate Merchant
   * @param {integer} id - Merchant id
   * @return {Promise<Merchant>} Returns a Merchant instance promise
   *
   * @static
   */
  Merchant.apiShow = function (id) {
    const err = new Error('not_found')
    if (isNaN(id)) return Promise.reject(err)
    else {
      const query = {
        attributes: ['id', 'name', 'imageUrl', 'budgetRating', 'color'],
        include: [sequelize.models.Category.scope('name')],
        where: { id: id }
      }
      return this.findOne(query).then(function (result) {
        if (result) return result
        else return Promise.reject(err)
      })
    }
  }
  /**
   * @method searchQuery
   * @description Returns the search query template
   * @param {Validator} validator - Validator class
   * @return {object} Query template
   *
   * @static
   */
  Merchant.searchQuery = function (validator) {
    return {
      attributes: [
        'id',
        'categoryId',
        'name',
        'imageUrl',
        'budgetRating',
        'color'
      ],
      include: [
        { model: sequelize.models.Category.scope('name') },
        { model: validator }
      ]
    }
  }
  /**
   * @method findByBeacon
   * @description Searches for the apprpriate Merchant via beacon information
   * @param {object} params - incoming request params
   * @return {Promise<Merchant>} Returns a Merchant instance promise
   *
   * @static
   */
  Merchant.findByBeacon = function (params) {
    const reject = Promise.reject
    const uid = params.uid
    const major = params.major
    if (uid && major !== undefined) {
      const query = this.searchQuery(
        sequelize.models.Validator.scope('active', {
          method: ['beacon', uid, major, params.minor]
        })
      )
      // FIXME: should be findOne
      return this.findAll(query).then(function (result) {
        if (result.length > 0) return result[0]
        else return reject(new NotFoundError('not_found'))
      })
    } else return reject(new Error('missing_validator_data'))
  }
  /**
   * @method findByQr
   * @description Searches for the apprpriate Merchant via qr token
   * @param {String} token - incoming token
   * @return {Promise<Merchant>} Returns a Merchant instance promise
   *
   * @static
   */
  Merchant.findByQr = function (token) {
    const query = this.searchQuery(
      sequelize.models.Validator.scope('active', { method: ['qr', token] })
    )
    // FIXME: should be findOne
    return this.findAll(query).then(function (result) {
      if (result.length > 0) return result[0]
      else return sequelize.Promise.reject(new Error('not_found'))
    })
  }
  /**
   * @method findByInviteCode
   * @description Returns a merchant from an invite_code
   * @param {string} inviteCode - inviteCode string
   * @param {object} t - sequelize transaction instance
   * @return {Promise<Merchant>} Returns a Merchant promise
   *
   * @static
   */
  Merchant.findByInviteCode = function (inviteCode, t) {
    return this.findOne({
      attributes: ['id', 'name', 'imageUrl'],
      where: { inviteCode: inviteCode },
      transaction: t
    })
  }
  /**
   * @method copy
   * @description Copies the identifier from one merchant to the other
   * @param {integer} id - Merchant id to copy
   * @param {Merchant} merchant - Current merchant instance
   * @param {OrganizationUser} user - Current user instance
   * @return {Promise<Merchant>} Returns a Merchant instance promise
   *
   * @static
   */
  Merchant.copy = function (id, merchant, user) {
    const Op = sequelize.Op
    return sequelize.transaction((t) => {
      if (!isNaN(id)) {
        return this.findOne({
          attributes: ['id', 'identifier'],
          where: {
            id: { [Op.eq]: id, [Op.ne]: merchant.id },
            identifier: { [Op.ne]: null },
            organizationId: merchant.organizationId
          },
          include: [{
            attributes: [],
            model: sequelize.models.OrganizationUserMerchant,
            where: { organizationUserId: user.id }
          }],
          transaction: t
        }).then(function (result) {
          if (result) {
            merchant.identifier = result.identifier
            return merchant.save({ transaction: t })
          } else throw new Error('not_found')
        })
      } else throw new Error('missing_merchant_data')
    })
  }
  /**
   * @method list
   * @description Returns a list of merchants filtered by merchant and params
   * @param {OrganizationUser} user - Organization User instance
   * @return {Promise<array>} Returns a Merchant array promise
   *
   * @static
   */
  Merchant.list = function (user) {
    let query = {
      attributes: ['id', 'isPublic', 'name', 'imageUrl', 'created_at']
    }
    if (user && user.organizationId) {
      query.include = [{
        attributes: [],
        model: sequelize.models.OrganizationUserMerchant,
        where: { organizationUserId: user.id }
      }]
    } else { // Only public data
      query.attributes = ['identifierSk', 'name', 'imageUrl']
      query.where = { isPublic: true }
    }
    return this.findAll(query)
  }
  /**
   * @method apiList
   * @description Returns a list of merchants
   * @return {Promise<array>} Returns a Merchant array promise
   *
   * @static
   */
  Merchant.apiList = function (limit) {
    const query = {
      attributes: [
        'id',
        'name',
        'budgetRating',
        'imageUrl',
        'backgroundImageUrl',
        'color'
      ],
      include: [{ model: sequelize.models.Category.scope('name') }],
      order: [['name', 'ASC'], ['id', 'ASC']]
    }
    return this.scope('active').findAll(query)
  }
  /**
   * @method distanceField
   * @description Returns a list of Merchants ordered by distance
   * @return {Promise<array>} Returns a Merchant array promise
   *
   * @static
   */
  Merchant.distanceField = function (lat, lng) {
    return [
      sequelize.fn(
        'earth_distance',
        sequelize.fn('ll_to_earth', lat, lng),
        sequelize.fn(
          'll_to_earth',
          sequelize.col('Shops.lat'),
          sequelize.col('Shops.lng')
        )
      ),
      distanceFieldName
    ]
  }
  /**
   * @method distanceOrder
   * @description Returns a list of Merchants ordered by distance
   * @return {Promise<array>} Returns a Merchant array promise
   *
   * @static
   */
  Merchant.distanceOrder = sequelize.literal(distanceFieldName + ' ASC')
  /**
   * @method distanceFilter
   * @description Returns a list of Merchants ordered by distance
   * @return {Promise<array>} Returns a Merchant array promise
   *
   * @static
   */
  Merchant.distanceFilter = function (lat, lng) {
    const earthDistance = sequelize.where(
      sequelize.fn(
        'earth_distance',
        sequelize.fn('ll_to_earth', lat, lng),
        sequelize.fn(
          'll_to_earth',
          sequelize.col('Shops.lat'),
          sequelize.col('Shops.lng')
        )
      ),
      '<',
      LocationDistanceRadius
    )
    const earthBox = sequelize.where(
      sequelize.fn('earth_box',
        sequelize.fn('ll_to_earth', lat, lng),
        LocationDistanceRadius
      ),
      ' @> ',
      sequelize.fn(
        'll_to_earth',
        sequelize.col('Shops.lat'),
        sequelize.col('Shops.lng')
      )
    )
    return { [sequelize.Op.and]: [earthDistance, earthBox] }
  }
  /**
   * @method apiNear
   * @description Returns a list of Merchants ordered by distance
   * @return {Promise<array>} Returns a Merchant array promise
   *
   * @static
   */
  Merchant.apiNear = function (lat, lng) {
    if (lat && lng) {
      const models = sequelize.models
      let shopFilter = this.distanceFilter(lat, lng)
      shopFilter[sequelize.Op.and].push({ isActive: true })
      const query = {
        attributes: [
          'id',
          'name',
          'budgetRating',
          'imageUrl',
          'backgroundImageUrl',
          'color'
        ],
        include: [
          {
            required: true,
            model: models.Shop,
            where: shopFilter,
            attributes: ['id', 'name', this.distanceField(lat, lng)],
            include: [{
              required: true,
              model: models.ShopCluster.scope('name')
            }]
          },
          { required: true, model: models.Category.scope('name') }
        ],
        order: [[sequelize.literal('"Shops.distance"'), 'ASC']]
      }
      return this.scope('active').findAll(query)
    } else return Promise.reject(new Error('missing_location_data'))
  }
  /**
   * @method adminShow
   * @description Returns a merchant instance with all admin values
   * @return {Promise<Merchant>} Returns a Merchant instance promise
   *
   * @static
   */
  Merchant.adminShow = function (id) {
    return new Promise((resolve, reject) => {
      const err = new Error('not_found')
      if (isNaN(id)) reject(err)
      else {
        this.scope('adminInclude').findById(id).then(function (result) {
          if (result) resolve(result)
          else reject(err)
        })
      }
    })
  }
  /**
   * @method adminList
   * @description Returns a list of merchants
   * @return {Promise<array>} Returns a Merchant array promise
   *
   * @static
   */
  Merchant.adminList = function (limit) {
    const query = {
      attributes: [
        'id',
        'isActive',
        'organizationId',
        'categoryId',
        'name',
        'created_at'
      ]
    }
    return this.scope('adminInclude').findAll(query)
  }
  /**
   * @method uploadImages
   * @description Returns a string array Promise with the resulting urls.
   * @return {Promise<array<string>>} Returns a string array Promise
   *
   * @static
   */
  Merchant.uploadImages = function (data) {
    return Promise.join(
      Aws.upload(data.image, null, this.logger),
      Aws.upload(data.background_image, null, this.logger)
    )
  }
  /**
   * @method adminCreateFromData
   * @description Creates a new Merchant with request data
   * @param {object} params - request parameters
   * @param {OrganizationUser} user - Organization User instance
   * @return {Promise<Merchant>} Returns a Merchant instance
   *
   * @static
   */
  Merchant.adminCreateFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.merchant
      if (data && !isNaN(data.organization_id) && !isNaN(data.category_id)) {
        return this.uploadImages(data)
          .spread((imageUrl, backgroundImageUrl) => {
            const insert = {
              organizationId: data.organization_id,
              categoryId: data.category_id,
              isActive: data.is_active,
              name: data.name,
              imageUrl: imageUrl,
              backgroundImageUrl: backgroundImageUrl,
              budgetRating: data.budget_rating,
              color: data.color,
              inviteCode: data.invite_code
            }
            return this.create(insert, { transaction: t })
          })
      } else throw new Error('missing_merchant_data')
    })
  }
  /**
   * @method adminUpdateFromData
   * @description Updates an Merchant via id from request params
   * @param {integer} id - Merchant id
   * @param {object} params - request parameters
   * @return {Promise<Merchant>} Returns a Merchant instance
   *
   * @static
   */
  Merchant.adminUpdateFromData = function (id, params) {
    return sequelize.transaction((t) => {
      const data = params.merchant
      if (!isNaN(id) && data) {
        return this.uploadImages(data)
          .spread((imageUrl, backgroundImageUrl) => {
            let update = {}
            if (data.name) update.name = data.name
            if (data.is_active !== undefined) update.isActive = data.is_active
            if (data.organization_id && !isNaN(data.organization_id)) {
              update.organizationId = data.organization_id
            }
            if (data.category_id && !isNaN(data.category_id)) {
              update.categoryId = data.category_id
            }
            if (imageUrl) update.imageUrl = imageUrl
            if (backgroundImageUrl) {
              update.backgroundImageUrl = backgroundImageUrl
            }
            if (data.budget_rating) update.budgetRating = data.budget_rating
            if (data.color) update.color = data.color
            if (data.invite_code) update.inviteCode = data.invite_code
            let query = { where: { id: id }, returning: true, transaction: t }
            return this.update(update, query)
              .spread(function (updated, result) {
                if (updated > 0) return result[0]
                else throw new Error('not_found')
              })
          })
      } else throw new Error('missing_merchant_data')
    })
  }
  /**
   * @method delete
   * @description Removes a Merchant by id
   * @param {integer} id - Merchant id
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  Merchant.delete = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.destroy({ where: { id: id }, transaction: t })
          .then(function (result) {
            if (result > 0) return true
            else throw new Error('not_found')
          })
      }
    })
  }
  /**
   * @method findForClient
   * @description Returns a merchant from an idenitifier
   * @param {string} identifier - identifier string
   * @param {object} t - sequelize transaction instance
   * @return {Promise<Merchant>} Returns a Merchant promise
   *
   * @static
   */
  Merchant.findForClient = function (identifier, t) {
    const Op = sequelize.Op
    return this.findOne({
      where: {
        [Op.or]: [
          { [Op.and]: { identifierSk: identifier, isPublic: true } },
          { [Op.and]: { identifierPk: identifier, isPublic: false } }
        ]
      },
      transaction: t
    })
  }
  /**
   * @method updateFromData
   * @description Updates an Merchant via id from request params
   * @param {integer} id - Merchant identifier
   * @param {object} params - request parameters
   * @param {OrganizationUser} user - OrganizationUser instance
   * @return {Promise<Merchant>} Returns a Merchant instance
   *
   * @static
   */
  Merchant.updateFromData = function (id, params, user) {
    return sequelize.transaction((t) => {
      const data = params.merchant
      if (!isNaN(id) && data) {
        return this.uploadImages(data)
          .spread((imageUrl, backgroundImageUrl) => {
            const OrganizationUserMerchant =
              sequelize.models.OrganizationUserMerchant
            let update = {}
            if (data.name) update.name = data.name
            if (imageUrl) update.imageUrl = imageUrl
            if (backgroundImageUrl) {
              update.backgroundImageUrl = backgroundImageUrl
            }
            if (data.is_public !== undefined) update.isPublic = data.is_public
            if (data.color) update.color = data.color
            let query = { where: { id: id }, returning: true, transaction: t }
            if (user.organizationId) {
              query.where.organizationId = user.organizationId
              query.include = [{
                required: true,
                attributes: ['id'],
                model: OrganizationUserMerchant,
                where: {
                  organizationUserId: user.id,
                  role: OrganizationUserMerchant.Role.ReadWrite
                }
              }]
            }
            return this.update(update, query)
              .spread(function (updated, result) {
                if (updated > 0) return result[0]
                else throw new Error('not_found')
              })
          })
      } else throw new Error('missing_merchant_data')
    })
  }
  /**
   * @method createFromData
   * @description Creates a new Merchant with request data
   * @param {object} params - request parameters
   * @param {OrganizationUser} user - OrganizationUser instance
   * @return {Promise<Merchant>} Returns a Merchant instance
   *
   * @static
   */
  Merchant.createFromData = function (params, user) {
    return sequelize.transaction((t) => {
      const data = params.merchant
      if (data) {
        return this.uploadImages(data)
          .spread((imageUrl, backgroundImageUrl) => {
            const insert = {
              organizationId: user.organizationId,
              categoryId: data.category_id,
              name: data.name,
              color: data.color,
              imageUrl: imageUrl,
              backgroundImageUrl: backgroundImageUrl
            }
            return this.create(insert, { transaction: t })
          })
          .then(function (merchant) {
            const OrganizationUserMerchant =
              sequelize.models.OrganizationUserMerchant
            return OrganizationUserMerchant.create({
              merchantId: merchant.id,
              organizationUserId: user.id,
              role: OrganizationUserMerchant.Role.ReadWrite
            }, { transaction: t }).then(function () {
              return merchant
            })
          })
      } else throw new Error('missing_merchant_data')
    })
  }
  /**
   * @method show
   * @description Returns a Merchant by id and user
   * @param {integer} id - Merchant id
   * @param {OrganizationUser} user - OrganizationUser instance
   * @returns {Promise<Merchant>} - Returns a Merchant promise or not found
   *
   * @static
   */
  Merchant.show = function (id, user) {
    return new Promise(function (resolve, reject) {
      const err = new Error('not_found')
      if (isNaN(id)) reject(err)
      else {
        sequelize.models.OrganizationUserMerchant.findForUserById(id, user)
          .then(function (result) {
            if (result) resolve(result.Merchant.toShow())
            else reject(err)
          })
      }
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toShow
   * @description Returns dashboard apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toShow = function () {
    return {
      id: this.id,
      name: this.name,
      color: this.color,
      image_url: this.imageUrl,
      background_image_url: this.backgroundImageUrl,
      category_id: this.categoryId,
      invite_code: this.inviteCode,
      category: this.Category.name
    }
  }
  /**
   * @method toView
   * @description Returns view apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toView = function () {
    return {
      id: this.id,
      name: this.name,
      color: this.color,
      image_url: this.imageUrl,
      identifier: this.identifier,
      background_image_url: this.backgroundImageUrl
    }
  }
  /**
   * @method toAdminList
   * @description Returns admin apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toAdminList = function () {
    let result = {
      id: this.id,
      category_id: this.categoryId,
      organization_id: this.organizationId,
      is_active: this.isActive,
      name: this.name,
      created_at: this.created_at
    }
    if (this.Category) result.category = this.Category.name
    if (this.Organization) result.organization = this.Organization.name
    return result
  }
  /**
   * @method toAdmin
   * @description Returns admin apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toAdmin = function () {
    let result = {
      id: this.id,
      category_id: this.categoryId,
      organization_id: this.organizationId,
      is_active: this.isActive,
      name: this.name,
      color: this.color,
      image_url: this.imageUrl,
      background_image_url: this.backgroundImageUrl,
      invite_code: this.inviteCode,
      budget_rating: this.budgetRating
    }
    if (this.Category) result.category = this.Category.name
    if (this.Organization) result.organization = this.Organization.name
    return result
  }
  /**
   * @method toApiNear
   * @description Returns api apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toApiNear = function () {
    const shop = this.Shops[0]
    return {
      id: this.id,
      name: this.name,
      image_url: this.imageUrl,
      shop_id: shop.id,
      shop_name: shop.name,
      background_image_url: this.backgroundImageUrl,
      category: this.Category.name,
      budget_rating: this.budgetRating,
      color: this.color
    }
  }
  /**
   * @method toApi
   * @description Returns api apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toApi = function () {
    return {
      id: this.id,
      name: this.name,
      image_url: this.imageUrl,
      background_image_url: this.backgroundImageUrl,
      category: this.Category.name,
      budget_rating: this.budgetRating,
      color: this.color
    }
  }
  /**
   * @method toApiSignUp
   * @description Returns api apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toApiSignUp = function () {
    return { id: this.id, name: this.name, image_url: this.imageUrl }
  }
  /**
   * @method toList
   * @description Returns list apprpriate data for a merchant
   * @returns {object} - merchant information
   *
   * @instance
   */
  Merchant.prototype.toList = function () {
    return {
      id: this.id,
      name: this.name,
      image_url: this.imageUrl,
      created_at: this.created_at
    }
  }
  return Merchant
}
