/**
 * Kazam Api
 * @name Sequelize PunchCard Definition
 * @file models/punchcard.js
 * @author Joel Cano
 */

'use strict'

const moment = require('moment')
const _ = require('lodash/object')
const date = require('../lib/date')
const decorator = require('../lib/decorate')
const notEmpty = require('../lib/sequelize/notempty')
const stringField = require('../lib/sequelize/string')
const identityField = require('../lib/sequelize/identity')

const Identity = { Unpublished: 0, Published: 1, Finished: 2 }
const Identities = [Identity.Unpublished, Identity.Published, Identity.Finished]
// const ValidationType = { Daily: 0 }
// const ValidationTypes = []

module.exports = function (sequelize, DataTypes) {
  let PunchCard = sequelize.define(
    // Model name
    'PunchCard',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      merchantId: {
        allowNull: false,
        field: 'merchant_id',
        type: DataTypes.BIGINT
      },
      identity: identityField(DataTypes, Identity.Unpublished, Identities),
      name: stringField(DataTypes, false),
      prize: stringField(DataTypes, false),
      rules: stringField(DataTypes, false),
      terms: {
        allowNull: false,
        type: DataTypes.TEXT,
        validate: notEmpty
      },
      redeemCode: {
        allowNull: false,
        field: 'redeem_code',
        type: DataTypes.STRING,
        validate: _.merge({}, notEmpty, {
          // isUppercase: { msg: 'not_uppercase' },
          // isAlphanumeric: { msg: 'not_alphanumeric' }
        })
      },
      punchLimit: {
        defaultValue: 3,
        allowNull: false,
        field: 'punch_limit',
        type: DataTypes.INTEGER,
        validate: {
          isInt: { msg: 'invalid_int' },
          min: { args: [3], msg: 'less_than' },
          max: { args: [15], msg: 'greater_than' }
        }
      },
      expiresAt: {
        allowNull: false,
        field: 'expires_at',
        type: DataTypes.DATE,
        validate: { isDate: date.isDate }
      },
      validationLimit: {
        type: DataTypes.INTEGER,
        field: 'validation_limit'
      },
      validationLimitType: {
        type: DataTypes.INTEGER,
        field: 'validation_limit_type'
      }
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'punch_cards',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method active
         * @description Returns query for published non expired punch cards
         * @returns {object} - query structure
         *
         * @static
         */
        active: function () {
          return {
            where: {
              identity: Identity.Published,
              expiresAt: { [sequelize.Op.gt]: new Date() }
            }
          }
        },
        /**
         * @method unpublishedForMerchant
         * @description Returns query for unpublished punch cards filter by
         *  merchant
         * @returns {object} - query structure
         *
         * @static
         */
        unpublishedForMerchant: function (merchantId) {
          return {
            where: { merchantId: merchantId, identity: Identity.Unpublished }
          }
        }
      }
    }
  )
  /**
   * Identity enum
   * @type {Object}
   */
  PunchCard.Identity = Identity
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  PunchCard.associate = function (models) {
    PunchCard.belongsTo(models.Merchant, { foreignKey: 'merchantId' })
    PunchCard.hasMany(models.UserPunchCard, { foreignKey: 'punchCardId' })
  }
  /**
   * @method toAdminList
   * @description Returns apprpriate admin data
   * @returns {Promise<array>} - Merchant array promise
   *
   * @static
   */
  PunchCard.toAdminList = 'toAdminList'
  PunchCard.decorate = decorator
  /**
   * @method activeForMerchantInclude
   * @description Returns query structure for active punchards filtered by
   *  merchant
   * @param  {integer} merchantId - Merchant id
   * @return {object} Returns query structure
   *
   * @static
   */
  PunchCard.activeForMerchantInclude = function (merchantId) {
    const Op = sequelize.Op
    return {
      attributes: [],
      required: true,
      model: this,
      where: {
        merchantId: merchantId,
        identity: Identity.Published,
        expiresAt: { [Op.gte]: new Date() }
      }
    }
  }
  /**
   * @method findActive
   * @description Returns a list of all non expired published punchcards
   * @param {object} t - sequelize transaction instance
   * @return {Promise<array>} Returns an PunchCard array promise
   *
   * @static
   */
  PunchCard.findActive = function (t) {
    return this.scope('active').findAll({ transaction: t })
  }
  /**
   * @method latest
   * @description Returns the latest punchcard for a merchant
   * @param  {Merchant} merchant - Merchant instance
   * @return {Promise<PunchCard>} Returns a PunchCard promise
   *
   * @static
   */
  PunchCard.latest = function (merchant) {
    return this.findOne({
      attributes: [
        'id',
        'identity',
        'name',
        'prize',
        'rules',
        'terms',
        'redeem_code',
        'punch_limit',
        'expires_at'
      ],
      where: { merchantId: merchant.id },
      order: [['created_at', 'DESC']]
    })
  }
  /**
   * @method active
   * @description Returns the current published punchcard for a merchant
   * @param  {Merchant} merchant - Merchant instance
   * @return {Promise<PunchCard>} Returns a PunchCard promise
   *
   * @static
   */
  PunchCard.active = function (merchant) {
    return this.scope('active').findOne({
      attributes: [
        'id',
        'name',
        'prize',
        'rules',
        'terms',
        'redeem_code',
        'punch_limit',
        'expires_at'
      ],
      where: { merchantId: merchant.id },
      order: [['created_at', 'DESC']]
    })
  }
  /**
   * @method list
   * @description Returns a list of punchcards filtered by merchant
   * @param  {Merchant} merchant - Merchant instance
   * @return {Promise<array>} Returns an PunchCard array promise
   *
   * @static
   */
  PunchCard.list = function (merchant) {
    let query = {
      attributes: [
        'id',
        'identity',
        'name',
        'prize',
        'rules',
        'terms',
        'punch_limit',
        'expires_at',
        'validation_limit'
      ]
    }
    if (merchant) query.where = { merchantId: merchant.id }
    return this.findAll(query)
  }
  /**
   * @method adminList
   * @description Returns a list of punchcards
   * @return {Promise<array>} Returns an PunchCard array promise
   *
   * @static
   */
  PunchCard.adminList = function () {
    const query = {
      attributes: ['id', 'name', 'identity', 'punchLimit', 'expiresAt'],
      include: [{ model: sequelize.models.Merchant.scope('name') }]
    }
    return this.findAll(query)
  }
  /**
   * @method adminShow
   * @description Returns a PunchCard by id
   * @param {integer} id - PunchCard id
   * @returns {Promise<PunchCard>} - Returns a PunchCard promise or not found
   *
   * @static
   */
  PunchCard.adminShow = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.findOne({ where: { id: id }, transaction: t })
          .then(function (result) {
            if (result) return result
            else throw new Error('not_found')
          })
      }
    })
  }
  /**
   * @method adminCreateFromData
   * @description Creates a new PunchCard with request data
   * @param {object} params - request parameters
   * @return {Promise<PunchCard>} Returns a PunchCard instance
   *
   * @static
   */
  PunchCard.adminCreateFromData = function (params) {
    return sequelize.transaction((t) => {
      const data = params.punch_card
      if (data) {
        let insert = {
          merchantId: data.merchant_id,
          name: data.name,
          prize: data.prize,
          rules: data.rules,
          terms: data.terms,
          punchLimit: data.punch_limit,
          redeemCode: data.redeem_code,
          expiresAt: data.expires_at
        }
        const validationLimit = parseInt(data.validation_limit)
        if (!isNaN(validationLimit)) {
          insert.validationLimit = validationLimit
        } else insert.validationLimit = null
        if (data.validation_limit_type !== undefined) {
          insert.validationLimitType = data.validation_limit_type
        }
        return this.create(insert, { transaction: t })
      } else throw new Error('missing_punch_card_data')
    })
  }
  /**
   * @method adminUpdateFromData
   * @description Updates an PunchCard via id from request params
   * @param {integer} id - PunchCard identifier
   * @param {object} params - request parameters
   * @return {Promise<PunchCard>} Returns a PunchCard instance
   *
   * @static
   */
  PunchCard.adminUpdateFromData = function (id, params) {
    return sequelize.transaction((t) => {
      const data = params.punch_card
      if (!isNaN(id) && data) {
        return this.findOne({ where: { id: id }, transaction: t })
          .then(function (card) {
            if (card) return card
            else throw new Error('not_found')
          }).then(function (card) {
            if (card.identity === Identity.Unpublished) {
              if (data.merchant_id) card.merchantId = data.merchant_id
              if (data.punch_limit) card.punchLimit = data.punch_limit
              // if (data.identity) card.identity = data.identity
            }
            if (data.name) card.name = data.name
            if (data.prize) card.prize = data.prize
            if (data.location) card.location = data.location
            if (data.rules) card.rules = data.rules
            if (data.terms) card.terms = data.terms
            if (data.redeem_code) card.redeemCode = data.redeem_code
            if (data.expires_at) card.expiresAt = data.expires_at
            const validationLimit = parseInt(data.validation_limit)
            if (!isNaN(validationLimit)) {
              card.validationLimit = validationLimit
            } else card.validationLimit = null
            if (data.validation_limit_type !== undefined) {
              card.validationLimitType = data.validation_limit_type
            }
            const d = data.expires_at
            if (d && date.validate(d)) card.expiresAt = d
            return card.save({ transaction: t })
          })
      } else throw new Error('missing_punch_card_data')
    })
  }
  /**
   * @method adminDelete
   * @description Removes a PunchCard by id and merchant
   * @param {integer} id - PunchCard id
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  PunchCard.adminDelete = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.destroy({ where: { id: id }, transaction: t })
          .then(function (result) {
            if (result > 0) return true
            else throw new Error('not_found')
          })
      }
    })
  }
  /**
   * @method createFromData
   * @description Creates a new PunchCard with request data
   * @param {object} params - request parameters
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<PunchCard>} Returns a PunchCard instance
   *
   * @static
   */
  PunchCard.createFromData = function (params, merchant) {
    return sequelize.transaction((t) => {
      const data = params.punch_card
      if (data) {
        let dateVal
        const d = data.expires_at
        if (d && date.validate(d)) {
          dateVal = d
          let insert = {
            merchantId: merchant.id,
            name: data.name,
            prize: data.prize,
            rules: data.rules,
            terms: data.terms,
            punchLimit: data.punch_limit,
            redeemCode: data.redeem_code,
            expiresAt: dateVal
          }
          const validationLimit = parseInt(data.validation_limit)
          if (!isNaN(validationLimit)) {
            insert.validationLimit = validationLimit
          } else insert.validationLimit = null
          if (data.validation_limit_type !== undefined) {
            insert.validationLimitType = data.validation_limit_type
          }
          return this.create(insert, { transaction: t })
        } else throw new Error('missing_punch_card_data')
      } else throw new Error('missing_punch_card_data')
    })
  }
  /**
   * @method updateFromData
   * @description Updates an PunchCard via id from request params
   * @param {integer} id - PunchCard identifier
   * @param {object} params - request parameters
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<PunchCard>} Returns a PunchCard instance
   *
   * @static
   */
  PunchCard.updateFromData = function (id, params, merchant) {
    return sequelize.transaction((t) => {
      const data = params.punch_card
      if (!isNaN(id) && data) {
        let update = {}
        if (data.name) update.name = data.name
        if (data.prize) update.prize = data.prize
        if (data.location) update.location = data.location
        if (data.rules) update.rules = data.rules
        if (data.terms) update.terms = data.terms
        if (data.redeem_code) update.redeemCode = data.redeem_code
        if (data.punch_limit) update.punchLimit = data.punch_limit
        if (data.validation_limit !== undefined) {
          update.validationLimit = data.validation_limit
        }
        if (data.validation_limit_type !== undefined) {
          update.validationLimitType = data.validation_limit_type
        }
        const d = data.expires_at
        if (d && date.validate(d)) update.expiresAt = d
        const query = { where: { id: id }, returning: true, transaction: t }
        return this.scope({
          method: ['unpublishedForMerchant', merchant.id]
        }).update(update, query).spread(function (updated, result) {
          if (updated > 0) return result[0]
          else throw new Error('not_found')
        })
      } else throw new Error('missing_punch_card_data')
    })
  }
  /**
   * @method publish
   * @description Publish a PunchCard via id and merchant
   * @param {integer} id - PunchCard identifier
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<PunchCard>} Returns a PunchCard instance
   *
   * @static
   */
  PunchCard.publish = function (id, merchant) {
    return sequelize.transaction((t) => {
      if (!isNaN(id)) {
        const merchantId = merchant.id
        const find = { where: { merchantId: merchantId }, transaction: t }
        this.logger.info({ action: 'publish' }, id)
        return this.scope('active').findOne(find).then((current) => {
          if (current) throw new Error('active_punch_card_exists')
          else {
            const update = { identity: Identity.Published }
            const query = { where: { id: id }, returning: true, transaction: t }
            return this.scope({
              method: ['unpublishedForMerchant', merchantId]
            }).update(update, query).spread(function (updated, result) {
              if (updated > 0) return result[0]
              else throw new Error('not_found')
            }).then((published) => {
              this.logger.debug({ action: 'publish' }, { id: id, published: true })
              /**
               * All users obtain the new PunchCard
               */
              return this.generateUserPunchCards(published, t)
                .then(function () { return published })
            })
          }
        })
      } else throw new Error('not_found')
    })
  }
  /**
   * @method adminPublish
   * @description Publish a PunchCard via id
   * @param {integer} id - PunchCard identifier
   * @return {Promise<PunchCard>} Returns a PunchCard instance
   *
   * @static
   */
  PunchCard.adminPublish = function (id) {
    return sequelize.transaction((t) => {
      if (!isNaN(id)) {
        this.logger.info({ action: 'publish' }, id)
        return this.findOne({
          where: { id: id, identity: Identity.Unpublished },
          transaction: t
        }).then((card) => {
          if (card) {
            return this.scope('active').findOne({
              where: { merchantId: card.merchantId },
              transaction: t
            }).then(function (current) {
              if (current) throw new Error('active_punch_card_exists')
              else return card
            })
          } else throw new Error('not_found')
        }).then((card) => {
          card.identity = Identity.Published
          return card.save({ transaction: t })
        }).then((published) => {
          this.logger.debug({ action: 'publish' }, { id: id, published: true })
          /**
           * All users obtain the new PunchCard
           */
          return this.generateUserPunchCards(published, t)
            .then(function () { return published })
        })
      } else throw new Error('not_found')
    })
  }
  /**
   * @method generateUserPunchCards
   * @description Create a PunchCard for every active user
   * @param {PunchCard} published - PunchCard instance
   * @param {object} t - sequelize transaction
   * @return {Promise} Returns a sequelize promise
   *
   * @static
   */
  PunchCard.generateUserPunchCards = function (published, t) {
    return sequelize.models.User.findActive(t).then(function (users) {
      let bulk = []
      const cardId = published.id
      for (let i = 0; i < users.length; ++i) {
        bulk.push({ userId: users[i].id, punchCardId: cardId })
      }
      return sequelize.models.UserPunchCard.bulkCreate(bulk, { transaction: t })
    })
  }
  /**
   * @method show
   * @description Returns a PunchCard by id and merchant
   * @param {integer} id - PunchCard id
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<PunchCard>} - Returns a PunchCard promise or not found
   *
   * @static
   */
  PunchCard.show = function (id, merchant) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.findOne({
          where: { id: id, merchantId: merchant.id },
          transaction: t
        }).then(function (result) {
          if (result) return result
          else throw new Error('not_found')
        })
      }
    })
  }
  /**
   * @method delete
   * @description Removes a PunchCard by id and merchant
   * @param {integer} id - PunchCard id
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  PunchCard.delete = function (id, merchant) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.scope({ method: ['unpublishedForMerchant', merchant.id] })
          .destroy({ where: { id: id }, transaction: t })
          .then(function (result) {
            if (result > 0) return true
            else throw new Error('not_found')
          })
      }
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toShow
   * @description Returns show apprpriate data for a PunchCard
   * @returns {object} - PunchCard information
   *
   * @instance
   */
  PunchCard.prototype.toShow = function () {
    return {
      id: this.id,
      identity: this.identity,
      name: this.name,
      prize: this.prize,
      rules: this.rules,
      terms: this.terms,
      punch_limit: this.punchLimit,
      redeem_code: this.redeemCode,
      expires_at: this.expiresAt,
      validation_limit: this.validationLimit,
      validation_limit_type: this.validationLimitType
    }
  }
  /**
   * @method toApiList
   * @description Returns api apprpriate data for a PunchCard
   * @returns {object} - PunchCard information
   *
   * @instance
   */
  PunchCard.prototype.toApiList = function () {
    return {
      id: this.id,
      identity: this.identity,
      name: this.name,
      prize: this.prize,
      punch_limit: this.punchLimit,
      merchant_name: this.Merchant.name,
      merchant_image_url: this.Merchant.imageUrl
    }
  }
  /**
   * @method toAdminList
   * @description Returns admin apprpriate data for a PunchCard
   * @returns {object} - PunchCard information
   *
   * @instance
   */
  PunchCard.prototype.toAdminList = function () {
    return {
      id: this.id,
      identity: this.identity,
      name: this.name,
      punch_limit: this.punchLimit,
      expires_at: this.expiresAt,
      merchant: this.Merchant.name
    }
  }
  /**
   * @method toAdminShow
   * @description Returns show apprpriate data for a PunchCard
   * @returns {object} - PunchCard information
   *
   * @instance
   */
  PunchCard.prototype.toAdminShow = function () {
    return {
      id: this.id,
      identity: this.identity,
      name: this.name,
      prize: this.prize,
      rules: this.rules,
      terms: this.terms,
      punch_limit: this.punchLimit,
      redeem_code: this.redeemCode,
      expires_at: this.expiresAt,
      validation_limit: this.validationLimit,
      validation_limit_type: this.validationLimitType
    }
  }
  /**
   * @method validationRange
   * @description Returns the current validation range for a PunchCard
   * @returns {array} - date array
   *
   * @instance
   */
  PunchCard.prototype.validationRange = function () {
    let range
    switch (this.validationLimitType) {
      default:
        range = [moment().startOf('day').toDate(), this.validationLimitEnd()]
    }
    return range
  }
  /**
   * @method validationLimitEnd
   * @description Returns admin apprpriate data for a PunchCard
   * @returns {object} - PunchCard information
   *
   * @instance
   */
  PunchCard.prototype.validationLimitEnd = function () {
    let end
    switch (this.validationLimitType) {
      default:
        end = moment().endOf('day').toDate()
    }
    return end
  }
  return PunchCard
}
