/**
 * Kazam Api
 * @name Sequelize Validator Definition
 * @file models/validator.js
 * @author Joel Cano
 */

'use strict'

const qr = require('qr-encode')
const uuid = require('uuid/v4')
const Aws = require('../lib/aws')
const decorator = require('../lib/decorate')
const page = require('../lib/sequelize/page')
const matches = require('validator/lib/matches')
const uuidField = require('../lib/sequelize/uuid')
const stringField = require('../lib/sequelize/string')
const identityField = require('../lib/sequelize/identity')

const Identity = { Beacon: 0, QR: 1 }
const Identities = [Identity.Beacon, Identity.QR]
const qrOpts = { type: 6, size: 6, level: 'Q' }

module.exports = function (sequelize, DataTypes) {
  let Validator = sequelize.define(
    // Model name
    'Validator',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      merchantId: {
        allowNull: false,
        field: 'merchant_id',
        type: DataTypes.BIGINT
      },
      shopId: {
        field: 'shop_id',
        type: DataTypes.BIGINT
      },
      isActive: {
        allowNull: false,
        field: 'is_active',
        type: DataTypes.BOOLEAN
      },
      identity: identityField(DataTypes, Identity.Beacon, Identities),
      name: stringField(DataTypes, false),
      uid: {
        defaultValue: '',
        type: DataTypes.STRING,
        validate: {
          notNull: function (value) {
            if (this.identity === Identity.Beacon && value === '') {
              throw new Error('empty')
            } else if (this.identity === Identity.QR) this.uid = null
          }
        }
      },
      major: {
        defaultValue: '',
        type: DataTypes.INTEGER,
        validate: {
          notNull: function (value) {
            if (this.identity === Identity.Beacon && value === '') {
              throw new Error('empty')
            } else if (this.identity === Identity.QR) this.major = null
          }
        }
      },
      minor: {
        defaultValue: '',
        type: DataTypes.INTEGER,
        validate: {
          notNull: function (value) {
            if (this.identity === Identity.Beacon && value === '') {
              throw new Error('empty')
            } else if (this.identity === Identity.QR) this.minor = null
          }
        }
      },
      token: {
        unique: true,
        defaultValue: '',
        type: DataTypes.TEXT,
        validate: {
          notNull: function (value) {
            if (this.identity === Identity.QR && value === '') {
              throw new Error('empty')
            } else if (this.identity === Identity.Beacon) this.token = null
          }
        }
      },
      imageUrl: {
        defaultValue: '',
        field: 'image_url',
        type: DataTypes.STRING,
        validate: {
          invalid: function (value) {
            if (this.identity === Identity.QR && (value === '')) {
              throw new Error('empty')
            } else if (this.identity === Identity.QR && !matches(value, /\\*.+\.gif/i)) {
              throw new Error('invalid_file')
            } else if (this.identity === Identity.Beacon) this.imageUrl = null
          }
        },
        get () {
          let value = this.getDataValue('imageUrl')
          if (value) value = Aws.getUrl(value)
          return value
        }
      },
      identifier: uuidField(DataTypes)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'validators',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method active
         * @description Returns query for active and conected validators
         * @returns {object} - query structure
         *
         * @static
         */
        active: {
          where: { isActive: true, shopId: { [sequelize.Op.ne]: null } }
        },
        /**
         * @method beacon
         * @description Beacon search query template
         * @param {String} uid - Beacon uuid
         * @param {integer} major - Beacon major
         * @returns {object} - query structure
         *
         * @static
         */
        beacon: function (uid, major, minor) {
          let query = {
            where: { uid: uid, major: major, identity: Identity.Beacon }
          }
          if (minor) query.where.minor = minor
          return query
        },
        /**
         * @method qr
         * @description QR search query template
         * @param {String} token - Token string
         * @returns {object} - query structure
         *
         * @static
         */
        qr: function (token) {
          return { where: { token: token, identity: Identity.QR } }
        }
      },
      /**
       * hooks
       * @description Model hooks
       * @type {Object}
       *
       */
      hooks: {
        /**
         * @method beforeDestroy
         * @description After destroy hook
         * @param {Merchant} instance - Removed instance
         *
         * @static
         */
        beforeDestroy: function (instance) {
          if (instance.imageUrl) {
            return Aws.remove(instance.imageUrl, Validator.logger)
          }
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * Identity enum
   * @type {Object}
   */
  Validator.Identity = Identity
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  Validator.associate = function (models) {
    Validator.belongsTo(models.Merchant, { foreignKey: 'merchantId' })
    Validator.belongsTo(models.Shop, { foreignKey: 'shopId' })
    Validator.hasMany(models.UserPunchCardValidation, {
      foreignKey: 'validatorId'
    })
  }
  /**
   * @method toRegion
   * @description Returns apprpriate beacon data
   * @returns {Promise<array>} - Validator array promise
   *
   * @static
   */
  Validator.toRegion = 'toRegion'
  /**
   * @method toBeacon
   * @description Returns apprpriate beacon data
   * @returns {Promise<array>} - Validator array promise
   *
   * @static
   */
  Validator.toBeacon = 'toBeacon'
  /**
   * @method toQr
   * @description Returns apprpriate qr data
   * @returns {Promise<array>} - Validator array promise
   *
   * @static
   */
  Validator.toQr = 'toQr'
  /**
   * @method toAdminBeacon
   * @description Returns apprpriate admin beacon data
   * @returns {Promise<array>} - Validator array promise
   *
   * @static
   */
  Validator.toAdminBeacon = 'toAdminBeacon'
  /**
   * @method toAdminQr
   * @description Returns apprpriate admin qr data
   * @returns {Promise<array>} - Validator array promise
   *
   * @static
   */
  Validator.toAdminQr = 'toAdminQr'
  Validator.decorate = decorator
  Validator.total = page.total(sequelize)
  /**
   * @method generateQr
   * @description Generate a QR validator instance
   * @param {Shop} shop - Shop instance
   * @param {Merchant} merchant - Merchant instance
   * @param {object} t - sequelize transaction instance
   * @returns {Promise<Validator>} - Validator promise
   *
   * @static
   */
  Validator.generateQr = function (shop, merchant, t) {
    const token = uuid()
    const data = qr(token, qrOpts)
    return Aws.qr(data, uuid()).then((url) => {
      return this.create({
        merchantId: merchant.id,
        shopId: shop.id,
        identity: Identity.QR,
        isActive: true,
        name: token,
        token: token,
        imageUrl: url
      }, { transaction: t })
    })
  }
  /**
   * @method toggle
   * @description Toggle isActive field for a validator
   * @param {Integer} id - Validator id
   * @param {Integer} identity - Validator identity
   * @param {Merchant} merchant - Merchant instance
   * @returns {object} - sequelize transaction instance
   *
   * @static
   */
  Validator.toggle = function (id, identity, merchant) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.findOne({
          where: { id: id, identity: identity, merchantId: merchant.id },
          include: [{ attributes: ['name'], model: sequelize.models.Shop }],
          transaction: t
        }).then(function (validator) {
          if (validator) {
            validator.isActive = !validator.isActive
            return validator.save({ transaction: t })
          } else throw new Error('not_found')
        })
      }
    })
  }
  /**
   * @method list
   * @description Returns apprpriate list data
   * @returns {Promise<array>} - Validator array promise
   *
   * @static
   */
  Validator.list = function (identity, merchant) {
    let query = {
      where: { identity: identity },
      include: [{
        required: true,
        attributes: ['name'],
        model: sequelize.models.Shop
      }]
    }
    if (merchant) query.where.merchantId = merchant.id
    else query.include.push({ model: sequelize.models.Merchant.scope('name') })
    return this.findAll(query)
  }
  /**
   * @method regions
   * @description Returns region data
   * @returns {Promise<Validator>} - Validator array
   *
   * @static
   */
  Validator.regions = function () {
    return this.scope('active').findAll({
      attributes: [[sequelize.fn('DISTINCT', sequelize.col('uid')), 'uid']],
      where: { identity: Identity.Beacon }
    })
  }
  /**
   * @method merchant
   * @description Returns merchant validators
   * @param {integer} merchantId - Merchant id
   * @param {integer} identity - Validator identity
   * @returns {Promise<Validator>} - Validator array
   *
   * @static
   */
  Validator.merchant = function (merchantId, identity) {
    return this.scope('active').findAll({
      attributes: ['uid', 'major', 'minor'],
      where: { identity: identity, merchantId: merchantId }
    })
  }
  /**
   * @method validateBeacon
   * @description Returns apprpriate list data
   * @param {string} uid - Validator uid
   * @param {integer} major - Validator major
   * @param {integer} minor - Validator minor
   * @param {integer} merchantId - Merchant id
   * @param {object} t - sequelize transaction object
   * @returns {Promise<Validator>} - Validator instance
   *
   * @static
   */
  Validator.validateBeacon = function (uid, major, minor, merchantId, t) {
    return this.scope(
      'active',
      { method: ['beacon', uid, major, minor] }
    ).findOne({
      attributes: ['id'],
      where: { merchantId: merchantId },
      transaction: t
    })
  }
  /**
   * @method validateQR
   * @description Returns apprpriate list data
   * @param {string} token - Incoming qr string
   * @param {integer} merchantId - Merchant id
   * @param {object} t - sequelize transaction object
   * @returns {Promise<Validator>} - Validator instance
   *
   * @static
   */
  Validator.validateQR = function (token, merchantId, t) {
    return this.scope('active', { method: ['qr', token] }).findOne({
      attributes: ['id'],
      where: { merchantId: merchantId },
      transaction: t
    })
  }
  /**
   * @method show
   * @description Returns a Validator by id and identity
   * @param {integer} id - Validator id
   * @param {integer} identity - Validator identity
   * @returns {Promise<Validator>} - Returns a Validator promise or not found
   *
   * @static
   */
  Validator.show = function (id, identity) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.findOne({
          where: { id: id, identity: identity },
          include: [
            {
              attributes: ['name'],
              model: sequelize.models.Shop
            },
            { model: sequelize.models.Merchant.scope('name') }
          ],
          transaction: t
        }).then(function (result) {
          if (result) return result
          else throw new Error('not_found')
        })
      }
    })
  }
  /**
   * @method createBeacon
   * @description Creates a new beacon via request params
   * @param {object} params - Incoming request params
   * @returns {Promise<Validator>} - Returns a beacon Validator promise
   *
   * @static
   */
  Validator.createBeacon = function (params) {
    return sequelize.transaction((t) => {
      const data = params.beacon
      if (data) {
        const insert = {
          merchantId: data.merchant_id,
          shopId: data.shop_id,
          identity: Identity.Beacon,
          isActive: data.is_active,
          name: data.name,
          uid: data.uid,
          major: data.major,
          minor: data.minor
        }
        return this.create(insert, { transaction: t })
      } else throw new Error('missing_beacon_data')
    })
  }
  /**
   * @method createQr
   * @description Create a new QR via params
   * @param {object} params - Request parameters
   * @returns {Promise<Validator>} - Returns a qr Validator promise
   *
   * @static
   */
  Validator.createQr = function (params) {
    return sequelize.transaction((t) => {
      const data = params.qr
      if (data) {
        const token = uuid()
        const img = qr(token, qrOpts)
        return Aws.qr(img, uuid()).then((url) => {
          const insert = {
            merchantId: data.merchant_id,
            shopId: data.shop_id,
            identity: Identity.QR,
            isActive: data.is_active,
            name: data.name,
            token: token,
            imageUrl: url
          }
          return this.create(insert, { transaction: t })
        })
      } else throw new Error('missing_qr_data')
    })
  }
  /**
   * @method updateBeacon
   * @description Updates a beacon via id and request params
   * @param {integer} id - Validator id
   * @param {object} params - Incoming request params
   * @returns {Promise<Validator>} - Returns a Validator promise or not found
   *
   * @static
   */
  Validator.updateBeacon = function (id, params) {
    return sequelize.transaction((t) => {
      const data = params.beacon
      if (!isNaN(id) && data) {
        let update = {}
        if (data.merchant_id) update.merchantId = data.merchant_id
        if (data.shop_id) update.shopId = data.shop_id
        if (data.name) update.name = data.name
        if (data.is_active !== undefined) update.isActive = data.is_active
        if (data.uid) update.uid = data.uid
        if (data.major) update.major = data.major
        if (data.minor) update.minor = data.minor
        let query = {
          where: { id: id, identity: Identity.Beacon },
          returning: true,
          transaction: t
        }
        return this.update(update, query).spread(function (updated, result) {
          if (updated > 0) return result[0]
          else throw new Error('not_found')
        })
      } else throw new Error('missing_beacon_data')
    })
  }
  /**
   * @method updateQr
   * @description Update a QR via id and params
   * @param {integer} id - Validator id
   * @param {object} params - Request parameters
   * @returns {Promise<Validator>} - returns a qr Validator promise
   *
   * @static
   */
  Validator.updateQr = function (id, params) {
    return sequelize.transaction((t) => {
      const data = params.qr
      if (!isNaN(id) && data) {
        let update = {}
        if (data.merchant_id) update.merchantId = data.merchant_id
        if (data.shop_id) update.shopId = data.shop_id
        if (data.name) update.name = data.name
        if (data.is_active !== undefined) update.isActive = data.is_active
        let query = {
          where: { id: id, identity: Identity.QR },
          returning: true,
          transaction: t
        }
        return this.update(update, query).spread(function (updated, result) {
          if (updated > 0) return result[0]
          else throw new Error('not_found')
        })
      } else throw new Error('missing_qr_data')
    })
  }
  /**
   * @method deleteBeacon
   * @description Removes a beacon Validator by id
   * @param {integer} id - Validator id
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  Validator.deleteBeacon = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        const query = {
          where: { id: id, identity: Identity.Beacon },
          transaction: t
        }
        return this.destroy(query).then(function (result) {
          if (result > 0) return true
          else throw new Error('not_found')
        })
      }
    })
  }
  /**
   * @method deleteQr
   * @description Removes a qr Validator by id
   * @param {integer} id - Validator id
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  Validator.deleteQr = function (id) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        const query = {
          where: { id: id, identity: Identity.QR },
          transaction: t
        }
        return this.destroy(query).then(function (result) {
          if (result > 0) return true
          else throw new Error('not_found')
        })
      }
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toRegion
   * @description Region apprpriate data
   * @returns {object} - Validator information
   *
   * @instance
   */
  Validator.prototype.toRegion = function () {
    let result = { uid: this.uid }
    if (this.major !== undefined) result.major = this.major
    if (this.minor !== undefined) result.minor = this.minor
    return result
  }
  /**
   * @method toBeacon
   * @description Beacon apprpriate data
   * @returns {object} - Validator information
   *
   * @instance
   */
  Validator.prototype.toBeacon = function () {
    let result = { id: this.id, name: this.name, is_active: this.isActive }
    if (this.Shop) result.shop = this.Shop.name
    return result
  }
  /**
   * @method toQr
   * @description Beacon apprpriate data
   * @returns {object} - Validator information
   *
   * @instance
   */
  Validator.prototype.toQr = function () {
    let result = { id: this.id, name: this.name, is_active: this.isActive }
    if (this.Shop) result.shop = this.Shop.name
    return result
  }
  /**
   * @method toAdminBeacon
   * @description Beacon admin apprpriate data
   * @returns {object} - Validator information
   *
   * @instance
   */
  Validator.prototype.toAdminBeacon = function () {
    let result = {
      id: this.id,
      merchant_id: this.merchantId,
      shop_id: this.shopId,
      name: this.name,
      uid: this.uid,
      major: this.major,
      minor: this.minor,
      is_active: this.isActive
    }
    if (this.Merchant) result.merchant = this.Merchant.name
    if (this.Shop) result.shop = this.Shop.name
    return result
  }
  /**
   * @method toAdminQr
   * @description QR admin apprpriate data
   * @returns {object} - Validator information
   *
   * @instance
   */
  Validator.prototype.toAdminQr = function () {
    let result = {
      id: this.id,
      merchant_id: this.merchantId,
      shop_id: this.shopId,
      name: this.name,
      image_url: this.imageUrl,
      is_active: this.isActive
    }
    if (this.Merchant) result.merchant = this.Merchant.name
    if (this.Shop) result.shop = this.Shop.name
    return result
  }
  return Validator
}
