/**
 * Kazam Api
 * @name Sequelize UserPunchCard Definition
 * @file models/user-punch-card.js
 * @author Joel Cano
 */

'use strict'

const map = require('async/map')
const Promise = require('bluebird')
const errors = require('../lib/error')
const decorator = require('../lib/decorate')
const UnVerifiedError = errors.UnVerifiedError
const LimitReachedError = errors.LimitReachedError
const uuidField = require('../lib/sequelize/uuid')
const identityField = require('../lib/sequelize/identity')

// constants
const Identity = { InProgress: 0, Ready: 1, Redeemed: 2, Expired: 3 }
const Identities = [Identity.InProgress, Identity.Ready, Identity.Redeemed]

const AlertNotificationSent = { First: 0, Second: 1 }

module.exports = function (sequelize, DataTypes) {
  let UserPunchCard = sequelize.define(
    // Model name
    'UserPunchCard',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      userId: {
        allowNull: false,
        field: 'user_id',
        type: DataTypes.BIGINT
      },
      punchCardId: {
        allowNull: true,
        field: 'punch_card_id',
        type: DataTypes.BIGINT
      },
      identity: identityField(DataTypes, Identity.InProgress, Identities),
      punchCount: {
        defaultValue: 0,
        allowNull: false,
        field: 'punch_count',
        type: DataTypes.INTEGER
      },
      alertNotifcationSent: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'alert_notifcation_sent'
      },
      identifier: uuidField(DataTypes)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'user_punch_cards',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method activeForUser
         * @description Returns query for non redeemed user punch cards
         * @param {integer} userId - User id
         * @returns {object} - query structure
         *
         * @static
         */
        activeForUser: function (userId) {
          // const Op = sequelize.Op
          return {
            attributes: [
              'id',
              'userId',
              'punchCount',
              'identity',
              'punchCardId'
            ],
            where: {
              userId: userId,
              identity: { $or: [Identity.InProgress, Identity.Ready] }
            }
          }
        },
        /**
         * @method activePunchCard
         * @description Returns query for user punch cards from active
         *  punch cards
         * @returns {object} - query structure
         *
         * @static
         */
        activePunchCard: function () {
          const Op = sequelize.Op
          const PunchCard = sequelize.models.PunchCard
          return {
            include: [
              {
                required: true,
                model: PunchCard,
                attributes: ['prize', 'punchLimit', 'expiresAt'],
                where: {
                  identity: PunchCard.Identity.Published,
                  expiresAt: { [Op.gt]: new Date() }
                },
                include: [
                  {
                    required: true,
                    model: sequelize.models.Merchant,
                    attributes: ['name', 'imageUrl']
                  }
                ]
              }
            ]
          }
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * Identity Enum
   * @type {Object}
   */
  UserPunchCard.Identity = Identity
  /**
   * AlertNotificationSent Enum
   * @type {Object}
   */
  UserPunchCard.AlertNotificationSent = AlertNotificationSent
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  UserPunchCard.associate = function (models) {
    UserPunchCard.belongsTo(models.User, { foreignKey: 'userId' })
    UserPunchCard.belongsTo(models.PunchCard, { foreignKey: 'punchCardId' })
    UserPunchCard.hasMany(models.UserPunchCardInvite, {
      foreignKey: 'userPunchCardId'
    })
    UserPunchCard.hasMany(models.UserPunchCardValidation, {
      foreignKey: 'userPunchCardId'
    })
  }
  /**
   * @method toApiList
   * @description Returns apprpriate api data
   * @returns {Promise<array>} - UserPunchCard array promise
   *
   * @static
   */
  UserPunchCard.toApiList = 'toApiList'
  /**
   * @method toApiMerchant
   * @description Returns apprpriate merchant api data
   * @returns {Promise<array>} - UserPunchCard array promise
   *
   * @static
   */
  UserPunchCard.toApiMerchant = 'toApiMerchant'
  UserPunchCard.decorate = decorator
  /**
   * @method punchOrder
   * @description Base PunchCard Order
   * @returns {array} - query template
   *
   * @static
   */
  UserPunchCard.punchOrder = [ ['identity', 'DESC'], ['punchCount', 'DESC'] ]
  /**
   * @method apiList
   * @description Returns a list of UserPunchCards
   * @param {User} user - User instance
   * @return {Promise<array>} Returns a UserPunchCard array promise
   *
   * @static
   */
  UserPunchCard.apiList = function (user) {
    const Op = sequelize.Op
    const Merchant = sequelize.models.Merchant
    const PunchCard = sequelize.models.PunchCard
    const order = this.punchOrder
    order.push([PunchCard, Merchant, 'name', 'ASC'])
    const query = {
      include: [
        {
          required: true,
          model: PunchCard,
          attributes: ['prize', 'punchLimit', 'expiresAt', 'validationLimit'],
          where: {
            identity: PunchCard.Identity.Published,
            expiresAt: { [Op.gt]: new Date() }
          },
          include: [{ required: true, model: Merchant.scope('punchCard') }]
        }
      ],
      order: order
    }
    return this.scope({ method: ['activeForUser', user.id] }).findAll(query)
  }
  /**
   * @method ready
   * @description Returns a list of ready UserPunchCards filtered by user
   * @param {User} user - User instance
   * @return {Promise<array>} Returns a UserPunchCard array promise
   *
   * @static
   */
  UserPunchCard.ready = function (user) {
    const Op = sequelize.Op
    const Merchant = sequelize.models.Merchant
    const PunchCard = sequelize.models.PunchCard
    const query = {
      attributes: ['id', 'punchCount', 'identity'],
      where: {
        userId: user.id,
        identity: Identity.Ready
      },
      include: [
        {
          required: true,
          model: PunchCard,
          attributes: ['prize', 'punchLimit', 'expiresAt'],
          where: {
            identity: PunchCard.Identity.Published,
            expiresAt: { [Op.gt]: new Date() }
          },
          include: [{ required: true, model: Merchant.scope('punchCard') }]
        }
      ],
      order: [['punchCount', 'ASC'], [PunchCard, Merchant, 'name', 'ASC']]
    }
    return this.findAll(query)
  }
  /**
   * @method merchant
   * @description Returns a list of UserPunchCards filtered by merchant
   * @return {Promise<array>} Returns a UserPunchCard array promise
   *
   * @static
   */
  UserPunchCard.merchant = function (merchantId, user) {
    const Op = sequelize.Op
    const PunchCard = sequelize.models.PunchCard
    const query = {
      include: [{
        required: true,
        model: PunchCard,
        attributes: [
          'prize',
          'terms',
          'rules',
          'punchLimit',
          'expiresAt',
          'validationLimit'
        ],
        where: {
          merchantId: merchantId,
          identity: PunchCard.Identity.Published,
          expiresAt: { [Op.gt]: new Date() }
        }
      }],
      order: [['identity', 'DESC'], ['punchCount', 'ASC']]
    }
    return this.scope({ method: ['activeForUser', user.id] }).findAll(query)
  }
  /**
   * @method  addValidationDate
   * @description Checks if the user punch card can be validate and adds the
   *  limit date if needed.
   * @param {array} elements - Incoming request params
   * @return {Promise<array>} Returns a UserPunchCard array
   *
   * @static
   */
  UserPunchCard.addValidationDate = function (elements) {
    return new Promise((resolve, reject) => {
      const cards = {}
      map(elements,
        function (item, cbk) {
          let can = cards[item.punchCardId]
          if (can === undefined) {
            sequelize.models.UserPunchCardValidation.canValidate(item)
              .then(function (result) {
                cards[item.punchCardId] = result
                if (!result) {
                  item.validationDate = item.PunchCard.validationLimitEnd()
                }
                cbk(null, item)
              })
          } else {
            if (!can) item.validationDate = item.PunchCard.validationLimitEnd()
            cbk(null, item)
          }
        },
        function (err, results) {
          if (err) reject(err)
          else resolve(results)
        }
      )
    })
  }
  /**
   * @method getValidator
   * @description Returns a validator according to incoming parameters
   * @param {object} params - Incoming request params
   * @param {integer} merchantId - Merchant id
   * @param {object} t - sequelize transaction instance
   * @return {Promise<Validator>} Returns a Validator promise
   *
   * @static
   */
  UserPunchCard.getValidator = function (params, merchantId, t) {
    const Validator = sequelize.models.Validator
    if (params.uid && params.major !== null && params.minor !== null) {
      return Validator.validateBeacon(
        params.uid,
        params.major,
        params.minor,
        merchantId,
        t
      )
    } else if (params.token) {
      return Validator.validateQR(params.token, merchantId, t)
    } else throw new Error('missing_validator_data')
  }
  /**
   * @method punch
   * @description Adds a new punch to the user's punchcard
   * @param {integer} id - UserPunchCard id
   * @param {integer} merchantId - Merchant id
   * @param {object} params - Incoming request params
   * @param {User} user - User instance
   * @return {Promise<UserPunchCard>} Returns a UserPunchCard promise
   *
   * @static
   */
  UserPunchCard.punch = function (id, merchantId, params, user) {
    return sequelize.transaction((t) => {
      const err = new Error('not_found')
      this.logger.info({ action: 'punch' }, id, params)
      if (isNaN(id) || isNaN(merchantId)) throw err
      else {
        this.logger.debug('punching', params)
        let validatorPromise = this.getValidator(params, merchantId, t)
        const Op = sequelize.Op
        const models = sequelize.models
        const PunchCard = models.PunchCard
        let cardPromise = this.findOne({
          where: { id: id, userId: user.id, identity: Identity.InProgress },
          include: [{
            required: true,
            model: PunchCard,
            attributes: [
              'merchantId',
              'punchLimit',
              'validationLimit',
              'validationLimitType'
            ],
            where: {
              merchantId: merchantId,
              identity: PunchCard.Identity.Published,
              expiresAt: { [Op.gt]: new Date() }
            }
          }],
          transaction: t
        })
        return Promise.join(
          cardPromise,
          validatorPromise,
          (userPunchCard, validator) => {
            if (userPunchCard && validator) {
              return [userPunchCard, validator]
            } else throw err
          }
        ).spread(function (userPunchCard, validator) {
          return models.UserPunchCardValidation.canValidate(userPunchCard, t)
            .then(function (can) {
              if (can) {
                return models.UserPunchCardValidation
                  .punch(validator, userPunchCard, t)
              } else throw new LimitReachedError('validation_limit')
            })
            .then(function () {
              return userPunchCard
            })
        }).then((userPunchCard) => {
          this.logger.info(
            { action: 'punch' },
            {
              id: userPunchCard.id,
              is_verified: user.isVerified,
              status: 'punched'
            }
          )
          ++userPunchCard.punchCount
          if (userPunchCard.punchCount >= userPunchCard.PunchCard.punchLimit) {
            // if ready generate new empty punch card
            userPunchCard.identity = Identity.Ready
            return Promise.join(
              userPunchCard.save({ transaction: t }),
              this.create({
                userId: userPunchCard.userId,
                punchCardId: userPunchCard.punchCardId
              }, { transaction: t }),
              function (old, current) { return old }
            )
          } else return userPunchCard.save({ transaction: t })
        })
      }
    })
  }
  /**
   * @method redeem
   * @description Redeems a user's punchcard
   * @param {integer} id - UserPunchCard id
   * @param {integer} merchantId - Merchant id
   * @param {object} params - Incoming request params
   * @param {User} user - User instance
   * @return {Promise<UserPunchCard>} Returns a UserPunchCard promise
   *
   * @static
   */
  UserPunchCard.redeem = function (id, merchantId, params, user) {
    return sequelize.transaction((t) => {
      /**
       * User can only redeem if verified
       */
      const envCheckVerification = process.env.KZM_CHECK_USER_VERIFICATION
      if (envCheckVerification === 'true' && !user.isVerified) {
        throw new UnVerifiedError('not_verified')
      }
      const err = new Error('not_found')
      this.logger.info({ action: 'redeem' }, id, params)
      if (isNaN(id) || isNaN(merchantId)) throw err
      else {
        this.logger.debug('redeeming', params)
        // Search for validator
        let validatorPromise = this.getValidator(params, merchantId, t)
        // Search for PunchCard
        const Op = sequelize.Op
        const models = sequelize.models
        const PunchCard = models.PunchCard
        let cardPromise = this.findOne({
          where: { id: id, userId: user.id, identity: Identity.Ready },
          include: [{
            required: true,
            model: PunchCard,
            attributes: [
              'merchantId',
              'prize',
              'terms',
              'rules',
              'punchLimit',
              'expiresAt',
              'redeemCode'
            ],
            where: {
              merchantId: merchantId,
              identity: PunchCard.Identity.Published,
              expiresAt: { [Op.gt]: new Date() }
            }
          }],
          transaction: t
        })
        return Promise.join(cardPromise, validatorPromise,
          (userPunchCard, validator) => {
            if (userPunchCard && validator) return [userPunchCard, validator]
            else throw err
          })
          .spread(function (userPunchCard, validator) {
            return models.UserPunchCardValidation
              .redeem(validator, userPunchCard, t)
              .then(function () { return userPunchCard })
          })
          .then((userPunchCard) => {
            userPunchCard.identity = Identity.Redeemed
            this.logger.info(
              { action: 'redeem' },
              {
                id: userPunchCard.id,
                is_verified: user.isVerified,
                status: 'redeemed'
              }
            )
            return userPunchCard.save({ transaction: t })
          })
      }
    })
  }
  /**
   * @method activePunchCountForMerchant
   * @description Total active punches for a merchant
   * @param {Merchant} merchant -  Merchant instance
   * @return {Promise<array>} Returns an array with total values
   *
   * @static
   */
  UserPunchCard.activePunchCountForMerchant = function (merchant) {
    const Op = sequelize.Op
    return this.findAll({
      attributes: [
        [
          sequelize.fn('SUM', sequelize.col('UserPunchCard.punch_count')),
          'total'
        ]
      ],
      where: { identity: { [Op.or]: [Identity.InProgress, Identity.Ready] } },
      include: [
        sequelize.models.PunchCard.activeForMerchantInclude(merchant.id)
      ],
      raw: true
    })
  }
  /**
   * @method readyPunchCountForMerchant
   * @description Total ready punches for a merchant
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<array>} Returns an array with total values
   *
   * @static
   */
  UserPunchCard.readyPunchCountForMerchant = function (merchant) {
    return this.findAll({
      attributes: [
        [sequelize.fn('COUNT', sequelize.col('UserPunchCard.id')), 'total']
      ],
      where: { identity: Identity.Ready },
      include: [
        sequelize.models.PunchCard.activeForMerchantInclude(merchant.id)
      ],
      raw: true
    })
  }
  /**
   * @method outstandingByGender
   * @description Return gender specific outstanding punches
   * @param {Merchant} merchant -  Merchant instance
   * @return {Promise<array>} Returns an array with total values
   *
   * @static
   */
  UserPunchCard.outstandingByGender = function (merchant) {
    const Op = sequelize.Op
    const models = sequelize.models
    const User = models.User
    const groupField = 'punchCount'
    return models.PunchCard.active(merchant)
      .then((card) => {
        if (card) {
          return this.findAll({
            attributes: [
              [sequelize.fn('COUNT', sequelize.col('User.id')), 'total'],
              groupField
            ],
            where: {
              punchCardId: card.id,
              punchCount: { [Op.gt]: 0 },
              identity: { [Op.or]: [Identity.InProgress, Identity.Ready] }
            },
            include: [{ required: true, model: User.scope('gender') }],
            group: [[groupField], [User, 'gender']],
            raw: true
          }).then(function (elements) {
            return [elements, card.dataValues.punch_limit]
          })
        } else return [[], 0]
      })
      .spread(function (elements, limit) {
        let result = { limit: limit, unspecified: {}, female: {}, male: {} }
        const length = elements.length
        const User = sequelize.models.User
        let element, key
        for (let i = 0; i < length; ++i) {
          element = elements[i]
          element.total = parseInt(element.total)
          key = element[groupField]
          switch (element['User.gender']) {
            case User.Gender.Female:
              result.female[key] = element.total
              break
            case User.Gender.Male:
              result.male[key] = element.total
              break
            default:
              result.unspecified[key] = element.total
          }
        }
        return result
      })
  }
  /**
   * @method expired
   * @description Finds all User PunchCards events in cronological order for a
   *  User
   * @param {User} user           User instance
   * @return {Promise<array>}     Returns an array of UserPunchCards
   *
   * @static
   */
  UserPunchCard.expired = function (user) {
    const limit = 20
    const Op = sequelize.Op
    const models = sequelize.models
    return this.findAll({
      attributes: ['id', 'identity', 'identifier'],
      where: {
        userId: user.id,
        punchCount: { [Op.gt]: 0 },
        identity: { [Op.not]: Identity.Redeemed }
      },
      include: [
        {
          required: true,
          model: models.PunchCard,
          attributes: ['expiresAt'],
          where: {
            identity: models.PunchCard.Identity.Published,
            expiresAt: { [Op.lt]: new Date() }
          },
          include: [{
            required: true,
            model: models.Merchant.scope('apiEvent')
          }]
        }
      ],
      limit: limit,
      raw: true
    })
  }
  /**
   * @method completed
   * @description Finds all User PunchCards events in cronological order for a
   *  User
   * @param {User} user           User instance
   * @return {Promise<array>}     Returns an array of UserPunchCards
   *
   * @static
   */
  UserPunchCard.completed = function (user) {
    const limit = 20
    const subQuery = `SELECT user_punch_card_validations.id
      FROM user_punch_card_validations
      WHERE user_punch_card_validations.user_punch_card_id = user_punch_cards.id
        AND user_punch_card_validations.identity = :validationIdentity
      ORDER BY user_punch_card_validations.created_at DESC
      LIMIT 1`
    const punchCards = `
      INNER JOIN punch_cards
        ON user_punch_cards.punch_card_id = punch_cards.id
        AND punch_cards.identity = :punchCardIdentity
        AND user_punch_cards.punch_count = punch_cards.punch_limit`
    const merchants = `
      INNER JOIN merchants ON punch_cards.merchant_id = merchants.id`
    const query = `
    SELECT
      user_punch_cards.id,
      user_punch_cards.identity,
      user_punch_cards.identifier,
      user_punch_card_validations.created_at,
      punch_cards.expires_at,
      merchants.name,
      merchants.image_url
    FROM user_punch_cards
    INNER JOIN user_punch_card_validations ON user_punch_card_validations.id = (
      ${subQuery}
    )
    ${punchCards}
    ${merchants}
    WHERE user_punch_cards.user_id = :userId
    LIMIT ${limit}`
    const models = sequelize.models
    return sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT,
      replacements: {
        userId: user.id,
        expiresAt: new Date(),
        punchCardIdentity: models.PunchCard.Identity.Published,
        validationIdentity: models.UserPunchCardValidation.Identity.Punch
      }
    })
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method toApiList
   * @description Returns apprpriate api list data
   * @returns {object} - UserPunchCard data
   *
   * @instance
   */
  UserPunchCard.prototype.toApiList = function () {
    const PunchCard = this.PunchCard
    const Merchant = PunchCard.Merchant
    const result = {
      id: this.id,
      identity: this.identity,
      prize: PunchCard.prize,
      punch_count: this.punchCount,
      punch_limit: PunchCard.punchLimit,
      merchant_id: Merchant.id,
      merchant_name: Merchant.name,
      merchant_image_url: Merchant.imageUrl,
      merchant_color: Merchant.color,
      category: Merchant.Category.name
    }
    if (this.validationDate) result.validation_date = this.validationDate
    return result
  }
  /**
   * @method toApiMerchant
   * @description Returns apprpriate merchant api data
   * @returns {object} - UserPunchCard data
   *
   * @instance
   */
  UserPunchCard.prototype.toApiMerchant = function () {
    let result = {
      id: this.id,
      identity: this.identity,
      prize: this.PunchCard.prize,
      punch_count: this.punchCount,
      punch_limit: this.PunchCard.punchLimit,
      expires_at: this.PunchCard.expiresAt,
      terms: this.PunchCard.terms,
      rules: this.PunchCard.rules
    }
    if (this.validationDate) result.validation_date = this.validationDate
    return result
  }
  /**
   * @method toPunch
   * @description Returns apprpriate merchant punch data
   * @returns {object} - UserPunchCard data
   *
   * @instance
   */
  UserPunchCard.prototype.toPunch = function () {
    return {
      id: this.id,
      identity: this.identity,
      punch_count: this.punchCount,
      punch_limit: this.PunchCard.punchLimit
    }
  }
  /**
   * @method toRedeem
   * @description Returns apprpriate merchant rdeem data
   * @returns {object} - UserPunchCard data
   *
   * @instance
   */
  UserPunchCard.prototype.toRedeem = function () {
    return {
      id: this.id,
      punch_count: this.punchCount,
      prize: this.PunchCard.prize,
      redeem_code: this.PunchCard.redeemCode,
      expires_at: this.PunchCard.expiresAt,
      terms: this.PunchCard.terms,
      rules: this.PunchCard.rules
    }
  }
  return UserPunchCard
}
