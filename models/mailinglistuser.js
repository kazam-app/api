/**
 * Kazam Api
 * @name Sequelize Mailing List User Definition
 * @file models/mailinglistuser.js
 * @author Joel Cano
 */

'use strict'

const mailchimp = require('../lib/mailchimp')
const fkField = require('../lib/sequelize/fk')
const stringField = require('../lib/sequelize/string')

const Interest = {
  Active: process.env.MAILCHIMP_USER_ACTIVE,
  Churned: process.env.MAILCHIMP_USER_CHURNED
}

module.exports = function (sequelize, DataTypes) {
  let MailingListUser = sequelize.define(
    // Model name
    'MailingListUser',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      mailingListId: fkField(DataTypes, false, 'mailing_list_id'),
      userId: fkField(DataTypes, false, 'user_id'),
      mailchimpId: stringField(DataTypes, true, 'mailchimp_id'),
      interest: stringField(DataTypes)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'mailing_list_users'
    }
  )
  /**
   * Class Methods
   *
   */
  MailingListUser.Interests = Interest
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  MailingListUser.associate = function (models) {
    MailingListUser.belongsTo(models.MailingList, {
      foreignKey: 'mailingListId'
    })
    MailingListUser.belongsTo(models.User, {
      foreignKey: 'userId'
    })
  }
  /**
   * @method createActive
   * @description create new instance with Interest Active set
   * @param {integer} userId - User id
   * @param {integer} mailingListId - MailingList id
   * @param {string} mailchimpId - Mailchimp API Member id
   * @return {Promise<MailingListUser>} MailingListUser promise
   *
   * @static
   */
  MailingListUser.createActive = function (userId, mailingListId, mailchimpId, t) {
    return this.create({
      userId: userId,
      mailingListId: mailingListId,
      mailchimpId: mailchimpId,
      interest: Interest.Active
    }, { transaction: t })
  }
  /**
   * @method getInterests
   * @description Returns interest values for a certain interest
   * @param {string} interest interest key to set
   * @returns {object} - Interest hash
   *
   * @static
   */
  MailingListUser.getInterests = function (interest) {
    let result = {}
    const keys = Object.keys(Interest)
    for (let i = 0; i < keys.length; ++i) {
      let element = keys[i]
      let current = Interest[element]
      result[current] = current === interest
    }
    return result
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method updateInterest
   * @description Changes the interest value to true
   * @param {string} interest current interest key
   * @returns {object} - Interest hash
   *
   * @instance
   */
  MailingListUser.prototype.updateInterest = function (interest) {
    const interests = MailingListUser.getInterests(interest)
    return mailchimp.updateMember(
      this.MailingList.mailchimpId,
      this.mailchimpId,
      { interests: interests }
    ).then(() => {
      this.interest = interest
      return this.save()
    })
  }
  return MailingListUser
}
