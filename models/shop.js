/**
 * Kazam Api
 * @name Sequelize Shop Definition
 * @file models/shop.js
 * @author Joel Cano
 */

'use strict'

const _ = require('lodash/math')
const Aws = require('../lib/aws')
const Promise = require('bluebird')
const geo = require('../lib/sequelize/geo')
const decorator = require('../lib/decorate')
const rawDecorator = require('../lib/rawdecorate')
const stringField = require('../lib/sequelize/string')

const LocationDistanceRadius = 4000.0
const distanceFieldName = 'distance'

module.exports = function (sequelize, DataTypes) {
  let Shop = sequelize.define(
    // Model name
    'Shop',
    /**
     * Model field definitions
     * @type {Object}
     */
    {
      merchantId: {
        allowNull: false,
        field: 'merchant_id',
        type: DataTypes.BIGINT
      },
      shopClusterId: {
        allowNull: true,
        type: DataTypes.BIGINT,
        field: 'shop_cluster_id'
      },
      name: stringField(DataTypes, false),
      isActive: {
        allowNull: false,
        field: 'is_active',
        defaultValue: false,
        type: DataTypes.BOOLEAN
      },
      location: stringField(DataTypes, false),
      city: stringField(DataTypes),
      postalCode: stringField(DataTypes, true, 'postal_code'),
      phone: stringField(DataTypes),
      state: stringField(DataTypes, false),
      country: {
        allowNull: false,
        defaultValue: 'MEX',
        type: DataTypes.STRING
      },
      lat: geo.lat(DataTypes),
      lng: geo.lng(DataTypes)
    },
    /**
     * Model options
     * @type {Object}
     */
    {
      underscored: true,
      tableName: 'shops',
      /**
       * scopes
       * @description Model scopes
       * @type {Object}
       *
       */
      scopes: {
        /**
         * @method name
         * @description Returns query for name
         * @returns {object} - query structure
         *
         * @static
         */
        name: { attributes: ['name'] },
        /**
         * @method active
         * @description Returns query for only active shops
         * @returns {object} - query structure
         *
         * @static
         */
        active: { where: { isActive: true } },
        /**
         * @method countByIdentity
         * @description Counts all user punchcard validations per shop for a
         *  merchant
         * @returns {object} - query structure
         *
         * @static
         */
        countByIdentity: function (merchantId, identity) {
          return {
            attributes: [
              'id',
              'name',
              [
                sequelize.fn(
                  'COUNT',
                  sequelize.col('Validators->UserPunchCardValidations.id')
                ),
                'total'
              ]
            ],
            where: { merchantId: merchantId },
            include: [
              {
                attributes: [],
                required: true,
                model: sequelize.models.Validator,
                include: [{
                  attributes: [],
                  required: true,
                  model: sequelize.models.UserPunchCardValidation,
                  where: { identity: identity }
                }]
              }
            ],
            group: [sequelize.col('Shop.id')]
          }
        }
      }
    }
  )
  /**
   * Class Methods
   *
   */
  /**
   * @method associate
   * @description Sequelize association initialze
   * @param {array} models - Sequelize model instance array
   *
   * @static
   */
  Shop.associate = function (models) {
    Shop.belongsTo(models.Merchant, { foreignKey: 'merchantId' })
    Shop.belongsTo(models.ShopCluster, { foreignKey: 'shopClusterId' })
    Shop.hasMany(models.Validator, { foreignKey: 'shopId' })
  }
  /**
   * @method list
   * @description Returns a list of Shops filtered by merchant
   * @param  {Merchant} merchant - Merchant instance
   * @return {Promise<array>} Returns an Shop array promise
   *
   * @static
   */
  Shop.list = function (merchant) {
    let query = {
      attributes: [
        'id',
        'merchant_id',
        'shop_cluster_id',
        'name',
        'is_active',
        'location',
        'postal_code',
        'phone',
        'city',
        'state',
        'country',
        'lat',
        'lng'
      ]
    }
    if (merchant) query.where = { merchantId: merchant.id }
    return this.findAll(query)
  }
  /**
   * @method show
   * @description Returns a Merchant by id and user
   * @param {integer} id - Merchant id
   * @param {OrganizationUser} user - OrganizationUser instance
   * @returns {Promise<Merchant>} - Returns a Merchant promise or not found
   *
   * @static
   */
  Shop.show = function (id, merchant) {
    return new Promise((resolve, reject) => {
      const err = new Error('not_found')
      if (isNaN(id)) reject(err)
      else {
        this.findOne({
          attributes: [
            'id',
            'merchantId',
            'shopClusterId',
            'name',
            'isActive',
            'location',
            'postalCode',
            'phone',
            'city',
            'state',
            'country',
            'lat',
            'lng'
          ],
          where: { id: id, merchantId: merchant.id }
        }).then(function (result) {
          if (result) resolve(result)
          else reject(err)
        })
      }
    })
  }
  /**
   * @method toApiList
   * @description Returns apprpriate api list data
   * @returns {Promise<array>} - Shop array promise
   *
   * @static
   */
  Shop.toApiList = 'toApiList'
  /**
   * @method toApiMerchant
   * @description Returns apprpriate api data
   * @returns {Promise<array>} - Merchant array promise
   *
   * @static
   */
  Shop.toApiMerchant = 'toApiMerchant'
  /**
   * @method toAdminList
   * @description Returns apprpriate admin list data
   * @returns {Promise<array>} - Merchant array promise
   *
   * @static
   */
  Shop.toAdminList = 'toAdminList'
  Shop.decorate = decorator
  /**
   * @method rawDecorate
   * @description Decorates items from a sequelize raw query
   * @returns {function} - Returns the processing function
   *
   * @static
   */
  Shop.rawDecorate = rawDecorator
  Shop.distanceField = function (lat, lng) {
    return [
      sequelize.fn(
        'earth_distance',
        sequelize.fn('ll_to_earth', lat, lng),
        sequelize.fn(
          'll_to_earth',
          sequelize.col('Shop.lat'),
          sequelize.col('Shop.lng')
        )
      ),
      distanceFieldName
    ]
  }
  Shop.distanceOrder = sequelize.literal(distanceFieldName + ' ASC')
  Shop.distanceFilter = function (lat, lng) {
    const earthDistance = sequelize.where(
      sequelize.fn(
        'earth_distance',
        sequelize.fn('ll_to_earth', lat, lng),
        sequelize.fn(
          'll_to_earth',
          sequelize.col('Shop.lat'),
          sequelize.col('Shop.lng')
        )
      ),
      '<',
      LocationDistanceRadius
    )
    const earthBox = sequelize.where(
      sequelize.fn('earth_box',
        sequelize.fn('ll_to_earth', lat, lng),
        LocationDistanceRadius
      ),
      ' @> ',
      sequelize.fn(
        'll_to_earth',
        sequelize.col('Shop.lat'),
        sequelize.col('Shop.lng')
      )
    )
    return { [sequelize.Op.and]: [earthDistance, earthBox] }
  }
  /**
   * @method optionalDistanceOrder
   * @description Api add optional Distance Order attributes to query
   * @returns {object} - Query structure
   *
   * @static
   */
  Shop.optionalDistanceOrder = function (query, lat, lng) {
    if (lat && lng) {
      lat = parseFloat(lat)
      lng = parseFloat(lng)
      if (!isNaN(lat) && !isNaN(lng)) {
        query.attributes.push(this.distanceField(lat, lng))
        query.order = this.distanceOrder
      }
    }
    return query
  }
  /**
   * @method toBreakdown
   * @description ToBreakdown list
   * @returns {array} - Shop array
   *
   * @static
   */
  Shop.toBreakdown = function (merchant) {
    return this.findAll({
      attributes: ['id', 'name'],
      where: { merchantId: merchant.id },
      raw: true
    })
  }
  /**
   * @method commonAttributes
   * @description Api commmon shop attributes
   * @returns {array} - String field array
   *
   * @static
   */
  Shop.commonAttributes = function () {
    return ['id', 'name', 'lat', 'lng']
  }
  /**
   * @method commonList
   * @description Api queries commmon shop attributes
   * @returns {object} - Query structure
   *
   * @static
   */
  Shop.commonList = function () {
    const models = sequelize.models
    let attrs = this.commonAttributes()
    attrs.push('merchantId')
    return {
      attributes: attrs,
      include: [
        { required: true, model: models.ShopCluster.scope('name') },
        {
          required: true,
          model: models.Merchant,
          attributes: ['name', 'color', 'imageUrl', 'budgetRating'],
          where: { isActive: true },
          include: [models.Category.scope('name')]
        }
      ]
    }
  }
  /**
   * @method apiList
   * @description Returns shops with optional location parameters
   * @param {float} lat - latitude
   * @param {float} lng - lng
   * @returns {Promise<array>} - Shop array promise
   *
   * @static
   */
  Shop.apiList = function (lat, lng) {
    let query = this.commonList()
    query = this.optionalDistanceOrder(query, lat, lng)
    return this.scope('active').findAll(query)
  }
  /**
   * @method near
   * @description Returns shops close to the provided location or all
   * @param {float} lat - latitude
   * @param {float} lng - lng
   * @returns {Promise<array>} - Shop array promise
   *
   * @static
   */
  Shop.near = function (lat, lng) {
    let query = this.commonList()
    // Apply only if valid floating poing numbers
    if (lat && lng) {
      lat = parseFloat(lat)
      lng = parseFloat(lng)
      if (!isNaN(lat) && !isNaN(lng)) {
        query.attributes.push(this.distanceField(lat, lng))
        query.where = this.distanceFilter(lat, lng)
        query.order = this.distanceOrder
      }
    }
    return this.scope('active').findAll(query)
  }
  /**
   * @method merchant
   * @description Returns filtered by merchant
   * @param {integer} merchantId - Merchant id
   * @param {float} lat - latitude
   * @param {float} lng - longitud
   * @returns {Promise<array>} - Shop array promise
   *
   * @static
   */
  Shop.merchant = function (merchantId, lat, lng) {
    const models = sequelize.models
    const merchant = {
      model: models.Merchant.scope('api'),
      include: [{ model: models.Category.scope('name') }]
    }
    const attrs = this.commonAttributes()
    let query = {
      attributes: attrs,
      include: [{ model: models.ShopCluster.scope('name') }, merchant],
      where: { merchantId: merchantId }
    }
    query = this.optionalDistanceOrder(query, lat, lng)
    return this.scope('active').findAll(query)
  }
  /**
   * @method updateFromData
   * @description Updates an Shop via id from request params
   * @param {integer} id - Shop identifier
   * @param {object} params - request parameters
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<Merchant>} Returns a Merchant instance
   *
   * @static
   */
  Shop.updateFromData = function (id, params, merchant) {
    return sequelize.transaction((t) => {
      const data = params.shop
      if (!isNaN(id) && data) {
        let update = {}
        if (data.name) update.name = data.name
        if (data.is_active !== undefined) update.isActive = data.is_active
        if (data.location) update.location = data.location
        if (data.postal_code) update.postalCode = data.postal_code
        if (data.phone) update.phone = data.phone
        if (data.city) update.city = data.city
        if (data.state) update.state = data.state
        if (data.country) update.country = data.country
        if (data.lat) update.lat = data.lat
        if (data.lng) update.lng = data.lng
        let query = { where: { id: id }, returning: true, transaction: t }
        if (merchant) query.where.merchantId = merchant.id
        return this.update(update, query).spread(function (updated, result) {
          if (updated > 0) return result[0]
          else throw new Error('not_found')
        })
      } else throw new Error('missing_shop_data')
    })
  }
  /**
   * @method createFromData
   * @description Creates a new Shop with request data
   * @param {object} params - request parameters
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<Shop>} Returns a Shop instance
   *
   * @static
   */
  Shop.createFromData = function (params, merchant) {
    return sequelize.transaction((t) => {
      const data = params.shop
      if (data) {
        const insert = {
          merchantId: merchant.id,
          isActive: data.is_active,
          name: data.name,
          location: data.location,
          postalCode: data.postal_code,
          phone: data.phone,
          city: data.city,
          state: data.state,
          country: data.country,
          lat: data.lat,
          lng: data.lng
        }
        return this.create(insert, { transaction: t }).then(function (shop) {
          return sequelize.models.Validator.generateQr(shop, merchant, t)
            .then(function (qr) {
              return shop
            })
        })
      } else throw new Error('missing_shop_data')
    })
  }
  /**
   * @method createFromData
   * @description Creates a new Shop with request data
   * @param {object} params - request parameters
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<Shop>} Returns a Shop instance
   *
   * @static
   */
  Shop.adminCreateFromData = function (params, merchant) {
    return sequelize.transaction((t) => {
      const data = params.shop
      if (data) {
        const insert = {
          merchantId: merchant.id,
          shopClusterId: data.shop_cluster_id,
          isActive: data.is_active,
          name: data.name,
          location: data.location,
          postalCode: data.postal_code,
          phone: data.phone,
          city: data.city,
          state: data.state,
          country: data.country,
          lat: data.lat,
          lng: data.lng
        }
        return this.create(insert, { transaction: t }).then(function (shop) {
          return sequelize.models.Validator.generateQr(shop, merchant, t)
            .then(function (qr) {
              return shop
            })
        })
      } else throw new Error('missing_shop_data')
    })
  }
  /**
   * @method adminUpdateFromData
   * @description Updates an Shop via id from request params
   * @param {integer} id - Shop identifier
   * @param {object} params - request parameters
   * @param {Merchant} merchant - Merchant instance
   * @return {Promise<Merchant>} Returns a Merchant instance
   *
   * @static
   */
  Shop.adminUpdateFromData = function (id, params, merchant) {
    return sequelize.transaction((t) => {
      const data = params.shop
      if (!isNaN(id) && data) {
        let update = {}
        if (data.name) update.name = data.name
        if (data.is_active !== undefined) update.isActive = data.is_active
        if (data.shop_cluster_id) update.shopClusterId = data.shop_cluster_id
        if (data.location) update.location = data.location
        if (data.postal_code) update.postalCode = data.postal_code
        if (data.phone) update.phone = data.phone
        if (data.city) update.city = data.city
        if (data.state) update.state = data.state
        if (data.country) update.country = data.country
        if (data.lat) update.lat = data.lat
        if (data.lng) update.lng = data.lng
        let query = { where: { id: id }, returning: true, transaction: t }
        if (merchant) query.where.merchantId = merchant.id
        return this.update(update, query).spread(function (updated, result) {
          if (updated > 0) return result[0]
          else throw new Error('not_found')
        })
      } else throw new Error('missing_shop_data')
    })
  }
  /**
   * @method adminDelete
   * @description Removes a Shop by id and merchant
   * @param {integer} id - Shop id
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  Shop.adminDelete = function (id, merchant) {
    return this.delete(id, merchant)
  }
  /**
   * @method delete
   * @description Removes a Shop by id and merchant
   * @param {integer} id - Shop id
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<boolean>} - Returns a boolean promise or not found
   *
   * @static
   */
  Shop.delete = function (id, merchant) {
    return sequelize.transaction((t) => {
      if (isNaN(id)) throw new Error('not_found')
      else {
        return this.destroy({
          where: { id: id, merchantId: merchant.id },
          transaction: t
        }).then(function (result) {
          if (result > 0) return true
          else throw new Error('not_found')
        })
      }
    })
  }
  /**
   * @method adminList
   * @description Returns a list of Shops filtered by merchant
   * @param  {Merchant} merchant - Merchant instance
   * @return {Promise<array>} Returns an Shop array promise
   *
   * @static
   */
  Shop.adminList = function (merchant) {
    return this.findAll({
      attributes: ['id', 'name'],
      where: { merchantId: merchant.id },
      include: [{ model: sequelize.models.ShopCluster.scope('name') }]
    })
  }
  /**
   * @method adminShow
   * @description Returns a Shop by id and user
   * @param {integer} id - Shop id
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<Shop>} - Returns a Shop promise or not found
   *
   * @static
   */
  Shop.adminShow = function (id, merchant) {
    return new Promise((resolve, reject) => {
      const err = new Error('not_found')
      if (isNaN(id)) reject(err)
      else {
        this.findOne({
          attributes: [
            'id',
            'merchantId',
            'shopClusterId',
            'name',
            'isActive',
            'location',
            'postalCode',
            'phone',
            'city',
            'state',
            'country',
            'lat',
            'lng'
          ],
          where: { id: id, merchantId: merchant.id }
        }).then(function (result) {
          if (result) resolve(result)
          else reject(err)
        })
      }
    })
  }
  /**
   * @method shopsByPunches
   * @description Returns shops for most redeems
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<array>} - Returns a Shop array
   *
   * @static
   */
  Shop.shopsByPunches = function (merchant) {
    return this.scope({
      method: [
        'countByIdentity',
        merchant.id,
        sequelize.models.UserPunchCardValidation.Identity.Punch
      ]
    }).findAll()
  }
  /**
   * @method shopsByRedeems
   * @description Returns shops for most redeems
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<array>} - Returns a Shop array
   *
   * @static
   */
  Shop.shopsByRedeems = function (merchant) {
    return this.scope({
      method: [
        'countByIdentity',
        merchant.id,
        sequelize.models.UserPunchCardValidation.Identity.Redeem
      ]
    }).findAll()
  }
  /**
   * @method topShops
   * @description Returns shops for most punches and redeems
   * @param {Merchant} merchant - Merchant instance
   * @returns {Promise<object>} - Returns a object
   *
   * @static
   */
  Shop.topShops = function (merchant) {
    return Promise.props({
      punches: this.shopsByPunches(merchant),
      redeems: this.shopsByRedeems(merchant)
    })
  }
  /**
   * @method toApiListUnique
   * @description Returns api list apprpriate data for a Shop
   * @param {object} item - Shop data
   * @returns {object} - Shop data
   *
   * @static
   */
  Shop.toApiListUnique = function (item) {
    return {
      id: item.id,
      merchant_id: item.merchantId,
      merchant_name: item['Merchant.name'],
      merchant_color: item['Merchant.color'],
      merchant_image_url: Aws.getUrl(item['Merchant.imageUrl']),
      merchant_background_image_url:
        Aws.getUrl(item['Merchant.backgroundImageUrl']),
      merchant_budget_rating: item['Merchant.budgetRating'],
      shop_cluster: item['ShopCluster.name'],
      category: item['Merchant.Category.name']
    }
  }
  /**
   * Instance Methods
   *
   */
  /**
   * @method distance
   * @description Returns a rounded distance value or placeholder
   * @returns {string} - distance value with measurement or -
   *
   * @instance
   */
  Shop.prototype.distance = function () {
    let distance = '-'
    const vals = this.dataValues
    if (vals.distance) {
      if (vals.distance > 5000) {
        distance = vals.distance / 1000
        distance = _.round(distance, 2) + 'km'
      } else distance = _.round(vals.distance, 2) + 'm'
    }
    return distance
  }
  /**
   * @method toApiMerchant
   * @description Returns api merchant apprpriate data for a Shop
   * @returns {object} - Shop data
   *
   * @instance
   */
  Shop.prototype.toApiMerchant = function () {
    return {
      id: this.id,
      distance: this.distance(),
      name: this.name,
      merchant_image_url: this.Merchant.imageUrl,
      merchant_budget_rating: this.Merchant.budgetRating,
      shop_cluster: this.ShopCluster.name,
      category: this.Merchant.Category.name,
      lat: this.lat,
      lng: this.lng
    }
  }
  /**
   * @method toApiList
   * @description Returns api list apprpriate data for a Shop
   * @returns {object} - Shop data
   *
   * @instance
   */
  Shop.prototype.toApiList = function () {
    return {
      id: this.id,
      distance: this.distance(),
      name: this.name,
      merchant_id: this.merchantId,
      merchant_name: this.Merchant.name,
      merchant_color: this.Merchant.color,
      merchant_image_url: this.Merchant.imageUrl,
      merchant_budget_rating: this.Merchant.budgetRating,
      shop_cluster: this.ShopCluster.name,
      category: this.Merchant.Category.name,
      lat: this.lat,
      lng: this.lng
    }
  }
  /**
   * @method toAdminList
   * @description Returns admin list apprpriate data for a Shop
   * @returns {object} - Shop information
   *
   * @instance
   */
  Shop.prototype.toAdminList = function () {
    return { id: this.id, name: this.name, shop_cluster: this.ShopCluster.name }
  }
  /**
   * @method toAdminShow
   * @description Returns admin show apprpriate data for a Shop
   * @returns {object} - Shop information
   *
   * @instance
   */
  Shop.prototype.toAdminShow = function () {
    return {
      id: this.id,
      merchant_id: this.merchantId,
      shop_cluster_id: this.shopClusterId,
      name: this.name,
      is_active: this.isActive,
      location: this.location,
      postal_code: this.postalCode,
      phone: this.phone,
      city: this.city,
      state: this.state,
      country: this.country,
      lat: this.lat,
      lng: this.lng
    }
  }
  /**
   * @method toShow
   * @description Returns show apprpriate data for a Shop
   * @returns {object} - Shop information
   *
   * @instance
   */
  Shop.prototype.toShow = function () {
    return {
      id: this.id,
      merchant_id: this.merchantId,
      shop_cluster_id: this.shopClusterId,
      name: this.name,
      is_active: this.isActive,
      location: this.location,
      postal_code: this.postalCode,
      phone: this.phone,
      city: this.city,
      state: this.state,
      country: this.country,
      lat: this.lat,
      lng: this.lng
    }
  }
  return Shop
}
