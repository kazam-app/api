/**
 * Kazam Api
 * @name Shipit Config
 * @file shipitfile.js
 * @author Joel Cano
 */

'use strict'

require('./config')

const uid = 'kazamapi'

module.exports = function (shipit) {
  require('shipit-deploy')(shipit)
  require('shipit-shared')(shipit)

  shipit.initConfig({
    default: {
      workspace: '/tmp/shipit',
      repositoryUrl: 'git+ssh://git@git.urbanity.xyz/kazam/api.git',
      ignores: ['.git', 'node_modules', 'docs'],
      keepReleases: 2,
      shallowClone: true,
      key: '~/.ssh/id_rsa',
      deleteOnRollback: false
    },
    stage: {
      deployTo: '/home/kazam/stage/kazam/api',
      servers: 'kazam@34.208.120.23',
      branch: 'master',
      shared: {
        overwrite: false,
        files: ['config/environments/.env', 'log/stage.log']
      }
    }
  })

  shipit.blTask('install', function () {
    // npm install --production
    return shipit.remote('cd ' + shipit.currentPath +
      ' && npm install --production -q'
    )
  })

  shipit.blTask('migrate', function () {
    // npm install --production
    return shipit.remote('cd ' + shipit.currentPath +
      ' && npm run migrate'
    )
  })

  shipit.blTask('forever-stop', function () {
    // Stop forever by uid
    return shipit.remote('forever stop ' + uid).catch(function (err) {
      console.log(err)
    })
  })

  shipit.blTask('forever-start', function () {
    // Start forever by uid
    const path = shipit.currentPath
    const cmd = 'cd ' + path +
      ' && forever start --uid ' + uid +
      ' --minUptime 5000 --spinSleepTime 5000 --append' +
      ' -l ' + path + '/log/' + shipit.environment + '.log' +
      ' index.js'
    return shipit.remote(cmd)
  })

  shipit.on('deployed', function () {
    shipit.start(['install', 'migrate', 'forever-stop', 'forever-start'])
  })
}
