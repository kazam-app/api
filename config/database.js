/**
 * Kazam Api
 * @name Database Configuration File
 * @file database.js
 * @author Joel Cano
 */

'use strict'

require('./index') // Requires dotenv config

const _ = require('lodash/object')
const env = process.env
const defaults = {
  host: env.DB_HOST,
  username: env.DB_USER,
  database: env.DB_NAME,
  password: env.DB_PASSWORD,
  dialect: env.DB_DIALECT,
  logging: (env.DB_LOGGING === undefined || env.DB_LOGGING === 'true') ? console.log : false
}

module.exports = {
  development: _.assign({}, defaults, {
    migrationStorage: 'json'
  }),
  test: _.assign({}, defaults, {
    migrationStorage: 'json',
    migrationStoragePath: 'sequelize-meta.test.json'
  }),
  gitlab: _.assign({}, defaults, {
    host: env.POSTGRES_PORT_5432_TCP_ADDR,
    migrationStorage: 'none'
  }),
  stage: _.assign({}, defaults, {
    migrationStorageTableName: 'sequelize_meta'
  }),
  production: _.assign({}, defaults, {
    migrationStorageTableName: 'sequelize_meta'
  })
}
