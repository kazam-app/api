/**
 * Kazam Api
 * @name Config Class Definition
 * @file config/index.js
 * @author Joel Cano
 */

'use strict'

const vars = process.env

/**
 * Config Constructor
 * @constructs Config
 */
function Config (file) {
  if (vars.NODE_ENV !== 'production') {
    const path = require('path')
    const dotenvSafe = require('dotenv-safe')
    const dotenvExpand = require('dotenv-expand')
    let env = ''
    if (!file) file = path.resolve(__dirname, 'environments', '.env')
    if (!vars.NODE_ENV) env = '.development'
    const silent = vars.SLIENT_CONFIG || false
    const empty = vars.ALLOW_EMPTY_CONFIG || false
    this.env = dotenvSafe.load({
      allowEmptyValues: empty,
      silent: silent,
      path: file + env,
      sample: file + '.example'
    })
    dotenvExpand(this.env.parsed)
  }
  this.db = require('./database')[vars.NODE_ENV || 'development']
  return this
}

module.exports = new Config()
