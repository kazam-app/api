'use strict'

module.exports = function () {
  const now = new Date()
  return [
    {
      name: 'Organization 1',
      created_at: now,
      updated_at: now
    },
    {
      name: 'Organization 2',
      created_at: now,
      updated_at: now
    }
  ]
}
