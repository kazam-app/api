'use strict'

const uuid = require('uuid/v4')

module.exports = function (org1, org2, category) {
  const now = new Date()
  return [
    {
      organization_id: org1.id,
      category_id: category.id,
      identifier_sk: uuid(),
      identifier_pk: uuid(),
      name: 'Merchant 1',
      invite_code: 'MARCA1',
      image_url: 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/19b8ce59-acf5-4114-828b-457a921480d9.jpg',
      created_at: now,
      updated_at: now
    },
    {
      organization_id: org2.id,
      category_id: category.id,
      identifier_sk: uuid(),
      identifier_pk: uuid(),
      name: 'Merchant 2',
      color: '#AAAAAA',
      image_url: 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/19b8ce59-acf5-4114-828b-457a921480d9.jpg',
      created_at: now,
      updated_at: now
    }
  ]
}
