'use strict'

module.exports = function (merchant1, merchant2) {
  const now = new Date()
  return [
    {
      merchant_id: merchant1.id,
      identity: 1,
      name: 'PunchCard 1',
      prize: 'Refill Gratis',
      rules: '5 punches',
      terms: 'Valido hasta el 3 de septiembre',
      punch_limit: 5,
      redeem_code: 'PUNCH1',
      expires_at: new Date(2017, 9, 3),
      created_at: now,
      updated_at: now
    },
    {
      merchant_id: merchant2.id,
      identity: 1,
      name: 'PunchCard 2',
      prize: 'Malteada Gratis',
      rules: '15 punches',
      terms: 'Valido hasta el 25 de agosto',
      punch_limit: 15,
      redeem_code: 'PUNCH2',
      expires_at: new Date(2017, 8, 25),
      created_at: now,
      updated_at: now
    }
  ]
}
