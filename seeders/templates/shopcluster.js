'use strict'

module.exports = function () {
  const now = new Date()
  return [{
    name: 'ShopCluster 1',
    location: 'Test Location',
    city: 'CDMX',
    postal_code: '01000',
    state: 'DF',
    country: 'MEX',
    lat: 19.4295566,
    lng: -99.2168844,
    created_at: now,
    updated_at: now
  }]
}
