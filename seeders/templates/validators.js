'use strict'

const uuid = require('uuid/v4')

module.exports = function (merchant1, merchant2, shop1, shop2) {
  const now = new Date()
  return [
    {
      merchant_id: merchant1.id,
      shop_id: shop1.id,
      is_active: true,
      identifier: uuid(),
      identity: 0, // Beacon iOS
      name: 'Beacon 1',
      uid: 'f7826da6-4fa2-4e98-8024-bc5b71e0893e',
      major: '11072',
      minor: '44058',
      created_at: now,
      updated_at: now
    },
    {
      merchant_id: merchant2.id,
      shop_id: shop2.id,
      is_active: true,
      identifier: uuid(),
      identity: 0, // Beacon Android
      name: 'Beacon 2',
      uid: 'f7826da6-4fa2-4e98-8024-bc5b71e0893e',
      major: '54390',
      minor: '23622',
      created_at: now,
      updated_at: now
    },
    {
      merchant_id: merchant1.id,
      shop_id: shop1.id,
      is_active: true,
      identifier: uuid(),
      identity: 1, // QR
      name: 'QR 1',
      token: 'b6cec1b0-d993-4ae3-9472-416485a1abd5',
      image_url: 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/qr-qr-7c54a896-968a-4dec-bce5-6840e1b5e014.gif',
      created_at: now,
      updated_at: now
    },
    {
      merchant_id: merchant2.id,
      shop_id: shop2.id,
      is_active: true,
      identifier: uuid(),
      identity: 1, // QR
      name: 'QR 2',
      token: '0872d903-58ef-4bf7-a54a-53a9b1ed89e6',
      image_url: 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/qr-qr-ff77bf27-443c-4191-9ae6-4c087cc512a5.gif',
      created_at: now,
      updated_at: now
    }
  ]
}
