'use strict'

const uuid = require('uuid/v4')

module.exports = function (user1, user2, punchCard1, punchCard2) {
  const now = new Date()
  return [
    {
      identity: 0,
      user_id: user1.id,
      punch_card_id: punchCard2.id,
      punch_count: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      identity: 0,
      user_id: user1.id,
      punch_card_id: punchCard2.id,
      punch_count: 1,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      identity: 1,
      user_id: user1.id,
      punch_card_id: punchCard1.id,
      punch_count: 5,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      identity: 0,
      user_id: user2.id,
      punch_card_id: punchCard1.id,
      punch_count: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      identity: 0,
      user_id: user2.id,
      punch_card_id: punchCard2.id,
      punch_count: 1,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      identity: 1,
      user_id: user2.id,
      punch_card_id: punchCard2.id,
      punch_count: 15,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    }
  ]
}
