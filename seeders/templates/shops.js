'use strict'

module.exports = function (merchant1, merchant2, shopCluster) {
  const now = new Date()
  return [
    {
      merchant_id: merchant1.id,
      shop_cluster_id: shopCluster.id,
      name: 'Shop 1',
      is_active: true,
      location: 'Test Location',
      phone: '52983890',
      city: 'CDMX',
      postal_code: '01000',
      state: 'DF',
      country: 'MEX',
      lat: 19.4295566,
      lng: -99.2168844,
      created_at: now,
      updated_at: now
    },
    {
      merchant_id: merchant2.id,
      shop_cluster_id: shopCluster.id,
      name: 'Shop 2',
      is_active: true,
      location: 'Test Location 2',
      phone: '52983890',
      city: 'CDMX',
      postal_code: '01000',
      state: 'DF',
      country: 'MEX',
      lat: 19.3399214,
      lng: -99.256796,
      created_at: now,
      updated_at: now
    }
  ]
}
