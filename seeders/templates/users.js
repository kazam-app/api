'use strict'

const uuid = require('uuid/v4')
const bcrypt = require('bcrypt')

module.exports = function () {
  const now = new Date()
  return [
    {
      is_valid: true,
      name: 'Working',
      last_names: 'Example',
      email: 'working@urban.mx',
      password: bcrypt.hashSync('12345678', 11),
      gender: 1,
      birthdate: new Date(1990, 8, 11),
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      is_valid: true,
      name: 'Working2',
      last_names: 'Example',
      email: 'working2@urban.mx',
      password: bcrypt.hashSync('12345678', 11),
      gender: 0,
      birthdate: new Date(2005, 8, 30),
      identifier: uuid(),
      created_at: now,
      updated_at: now
    }
  ]
}
