'use strict'

const uuid = require('uuid/v4')

module.exports = function (validator1, validator2, complete1, complete2, active1, active2) {
  const now = new Date()
  return [
    {
      validator_id: validator1.id,
      user_punch_card_id: active1.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      validator_id: validator2.id,
      user_punch_card_id: active2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    // 5 punches
    // {
    //   validator_id: validator1.id,
    //   user_punch_card_id: complete1.id,
    //   identity: 0,
    //   identifier: uuid(),
    //   created_at: now,
    //   updated_at: now
    // },
    {
      validator_id: validator1.id,
      user_punch_card_id: complete1.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      validator_id: validator1.id,
      user_punch_card_id: complete1.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      validator_id: validator1.id,
      user_punch_card_id: complete1.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    {
      validator_id: validator1.id,
      user_punch_card_id: complete1.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    // 15 punches
    { // 1
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 2
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 3
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 4
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 5
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 6
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 7
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 8
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 9
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 10
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 11
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 12
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 13
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 14
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    },
    { // 15
      validator_id: validator2.id,
      user_punch_card_id: complete2.id,
      identity: 0,
      identifier: uuid(),
      created_at: now,
      updated_at: now
    }
  ]
}
