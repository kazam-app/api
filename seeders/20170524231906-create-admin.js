'use strict'

const bcrypt = require('bcrypt')

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('organization_users', [{
      is_valid: true,
      name: 'Admin',
      last_names: 'LastAdmin',
      email: 'testadmin@urbanity.mx',
      password: bcrypt.hashSync('12345678', 11),
      created_at: new Date(),
      updated_at: new Date()
    }])
  },
  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('organization_users', null, {})
  }
}
