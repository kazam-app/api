'use strict'

require('../config')
const uuid = require('uuid/v4')
const bcrypt = require('bcrypt')
const Promise = require('bluebird')

module.exports = {
  up: function (queryInterface, Sequelize) {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'stage') {
      const now = new Date()
      return Promise.join(
        queryInterface.bulkInsert('categories', [{
          name: 'Category 1',
          image_url: 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/19b8ce59-acf5-4114-828b-457a921480d9.jpg',
          created_at: now,
          updated_at: now
        }], { returning: true }),
        queryInterface.bulkInsert('shop_clusters',
          require('./templates/shopcluster')(),
          { returning: true }
        ),
        queryInterface.bulkInsert('organizations',
          require('./templates/organizations')(),
          { returning: true }
        ),
        queryInterface.bulkInsert('users',
          require('./templates/users')(),
          { returning: true }
        ),
        function (categories, shopClusters, organizations, users) {
          const category = categories[0]
          const shopCluster = shopClusters[0]
          const org1 = organizations[0]
          const org2 = organizations[1]
          return queryInterface.bulkInsert('merchants',
            require('./templates/merchants')(org1, org2, category),
            { returning: true }
          ).then(function (merchants) {
            const merchant = merchants[0]
            return queryInterface.bulkInsert('organization_users', [{
              organization_id: merchant.organization_id,
              is_valid: true,
              name: 'John',
              last_names: 'Doe',
              email: 'test@urban.mx',
              password: bcrypt.hashSync('12345678', 11),
              created_at: new Date(),
              updated_at: new Date()
            }], { returning: true }).then(function (users) {
              return queryInterface.bulkInsert('organization_user_merchants', [{
                organization_user_id: users[0].id,
                merchant_id: merchant.id,
                role: 1, // ReadWrite
                created_at: new Date(),
                updated_at: new Date()
              }])
            }).then(function () {
              return [shopCluster, merchants, users]
            })
          })
        }
      ).spread(function (shopCluster, merchants, users) {
        const merchant1 = merchants[0]
        const merchant2 = merchants[1]
        return Promise.join(
          queryInterface.bulkInsert('punch_cards',
            require('./templates/punchcards')(merchant1, merchant2),
            { returning: true }),
          queryInterface.bulkInsert('shops',
            require('./templates/shops')(merchant1, merchant2, shopCluster),
            { returning: true }),
          function (punchCards, shops) {
            const user1 = users[0]
            const user2 = users[1]
            const punchCard1 = punchCards[0]
            const punchCard2 = punchCards[1]
            return queryInterface.bulkInsert(
              'user_punch_cards',
              require('./templates/userpunchcards')(user1, user2, punchCard1, punchCard2),
              { returning: true }
            ).then(function (userPunchCards) {
              return [merchant1, merchant2, shops[0], shops[1], user1, user2, userPunchCards]
            })
          }
        )
      }).spread(function (merchant1, merchant2, shop1, shop2, user1, user2, userPunchCards) {
        return queryInterface.bulkInsert('validators',
          require('./templates/validators')(merchant1, merchant2, shop1, shop2),
          { returning: true }
        ).then(function (validators) {
          return [merchant1, user1, user2, userPunchCards, validators]
        })
      }).spread(function (merchant1, user1, user2, userPunchCards, validators) {
        const validator1 = validators[0]
        const validator2 = validators[1]
        const active1 = userPunchCards[1]
        const active2 = userPunchCards[4]
        const complete1 = userPunchCards[2]
        const complete2 = userPunchCards[5]
        const now = new Date()
        return Promise.join(
          queryInterface.bulkInsert('user_punch_card_validations',
            require('./templates/userpunchcardvalidations')(validator1, validator2, complete1, complete2, active1, active2)
          ),
          queryInterface.bulkInsert('user_punch_card_invites', [
            {
              merchant_id: merchant1.id,
              user_punch_card_id: complete1.id,
              identifier: uuid(),
              created_at: now,
              updated_at: now
            }
          ])
        )
      }).catch(console.log)
    }
  },
  down: function (queryInterface, Sequelize) {
    // return queryInterface.bulkDelete('categories', null, {})
  }
}
