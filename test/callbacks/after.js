/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha After Hook Definition
 * @file test/callbacks/after.js
 * @author Joel Cano
 */

'use strict'

// after test suite
module.exports = after(function (done) {
  server.close(function () {
    done()
  })
})
