/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha After Each Hook Definition
 * @file test/callbacks/afterEach.js
 * @author Joel Cano
 */

'use strict'

module.exports = afterEach(function (done) {
  global.jwtOrganization = null
  global.jwtUser = null
  global.jwtPass = null
  global.jwtMerchant = null
  global.jwtToken = null
  done()
})
