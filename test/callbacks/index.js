/**
 * Kazam Api
 * @name Mocha Hooks
 * @file test/callbacks/index.js
 * @author Joel Cano
 */

'use strict'

require('./before')
require('./beforeEach')
require('./afterEach')
require('./after')
