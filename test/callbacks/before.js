/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Before Hook Definition
 * @file test/callbacks/before.js
 * @author Joel Cano
 */

'use strict'

const server = require('../../')

// Before Test Suite
module.exports = before(function (done) {
  global.server = server
  global.models = server.models
  done()
})
