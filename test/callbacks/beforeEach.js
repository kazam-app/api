/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Before Each Hook Definition
 * @file test/callbacks/beforeEach.js
 * @author Joel Cano
 */

'use strict'

const eachSeries = require('async/eachSeries')

module.exports = beforeEach(function (done) {
  eachSeries(server.models,
    function (model, cbk) {
      model.destroy({ truncate: true, cascade: true, force: true })
        .then(function () { cbk() })
        .catch(cbk)
    },
    done
  )
})
