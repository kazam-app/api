/**
 * Kazam Api
 * @name Mocha tests loader
 * @file test/test.js
 * @author Joel Cano
 */

'use strict'
// set test env
process.env.NODE_ENV = process.env.NODE_ENV || 'test'
// set server port
process.env.NODE_PORT = process.env.NODE_PORT || 5333
// empty config
process.env.ALLOW_EMPTY_CONFIG = process.env.ALLOW_EMPTY_CONFIG || true
// config
global.expect = require('chai').expect
// require all .test.js files
const files = require('glob').sync('./test/**/*.test.js', {})
for (let i = 0; i < files.length; ++i) require('../' + files[i])

// require('./lib/mailchimp.test.js')
// require('./models/userpunchcardvalidation.test.js')
// require('./models/userpunchcard.test.js')
// require('./models/user.test.js')
// require('./features/api/user.test.js')
