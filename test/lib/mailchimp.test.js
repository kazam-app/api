/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Mailchimp Test
 * @file test/lib/mailchimp.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const mailchimp = require('../../lib/mailchimp')

const list = process.env.MAILCHIMP_LIST || 'e24cfef922'
const member = 'f97f2d2758656eda03e87577dbf46444'

module.exports = describe('Mailchimp', function () {
  xdescribe('classMethods', function () {
    describe('#lists', function () {
      it('should return lists', function () {
        return mailchimp.lists().then(function (result) {
          console.log(result)
          expect(result).to.be.an('object')
        })
      })
    })

    xdescribe('#list', function () {
      it('should return lists', function () {
        return mailchimp.list(list).then(function (result) {
          expect(result).to.be.an('object')
        })
      })
    })

    xdescribe('#groups', function () {
      it('should return all groups related to a list', function () {
        return mailchimp.groups(list).then(function (result) {
          expect(result).to.be.an('object')
        })
      })
    })

    xdescribe('#updateMember', function () {
      it('should return all groups related to a list', function () {
        const params = { interests: { c2c63801c0: false, '181ad4f5bf': true } }
        return mailchimp.updateMember(list, member, params)
          .then(function (result) {
            expect(result).to.be.an('object')
          })
      })
    })

    xdescribe('#subscribe', function () {
      it('should subscribe a new member', function () {
        const member = { email_address: 'test+01@kzm.mx', status: 'subscribed' }
        return mailchimp.subscribe(list, member).then(function (result) {
          expect(result).to.be.an('object')
          expect(result.id).to.eq('MAILCHIMP_NOT_ACTIVE')
        })
      })
    })
  })
})
