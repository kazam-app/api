/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha User Feature Test
 * @file test/features/user.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const request = require('supertest')
const path = require('../helpers/path')
const session = require('../helpers/session')
const response = require('../helpers/response')
const userFactory = require('../helpers/factories/user')

module.exports = describe('UserController', function () {
  describe('GET /users/:id', function () {
    describe('authorization rules', function () {
      const testPath = path.org('/users/ABC')

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.org('/users/')

      beforeEach(session.fullRead)

      it('should fail to get a User with invalid id', function (done) {
        userFactory.model().spread(function (user) {
          request(server)
            .get(testPath + 'ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should get a User via identifier', function (done) {
        userFactory.model().spread(function (user) {
          request(server)
            .get(testPath + user.identifier)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const element = res.body
              expect(element).to.have.property('name', user.name)
              expect(element).to.have.property('last_names', user.lastNames[0])
              expect(element).to.have.property('email', user.email)
              expect(element).to.have.property('birthdate', user.birthdate)
              expect(element).to.have.property('merchant_total', 0)
              expect(element).to.have.property('weekly_average', 0)
              expect(element).to.have.property('monthly_average', 0)
              expect(element).to.include.keys(['top_shops', 'history'])
              expect(element.top_shops).to.be.an('array')
              expect(element.history).to.be.an('array')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
