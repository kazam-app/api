/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Analytics Feature Test
 * @file test/features/analytics.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const set = require('mocha-let')
const moment = require('moment')
const Promise = require('bluebird')
const request = require('supertest')
const session = require('../helpers/session')
const response = require('../helpers/response')
const validatorFactory = require('../helpers/factories/validator')
const analyticsValidator = require('../helpers/validators/analytics')
const userPunchCardFactory = require('../helpers/factories/userpunchcard')

module.exports = describe('AnalyticsController', function () {
  set('validateAnalytics', function () { return analyticsValidator })

  describe('GET /analytics', function () {
    beforeEach(session.fullRead)

    const testPath = '/analytics'

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return relevant merchant analytics data', function (done) {
      const orgId = jwtOrganization.id
      const merchId = jwtMerchant.id
      const future = moment().add(1, 'd')
      Promise.join(
        userPunchCardFactory.model(
          orgId,
          merchId,
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future // expiresAt
        ),
        validatorFactory.beacon(orgId, merchId),
        userPunchCardFactory.model(
          orgId,
          merchId,
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          null, // userId
          undefined, // identity
          5 // punches
        ),
        // userPunchCardFactory.model(
        //   orgId,
        //   merchId,
        //   undefined, // isPublic
        //   null, // identifier
        //   null, // Category
        //   null, // code
        //   null, // PunchCard
        //   models.PunchCard.Identity.Published, // identity published
        //   future, // expiresAt
        //   null, // userId
        //   models.UserPunchCard.Identity.Ready, // identity
        //   5 // punches
        // ),
        function (userPunchStruct, validatorStruct) {
          const userPunchCard = userPunchStruct[4]
          const beacon = validatorStruct[4]
          return Promise.join(
            models.UserPunchCardInvite.create({
              merchantId: merchId,
              userPunchCardId: userPunchCard.id
            }),
            models.UserPunchCardValidation.create({
              userPunchCardId: userPunchCard.id,
              validatorId: beacon.id
            }),
            function (inviteStruct, validationStruct) {
              ++userPunchCard.punchCount
              ++userPunchCard.punchCount
              return userPunchCard.save()
            })
        }
      ).then((card) => {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            this.validateAnalytics(res.body)
          }).end(response.end(done))
      }).catch(done)
    })
  })
})
