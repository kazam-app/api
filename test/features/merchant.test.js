/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Merchant Feature Test
 * @file test/features/merchant.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const image = require('../helpers/image')
const session = require('../helpers/session')
const response = require('../helpers/response')
const merchantFactory = require('../helpers/factories/merchant')
const categoryFactory = require('../helpers/factories/category')

module.exports = describe('MerchantController', function () {
  set('validateMerchantList', function () {
    return function (element, merchant) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', merchant.name)
      expect(element).to.have.property('image_url', merchant.imageUrl)
      expect(element).to.include.keys(['id', 'created_at'])
    }
  })

  set('validateMerchant', function () {
    return function (element, merchant) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', merchant.name)
      expect(element).to.have.property('image_url', merchant.imageUrl)
      expect(element).to.have.property('background_image_url', merchant.backgroundImageUrl)
      expect(element).to.include.keys('id')
    }
  })

  set('validateJsonMerchant', function () {
    return function (element, merchant) {
      const img = 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/no_aws.jpg'
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', merchant.name)
      expect(element).to.have.property('image_url', img)
      expect(element).to.have.property('background_image_url', img)
      expect(element).to.include.keys(['id'])
    }
  })

  // OrganizationUser
  beforeEach(session.simple)

  describe('GET /merchants', function () {
    const testPath = '/merchants'

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a merchant list associated with a merchant user', function (done) {
      Promise.join(
        merchantFactory.model(),
        merchantFactory.model(jwtUser.organizationId),
        function (invalid, valid) {
          valid = valid[1]
          return models.OrganizationUserMerchant.create({
            organizationUserId: jwtUser.id,
            merchantId: valid.id
          }).then(function () {
            return valid
          })
        })
        .then(function (valid) {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateMerchantList(body[0], valid)
            }.bind(this)).end(response.end(done))
        }.bind(this)).catch(done)
    })
  })

  describe('GET /merchants/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get('/merchants/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but invalid id', function (done) {
        request(server)
          .get('/merchants/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should return the current merchant', function (done) {
        request(server)
          .get('/merchants/' + jwtMerchant.id)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            const body = res.body
            this.validateMerchant(res.body, jwtMerchant)
            expect(body).to.have.property('invite_code')
            expect(body).to.have.property('category')
          }.bind(this)).end(response.end(done))
      })
    })
  })

  describe('POST /merchants', function () {
    const testPath = '/merchants'

    it('should expect valid authorization header', function (done) {
      request(server)
        .post(testPath)
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should fail creating a merchant with invalid params', function (done) {
      request(server)
        .post('/merchants')
        .set('Authorization', 'Bearer ' + jwtToken)
        .expect(response.status.badRequest)
        .expect(function (res) {
          response.badRequest(res.body, 'missing_merchant_data')
        }).end(response.end(done))
    })

    it('should create a new merchant with valid params', function (done) {
      categoryFactory.model().spread(function (category) {
        const validMerchant = {
          category_id: category.id,
          name: 'Test Merchant',
          color: '#000000',
          image: image.base64,
          background_image: image.base64
        }
        request(server)
          .post(testPath)
          .send({ merchant: validMerchant })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            this.validateJsonMerchant(res.body, validMerchant)
          }.bind(this)).end(response.end(done))
      }.bind(this)).catch(done)
    })
  })

  describe('PUT /merchants/:id', function () {
    it('should expect valid authorization header', function (done) {
      request(server)
        .put('/merchants/ABC')
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should fail updating a merchant with invalid data', function (done) {
      request(server)
        .put('/merchants/ABC')
        .set('Authorization', 'Bearer ' + jwtToken)
        .expect(response.status.badRequest)
        .expect(function (res) {
          response.badRequest(res.body, 'missing_merchant_data')
        }).end(response.end(done))
    })

    it('should fail updating a merchant with invalid id', function (done) {
      const validMerchant = { name: 'Updated Merchant', image: 'updated.png' }
      request(server)
        .put('/merchants/ABC')
        .send({ merchant: validMerchant })
        .set('Authorization', 'Bearer ' + jwtToken)
        .expect(response.status.badRequest)
        .expect(function (res) {
          response.badRequest(res.body, 'missing_merchant_data')
        }).end(response.end(done))
    })

    it('should fail updating a merchant with valid id from another organization', function (done) {
      merchantFactory.model().spread(function (organization, merchant) {
        const validMerchant = { name: 'Updated Merchant', image: 'updated.png' }
        request(server)
          .put('/merchants/' + merchant.id)
          .send({ merchant: validMerchant })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      }).catch(done)
    })

    it('should update a merchant with valid data', function (done) {
      merchantFactory.model(jwtUser.organizationId).spread(function (organization, merchant) {
        const validMerchant = { name: 'Updated Merchant', image: 'updated.png', background_image: 'updated.png' }
        request(server)
          .put('/merchants/' + merchant.id)
          .send({ merchant: validMerchant })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            const body = res.body
            this.validateJsonMerchant(body, validMerchant)
            expect(body).to.have.property('id', merchant.id)
          }.bind(this)).end(response.end(done))
      }.bind(this)).catch(done)
    })
  })

  describe('PUT /merchants/:id/copy', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put('/merchants/ABC/copy')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put('/merchants/ABC/copy')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail with invalid id', function (done) {
        merchantFactory.model().spread(function (org, merch) {
          request(server)
            .put('/merchants/ABC/copy')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_merchant_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail with valid id from another organization', function (done) {
        merchantFactory.model().spread(function (org, merch) {
          request(server)
            .put('/merchants/' + merch.id + '/copy')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail with valid id from the same organization but no permissions', function (done) {
        merchantFactory.model(jwtOrganization.id).spread(function (org, merch) {
          request(server)
            .put('/merchants/' + merch.id + '/copy')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail with valid id from the same merchant', function (done) {
        request(server)
          .put('/merchants/' + jwtMerchant.id + '/copy')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should copy the identifier with valid id', function (done) {
        merchantFactory.model(jwtOrganization.id, null, undefined, 'ABC')
          .spread(function (org, merch) {
            return models.OrganizationUserMerchant.create({
              organizationUserId: jwtUser.id,
              merchantId: merch.id,
              role: models.OrganizationUserMerchant.Role.ReadWrite
            }).then(function (result) {
              return merch
            })
          }).then(function (merch) {
            request(server)
              .put('/merchants/' + merch.id + '/copy')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect(function (res) {
                const body = res.body
                this.validateMerchant(body, jwtMerchant)
                expect(body).to.have.property('id', jwtMerchant.id)
                expect(body).to.have.property('identifier', merch.identifier)
              }.bind(this)).end(response.end(done))
          }.bind(this)).catch(done)
      })
    })
  })
})
