/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha QR Feature Test
 * @file test/features/qr.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../helpers/path')
const session = require('../helpers/session')
const response = require('../helpers/response')
const qrFactory = require('../helpers/factories/validator')
const beaconValidator = require('../helpers/validators/validator')

module.exports = describe('QrController', function () {
  set('validateQr', function () {
    return beaconValidator.beacon
  })

  describe('GET /qrs', function () {
    beforeEach(session.fullRead)

    const testPath = path.org('/qrs')

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(401)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a qr list filtered by merchant', function (done) {
      Promise.join(
        qrFactory.qr(),
        qrFactory.qr(jwtOrganization.id, jwtMerchant.id),
        function (invalid, validStruct) {
          const shop = validStruct[3]
          const qr = validStruct[4]
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect(function (res) {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateQr(body[0], qr, true, shop)
            }.bind(this)).end(response.end(done))
        }.bind(this)
      ).catch(done)
    })
  })

  describe('PUT /qrs/:id/toggle', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put('/qrs/ABC/toggle')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put('/qrs/ABC/toggle')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail with an invalid id', function (done) {
        request(server)
          .put('/qrs/ABC/toggle')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should toggle a paused beacon', function (done) {
        qrFactory.qr(
          jwtOrganization.id,
          jwtMerchant.id,
          undefined, // isPublic
          null, // identifier
          null, // scId
          null, // shopId
          null, // id
          false // isActive
        ).spread(function (org, merch, shopCluster, shop, qr) {
          request(server)
            .put('/qrs/' + qr.id + '/toggle')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect(function (res) {
              const body = res.body
              this.validateQr(body, qr, true, shop)
            }.bind(this)).end(response.end(done))
        }.bind(this)).catch(done)
      })

      it('should toggle an active beacon', function (done) {
        qrFactory.qr(jwtOrganization.id, jwtMerchant.id)
          .spread(function (org, merch, shopCluster, shop, qr) {
            request(server)
              .put('/qrs/' + qr.id + '/toggle')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect(function (res) {
                const body = res.body
                this.validateQr(body, qr, false, shop)
              }.bind(this)).end(response.end(done))
          }.bind(this)).catch(done)
      })
    })
  })
})
