/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Shop Feature Test
 * @file test/features/shop.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const session = require('../helpers/session')
const response = require('../helpers/response')
const shopFactory = require('../helpers/factories/shop')
const shopValidator = require('../helpers/validators/shop')
const merchantFactory = require('../helpers/factories/merchant')

module.exports = describe('ShopController', function () {
  set('getShop', function () {
    return function (orgId, merchId) {
      return merchantFactory.model(orgId, merchId)
        .spread(function (organization, merchant) {
          return models.Shop.create({
            merchantId: merchant.id,
            // shopClusterId: shopCluster.id,
            name: 'Test Shop',
            isActive: true,
            location: 'Test Location',
            city: 'Test City',
            postalCode: '01219',
            phone: '1234567890',
            state: 'DF',
            country: 'MX'
          })
        })
    }
  })

  set('validateShop', function () {
    return shopValidator.model
  })

  set('validateJsonShop', function () {
    return shopValidator.json
  })

  describe('GET /shops', function () {
    beforeEach(session.fullRead)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/shops')
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a list of Shops filtered by a merchant', function (done) {
      Promise.join(
        this.getShop(),
        this.getShop(jwtOrganization.id, jwtMerchant.id),
        (invalid, valid) => {
          request(server)
            .get('/shops')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateShop(body[0], valid, jwtMerchant)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })

  describe('GET /shops/:id', function () {
    beforeEach(session.fullRead)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/shops/ABC')
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should fail with valid token but invalid id', function (done) {
      request(server)
        .get('/shops/ABC')
        .set('Authorization', 'Bearer ' + jwtToken)
        .expect(response.status.badRequest)
        .expect(function (res) {
          response.badRequest(res.body, 'not_found')
        }).end(response.end(done))
    })

    it('should return a Shop filtered by a merchant with a valid id', function (done) {
      shopFactory.model(jwtOrganization.id, jwtMerchant.id)
        .spread((org, merch, shopCluster, shop) => {
          request(server)
            .get('/shops/' + shop.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              this.validateShop(body, shop, jwtMerchant, shopCluster)
            }).end(response.end(done))
        }).catch(done)
    })
  })

  describe('POST /shops', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post('/shops')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .post('/shops')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail to create a new shop with invalid params', function (done) {
        merchantFactory.model(jwtOrganization.id, jwtMerchant.id).spread(function (organization, merchant) {
          request(server)
            .post('/shops')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_shop_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should create a new shop with valid params', function (done) {
        merchantFactory.model(jwtOrganization.id, jwtMerchant.id).spread((organization, merchant) => {
          const validShop = {
            is_active: false,
            name: 'Test Shop',
            location: 'Test Location',
            city: 'CDMX',
            postal_code: '01219',
            phone: '551223446',
            state: 'DF'
          }
          request(server)
            .post('/shops')
            .send({ shop: validShop })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateJsonShop(res.body, validShop, merchant)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('PUT /shops/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put('/shops/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put('/shops/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      set('validShop', function () {
        return { name: 'Updated Shop', location: 'Updated Location' }
      })

      beforeEach(session.fullReadWrite)

      it('should fail updating a shop with invalid id', function (done) {
        request(server)
          .put('/shops/ABC')
          .send({ shop: this.validShop })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_shop_data')
          }).end(response.end(done))
      })

      it('should fail updating a shop with valid id from another organization', function (done) {
        this.getShop().then((shop) => {
          request(server)
            .put('/shops/' + shop.id)
            .send({ shop: this.validShop })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail updating a shop with valid id from another merchant', function (done) {
        this.getShop(jwtOrganization.id).then((shop) => {
          request(server)
            .put('/shops/' + shop.id)
            .send({ shop: this.validShop })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should update a shop with valid data', function (done) {
        shopFactory.model(jwtOrganization.id, jwtMerchant.id).spread((org, merch, shopCluster, shop) => {
          request(server)
            .put('/shops/' + shop.id)
            .send({ shop: this.validShop })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.have.property('id', shop.id)
            }).end((err, res) => {
              if (err) done(err)
              else {
                shop.reload().then(() => {
                  this.validateShop(res.body, shop, jwtMerchant, shopCluster)
                  done()
                })
              }
            })
        }).catch(done)
      })
    })
  })

  describe('DELETE /shops/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del('/shops/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .del('/shops/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail removing a shop with invalid id', function (done) {
        request(server)
          .del('/shops/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should fail updating a shop with valid id from another organization', function (done) {
        this.getShop().then(function (shop) {
          request(server)
            .del('/shops/' + shop.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail remove a shop with valid id from another merchant', function (done) {
        this.getShop(jwtOrganization.id).then(function (shop) {
          request(server)
            .del('/shops/' + shop.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should remove a shop with valid id', function (done) {
        this.getShop(jwtOrganization.id, jwtMerchant.id).then(function (shop) {
          request(server)
            .del('/shops/' + shop.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
