/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha UserPunchCardValidation Feature Test
 * @file test/features/userpunchcardvalidation.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const moment = require('moment')
const request = require('supertest')
const session = require('../helpers/session')
const response = require('../helpers/response')
const validatorFactory = require('../helpers/factories/validator')
const userPunchCardFactory = require('../helpers/factories/userpunchcard')

module.exports = describe('UserPunchCardValidationController', function () {
  describe('GET /breakdown', function () {
    beforeEach(session.fullRead)

    const testPath = '/breakdown'

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should fail without a range', function (done) {
      const orgId = jwtOrganization.id
      const merchId = jwtMerchant.id
      const current = '2018-02-01T06:00:00.000Z'
      validatorFactory.beacon(orgId, merchId).spread(function (org, merch, shopCluster, shop, beacon) {
        const UserPunchCardValidation = models.UserPunchCardValidation
        const future = moment(current).add(1, 'd')
        const validationLimit = 1
        return userPunchCardFactory.model(
          org.id, // Organization
          merch.id, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          null, // User
          models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
          1, // PunchCount
          validationLimit
        ).spread(function (org, merch, punch, user, userPunchCard) {
          return UserPunchCardValidation.create({
            validatorId: beacon.id, userPunchCardId: userPunchCard.id, created_at: moment(current)
          })
        }).then(function () {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_date_range')
            }).end(response.end(done))
        })
      }).catch(done)
    })

    it('should return breakdown information', function (done) {
      const current = '2018-02-01T06:00:00.000Z'
      const orgId = jwtOrganization.id
      const merchId = jwtMerchant.id
      validatorFactory.beacon(orgId, merchId).spread(function (org, merch, shopCluster, shop, beacon) {
        const UserPunchCardValidation = models.UserPunchCardValidation
        const future = moment(current).add(1, 'd')
        const validationLimit = 1
        return userPunchCardFactory.model(
          org.id, // Organization
          merch.id, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          null, // User
          models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
          1, // PunchCount
          validationLimit
        ).spread(function (org, merch, punch, user, userPunchCard) {
          return UserPunchCardValidation.create({
            validatorId: beacon.id, userPunchCardId: userPunchCard.id, created_at: moment(current)
          })
        }).then(function () {
          const startAt = moment(current)
          const endAt = moment(current).add(1, 'M')
          request(server)
            .get(testPath)
            .send({ starts_at: startAt.toISOString(), ends_at: endAt.toISOString() })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              const body = res.body
              expect(body).to.be.an('object')
              expect(body).to.have.keys(['shops', 'intervals'])
              expect(body.intervals).to.eq(4)
              const shops = body.shops
              expect(shops).to.be.an('array')
              expect(shops.length).to.eq(1)
              const resultShop = shops[0]
              expect(resultShop).to.be.an('object')
              expect(resultShop.id).to.eq(shop.id)
              expect(resultShop.name).to.eq(shop.name)
              const breakdown = resultShop.breakdown
              expect(breakdown).to.be.an('array')
              expect(breakdown.length).to.eq(body.intervals)
              const firstBreakdown = breakdown[0]
              expect(firstBreakdown).to.be.an('array')
              expect(firstBreakdown.length).to.eq(1)
              const secondBreakdown = breakdown[1]
              expect(secondBreakdown).to.be.eq(null)
              const thirdBreakdown = breakdown[2]
              expect(thirdBreakdown).to.be.eq(null)
              const fourthBreakdown = breakdown[3]
              expect(fourthBreakdown).to.be.eq(null)
            }).end(response.end(done))
        })
      }).catch(done)
    })
  })
})
