/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Beacon Feature Test
 * @file test/features/beacon.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../helpers/path')
const session = require('../helpers/session')
const response = require('../helpers/response')
const beaconFactory = require('../helpers/factories/validator')
const beaconValidator = require('../helpers/validators/validator')

module.exports = describe('BeaconController', function () {
  set('validateBeacon', function () {
    return beaconValidator.beacon
  })

  describe('GET /beacons', function () {
    beforeEach(session.fullRead)

    const testPath = path.org('/beacons')

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a beacon list filtered by merchant', function (done) {
      Promise.join(
        beaconFactory.beacon(),
        beaconFactory.beacon(jwtOrganization.id, jwtMerchant.id),
        (invalid, validStruct) => {
          const shop = validStruct[3]
          const beacon = validStruct[4]
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateBeacon(body[0], beacon, true, shop)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })

  describe('PUT /beacons/:id/toggle', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put('/beacons/ABC/toggle')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put('/beacons/ABC/toggle')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail with an invalid id', function (done) {
        request(server)
          .put('/beacons/ABC/toggle')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should toggle a paused beacon', function (done) {
        beaconFactory.beacon(
          jwtOrganization.id,
          jwtMerchant.id,
          undefined, // isPublic
          null, // identifier
          null, // scId
          null, // shopId
          null, // id
          false // isActive
        ).spread((org, merch, shopCluster, shop, beacon) => {
          request(server)
            .put('/beacons/' + beacon.id + '/toggle')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect((res) => {
              const body = res.body
              this.validateBeacon(body, beacon, true, shop)
            }).end(response.end(done))
        }).catch(done)
      })

      it('should toggle an active beacon', function (done) {
        beaconFactory.beacon(jwtOrganization.id, jwtMerchant.id)
          .spread((org, merch, shopCluster, shop, beacon) => {
            request(server)
              .put('/beacons/' + beacon.id + '/toggle')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect((res) => {
                const body = res.body
                this.validateBeacon(body, beacon, false, shop)
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })
})
