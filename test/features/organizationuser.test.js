/* eslint-env mocha */
/**
 * Kazam Api
 * @description Mocha Organization User Feature Test
 * @file test/features/organizationuser.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const session = require('../helpers/session')
const response = require('../helpers/response')
const merchantFactory = require('../helpers/factories/merchant')
const organizationUserFactory = require('../helpers/factories/organizationuser')
const organizationUserValidator = require('../helpers/validators/organizationuser')

module.exports = describe('OrganizationUserController', function () {
  set('validateUser', function () {
    return organizationUserValidator.model
  })

  set('validateProfile', function () {
    return organizationUserValidator.profile
  })

  set('validateJwtUser', function () {
    return organizationUserValidator.json
  })

  describe('Sessionless features', function () {
    describe('POST /sign_up', function () {
      it('should return a badrequest error with incorrect params', function (done) {
        request(server)
          .post('/sign_up')
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_user_data')
          }).end(response.end(done))
      })

      it('should fail without organization data', function (done) {
        request(server)
          .post('/sign_up')
          .send({ user: {} })
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_organization_data')
          }).end(response.end(done))
      })

      it('should fail with invalid invite token', function (done) {
        request(server)
          .post('/sign_up')
          .send({ user: {}, token: 'ABC' })
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'invalid_token')
          }).end(response.end(done))
      })

      it('should return fail with invalid data', function (done) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: '',
          email: 'INVALID_EMAIL',
          password: pass,
          password_confirmation: pass
        }
        const params = {
          user: validUser,
          organization: { name: 'Test Organization' }
        }
        request(server)
          .post('/sign_up')
          .send(params)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.validation(res.body, {
              last_names: ['empty'],
              email: ['invalid_email']
            })
          }).end(response.end(done))
      })

      it('should return a token with correct invite params', function (done) {
        organizationUserFactory.model().spread(function (org, user) {
          return models.OrganizationUserInvite.create({
            organizationUserId: user.id,
            email: 'test@u360.test'
          })
        }).then(function (invite) {
          const pass = '12345678'
          const validUser = {
            name: 'Name',
            last_names: 'Last Names',
            password: pass,
            password_confirmation: pass
          }
          const params = {
            user: validUser,
            token: invite.jwt()
          }
          request(server)
            .post('/sign_up')
            .send(params)
            .expect(response.status.success)
            .expect(function (res) {
              const body = res.body
              expect(body).to.be.an('object')
              expect(body).to.include.keys('token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return token with correct params', function (done) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: 'Last Names',
          email: 'foo@bar.com',
          password: pass,
          password_confirmation: pass
        }
        const params = {
          user: validUser,
          organization: { name: 'Test Organization' }
        }
        request(server)
          .post('/sign_up')
          .send(params)
          .expect(response.status.success)
          .expect(function (res) {
            const body = res.body
            expect(body).to.be.an('object')
            expect(body).to.include.keys('token')
          }).end(response.end(done))
      })
    })

    describe('POST /login', function () {
      beforeEach(session.simple)

      it('should return an badrequest error with incorrect params', function (done) {
        request(server)
          .post('/login')
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'login_fail')
          }).end(response.end(done))
      })

      it('should return an unauthorized error with correct params but incorrect values', function (done) {
        request(server)
          .post('/login')
          .send({ email: 'foo@bar.com', password: '123456' })
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'login_fail')
          }).end(response.end(done))
      })

      it('should return a user and token with correct params', function (done) {
        request(server)
          .post('/login')
          .send({ email: jwtUser.email, password: '12345678' })
          .expect(response.status.success)
          .expect((res) => {
            this.validateJwtUser(res.body, jwtUser)
          }).end(response.end(done))
      })
    })

    describe('POST /refresh', function () {
      it('should return an badrequest error with invalid token', function (done) {
        organizationUserFactory.model().spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Validate
          })
        }).then(function (token) {
          request(server)
            .post('/refresh')
            .send({ token: 'ABC' })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail with an unexpired token', function (done) {
        organizationUserFactory.model().spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Validate
          })
        }).then(function (token) {
          request(server)
            .post('/refresh')
            .send({ token: token.jwt() })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should succeed with a valid token', function (done) {
        organizationUserFactory.model().spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Validate,
            expiresAt: new Date(86400000)
          })
        }).then(function (token) {
          request(server)
            .post('/refresh')
            .send({ token: token.jwt() })
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })

    describe('POST /resend', function () {
      it('should return an badrequest error with invalid token', function (done) {
        organizationUserFactory.model().spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Validate
          })
        }).then(function (token) {
          request(server)
            .post('/resend')
            .send({ token: 'ABC' })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should succeed with a valid token', function (done) {
        organizationUserFactory.model().spread(function (org, user) {
          return Promise.join(
            models.OrganizationUserToken.create({
              organizationUserId: user.id,
              identity: models.OrganizationUserToken.Identity.SignUp
            }),
            models.OrganizationUserToken.create({
              organizationUserId: user.id,
              identity: models.OrganizationUserToken.Identity.Validate
            }),
            function (sign, validate) {
              return [sign, validate]
            }
          )
        }).spread(function (sign, validate) {
          request(server)
            .post('/resend')
            .send({ token: sign.jwt() })
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })

    describe('POST /validate', function () {
      it('should return an badrequest error with invalid token', function (done) {
        organizationUserFactory.model().spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Validate
          })
        }).then(function (token) {
          request(server)
            .post('/validate')
            .send({ token: 'ABC' })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return an badrequest error with an expired token', function (done) {
        organizationUserFactory.model(null, null, false).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Validate,
            expiresAt: new Date()
          })
        }).then(function (token) {
          request(server)
            .post('/validate')
            .send({ token: token.jwt() })
            .expect(response.status.gone)
            .expect(function (res) {
              response.gone(res.body, 'expired_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should validate a user with a correct token', function (done) {
        organizationUserFactory.model(null, null, false).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Validate
          }).then(function (token) {
            return [token, user]
          })
        }).spread((token, user) => {
          const data = {
            token: token.jwt(),
            password: '12345678',
            password_confirmation: '12345678'
          }
          request(server)
            .post('/validate')
            .send(data)
            .expect(response.status.success)
            .expect((res) => {
              this.validateJwtUser(res.body, user)
            }).end(response.end(done))
        }).catch(done)
      })
    })

    describe('POST /request', function () {
      const testUrl = '/request'

      it('should return success with invalid params', function (done) {
        request(server)
          .post(testUrl)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })

      it('should return success with invalid email', function (done) {
        organizationUserFactory.model(null, null, false)
          .spread(function (org, user) {
            request(server)
              .post(testUrl)
              .send({ email: user.email })
              .expect(response.status.success)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should return success with valid email and expired token', function (done) {
        organizationUserFactory.model(null, null, true).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Recovery,
            expiresAt: new Date()
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .post(testUrl)
            .send({ email: user.email })
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return success with valid email and existing token', function (done) {
        organizationUserFactory.model(null, null, true).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Recovery
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .post(testUrl)
            .send({ email: user.email })
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return success with valid email and no token', function (done) {
        organizationUserFactory.model(null, null, true)
          .spread(function (org, user) {
            request(server)
              .post(testUrl)
              .send({ email: user.email })
              .expect(response.status.success)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })
    })

    describe('PUT /recover', function () {
      const testUrl = '/recover'

      it('should return an badrequest error with invalid token', function (done) {
        organizationUserFactory.model(null, null, true).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Recovery
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .put(testUrl)
            .send({ token: 'ABC' })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return an badrequest error with an expired token', function (done) {
        organizationUserFactory.model(null, null, true).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Recovery,
            expiresAt: new Date()
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .put(testUrl)
            .send({ token: token.jwt() })
            .expect(response.status.gone)
            .expect(function (res) {
              response.gone(res.body, 'expired_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return an badrequest error with an unvalidated user', function (done) {
        organizationUserFactory.model(null, null, false).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Recovery
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .put(testUrl)
            .send({ token: token.jwt() })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'token_not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return an badrequest error with invalid params', function (done) {
        organizationUserFactory.model(null, null, true).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Recovery
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .put(testUrl)
            .send({ token: token.jwt(), password: '1234567890' })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'password_error')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should recover a user with a correct token', function (done) {
        organizationUserFactory.model(null, null, true).spread(function (org, user) {
          return models.OrganizationUserToken.create({
            organizationUserId: user.id,
            identity: models.OrganizationUserToken.Identity.Recovery
          }).then(function (token) {
            return [token, user]
          })
        }).spread((token, user) => {
          const data = { token: token.jwt(), password: 'ABCDEFGH', password_confirmation: 'ABCDEFGH' }
          request(server)
            .put(testUrl)
            .send(data)
            .expect(response.status.success)
            .expect((res) => {
              this.validateJwtUser(res.body, user)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('Sessionful features', function () {
    describe('GET /permissions/:relation', function () {
      // Organization User
      beforeEach(session.simple)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get('/permissions/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with a merchant from another organization', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          return models.OrganizationUserMerchant.create({
            organizationUserId: jwtUser.id,
            merchantId: merchant.id
          }).then(function () {
            return merchant
          })
        }).then(function (merchant) {
          request(server)
            .get('/permissions/' + merchant.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'login_fail')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return a user and token with correct token and valid merchant', function (done) {
        merchantFactory.model(jwtOrganization.id).spread(function (org, merchant) {
          return models.OrganizationUserMerchant.create({
            organizationUserId: jwtUser.id,
            merchantId: merchant.id
          }).then(function () {
            return merchant
          })
        }).then((merchant) => {
          request(server)
            .get('/permissions/' + merchant.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateJwtUser(res.body, jwtUser)
            }).end(response.end(done))
        }).catch(done)
      })
    })

    describe('POST /users/:email', function () {
      const testPath = '/users/'

      describe('authorization rules', function () {
        beforeEach(session.fullRead)

        it('should expect valid authorization header', function (done) {
          request(server)
            .post(testPath + 'test@test.com')
            .expect(response.status.unauthorized)
            .expect(function (res) {
              response.unauthorized(res.body)
            }).end(response.end(done))
        })

        it('should fail with valid token but wrong perms', function (done) {
          request(server)
            .post(testPath + 'test@test.com')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.unauthorized)
            .expect(function (res) {
              response.unauthorized(res.body, i18n.__('organization_auth_fail'))
            }).end(response.end(done))
        })
      })

      describe('feature', function () {
        beforeEach(session.fullReadWrite)

        it('should fail with an invalid email', function (done) {
          request(server)
            .post(testPath + 'ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'email_error')
            }).end(response.end(done))
        })

        it('should return false with an existing email', function (done) {
          const valid = {
            organizationUserId: jwtUser.id,
            email: 'invite@test.com'
          }
          models.OrganizationUserInvite.create(valid)
            .then(function (invite) {
              request(server)
                .post(testPath + valid.email)
                .set('Authorization', 'Bearer ' + jwtToken)
                .expect(response.status.success)
                .expect(function (res) {
                  expect(res.body).to.eql('false')
                }).end(response.end(done))
            }).catch(done)
        })

        it('should return true with a new email', function (done) {
          request(server)
            .post(testPath + 'invite@test.com')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        })
      })
    })

    describe('DELETE /users/:id', function () {
      describe('authorization rules', function () {
        beforeEach(session.fullRead)

        it('should expect valid authorization header', function (done) {
          request(server)
            .del('/users/ABC')
            .expect(response.status.unauthorized)
            .expect(function (res) {
              response.unauthorized(res.body)
            }).end(response.end(done))
        })

        it('should fail with valid token but only read role', function (done) {
          request(server)
            .del('/users/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.unauthorized)
            .expect(function (res) {
              response.unauthorized(res.body, i18n.__('organization_auth_fail'))
            }).end(response.end(done))
        })
      })

      describe('feature', function () {
        beforeEach(session.fullReadWrite)

        it('should fail with an invalid id', function (done) {
          request(server)
            .del('/users/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        })

        it('should fail with current user id', function (done) {
          request(server)
            .del('/users/' + jwtUser.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        })

        it('should fail with a valid id from another organization', function (done) {
          organizationUserFactory.model().spread(function (org, user) {
            request(server)
              .del('/users/' + user.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.badRequest)
              .expect(function (res) {
                response.badRequest(res.body, 'not_found')
              }).end(response.end(done))
          }).catch(done)
        })

        it('should delete a user with a valid id', function (done) {
          organizationUserFactory.model(jwtUser.organizationId).spread(function (org, user) {
            request(server)
              .del('/users/' + user.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
        })
      })
    })

    describe('DELETE /log_out', function () {
      beforeEach(session.fullRead)

      const testPath = '/log_out'

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should return true with correct params', function (done) {
        request(server)
          .del(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })
    })

    describe('GET /profile', function () {
      beforeEach(session.fullRead)

      const testPath = '/profile'

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should return the current organization user with a token', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            this.validateProfile(res.body, jwtUser, jwtOrganization)
          }).end(response.end(done))
      })
    })

    describe('PUT /profile', function () {
      beforeEach(session.fullRead)

      const testPath = '/profile'

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail updating without valid data', function (done) {
        request(server)
          .put(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_user_data')
          }).end(response.end(done))
      })

      it('should update the user with valid data', function (done) {
        const valid = { name: 'Name 2', last_names: 'LastName 2' }
        request(server)
          .put(testPath)
          .send({ user: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.have.property('id', jwtUser.id)
          }).end(function (err, res) {
            if (err) done(err)
            else {
              const body = res.body
              jwtUser.reload().then(function () {
                expect(body).to.be.an('object')
                expect(body).to.have.property('name', jwtUser.name)
                expect(body).to.have.property('last_names', jwtUser.lastNames)
                expect(body).to.have.property('email', jwtUser.email)
                expect(body).to.include.keys('id')
                expect(body).to.not.include.keys('password')
                done()
              }).catch(done)
            }
          })
      })
    })

    describe('PUT /profile/password', function () {
      beforeEach(session.fullRead)

      const testPath = '/profile/password'

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail updating without valid data', function (done) {
        request(server)
          .put(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_user_data')
          }).end(response.end(done))
      })

      it('should update the user with valid password data', function (done) {
        const valid = { password: '123456789', password_confirmation: '123456789' }
        request(server)
          .put(testPath)
          .send({ user: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.have.property('id', jwtUser.id)
          }).end(function (err, res) {
            if (err) done(err)
            else {
              const body = res.body
              jwtUser.reload().then(function () {
                expect(body).to.be.an('object')
                expect(body).to.have.property('name', jwtUser.name)
                expect(body).to.have.property('last_names', jwtUser.lastNames)
                expect(body).to.have.property('email', jwtUser.email)
                expect(body).to.include.keys('id')
                expect(body).to.not.include.keys('password')
                done()
              }).catch(done)
            }
          })
      })
    })
  })
})
