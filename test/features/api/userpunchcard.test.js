/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha UserPunchCard API Feature Test
 * @file test/features/api/userpunchcard.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const set = require('mocha-let')
const moment = require('moment')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const merchantFactory = require('../../helpers/factories/merchant')
const validatorFactory = require('../../helpers/factories/validator')
const userPunchCardFactory = require('../../helpers/factories/userpunchcard')
const userPunchCardValidator = require('../../helpers/validators/userpunchcard')

module.exports = describe('api/UserPunchCardController', function () {
  set('validateUserPunchCard', function () {
    return userPunchCardValidator.model
  })

  set('validateMerchantUserPunchCard', function () {
    return userPunchCardValidator.merchant
  })

  set('validatePunch', function () {
    return userPunchCardValidator.punch
  })

  set('validateRedeem', function () {
    return userPunchCardValidator.redeem
  })

  describe('GET /v1/punch_cards', function () {
    const testPath = path.api('/punch_cards')

    beforeEach(session.user)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a list of UserPunchCards filtered by a user', function (done) {
      const past = moment().subtract(1, 'd')
      const future = moment().add(1, 'd')
      const PunchCard = models.PunchCard
      const UserPunchCard = models.UserPunchCard
      Promise.join(
        userPunchCardFactory.model(),
        userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Unpublished, // identity un published
          future, // expiresAt
          jwtUser.id // User
        ),
        userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id, // User
          UserPunchCard.Identity.Redeemed // identity Redeemed
        ),
        userPunchCardFactory.model( // Valid Published Un redeemed
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id, // User
          UserPunchCard.Identity.Ready, // identity ready
          3 // UserPunchCard punchCount
        ),
        userPunchCardFactory.model( // Valid Published Un redeemed
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id // User
        ),
        userPunchCardFactory.model( // Valid Published Un redeemed finished
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Unpublished, // identity un published
          past, // expiresAt
          jwtUser.id // User
        ),
        userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          past, // expiresAt
          jwtUser.id, // User
          UserPunchCard.Identity.Ready // identity ready
        ),
        userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          past, // expiresAt
          jwtUser.id // User
        ),
        (invalid, invalid2, invalid3, valid, valid2) => {
          const cat = valid[5]
          const userPunch = valid[4]
          const punch = valid[2]
          const merch = valid[1]
          const cat1 = valid2[5]
          const userPunch1 = valid2[4]
          const punch1 = valid2[2]
          const merch1 = valid2[1]
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(2)
              // Order by identity DESC -> Ready first
              this.validateUserPunchCard(body[0], userPunch, punch, merch, cat)
              this.validateUserPunchCard(body[1], userPunch1, punch1, merch1, cat1)
            }).end(response.end(done))
        }
      ).catch(done)
    })

    it('should return a list of UserPunchCards filtered by a user with limited validation', function (done) {
      validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
        const UserPunchCardValidation = models.UserPunchCardValidation
        const future = moment().add(1, 'd')
        const validationLimit = 1
        return userPunchCardFactory.model(
          org.id, // Organization
          merch.id, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id, // User
          models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
          1, // PunchCount
          validationLimit
        ).spread(function (org, merch, punch, user, userPunchCard) {
          return UserPunchCardValidation.create({
            validatorId: beacon.id, userPunchCardId: userPunchCard.id
          }).then(function (result) {
            return [punch, userPunchCard]
          })
        }).spread(function (punch, userPunchCard) {
          expect(punch.validationLimit).to.eql(validationLimit)
          // console.log(result)
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              expect(body[0]).to.have.property('validation_date', punch.validationLimitEnd().toISOString())
            }).end(response.end(done))
        })
      }).catch(done)
    })
  })

  describe('GET /v1/punch_cards/ready', function () {
    const testPath = path.api('/punch_cards/ready')

    beforeEach(session.user)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a list of UserPunchCards filtered by a user', function (done) {
      const past = moment().subtract(1, 'd')
      const future = moment().add(1, 'd')
      const PunchCard = models.PunchCard
      const UserPunchCard = models.UserPunchCard
      Promise.join(
        userPunchCardFactory.model(), // invalid
        userPunchCardFactory.model( // invalid2
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Unpublished, // identity un published
          future, // expiresAt
          jwtUser.id // User
        ),
        userPunchCardFactory.model( // invalid3
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id, // User
          UserPunchCard.Identity.Redeemed // identity Redeemed
        ),
        userPunchCardFactory.model( // Valid Published Un redeemed
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id, // User
          UserPunchCard.Identity.Ready, // identity ready
          3 // UserPunchCard punchCount
        ),
        userPunchCardFactory.model( // Invalid4
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id // User
        ),
        userPunchCardFactory.model( // Valid Published Un redeemed finished
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Unpublished, // identity un published
          past, // expiresAt
          jwtUser.id // User
        ),
        userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          past, // expiresAt
          jwtUser.id, // User
          UserPunchCard.Identity.Ready // identity ready
        ),
        userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          PunchCard.Identity.Published, // identity published
          past, // expiresAt
          jwtUser.id // User
        ),
        (invalid, invalid2, invalid3, valid) => {
          const cat = valid[5]
          const userPunch = valid[4]
          const punch = valid[2]
          const merch = valid[1]
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              // Order by identity DESC -> Ready first
              this.validateUserPunchCard(body[0], userPunch, punch, merch, cat)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })

  describe('GET /v1/merchants/:merchant_id/punch_cards', function () {
    const testPath = path.api('/merchants/')

    beforeEach(session.user)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath + 'ABC/punch_cards')
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a list of UserPunchCards filtered by a user and merchant', function (done) {
      const past = moment().subtract(1, 'd')
      const future = moment().add(1, 'd')
      const PunchCard = models.PunchCard
      const UserPunchCard = models.UserPunchCard
      merchantFactory.model().spread((org, merch) => {
        Promise.join(
          userPunchCardFactory.model(),
          userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            PunchCard.Identity.Unpublished, // identity un published
            future, // expiresAt
            jwtUser.id // User
          ),
          userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id, // User
            UserPunchCard.Identity.Redeemed // identity Redeemed
          ),
          userPunchCardFactory.model( // Valid 1 new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id // User
          ),
          userPunchCardFactory.model( // Valid 2 punch card ready for redeem
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id, // User
            UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ),
          userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            PunchCard.Identity.Unpublished, // identity un published
            past, // expiresAt
            jwtUser.id // User
          ),
          userPunchCardFactory.model( // Already expired
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            PunchCard.Identity.Published, // identity published
            past, // expiresAt
            jwtUser.id, // User
            UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ),
          userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            PunchCard.Identity.Published, // identity published
            past, // expiresAt
            jwtUser.id // User
          ),
          (invalid, invalid2, invalid3, valid, valid2) => {
            const userPunch1 = valid[4]
            const punch1 = valid[2]
            const userPunch2 = valid2[4]
            const punch2 = valid2[2]
            request(server)
              .get(testPath + merch.id + '/punch_cards')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                const body = res.body
                expect(body).to.have.lengthOf(2)
                // Order by identity DESC -> Ready first
                this.validateMerchantUserPunchCard(body[1], userPunch1, punch1, merch)
                this.validateMerchantUserPunchCard(body[0], userPunch2, punch2, merch)
              }).end(response.end(done))
          }
        ).catch(done)
      })
    })
  })

  describe('POST /v1/merchants/:merchant_id/punch/:id', function () {
    const testPath = path.api('/merchants/')

    context('session features', function () {
      beforeEach(session.unVerifiedUser)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath + 'ABC/punch/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should add a punch via a beacon validator to an unVerified user', function (done) {
        validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id // User
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread((org, merch, punch, user, userPunchCard, beacon) => {
          request(server)
            .post(testPath + merch.id + '/punch/' + userPunchCard.id)
            .send({ uid: beacon.uid, major: beacon.major, minor: beacon.minor })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validatePunch(res.body, userPunchCard, punch)
            }).end(response.end(done))
        })
      })
    })

    context('feature', function () {
      beforeEach(session.user)

      it('should expect a valid merchant id', function (done) {
        merchantFactory.model().spread(function (org, merch) {
          request(server)
            .post(testPath + 'NOT_VALID/punch/NOT_VALID')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should expect a valid user punch card id', function (done) {
        merchantFactory.model().spread(function (org, merch) {
          request(server)
            .post(testPath + merch.id + '/punch/NOT_VALID')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail without validator data', function (done) {
        const future = moment().add(1, 'd')
        userPunchCardFactory.model( // Valid new punch card
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id // User
        ).spread(function (org, merch, punch, user, userPunchCard) {
          request(server)
            .post(testPath + merch.id + '/punch/' + userPunchCard.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_validator_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail with ready punch card', function (done) {
        validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          request(server)
            .post(testPath + merch.id + '/punch/' + userPunchCard.id)
            .send({ uid: beacon.uid, major: beacon.major, minor: beacon.minor })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail when validation limit is reached', function (done) {
        validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id, // User
            undefined,
            undefined,
            0
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread((org, merch, punch, user, userPunchCard, beacon) => {
          request(server)
            .post(testPath + merch.id + '/punch/' + userPunchCard.id)
            .send({ uid: beacon.uid, major: beacon.major, minor: beacon.minor })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.tooMany)
            .expect((res) => {
              response.tooMany(res.body, 'validation_limit')
            }).end(response.end(done))
        })
      })

      it('should add a punch via a beacon validator', function (done) {
        validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id // User
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread((org, merch, punch, user, userPunchCard, beacon) => {
          request(server)
            .post(testPath + merch.id + '/punch/' + userPunchCard.id)
            .send({ uid: beacon.uid, major: beacon.major, minor: beacon.minor })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validatePunch(res.body, userPunchCard, punch)
            }).end(response.end(done))
        })
      })

      it('should add a punch via a qr validator', function (done) {
        validatorFactory.qr().spread(function (org, merch, shopCluster, shop, qr) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id // User
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, qr]
          })
        }).spread((org, merch, punch, user, userPunchCard, qr) => {
          request(server)
            .post(testPath + merch.id + '/punch/' + userPunchCard.id)
            .send({ token: qr.token })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validatePunch(res.body, userPunchCard, punch)
            }).end(response.end(done))
        })
      })
    })
  })

  describe('POST /v1/merchants/:merchant_id/redeem/:id', function () {
    const testPath = path.api('/merchants/')

    context('session features', function () {
      beforeEach(session.unVerifiedUser)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath + 'ABC/redeem/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should expect a verified user', function (done) {
        const future = moment().add(1, 'd')
        userPunchCardFactory.model( // Valid new punch card
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id // User
        ).spread(function (org, merch, punch, user, userPunchCard) {
          process.env.KZM_CHECK_USER_VERIFICATION = 'true'
          request(server)
            .post(testPath + merch.id + '/redeem/' + userPunchCard.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.forbbiden)
            .expect(function (res) {
              response.forbbiden(res.body, 'not_verified')
            }).end(response.end(done))
        }).catch(done)
      })
    })

    context('feature', function () {
      beforeEach(session.user)

      it('should expect a valid merchant id', function (done) {
        merchantFactory.model().spread(function (org, merch) {
          request(server)
            .post(testPath + 'NOT_VALID/redeem/NOT_VALID')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should expect a valid user punch card id', function (done) {
        merchantFactory.model().spread(function (org, merch) {
          request(server)
            .post(testPath + merch.id + '/redeem/NOT_VALID')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail without validator data', function (done) {
        const future = moment().add(1, 'd')
        userPunchCardFactory.model( // Valid new punch card
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          jwtUser.id // User
        ).spread(function (org, merch, punch, user, userPunchCard) {
          request(server)
            .post(testPath + merch.id + '/redeem/' + userPunchCard.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_validator_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail with an in progress punch card', function (done) {
        validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id // User
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          request(server)
            .post(testPath + merch.id + '/redeem/' + userPunchCard.id)
            .send({ uid: beacon.uid, major: beacon.major, minor: beacon.minor })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should redeem a punch card via a beacon validator', function (done) {
        validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread((org, merch, punch, user, userPunchCard, beacon) => {
          request(server)
            .post(testPath + merch.id + '/redeem/' + userPunchCard.id)
            .send({ uid: beacon.uid, major: beacon.major, minor: beacon.minor })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateRedeem(res.body, userPunchCard, punch)
            }).end(response.end(done))
        })
      })

      it('should redeem a punch card via a qr validator', function (done) {
        validatorFactory.qr().spread(function (org, merch, shopCluster, shop, qr) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            jwtUser.id, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, qr]
          })
        }).spread((org, merch, punch, user, userPunchCard, qr) => {
          request(server)
            .post(testPath + merch.id + '/redeem/' + userPunchCard.id)
            .send({ token: qr.token })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateRedeem(res.body, userPunchCard, punch)
            }).end(response.end(done))
        })
      })
    })
  })
})
