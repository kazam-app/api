/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha About API Feature Test
 * @file test/features/api/about.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const request = require('supertest')
const path = require('../../helpers/path')
const response = require('../../helpers/response')

module.exports = describe('api/AboutController', function () {
  describe('GET /v1/version', function () {
    const testPath = path.api('/version')

    it('should return the app should be updated', function (done) {
      request(server)
        .get(testPath)
        .expect(response.status.success)
        .expect((res) => {
          const body = res.body
          expect(body).to.have.property('update', true)
        }).end(response.end(done))
    })

    it('should return the app should not be updated', function (done) {
      const version = '1.0.0'
      request(server)
        .get(testPath)
        .send({ version: version })
        .expect(response.status.success)
        .expect((res) => {
          const body = res.body
          expect(body).to.have.property('update', false)
        }).end(response.end(done))
    })
  })
})
