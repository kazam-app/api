/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Merchant API Feature Test
 * @file test/api/merchant.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const shopFactory = require('../../helpers/factories/shop')
const merchantFactory = require('../../helpers/factories/merchant')
const validatorFactory = require('../../helpers/factories/validator')

module.exports = describe('api/MerchantController', function () {
  set('validateMerchant', function () {
    return function (element, merchant, category) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', merchant.name)
      expect(element).to.have.property('image_url', merchant.imageUrl)
      expect(element).to.have.property('budget_rating', merchant.budgetRating)
      expect(element).to.have.property('color', merchant.color)
      expect(element).to.have.property('category', category.name)
      expect(element).to.include.keys('id')
    }
  })

  set('validateShowMerchant', function () {
    return function (element, merchant, category) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', merchant.name)
      expect(element).to.have.property('image_url', merchant.imageUrl)
      expect(element).to.have.property('budget_rating', merchant.budgetRating)
      expect(element).to.have.property('color', merchant.color)
      expect(element).to.have.property('category', category.name)
      expect(element).to.include.keys('id')
    }
  })

  describe('GET /v1/merchants', function () {
    const testPath = path.api('/merchants')

    beforeEach(session.user)

    it('should return a merchant list without auth', function (done) {
      merchantFactory.model().spread((org, merch, cat) => {
        request(server)
          .get(testPath)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            this.validateMerchant(body[0], merch, cat)
          }).end(response.end(done))
      }).catch(done)
    })

    it('should return a merchant list', function (done) {
      merchantFactory.model().spread((org, merch, cat) => {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            this.validateMerchant(body[0], merch, cat)
          }).end(response.end(done))
      }).catch(done)
    })
  })

  describe('GET /v1/merchants/near', function () {
    const testPath = path.api('/merchants/near')
    const location = { lat: 19.4286506, lng: -99.2159143 }

    beforeEach(session.user)

    it('should return a near merchant list without auth', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop, cat) => {
        request(server)
          .get(testPath)
          .send(location)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            const element = body[0]
            this.validateMerchant(element, merch, cat)
            expect(element).to.have.property('shop_id', shop.id)
            expect(element).to.have.property('shop_name', shop.name)
          }).end(response.end(done))
      }).catch(done)
    })

    it('should return a near merchant list', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop, cat) => {
        request(server)
          .get(testPath)
          .send(location)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            const element = body[0]
            this.validateMerchant(element, merch, cat)
            expect(element).to.have.property('shop_id', shop.id)
            expect(element).to.have.property('shop_name', shop.name)
          }).end(response.end(done))
      }).catch(done)
    })
  })

  describe('GET /v1/merchants/:id', function () {
    const testPath = path.api('/merchants/')

    beforeEach(session.user)

    it('should expect a valid merchant id without auth', function (done) {
      merchantFactory.model().spread(function (org, merch) {
        request(server)
          .get(testPath + 'NOT_VALID')
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      }).catch(done)
    })

    it('should return a merchant with a valid id without auth', function (done) {
      merchantFactory.model().spread((org, merch, cat) => {
        request(server)
          .get(testPath + merch.id)
          .expect(response.status.success)
          .expect((res) => {
            this.validateShowMerchant(res.body, merch, cat)
          }).end(response.end(done))
      }).catch(done)
    })

    it('should expect a valid merchant id', function (done) {
      merchantFactory.model().spread(function (org, merch) {
        request(server)
          .get(testPath + 'NOT_VALID')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      }).catch(done)
    })

    it('should return a merchant with a valid id', function (done) {
      merchantFactory.model().spread((org, merch, cat) => {
        request(server)
          .get(testPath + merch.id)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            this.validateShowMerchant(res.body, merch, cat)
          }).end(response.end(done))
      }).catch(done)
    })
  })

  describe('GET /v1/merchants/beacon', function () {
    const testPath = path.api('/merchants/beacon')

    beforeEach(session.user)

    it('should return badrequest without data', function (done) {
      validatorFactory.beacon().spread(function (org, merch, sc, shop, beacon) {
        request(server)
          .get(testPath)
          .expect(response.status.badRequest)
          .expect((res) => {
            response.badRequest(res.body, 'missing_validator_data')
          }).end(response.end(done))
      })
    })

    it('should return not found with invalid data', function (done) {
      validatorFactory.beacon().spread(function (org, merch, sc, shop, beacon) {
        request(server)
          .get(testPath)
          .send({ uid: beacon.uid, major: beacon.major + 10 })
          .expect(response.status.notFound)
          .expect((res) => {
            response.notFound(res.body, 'not_found')
          }).end(response.end(done))
      })
    })

    it('should return a merchant with a valid beacon without auth', function (done) {
      validatorFactory.beacon().spread((org, merch, sc, shop, beacon, cat) => {
        request(server)
          .get(testPath)
          .send({ uid: beacon.uid, major: beacon.major })
          .expect(response.status.success)
          .expect((res) => {
            this.validateShowMerchant(res.body, merch, cat)
          }).end(response.end(done))
      })
    })

    it('should return a merchant with a valid beacon', function (done) {
      validatorFactory.beacon().spread((org, merch, sc, shop, beacon, cat) => {
        request(server)
          .get(testPath)
          .send({ uid: beacon.uid, major: beacon.major })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            this.validateShowMerchant(res.body, merch, cat)
          }).end(response.end(done))
      })
    })
  })

  describe('GET /v1/merchants/qr', function () {
    const testPath = path.api('/merchants/qr/')

    beforeEach(session.user)

    it('should return a merchant with a valid qr without auth', function (done) {
      validatorFactory.qr().spread((org, merch, sc, shop, qr, cat) => {
        request(server)
          .get(testPath + qr.token)
          .expect(response.status.success)
          .expect((res) => {
            this.validateShowMerchant(res.body, merch, cat)
          }).end(response.end(done))
      })
    })

    it('should return a merchant with a valid qr', function (done) {
      validatorFactory.qr().spread((org, merch, sc, shop, qr, cat) => {
        request(server)
          .get(testPath + qr.token)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            this.validateShowMerchant(res.body, merch, cat)
          }).end(response.end(done))
      })
    })

    it('should return a merchant with a valid qr', function (done) {
      Promise.join(
        validatorFactory.qr(),
        validatorFactory.qr(),
        (qrStruct1, qrStruct2) => {
          // org, merch, sc, shop, qr, cat
          const merch = qrStruct2[1]
          const qr = qrStruct2[4]
          const cat = qrStruct2[5]
          request(server)
            .get(testPath + qr.token)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateShowMerchant(res.body, merch, cat)
            }).end(response.end(done))
        }
      )
    })
  })
})
