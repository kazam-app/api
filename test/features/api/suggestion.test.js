/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Suggestion API Feature Test
 * @file test/features/api/suggestion.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const suggestionFactory = require('../../helpers/factories/suggestion')
const suggestionValidator = require('../../helpers/validators/suggestion')

module.exports = describe('api/SuggestionController', function () {
  describe('POST /v1/suggestions', function () {
    const testPath = path.api('/suggestions')

    beforeEach(session.user)

    it('should expect valid authorization header', function (done) {
      request(server)
        .post(testPath)
        .expect(401)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should create a suggestion', function (done) {
      const valid = { name: 'Test Suggestion' }
      suggestionFactory.model().spread((org, merch, cat) => {
        request(server)
          .post(testPath)
          .send({ suggestion: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(200)
          .expect((res) => {
            suggestionValidator.json(res.body, valid)
          }).end(response.end(done))
      }).catch(done)
    })
  })
})
