/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Shop API Feature Test
 * @file test/features/api/shop.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const shopFactory = require('../../helpers/factories/shop')

// const location = { lat: 'ABC', lng: -99.2159143 }
const location = { lat: 19.4286506, lng: -99.2159143 }
const distance = '41.42m'

module.exports = describe('api/ShopController', function () {
  set('validateShop', function () {
    return function (element, shop, merchant) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('id', shop.id)
      expect(element).to.have.property('name', shop.name)
      expect(element).to.have.property('merchant_id', merchant.id)
      expect(element).to.have.property('merchant_name', merchant.name)
      expect(element).to.have.property('merchant_color', merchant.color)
      expect(element).to.have.property('merchant_image_url', merchant.imageUrl)
      expect(element).to.have.property('merchant_budget_rating', merchant.budgetRating)
      expect(element).to.have.property('lat', shop.lat)
      expect(element).to.have.property('lng', shop.lng)
      expect(element).to.include.keys(['category', 'shop_cluster', 'distance'])
    }
  })

  set('validateMerchantShop', function () {
    return function (element, shop, merchant) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', shop.name)
      expect(element).to.have.property('merchant_image_url', merchant.imageUrl)
      expect(element).to.have.property('merchant_budget_rating', merchant.budgetRating)
      expect(element).to.have.property('lat', shop.lat)
      expect(element).to.have.property('lng', shop.lng)
      expect(element).to.include.keys(['id', 'category', 'shop_cluster', 'distance'])
    }
  })

  beforeEach(session.user)

  describe('GET /v1/shops', function () {
    const testPath = path.api('/shops')

    it('should order by coordinates', function (done) {
      Promise.join(
        shopFactory.model(
          null, // Org
          null, // Merch
          undefined, // isPublic
          null, // identifier
          null, // shopCluster
          null, // id
          19.7083908, // lat
          -99.2300764 // lng
        ),
        shopFactory.model(),
        (firstStruct, validStruct) => {
          const merch = validStruct[1]
          const shop = validStruct[3]
          const merch2 = firstStruct[1]
          const shop2 = firstStruct[3]
          request(server)
            .get(testPath)
            .send(location)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(2)
              const element = body[0]
              this.validateShop(element, shop, merch)
              expect(element.distance).to.eql(distance)
              const element2 = body[1]
              this.validateShop(element2, shop2, merch2)
              expect(element2.distance).to.eql('31.18km')
            }).end(response.end(done))
        }
      ).catch(done)
    })

    it('should return a shop list', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop) => {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            const element = body[0]
            this.validateShop(element, shop, merch)
            expect(element.distance).to.eql('-')
          }).end(response.end(done))
      }).catch(done)
    })

    it('should return a shop list without auth', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop) => {
        request(server)
          .get(testPath)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            const element = body[0]
            this.validateShop(element, shop, merch)
            expect(element.distance).to.eql('-')
          }).end(response.end(done))
      }).catch(done)
    })
  })

  describe('GET /v1/shops/near', function () {
    const testPath = path.api('/shops/near')

    it('should filter by coordinates', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop) => {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .send(location)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            const element = body[0]
            this.validateShop(element, shop, merch)
            expect(element.distance).to.eql(distance)
          }).end(response.end(done))
      }).catch(done)
    })

    it('should filter by coordinates without auth', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop) => {
        request(server)
          .get(testPath)
          .send(location)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            const element = body[0]
            this.validateShop(element, shop, merch)
            expect(element.distance).to.eql(distance)
          }).end(response.end(done))
      }).catch(done)
    })

    it('should return a shop list', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop) => {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            this.validateShop(body[0], shop, merch)
          }).end(response.end(done))
      }).catch(done)
    })

    it('should return a shop list without auth', function (done) {
      shopFactory.model().spread((org, merch, shopCluster, shop) => {
        request(server)
          .get(testPath)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            this.validateShop(body[0], shop, merch)
          }).end(response.end(done))
      }).catch(done)
    })
  })

  describe('GET /v1/merchants/:merchant_id/shops', function () {
    const testPath = path.api('/merchants/')

    it('should order by coordinates', function (done) {
      Promise.join(
        shopFactory.model(),
        shopFactory.model(),
        (invalidStruct, validStruct) => {
          const merch = validStruct[1]
          const shop = validStruct[3]
          request(server)
            .get(testPath + merch.id + '/shops')
            .send(location)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              const element = body[0]
              this.validateMerchantShop(element, shop, merch)
              expect(element.distance).to.eql(distance)
            }).end(response.end(done))
        }
      ).catch(done)
    })

    it('should return a shop list filtered by a merchant without auth', function (done) {
      Promise.join(
        shopFactory.model(),
        shopFactory.model(),
        (invalidStruct, validStruct) => {
          const merch = validStruct[1]
          const shop = validStruct[3]
          request(server)
            .get(testPath + merch.id + '/shops')
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateMerchantShop(body[0], shop, merch)
            }).end(response.end(done))
        }
      ).catch(done)
    })

    it('should return a shop list filtered by a merchant', function (done) {
      Promise.join(
        shopFactory.model(),
        shopFactory.model(),
        (invalidStruct, validStruct) => {
          const merch = validStruct[1]
          const shop = validStruct[3]
          request(server)
            .get(testPath + merch.id + '/shops')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateMerchantShop(body[0], shop, merch)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })
})
