/* eslint-env mocha */
/**
 * KAzam Api
 * @name Mocha Beacon API Feature Test
 * @file test/features/api/beacon.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const validatorFactory = require('../../helpers/factories/validator')

module.exports = describe('api/BeaconController', function () {
  describe('GET /v1/beacons/regions', function () {
    const testPath = path.api('/beacons/regions')

    beforeEach(session.user)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath)
        .expect(401)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a list beacon regions', function (done) {
      validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, region) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(200)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            const element = body[0]
            expect(element).to.have.property('uid', region.uid)
          }).end(response.end(done))
      })
    })
  })

  describe('GET /v1/merchants/:merchant_id/beacons', function () {
    const testPath = path.api('/merchants/')

    beforeEach(session.user)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get(testPath + 'ABC/beacons')
        .expect(401)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a list beacon regions', function (done) {
      Promise.join(
        validatorFactory.beacon(),
        validatorFactory.beacon(),
        function (valid) {
          const merch = valid[1]
          const beacon = valid[4]
          request(server)
            .get(testPath + merch.id + '/beacons')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              const element = body[0]
              expect(element).to.have.property('uid', beacon.uid)
              expect(element).to.have.property('major', beacon.major)
              expect(element).to.have.property('minor', beacon.minor)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })
})
