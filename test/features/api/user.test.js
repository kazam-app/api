/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha User API Feature Test
 * @file test/features/user.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const set = require('mocha-let')
const moment = require('moment')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const userFactory = require('../../helpers/factories/user')
const userValidator = require('../../helpers/validators/user')
const merchantFactory = require('../../helpers/factories/merchant')
const validatorFactory = require('../../helpers/factories/validator')
const userPunchCardFactory = require('../../helpers/factories/userpunchcard')

module.exports = describe('api/UserController', function () {
  set('validateJsonUser', function () {
    return userValidator.json
  })

  set('validateJwtUser', function () {
    return userValidator.model
  })

  set('validateCurrent', function () {
    return userValidator.current
  })

  context('sessionless features', function () {
    describe('POST /v1/sign_up', function () {
      const testPath = path.api('/sign_up')

      it('should return a badrequest error with incorrect params', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_user_data')
          }).end(response.end(done))
      })

      it('should return a user and token with correct params', function (done) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: 'Last Names',
          email: 'testmail@kzm.com',
          password: pass,
          password_confirmation: pass
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.success)
          .expect((res) => {
            this.validateJsonUser(res.body, validUser)
          }).end(response.end(done))
      })

      it('should return a user and token with empty gender', function (done) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: 'Last Names',
          email: 'testmail@kzm.com',
          gender: '',
          password: pass,
          password_confirmation: pass
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.success)
          .expect((res) => {
            this.validateJsonUser(res.body, validUser)
          }).end(response.end(done))
      })

      it('should fail with incorrect fb login', function (done) {
        const validUser = {
          fb_id: 'ABCD',
          fb_token: 'ABCDEFG',
          fb_expires_at: '10/10/2017',
          name: 'Name',
          last_names: 'Last Names',
          email: 'test@u360.test'
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'fb_error')
          }).end(response.end(done))
      })

      xit('should return a user and token with correct fb params', function (done) {
        const validUser = {
          fb_id: '502035566',
          fb_token: 'EAAGsKSHAOHUBANdSrCPh29ERsRpQKtRYGqQPJHTqe5gl7ZBAMXnXB8LLfeNOBWwdJOO2H7aFZCuiT5ijYYS97xnVqGoAWbYOHko8DNAzfZBjSmOqe3NsTweMtxzh6PD71g9m08fZAPNG2ZCWkzNsdBFg7M8Vz1WZCSJx08mEGXZCsIAtZA0ZBqDIQ8pikZB1HqoJsZD',
          fb_expires_at: 123121221,
          name: 'Name',
          last_names: 'Last Names',
          email: 'test@u360.test'
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.success)
          .expect((res) => {
            this.validateJsonUser(res.body, validUser)
          }).end(response.end(done))
      })

      it('should fail with an incorrect invite code', function (done) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: 'Last Names',
          email: 'test@u360.test',
          password: pass,
          password_confirmation: pass,
          invite_code: 'NOT_CORRECT'
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'invalid_invite_code')
          }).end(response.end(done))
      })

      it('should return a user and token with correct params and a correct invite code', function (done) {
        const code = 'INVITE_CODE'
        merchantFactory.model(null, null, undefined, null, null, code)
          .spread((org, merchant) => {
            const pass = '12345678'
            const validUser = {
              name: 'Name',
              last_names: 'Last Names',
              email: 'test@u360.test',
              password: pass,
              password_confirmation: pass,
              invite_code: code
            }
            const params = { user: validUser }
            request(server)
              .post(testPath)
              .send(params)
              .expect(response.status.success)
              .expect((res) => {
                this.validateJsonUser(res.body, validUser)
                console.log('TODO validate!!!!!!!!!! invite code punch')
              }).end(response.end(done))
          }).catch(done)
      })
    })

    describe('POST /v1/check', function () {
      const testPath = path.api('/check')

      it('should return a badrequest error with incorrect params', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_user_data')
          }).end(response.end(done))
      })

      it('should return errors with missing params', function (done) {
        const pass = '12345678'
        const validUser = {
          // name: 'Name',
          // last_names: 'Last Names',
          // email: 'test@u360.test',
          password: pass,
          password_confirmation: pass
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.validation(res.body, {
              name: ['empty'],
              last_names: ['empty'],
              email: ['empty']
            })
          }).end(response.end(done))
      })

      it('should return errors with incorrect invalid params', function (done) {
        const pass = '12345678'
        const validUser = {
          name: '',
          // last_names: 'Last Names',
          email: 'INVALID_EMAIL',
          password: pass,
          password_confirmation: pass
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.validation(res.body, {
              name: ['empty'],
              last_names: ['empty'],
              email: ['invalid_email']
            })
          }).end(response.end(done))
      })

      it('should return a user and token with empty gender', function (done) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: 'Last Names',
          email: 'testmail@kzm.com',
          gender: '',
          password: pass,
          password_confirmation: pass
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.success)
          .expect((res) => {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })

      it('should return true with correct params', function (done) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: 'Last Names',
          email: 'test@u360.test',
          password: pass,
          password_confirmation: pass
        }
        const params = { user: validUser }
        request(server)
          .post(testPath)
          .send(params)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })
    })

    describe('POST /v1/login', function () {
      beforeEach(session.user)

      const testPath = path.api('/login')

      it('should return an badrequest error with incorrect params', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'login_fail')
          }).end(response.end(done))
      })

      it('should return an unauthorized error with correct params but incorrect values', function (done) {
        request(server)
          .post(testPath)
          .send({ email: 'foo@bar.com', password: '123456' })
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'login_fail')
          }).end(response.end(done))
      })

      it('should return a user and token with correct params', function (done) {
        request(server)
          .post(testPath)
          .send({ email: jwtUser.email, password: jwtPass })
          .expect(response.status.success)
          .expect((res) => {
            this.validateJwtUser(res.body, jwtUser)
          }).end(response.end(done))
      })
    })

    describe('POST /v1/validate', function () {
      const testPath = path.api('/validate')

      it('should return an badrequest error with invalid token', function (done) {
        userFactory.model().spread(function (user, created) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Validate
          })
        }).then(function (token) {
          request(server)
            .post(testPath)
            .send({ token: 'ABC' })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return a gone error with an expired token', function (done) {
        userFactory.model(null, false).spread(function (user, created) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Validate,
            expiresAt: new Date()
          })
        }).then(function (token) {
          request(server)
            .post(testPath)
            .send({ token: token.jwt() })
            .expect(response.status.gone)
            .expect(function (res) {
              response.gone(res.body, 'expired_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should not validate a user with a correct token but no email', function (done) {
        userFactory.model(null, false).spread(function (user, created) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Validate
          }).then(function (token) {
            return [token, user]
          })
        }).spread((token) => {
          const data = { token: token.jwt() }
          request(server)
            .post(testPath)
            .send(data)
            .expect(response.status.badRequest)
            .expect((res) => {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should validate a user with a correct token and email', function (done) {
        userFactory.model(null, false, 'valid.email@testKzm.mx').spread(function (user, created) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Validate
          }).then(function (token) {
            return [token, user]
          })
        }).spread((token, user) => {
          const data = { token: token.jwt() }
          request(server)
            .post(testPath)
            .send(data)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })

    describe('POST /v1/recovery', function () {
      const testPath = path.api('/recovery')

      it('should return success with invalid params', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })

      it('should return success with invalid email', function (done) {
        userFactory.model(null, false)
          .spread(function (org, user) {
            request(server)
              .post(testPath)
              .send({ email: user.email })
              .expect(response.status.success)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should return success with valid email and expired token', function (done) {
        userFactory.model(null, true).spread(function (user) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Recovery,
            expiresAt: new Date()
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .post(testPath)
            .send({ email: user.email })
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return success with valid email and existing token', function (done) {
        userFactory.model(null, true).spread(function (user) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Recovery
          }).then(function (token) {
            return [token, user]
          })
        }).spread(function (token, user) {
          request(server)
            .post(testPath)
            .send({ email: user.email })
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return success with valid email and no token', function (done) {
        userFactory.model(null, true)
          .spread(function (user) {
            request(server)
              .post(testPath)
              .send({ email: user.email })
              .expect(response.status.success)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })
    })

    describe('POST /v1/verify', function () {
      const testPath = path.api('/verify')

      it('should return badrequest with invalid params', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'invalid_token')
          }).end(response.end(done))
      })

      it('should return an badrequest error with invalid token', function (done) {
        userFactory.unVerified().then(function (user) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Verify
          })
        }).then(function (token) {
          request(server)
            .post(testPath)
            .send({ token: 'INVALID' })
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'invalid_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return a gone error with an expired token', function (done) {
        userFactory.model(null, false).spread(function (user, created) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Verify,
            expiresAt: new Date()
          })
        }).then(function (token) {
          request(server)
            .post(testPath)
            .send({ token: token.jwt() })
            .expect(response.status.gone)
            .expect(function (res) {
              response.gone(res.body, 'expired_token')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should return success with valid token', function (done) {
        userFactory.unVerified().then(function (user) {
          return models.UserToken.create({
            userId: user.id,
            identity: models.UserToken.Identity.Verify
          }).then(function (token) {
            return [user, token]
          })
        }).spread(function (user, token) {
          request(server)
            .post(testPath)
            .send({ token: token.jwt() })
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(function (err, res) {
              user.reload().then(function (user) {
                expect(user).to.have.property('isVerified', true)
                response.end(done)(err, res)
              })
            })
        }).catch(done)
      })
    })
  })

  context('sessionful features', function () {
    describe('POST /v1/verification', function () {
      const testPath = path.api('/verification')

      context('unverified', function () {
        beforeEach(session.unVerifiedUser)

        it('should expect valid authorization header', function (done) {
          request(server)
            .post(testPath)
            .expect(response.status.unauthorized)
            .expect(function (res) {
              response.unauthorized(res.body)
            }).end(response.end(done))
        })

        it('should return success with an unVerified user', function (done) {
          request(server)
            .post(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.have.property('sent', true)
            }).end(response.end(done))
        })
      })

      context('verified', function () {
        beforeEach(session.user)

        it('should fail with a verified user', function (done) {
          request(server)
            .post(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'user_verified')
            }).end(response.end(done))
        })
      })
    })

    describe('DELETE /v1/log_out', function () {
      beforeEach(session.user)
      const testPath = path.api('/log_out')

      it('should return an badrequest error with incorrect params', function (done) {
        request(server)
          .del(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should return true with correct params', function (done) {
        request(server)
          .del(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })
    })

    set('validateCommon', function () {
      return function (body) {
        expect(body).to.be.an('object')
        expect(body).to.have.property('id')
        expect(body).to.have.property('identifier')
        expect(body).to.have.property('is_verified', true)
        expect(body).to.have.property('name')
        expect(body).to.have.property('last_names')
        expect(body).to.have.property('email')
        expect(body).to.have.property('birthdate')
        expect(body).to.have.property('gender')
        expect(body).to.have.property('created_at')
      }
    })

    describe('GET /v1/profile', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile')

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should return current user information', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            this.validateCommon(body)
            this.validateCurrent(body, jwtUser)
            expect(body).to.have.property('name', jwtUser.name)
            expect(body).to.have.property('last_names', jwtUser.lastNames)
            expect(body).to.have.property('birthdate', jwtUser.birthdate)
            expect(body).to.have.property('gender', jwtUser.gender)
          }).end(response.end(done))
      })
    })

    describe('GET /v1/profile/verified', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile/verified')

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should return current user information', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            expect(res.body).to.have.property('is_verified', jwtUser.isVerified)
          }).end(response.end(done))
      })
    })

    describe('GET /v1/profile/permissions', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile/permissions')

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should return current user information', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            expect(res.body).to.have.property(
              'has_expiry_notify_permission',
              true
            )
          }).end(response.end(done))
      })
    })

    describe('PUT /v1/profile', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile')

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail updating a user without data', function (done) {
        request(server)
          .put(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_user_data')
          }).end(response.end(done))
      })

      it('should not update a user with invalid dates', function (done) {
        const invalid = { birthdate: 'INVALID_DATE' }
        request(server)
          .put(testPath)
          .send({ user: invalid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            this.validateCommon(body)
            this.validateCurrent(body, jwtUser)
            expect(body).to.have.property('birthdate', jwtUser.birthdate)
          }).end(response.end(done))
      })

      it('should update a user with valid date', function (done) {
        const valid = { birthdate: '1990-08-11' }
        request(server)
          .put(testPath)
          .send({ user: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            this.validateCommon(body)
            this.validateCurrent(body, jwtUser)
            expect(body).to.have.property('birthdate', valid.birthdate)
          }).end(response.end(done))
      })

      it('should update a user with valid gender', function (done) {
        const valid = { gender: 0 }
        request(server)
          .put(testPath)
          .send({ user: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            this.validateCommon(body)
            this.validateCurrent(body, jwtUser)
            expect(body).to.have.property('gender', valid.gender)
          }).end(response.end(done))
      })

      it('should update a user with valid data', function (done) {
        const valid = { name: 'UPDATED NAME' }
        request(server)
          .put(testPath)
          .send({ user: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            this.validateCommon(body)
            this.validateCurrent(body, jwtUser)
            expect(body).to.have.property('name', valid.name)
          }).end(response.end(done))
      })
    })

    describe('PUT /v1/profile/email', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile/email')

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail updating an email with without data', function (done) {
        request(server)
          .put(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_email')
          }).end(response.end(done))
      })

      it('should fail updating an email with invalid data', function (done) {
        const invalid = { email: 'updated@urban.mx' }
        request(server)
          .put(testPath)
          .send({ user: invalid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_email')
          }).end(response.end(done))
      })

      it('should update a user with valid data', function (done) {
        const valid = { email: 'updated@urban.mx', password: jwtPass }
        request(server)
          .put(testPath)
          .send({ user: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })
    })

    describe('PUT /v1/profile/password', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile/password')

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail updating password with invalid data', function (done) {
        request(server)
          .put(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_password_data')
          }).end(response.end(done))
      })

      it('should update a password with valid data', function (done) {
        const valid = '12345678ABC'
        request(server)
          .put(testPath)
          .send({ password: valid, password_confirmation: valid, old_password: jwtPass })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })
    })

    describe('PUT /v1/profile/player', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile/player')

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail updating player with invalid data', function (done) {
        request(server)
          .put(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_user_data')
          }).end(response.end(done))
      })

      it('should update a player with valid data', function (done) {
        const valid = '12345678ABC'
        request(server)
          .put(testPath)
          .send({ user: { player_id: valid } })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect(function (res) {
            expect(res.body).to.eql('true')
          }).end(response.end(done))
      })
    })

    describe('GET /v1/profile/history', function () {
      beforeEach(session.user)
      const testPath = path.api('/profile/history')

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should return an Event list', function (done) {
        const current = '2018-02-01T06:00:00.000Z'
        validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const UserPunchCardValidation = models.UserPunchCardValidation
            const future = moment(current).add(1, 'd')
            const validationLimit = 1
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              jwtUser.id, // User
              // UserPunchCard InProgress
              models.UserPunchCard.Identity.InProgress,
              1, // PunchCount
              validationLimit
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: moment(current)
              }).then(function (result) {
                return [merch, shop, user, punch, userPunchCard, result]
              })
            }).spread(function (merch, shop, user, punchCard, userPunchCard, validation) {
              request(server)
                .get(testPath)
                .set('Authorization', 'Bearer ' + jwtToken)
                .expect(response.status.success)
                .expect((res) => {
                  const body = res.body
                  expect(body).to.be.an('array')
                  expect(body).to.have.lengthOf(2)
                  // card expiry
                  const expiry = body[0]
                  expect(expiry).to.be.an('object')
                  expect(expiry).to.have.property(
                    'id',
                    userPunchCard.identifier
                  )
                  // HistoryCardEvent.Expired
                  expect(expiry.card).to.be.an('object')
                  expect(expiry.card).to.have.property(
                    'identity',
                    0
                  )
                  expect(expiry).to.not.have.property('validation')
                  expect(expiry).to.have.property('event_type', 1)
                  expect(expiry).to.have.property(
                    'event_at',
                    future.toISOString()
                  )
                  expect(expiry.punch_card).to.be.an('object')
                  expect(expiry.punch_card).to.have.property(
                    'id',
                    userPunchCard.identifier
                  )
                  expect(expiry.punch_card).to.have.property(
                    'identity',
                    3 // Expired
                  )
                  expect(expiry.merchant).to.be.an('object')
                  expect(expiry.merchant).to.have.property(
                    'name',
                    merch.name
                  )
                  expect(expiry.merchant).to.have.property(
                    'image_url',
                    merch.imageUrl
                  )
                  // card punch
                  const punch = body[1]
                  expect(punch).to.be.an('object')
                  expect(punch).to.have.property(
                    'id',
                    validation.identifier
                  )
                  // HistoryEvent.Validation
                  expect(punch.validation).to.be.an('object')
                  expect(punch.validation).to.have.property(
                    'identity',
                    validation.identity
                  )
                  expect(punch).to.not.have.property('card')
                  expect(punch).to.have.property('event_type', 0)
                  expect(punch).to.have.property('event_at', current)
                  expect(punch.punch_card).to.be.an('object')
                  expect(punch.punch_card).to.have.property(
                    'id',
                    userPunchCard.identifier
                  )
                  expect(punch.punch_card).to.have.property(
                    'identity',
                    3 // Expired
                  )
                  expect(punch.merchant).to.be.an('object')
                  expect(punch.merchant).to.have.property(
                    'name',
                    merch.name
                  )
                  expect(punch.merchant).to.have.property(
                    'image_url',
                    merch.imageUrl
                  )
                  expect(punch.shop).to.be.an('object')
                  expect(punch.shop).to.have.property(
                    'name',
                    shop.name
                  )
                }).end(response.end(done))
            })
          })
      })
    })
  })
})
