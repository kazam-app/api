/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Category Feature Test
 * @file category.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const set = require('mocha-let')
const request = require('supertest')
const session = require('../helpers/session')
const response = require('../helpers/response')
const categoryFactory = require('../helpers/factories/category')
const categoryValidator = require('../helpers/validators/category')

module.exports = describe('CategoryController', function () {
  set('validateCategory', function () {
    return categoryValidator.model
  })

  beforeEach(session.simple)

  describe('GET /categories', function () {
    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/categories')
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a category list', function (done) {
      categoryFactory.model().spread((category) => {
        request(server)
          .get('/categories')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            const body = res.body
            expect(body).to.have.lengthOf(1)
            this.validateCategory(body[0], category)
          }).end(response.end(done))
      }).catch(done)
    })
  })
})
