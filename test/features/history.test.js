/* eslint-env mocha */
/**
 * Merchants Api
 * @name Mocha Beacon Feature Test
 * @file beacon.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const session = require('../helpers/session')
const response = require('../helpers/response')
const historyValidator = require('../helpers/validators/history')
const validatorFactory = require('../helpers/factories/validator')
const userPunchCardFactory = require('../helpers/factories/userpunchcard')

module.exports = describe('HistoryController', function () {
  set('validateHistory', function () { return historyValidator })

  describe('GET /history', function () {
    beforeEach(session.fullRead)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/history')
        .expect(401)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return an user punch card validation list', function (done) {
      const orgId = jwtOrganization.id
      const merchId = jwtMerchant.id
      Promise.join(
        userPunchCardFactory.model(orgId, merchId),
        validatorFactory.beacon(orgId, merchId),
        (userPunchStruct, validatorStruct) => {
          const merch = userPunchStruct[1]
          const userPunchCard = userPunchStruct[4]
          const beacon = validatorStruct[4]
          return Promise.join(
            models.UserPunchCardInvite.create({
              merchantId: merch.id,
              userPunchCardId: userPunchCard.id
            }),
            models.UserPunchCardValidation.create({
              validatorId: beacon.id,
              userPunchCardId: userPunchCard.id
            }),
            (inviteStruct, validationStruct) => {
              request(server)
                .get('/history')
                .set('Authorization', 'Bearer ' + jwtToken)
                .expect(200)
                .expect((res) => {
                  const data = res.body
                  expect(data).to.have.lengthOf(1)
                  const element = data[0]
                  this.validateHistory(element)
                  expect(element).to.not.have.property('name')
                  expect(element).to.not.have.property('last_names')
                  expect(element).to.not.have.property('email')
                }).end(response.end(done))
            }
          )
        }
      ).catch(done)
    })
  })
})
