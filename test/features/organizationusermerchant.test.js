/* eslint-env mocha */
/**
 * Kazam Api
 * Mocha OrganizationUserMerchant Feature Test
 * @file test/features/organizationuser.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const i18n = require('i18n')
const request = require('supertest')
const session = require('../helpers/session')
const response = require('../helpers/response')
const organizationUserFactory = require('../helpers/factories/organizationuser')

module.exports = describe('OrganizationUserMerchantController', function () {
  describe('POST /users/:id/merchant', function () {
    const testPath = '/users/'

    describe('authorization rules', function () {
      beforeEach(session.simple)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath + 'ABC/merchant')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but wrong perms', function (done) {
        request(server)
          .post(testPath + 'ABC/merchant')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail with invalid data', function (done) {
        request(server)
          .post(testPath + 'ABC/merchant')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_organization_user_merchant_data')
          }).end(response.end(done))
      })

      it('should succeed setting another user\'s permissions', function (done) {
        organizationUserFactory.model(jwtUser.organizationId)
          .spread(function (org, user) {
            const role = models.OrganizationUserMerchant.Role.ReadWrite
            const valid = {
              merchant_id: jwtMerchant.id,
              role: role
            }
            request(server)
              .post(testPath + user.id + '/merchant')
              .set('Authorization', 'Bearer ' + jwtToken)
              .send({ organization_user_merchant: valid })
              .expect(response.status.success)
              .expect(function (res) {
                const body = res.body
                expect(body).to.be.an('object')
                expect(body).to.include.keys('id')
                expect(body).to.have.property('role', role)
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })
})
