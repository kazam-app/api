/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Category Admin Feature Test
 * @file test/features/admin/category.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const categoryFactory = require('../../helpers/factories/category')
const categoryValidator = require('../../helpers/validators/category')

module.exports = describe('admin/CategoryController', function () {
  set('validateCategory', function () {
    return categoryValidator.model
  })

  set('validateJsonCategory', function () {
    return categoryValidator.json
  })

  const testPath = path.admin('/categories')

  describe('GET /admin/categories', function () {
    context('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    context('feature', function () {
      beforeEach(session.admin)

      it('should return a category list', function (done) {
        categoryFactory.model().spread((category) => {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateCategory(body[0], category)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('GET /admin/categories/:id', function () {
    context('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    context('feature', function () {
      beforeEach(session.admin)

      it('should fail retreving a category with invalid id', function (done) {
        categoryFactory.model().spread(function (valid) {
          request(server)
            .get(testPath + '/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should retreive a category with valid id', function (done) {
        categoryFactory.model().spread((valid) => {
          request(server)
            .get(testPath + '/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateCategory(res.body, valid)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('POST /admin/categories', function () {
    context('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    context('feature', function () {
      beforeEach(session.admin)

      it('should fail creating a category with invalid params', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_category_data')
          }).end(response.end(done))
      })

      it('should fail to create a new category with invalid params', function (done) {
        request(server)
          .post(testPath)
          .send({ category: {} })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.validation(res.body, { name: ['empty'] })
          }).end(response.end(done))
      })

      it('should create a new category with valid params', function (done) {
        const valid = { name: 'Test Category', image: 'NO_AWS_STREAM' }
        request(server)
          .post(testPath)
          .send({ category: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .end((err, res) => {
            if (err) done(err)
            else {
              const body = res.body
              models.Category.findById(body.id).then((category) => {
                this.validateCategory(body, category)
                done()
              }).catch(done)
            }
          })
      })
    })
  })

  describe('PUT /admin/categories/:id', function () {
    context('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    context('feature', function () {
      beforeEach(session.admin)

      it('should fail updating a category with invalid params', function (done) {
        categoryFactory.model().spread(function (category) {
          request(server)
            .put(testPath + '/' + category.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_category_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail updating a category with invalid id', function (done) {
        categoryFactory.model().spread(function (category) {
          const update = { name: 'Updated Category' }
          request(server)
            .put(testPath + '/ABC')
            .send({ category: update })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_category_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should update a category with valid params', function (done) {
        categoryFactory.model().spread((category) => {
          const update = { name: 'Updated Category' }
          request(server)
            .put(testPath + '/' + category.id)
            .send({ category: update })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.have.property('id', category.id)
            }).end((err, res) => {
              if (err) done(err)
              else {
                category.reload().then(() => {
                  this.validateCategory(res.body, category)
                  done()
                }).catch(done)
              }
            })
        }).catch(done)
      })
    })
  })

  describe('DELETE /admin/categories/:id', function () {
    context('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .del(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    context('feature', function () {
      beforeEach(session.admin)

      it('should fail deleting a category with invalid id', function (done) {
        categoryFactory.model().spread(function (valid) {
          request(server)
            .del(testPath + '/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should delete a category with valid id', function (done) {
        categoryFactory.model().spread(function (valid) {
          request(server)
            .del(testPath + '/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
