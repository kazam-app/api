/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Shop Admin Feature Test
 * @file test/features/admin/shop.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const shopFactory = require('../../helpers/factories/shop')
const shopValidator = require('../../helpers/validators/shop')
const merchantFactory = require('../../helpers/factories/merchant')
const shopClusterFactory = require('../../helpers/factories/shopcluster')

module.exports = describe('admin/ShopController', function () {
  set('validateListShop', function () {
    return shopValidator.adminList
  })

  set('validateJsonShop', function () {
    return shopValidator.adminJson
  })

  set('validateShop', function () {
    return shopValidator.admin
  })

  describe('GET /admin/merchants/:merchant_id/shops', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      const testPath = path.admin('/merchants/ABC/shops')
      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      const testPath = path.admin('/merchants/')
      it('should return a shop list', function (done) {
        Promise.join(
          shopFactory.model(),
          shopFactory.model(),
          (invalid, valid) => {
            const merch = valid[1]
            const sc = valid[2]
            const shop = valid[3]
            request(server)
              .get(testPath + merch.id + '/shops')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                const body = res.body
                expect(body).to.have.lengthOf(1)
                this.validateListShop(body[0], shop, sc)
              }).end(response.end(done))
          }
        ).catch(done)
      })
    })
  })

  describe('GET /admin/merchants/:merchant_id/shops/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      const testPath = path.admin('/merchants/ABC/shops/ABC')
      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      const testPath = path.admin('/merchants/')
      it('should fail retreving a shop with invalid id', function (done) {
        shopFactory.model().spread(function (org, merch, shopCluster, valid) {
          request(server)
            .get(testPath + merch.id + '/shops/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should retreive a shop with valid id', function (done) {
        shopFactory.model().spread((org, merch, sc, valid) => {
          request(server)
            .get(testPath + merch.id + '/shops/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateShop(res.body, valid, sc)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('POST /admin/merchants/:merchant_id/shops', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/merchants/ABC/shops')

      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/merchants/')

      beforeEach(session.admin)

      it('should fail to create a new shop with invalid merchant', function (done) {
        request(server)
          .post(testPath + 'ABC/shops')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'merchant_not_found')
          }).end(response.end(done))
      })

      it('should fail to create a new shop with invalid params', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          request(server)
            .post(testPath + merchant.id + '/shops')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_shop_data')
            }).end(response.end(done))
        })
      })

      it('should create a new shop with valid params', function (done) {
        Promise.join(
          shopClusterFactory.model(),
          merchantFactory.model(),
          (scStruct, merchantStruct) => {
            const sc = scStruct[0]
            const merchant = merchantStruct[1]
            const valid = {
              is_active: false,
              merchant_id: merchant.id,
              shop_cluster_id: sc.id,
              name: 'Test Shop',
              location: 'Test Location',
              city: 'CDMX',
              postal_code: '01219',
              phone: '551223446',
              state: 'DF'
            }
            request(server)
              .post(testPath + merchant.id + '/shops')
              .send({ shop: valid })
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                this.validateJsonShop(res.body, valid)
              }).end(response.end(done))
          }
        ).catch(done)
      })
    })
  })

  describe('PUT /admin/merchants/:merchant_id/shops/:id', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/merchants/ABC/shops/ABC')

      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .put(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/merchants/')

      beforeEach(session.admin)

      it('should fail updating a shop with invalid merchant', function (done) {
        shopFactory.model().spread(function (org, merch, sc, valid) {
          request(server)
            .put(testPath + 'ABC/shops/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'merchant_not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail updating a shop with invalid id', function (done) {
        shopFactory.model().spread(function (org, merch, sc, valid) {
          const update = { name: 'Updated Shop' }
          request(server)
            .put(testPath + merch.id + '/shops/ABC')
            .send({ shop: update })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_shop_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail updating a shop with invalid params', function (done) {
        shopFactory.model().spread(function (org, merch, sc, valid) {
          request(server)
            .put(testPath + merch.id + '/shops/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_shop_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should update a shop with valid params', function (done) {
        shopFactory.model().spread((org, merch, sc, valid) => {
          const update = { name: 'Updated Shop' }
          request(server)
            .put(testPath + merch.id + '/shops/' + valid.id)
            .send({ shop: update })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.have.property('id', valid.id)
            }).end((err, res) => {
              if (err) done(err)
              else {
                valid.reload().then(() => {
                  this.validateShop(res.body, valid, sc)
                  done()
                }).catch(done)
              }
            })
        }).catch(done)
      })
    })
  })

  describe('DELETE /admin/merchants/:merchant_id/shops/:id', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/merchants/ABC/shops/ABC')

      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .del(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/merchants/')

      beforeEach(session.admin)

      it('should fail deleting a shop with invalid merchant', function (done) {
        shopFactory.model().spread(function (org, merch, sc, valid) {
          request(server)
            .del(testPath + 'ABC/shops/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'merchant_not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail delete a shop with invalid id', function (done) {
        shopFactory.model().spread(function (org, merch, sc, valid) {
          request(server)
            .del(testPath + merch.id + '/shops/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should delete a shop with valid id', function (done) {
        shopFactory.model().spread(function (org, merch, sc, valid) {
          request(server)
            .del(testPath + merch.id + '/shops/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
