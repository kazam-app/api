/* eslint-env mocha */
/**
 * Kazam Api
 * Mocha OrganizationUserMerchant Admin Feature Test
 * @file test/features/admin/organizationusermerchant.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const merchantFactory = require('../../helpers/factories/merchant')
const organizationUserFactory = require('../../helpers/factories/organizationuser')

module.exports = describe('admin/OrganizationUserMerchantController', function () {
  const testPath = path.admin('/merchants/')

  describe('GET /admin/merchants/:merchant_id/users', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath + 'ABC/users')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath + 'ABC/users')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should succeed setting another user\'s permissions', function (done) {
        organizationUserFactory.model()
          .spread(function (org, user) {
            return merchantFactory.model(org.id).spread(function (org, merch) {
              return [user, merch]
            })
          }).spread(function (user, merch) {
            return models.OrganizationUserMerchant.create({
              merchantId: merch.id,
              organizationUserId: user.id,
              role: models.OrganizationUserMerchant.Role.ReadWrite
            }).then(function (organizationUserMerchant) {
              return [user, merch, organizationUserMerchant]
            })
          }).spread(function (organizationUser, merch, organizationUserMerchant) {
            request(server)
              .get(testPath + merch.id + '/users')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect(function (res) {
                const body = res.body
                expect(body).to.be.an('array')
                const element = body[0]
                expect(element).to.be.an('object')
                expect(element).to.have.property('id', organizationUserMerchant.id)
                expect(element).to.have.property('role', organizationUserMerchant.role)
                const user = element.organization_user
                expect(user).to.be.an('object')
                expect(user).to.have.property('id', organizationUser.id)
                expect(user).to.have.property('name', organizationUser.name)
                expect(user).to.have.property('last_names', organizationUser.lastNames)
                expect(user).to.have.property('email', organizationUser.email)
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })
})
