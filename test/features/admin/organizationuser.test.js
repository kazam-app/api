/* eslint-env mocha */
/**
 * Kazam Api
 * Mocha Organization User Admin Feature Test
 * @file test/features/admin/organizationuser.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const set = require('mocha-let')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const userValidator = require('../../helpers/validators/organizationuser')

module.exports = describe('admin/OrganizationUserController', function () {
  set('validateJwtUser', function () {
    return userValidator.json
  })

  describe('POST /admin/login', function () {
    beforeEach(session.admin)

    const testPath = path.admin('/login')

    it('should return a badrequest error with incorrect params', function (done) {
      request(server)
        .post(testPath)
        .expect(400)
        .expect(function (res) {
          response.badRequest(res.body, 'login_fail')
        }).end(response.end(done))
    })

    it('should return an unauthorized error with correct params but incorrect values', function (done) {
      request(server)
        .post(testPath)
        .send({ email: 'foo@bar.com', password: '123456' })
        .expect(400)
        .expect(function (res) {
          response.badRequest(res.body, 'login_fail')
        }).end(response.end(done))
    })

    it('should return a user and token with correct params', function (done) {
      request(server)
        .post(testPath)
        .send({ email: jwtUser.email, password: jwtPass })
        .expect(200)
        .expect((res) => {
          this.validateJwtUser(res.body, jwtUser)
        }).end(response.end(done))
    })
  })
})
