/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha PunchCard Feature Test
 * @file test/features/admin/punchcard.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const moment = require('moment')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const userFactory = require('../../helpers/factories/user')
const merchantFactory = require('../../helpers/factories/merchant')
const punchCardFactory = require('../../helpers/factories/punchcard')
const punchCardValidator = require('../../helpers/validators/punchcard')

module.exports = describe('admin/PunchCardController', function () {
  set('validatePunchCard', function () {
    return punchCardValidator.model
  })

  set('validateListPunchCard', function () {
    return punchCardValidator.adminList
  })

  set('validateJsonPunchCard', function () {
    return punchCardValidator.json
  })

  describe('GET /admin/punch_cards', function () {
    const testPath = path.admin('/punch_cards')

    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return a punch card list', function (done) {
        punchCardFactory.model()
          .spread((org, merchant, punch) => {
            request(server)
              .get(testPath)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                const body = res.body
                expect(body).to.have.lengthOf(1)
                this.validateListPunchCard(body[0], punch, merchant)
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })

  describe('GET /admin/punch_cards/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      const testPath = path.admin('/punch_cards/ABC')
      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      const testPath = path.admin('/punch_cards/')
      it('should fail retreving a punch card with invalid id', function (done) {
        punchCardFactory.model().spread(function (org, merchant, valid) {
          request(server)
            .get(testPath + '/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should retreive a punch card with valid id', function (done) {
        punchCardFactory.model().spread((org, merchant, valid) => {
          request(server)
            .get(testPath + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validatePunchCard(res.body, valid)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('POST /admin/punch_cards', function () {
    const testPath = path.admin('/punch_cards')

    describe('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail to create a new punch card with invalid params', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_punch_card_data')
          }).end(response.end(done))
      })

      it('should fail to create a new punch card with invalid date', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          const invalid = {
            merchant_id: merchant.id,
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 3,
            redeem_code: 'REDEEMCODE5',
            expires_at: 'INVALID_DATE'
          }
          request(server)
            .post(testPath)
            .send({ punch_card: invalid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.validation(res.body, { expires_at: ['invalid_date'] })
            }).end(response.end(done))
        })
      })

      it('should fail to create a new punch card with invalid redeem code')

      it('should fail to create a new punch card with less than 3 punch limit', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          const invalid = {
            merchant_id: merchant.id,
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 2,
            redeem_code: 'REDEEMCODE5',
            expires_at: new Date()
          }
          request(server)
            .post(testPath)
            .send({ punch_card: invalid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.validation(res.body, { punch_limit: ['less_than'] })
            }).end(response.end(done))
        })
      })

      it('should fail to create a new punch card with more than 15 punch limit', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          const invalid = {
            merchant_id: merchant.id,
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 16,
            redeem_code: 'REDEEMCODE5',
            expires_at: new Date()
          }
          request(server)
            .post(testPath)
            .send({ punch_card: invalid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.validation(res.body, { punch_limit: ['greater_than'] })
            }).end(response.end(done))
        })
      })

      it('should create a new punch card with valid params', function (done) {
        merchantFactory.model().spread((org, merchant) => {
          const valid = {
            merchant_id: merchant.id,
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 3,
            redeem_code: 'REDEEMCODE5',
            expires_at: new Date()
          }
          request(server)
            .post(testPath)
            .send({ punch_card: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateJsonPunchCard(res.body, valid, merchant)
            }).end(response.end(done))
        })
      })

      it('should create a new punch card validation limit with valid params', function (done) {
        merchantFactory.model().spread((org, merchant) => {
          const valid = {
            merchant_id: merchant.id,
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 3,
            redeem_code: 'REDEEMCODE5',
            expires_at: new Date(),
            validation_limit: 11
          }
          request(server)
            .post(testPath)
            .send({ punch_card: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateJsonPunchCard(res.body, valid, merchant)
            }).end(response.end(done))
        })
      })
    })
  })

  describe('PUT /admin/punch_cards/:id', function () {
    const testPath = path.admin('/punch_cards/')

    describe('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + 'ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put(testPath + 'ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail updating a punch card with invalid id', function (done) {
        request(server)
          .put(testPath + 'ABC')
          .send({ punch_card: {} })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_punch_card_data')
          }).end(response.end(done))
      })

      it('should update a punch card with valid data', function (done) {
        const valid = { name: 'Updated PunchCard', prize: 'Updated Prize', validation_limit: 10 }
        punchCardFactory.model().spread((org, merch, punch) => {
          request(server)
            .put(testPath + punch.id)
            .send({ punch_card: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.have.property('id', punch.id)
            }).end((err, res) => {
              if (err) done(err)
              else {
                punch.reload().then(() => {
                  this.validatePunchCard(res.body, punch)
                  done()
                })
              }
            })
        }).catch(done)
      })
    })
  })

  describe('PUT /admin/punch_cards/:id/publish', function () {
    const testPath = path.admin('/punch_cards/')

    describe('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + 'ABC/publish')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put(testPath + 'ABC/publish')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail publishing a punch card with invalid id', function (done) {
        request(server)
          .put(testPath + 'ABC/publish')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should fail publishing a punch card with valid id from with another published punch card', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          return Promise.join(
            punchCardFactory.model(
              org.id, // Organization
              merchant.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity
              moment().add(1, 'days') // expiresAt
            ),
            punchCardFactory.model(org.id, merchant.id),
            function (published, unpublished) {
              const punch = unpublished[2]
              request(server)
                .put(testPath + punch.id + '/publish')
                .set('Authorization', 'Bearer ' + jwtToken)
                .expect(response.status.badRequest)
                .expect(function (res) {
                  response.badRequest(res.body, 'active_punch_card_exists')
                }).end(response.end(done))
            }
          )
        }).catch(done)
      })

      it('should fail publishing a published punch card', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          return Promise.join(
            punchCardFactory.model(
              org.id, // Organization
              merchant.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity
              moment().add(1, 'days') // expiresAt
            ),
            punchCardFactory.model(org.id, merchant.id),
            function (published, unpublished) {
              const punch = published[2]
              request(server)
                .put(testPath + punch.id + '/publish')
                .set('Authorization', 'Bearer ' + jwtToken)
                .expect(response.status.badRequest)
                .expect(function (res) {
                  response.badRequest(res.body, 'not_found')
                }).end(response.end(done))
            }
          )
        }).catch(done)
      })

      it('should publish a punch card with valid id', function (done) {
        Promise.join(
          userFactory.model(),
          punchCardFactory.model(),
          punchCardFactory.model( // Published from another merchant
            null, // Organization
            null, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity
            moment().add(1, 'days') // expiresAt
          ),
          (user, punchStruct) => {
            const punch = punchStruct[2]
            request(server)
              .put(testPath + punch.id + '/publish')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                expect(res.body).to.have.property('identity', models.PunchCard.Identity.Published)
              }).end((err, res) => {
                if (err) done(err)
                else {
                  punch.reload().then(() => {
                    this.validatePunchCard(res.body, punch)
                    done()
                  })
                }
              })
          }
        )
      })
    })
  })

  describe('DELETE /admin/punch_cards/:id', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/punch_cards/ABC')

      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .del(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/punch_cards/')

      beforeEach(session.admin)

      it('should fail removing a punch card with invalid id', function (done) {
        request(server)
          .del(testPath + 'ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should remove a punch card with valid id', function (done) {
        punchCardFactory.model()
          .spread(function (org, merchant, punch) {
            request(server)
              .del(testPath + punch.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })
})
