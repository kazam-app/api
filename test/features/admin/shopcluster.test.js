/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha ShopCluster Admin Feature Test
 * @file test/feature/admin/shopcluster.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')

module.exports = describe('admin/ShopClusterController', function () {
  set('getShopCluster', function () {
    return models.ShopCluster.create({
      name: 'Test ShopCluster',
      location: 'Test Location',
      city: 'CDMX',
      postalCode: '01217',
      state: 'DF'
    })
  })

  set('validateShopCluster', function () {
    return function (element, model) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', model.name)
      expect(element).to.have.property('location', model.location)
      expect(element).to.include.keys('id')
    }
  })

  set('validateJsonShopCluster', function () {
    return function (element, object) {
      expect(element).to.be.an('object')
      expect(element).to.have.property('name', object.name)
      expect(element).to.have.property('location', object.location)
      expect(element).to.have.property('postal_code', object.postal_code)
      expect(element).to.have.property('city', object.city)
      expect(element).to.have.property('state', object.state)
      expect(element).to.include.keys(['id', 'country'])
    }
  })

  const testPath = path.admin('/shop_clusters')

  describe('GET /admin/shop_clusters', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return a list of shops clusters', function (done) {
        this.getShopCluster.then(function (valid) {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              const list = res.body
              expect(list).to.have.lengthOf(1)
              this.validateShopCluster(list[0], valid)
            }.bind(this)).end(response.end(done))
        }.bind(this)).catch(done)
      })
    })
  })

  describe('GET /admin/shop_clusters/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with invalid token but invalid id', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail with valid token but invalid id', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should return the current shop cluster', function (done) {
        this.getShopCluster.then((valid) => {
          request(server)
            .get(testPath + '/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateShopCluster(res.body, valid)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('POST /admin/shop_clusters', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail creating a shop cluster with invalid params', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_shop_cluster_data')
          }).end(response.end(done))
      })

      it('should create a new shop cluster with valid params', function (done) {
        const valid = {
          name: 'Test ShopCluster',
          location: 'Test Location',
          city: 'CDMX',
          postal_code: '01219',
          state: 'DF'
        }
        request(server)
          .post(testPath)
          .send({ shop_cluster: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            this.validateJsonShopCluster(res.body, valid)
          }).end(response.end(done))
      })
    })
  })

  describe('PUT /admin/shop_clusters/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail updating a shop cluster with invalid params', function (done) {
        this.getShopCluster.then(function (valid) {
          request(server)
            .put(testPath + '/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_shop_cluster_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail updating a shop cluster with invalid id', function (done) {
        this.getShopCluster.then(function (valid) {
          const validShopCluster = { name: 'Updated Shop Cluster' }
          request(server)
            .put(testPath + '/ABC')
            .send({ shop_cluster: validShopCluster })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_shop_cluster_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should update a shop cluster with valid params', function (done) {
        this.getShopCluster.then((shopCluster) => {
          const validShopCluster = { name: 'Updated Shop Cluster' }
          request(server)
            .put(testPath + '/' + shopCluster.id)
            .send({ shop_cluster: validShopCluster })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.have.property('id', shopCluster.id)
            }).end((err, res) => {
              if (err) done(err)
              else {
                shopCluster.reload().then(() => {
                  this.validateShopCluster(res.body, shopCluster)
                  done()
                }).catch(done)
              }
            })
        }).catch(done)
      })
    })
  })

  describe('DELETE /admin/shop_clusters/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .del(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail deleting a shop cluster with invalid id', function (done) {
        this.getShopCluster.then(function (valid) {
          request(server)
            .del(testPath + '/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should delete a shop cluster with valid id', function (done) {
        this.getShopCluster.then(function (valid) {
          request(server)
            .del(testPath + '/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
