/* eslint-env mocha */
/**
 * Merchants Api
 * @name Mocha QR Feature Test
 * @file qr.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const qrFactory = require('../../helpers/factories/validator')
const qrValidator = require('../../helpers/validators/validator')
const merchantFactory = require('../../helpers/factories/merchant')

module.exports = describe('admin/QrController', function () {
  set('validateQr', function () {
    return qrValidator.qr
  })

  set('validateJsonQr', function () {
    return qrValidator.qrJson
  })

  const testPath = path.admin('/qrs')

  describe('GET /admin/qrs', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return a qr list', function (done) {
        qrFactory.qr()
          .spread((org, merch, shopCluster, shop, qr) => {
            request(server)
              .get(testPath)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect((res) => {
                const body = res.body
                expect(body).to.have.lengthOf(1)
                this.validateQr(body[0], qr, true, shop)
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })

  describe('GET /admin/qrs/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail retreving a qr with invalid id', function (done) {
        qrFactory.qr()
          .spread(function (org, merch, shopCluster, shop, qr) {
            request(server)
              .get(testPath + '/ABC')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(400)
              .expect(function (res) {
                response.badRequest(res.body, 'not_found')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should retreive a qr with valid id', function (done) {
        qrFactory.qr()
          .spread(function (org, merch, shopCluster, shop, valid) {
            request(server)
              .get(testPath + '/' + valid.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect(function (res) {
                const body = res.body
                this.validateQr(body, valid, true, shop)
                expect(body).to.have.property('shop', shop.name)
                expect(body).to.have.property('shop_id', shop.id)
                expect(body).to.have.property('merchant_id', merch.id)
              }.bind(this)).end(response.end(done))
          }.bind(this)).catch(done)
      })
    })
  })

  describe('POST /admin/qrs', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail to create a new qr without params', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_qr_data')
          }).end(response.end(done))
      })

      it('should fail to create a new beacon with invalid params', function (done) {
        request(server)
          .post(testPath)
          .send({ qr: {} })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.validation(res.body,
              {
                name: ['empty'],
                is_active: ['empty'],
                merchant_id: ['empty']
              }
            )
          }).end(response.end(done))
      })

      it('should create a new qr with valid params', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          const valid = {
            merchant_id: merchant.id,
            is_active: false,
            name: 'Test QR'
          }
          request(server)
            .post(testPath)
            .send({ qr: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect(function (res) {
              this.validateJsonQr(res.body, valid, merchant)
            }.bind(this)).end(response.end(done))
        }.bind(this))
      })
    })
  })

  describe('PUT /admin/qrs/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail updating a qr with invalid data', function (done) {
        qrFactory.qr()
          .spread(function (org, merch, shopCluster, shop, qr) {
            request(server)
              .put(testPath + '/ABC')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(400)
              .expect(function (res) {
                response.badRequest(res.body, 'missing_qr_data')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should fail updating a qr with invalid id', function (done) {
        qrFactory.qr()
          .spread(function (org, merch, shopCluster, shop, qr) {
            request(server)
              .put(testPath + '/ABC')
              .send({ qr: {} })
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(400)
              .expect(function (res) {
                response.badRequest(res.body, 'missing_qr_data')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should update a qr with valid id', function (done) {
        qrFactory.qr().spread((org, merch, shopCluster, shop, qr) => {
          const valid = { name: 'Updated QR' }
          request(server)
            .put(testPath + '/' + qr.id)
            .send({ qr: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect(function (res) {
              expect(res.body).to.have.property('id', qr.id)
            }).end((err, res) => {
              if (err) done(err)
              else {
                const body = res.body
                qr.reload().then(() => {
                  this.validateQr(body, qr, true, shop)
                  expect(body).to.have.property('shop_id', shop.id)
                  expect(body).to.have.property('merchant_id', merch.id)
                  done()
                }).catch(done)
              }
            })
        }).catch(done)
      })
    })
  })

  describe('DELETE /admin/qrs/:id', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/qrs/ABC')

      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .del(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/qrs/')

      beforeEach(session.admin)

      it('should fail removing a qr with invalid id', function (done) {
        request(server)
          .del(testPath + 'ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should remove a qr with valid id', function (done) {
        qrFactory.qr()
          .spread(function (org, merch, shopCluster, shop, qr) {
            request(server)
              .del(testPath + qr.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })
})
