/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Analytics Admin Feature Test
 * @file test/features/admin/analytics.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const moment = require('moment')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const merchantFactory = require('../../helpers/factories/merchant')
const validatorFactory = require('../../helpers/factories/validator')
const analyticsValidator = require('../../helpers/validators/analytics')
const userPunchCardFactory = require('../../helpers/factories/userpunchcard')

module.exports = describe('admin/AnalyticsController', function () {
  set('validateAnalytics', function () { return analyticsValidator })

  describe('GET /admin/merchants/:merchant_id/analytics', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      const testPath = path.admin('/merchants/ABC/analytics')
      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      const testPath = path.admin('/merchants/')
      it('should return relevant merchant analytics data', function (done) {
        merchantFactory.model().spread((org, merch) => {
          const orgId = org.id
          const merchId = merch.id
          const future = moment().add(1, 'd')
          return Promise.join(
            userPunchCardFactory.model(
              orgId,
              merchId,
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future // expiresAt
            ),
            validatorFactory.beacon(orgId, merchId),
            userPunchCardFactory.model(
              orgId,
              merchId,
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // userId
              undefined, // identity
              5 // punches
            ),
            // userPunchCardFactory.model(
            //   orgId,
            //   merchId,
            //   undefined, // isPublic
            //   null, // identifier
            //   null, // Category
            //   null, // code
            //   null, // PunchCard
            //   models.PunchCard.Identity.Published, // identity published
            //   future, // expiresAt
            //   null, // userId
            //   models.UserPunchCard.Identity.Ready, // identity
            //   5 // punches
            // ),
            function (userPunchStruct, validatorStruct) {
              const userPunchCard = userPunchStruct[4]
              const beacon = validatorStruct[4]
              return Promise.join(
                models.UserPunchCardInvite.create({ merchantId: merchId, userPunchCardId: userPunchCard.id }),
                models.UserPunchCardValidation.create({ userPunchCardId: userPunchCard.id, validatorId: beacon.id }),
                function (inviteStruct, validationStruct) {
                  ++userPunchCard.punchCount
                  ++userPunchCard.punchCount
                  return userPunchCard.save()
                })
            }
          ).then((card) => {
            request(server)
              .get(testPath + merch.id + '/analytics')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                this.validateAnalytics(res.body)
              }).end(response.end(done))
          })
        }).catch(done)
      })
    })
  })
})
