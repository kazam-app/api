/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Suggestion Admin Feature Test
 * @file test/features/admin/suggestion.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const suggestionFactory = require('../../helpers/factories/suggestion')
const suggestionValidator = require('../../helpers/validators/suggestion')

module.exports = describe('admin/SuggestionController', function () {
  describe('GET /admin/suggestions', function () {
    const testPath = path.admin('/suggestions')

    describe('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return a suggestion list', function (done) {
        suggestionFactory.model().spread((user, suggestion) => {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const element = res.body[0]
              suggestionValidator.model(element, suggestion)
              expect(element).to.have.property('user')
              const user = element.user
              expect(user).to.have.property('identifier', user.identifier)
              expect(user).to.have.property('name', user.name)
              expect(user).to.have.property('email', user.email)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
