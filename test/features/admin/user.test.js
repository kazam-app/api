/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha User Admin Feature Test
 * @file test/features/admin/user.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const userFactory = require('../../helpers/factories/user')
const merchantFactory = require('../../helpers/factories/merchant')

module.exports = describe('admin/UserController', function () {
  describe('GET /admin/users', function () {
    const testPath = path.admin('/users')

    describe('authorization rules', function () {
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return a user list', function (done) {
        userFactory.model().spread(function (user) {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const list = res.body
              expect(list).to.have.lengthOf(1)
              const element = list[0]
              expect(element).to.have.property('name', user.name)
              expect(element).to.have.property('last_names', user.lastNames)
              expect(element).to.have.property('email', user.email)
              expect(element).to.have.property('is_verified', user.isVerified)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('GET /admin/users/:identifier', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/users/ABC')
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/users/')
      beforeEach(session.admin)

      it('should return user detail information', function (done) {
        userFactory.model().spread(function (user) {
          request(server)
            .get(testPath + user.identifier)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const element = res.body
              expect(element).to.have.property('name', user.name)
              expect(element).to.have.property('last_names', user.lastNames)
              expect(element).to.have.property('email', user.email)
              expect(element).to.have.property('is_verified', user.isVerified)
              expect(element).to.have.property('sign_up_type', user.signUpType())
              expect(element).to.have.property('gender', user.gender)
              expect(element).to.have.property('birthdate', user.birthdate)
              expect(element).to.include.keys(['created_at', 'top_merchants', 'top_shops', 'history', 'categories', 'averages'])
              expect(element.top_merchants).to.be.an('array')
              expect(element.top_shops).to.be.an('array')
              expect(element.history).to.be.an('array')
              expect(element.categories).to.be.an('array')
              expect(element.averages).to.be.an('object')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('GET /admin/merchants/:merchant_id/users/:identifier', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/merchants/:merchant_id/users/ABC')
      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/merchants/')
      beforeEach(session.admin)

      it('should return user detail information', function (done) {
        Promise.join(merchantFactory.model(), userFactory.model(), function (merchStruct, userStruct) {
          const merch = merchStruct[1]
          const user = userStruct[0]
          request(server)
            .get(testPath + merch.id + '/users/' + user.identifier)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const element = res.body
              expect(element).to.have.property('name', user.name)
              expect(element).to.have.property('last_names', user.lastNames)
              expect(element).to.have.property('email', user.email)
              expect(element).to.have.property('birthdate', user.birthdate)
              expect(element).to.have.property('merchant_total', 0)
              expect(element).to.have.property('weekly_average', 0)
              expect(element).to.have.property('monthly_average', 0)
              expect(element).to.include.keys(['top_shops', 'history'])
              expect(element.top_shops).to.be.an('array')
              expect(element.history).to.be.an('array')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
