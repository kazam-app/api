/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Merchant Admin Feature Test
 * @file test/features/admin/merchant.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const image = require('../../helpers/image')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const merchantFactory = require('../../helpers/factories/merchant')
const merchantValidator = require('../../helpers/validators/merchant')
const categoryFactory = require('../../helpers/factories/category')
const organizationFactory = require('../../helpers/factories/organization')

module.exports = describe('admin/MerchantController', function () {
  set('validateListMerchant', function () {
    return merchantValidator.adminList
  })

  set('validateMerchant', function () {
    return merchantValidator.model
  })

  set('validateJsonMerchant', function () {
    return merchantValidator.json
  })

  const testPath = path.admin('/merchants')

  describe('GET /admin/merchants', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return a merchant list', function (done) {
        merchantFactory.model().spread((org, merch, category) => {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateListMerchant(body[0], merch)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('GET /admin/merchants/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with invalid token but invalid id', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail with valid token but invalid id', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should return the current merchant', function (done) {
        merchantFactory.model().spread((org, merch, category) => {
          request(server)
            .get(testPath + '/' + merch.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              this.validateMerchant(body, merch, org, category)
              expect(body).to.have.property('category', category.name)
              expect(body).to.have.property('organization', org.name)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('POST /admin/merchants', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail creating a merchant with invalid params', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_merchant_data')
          }).end(response.end(done))
      })

      it('should create a new merchant with minimum valid params', function (done) {
        Promise.join(
          organizationFactory.model(),
          categoryFactory.model(),
          (org, category) => {
            org = org[0]
            category = category[0]
            const validMerchant = {
              organization_id: org.id,
              category_id: category.id,
              name: 'Test Merchant',
              color: '#AAAAAA',
              image: image.base64,
              background_image: image.base64
            }
            request(server)
              .post(testPath)
              .send({ merchant: validMerchant })
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                this.validateJsonMerchant(res.body, validMerchant, category, org)
              }).end(response.end(done))
          }
        )
      })

      it('should create a new merchant with valid params', function (done) {
        Promise.join(
          organizationFactory.model(),
          categoryFactory.model(),
          (org, category) => {
            org = org[0]
            category = category[0]
            const validMerchant = {
              organization_id: org.id,
              category_id: category.id,
              name: 'Test Merchant',
              color: '#AAAAAA',
              budget_rating: 3,
              image: image.base64,
              background_image: image.base64
            }
            request(server)
              .post(testPath)
              .send({ merchant: validMerchant })
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                this.validateJsonMerchant(res.body, validMerchant, category, org)
              }).end(response.end(done))
          }
        )
      })
    })
  })

  describe('PUT /admin/merchants/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail with and invalid params', function (done) {
        merchantFactory.model().spread(function (org, merch, category) {
          request(server)
            .put(testPath + '/' + merch.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_merchant_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail updating a merchant with invalid id', function (done) {
        merchantFactory.model().spread(function (org, merch, category) {
          const validMerchant = { name: 'Updated Merchant' }
          request(server)
            .put(testPath + '/ABC')
            .send({ merchant: validMerchant })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_merchant_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should update a merchant with valid params', function (done) {
        merchantFactory.model().spread((org, merch, category) => {
          const validMerchant = { name: 'Updated Merchant', image: 'TEST_STREAM', background_image: 'TEST_STREAM' }
          request(server)
            .put(testPath + '/' + merch.id)
            .send({ merchant: validMerchant })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              const img = 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/no_aws.jpg'
              const body = res.body
              expect(body).to.have.property('id', merch.id)
              expect(body).to.have.property('name', validMerchant.name)
              expect(body).to.have.property('image_url', img)
              expect(body).to.have.property('background_image_url', img)
            }).end((err, res) => {
              if (err) done(err)
              else {
                merch.reload().then(() => {
                  this.validateMerchant(res.body, merch, org, category)
                  done()
                }).catch(done)
              }
            })
        }).catch(done)
      })
    })
  })

  describe('DELETE /admin/merchants/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .del(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail deleting a merchant with invalid id', function (done) {
        merchantFactory.model().spread(function (org, merch, category) {
          request(server)
            .del(testPath + '/ABC')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should delete a merchant with valid id', function (done) {
        merchantFactory.model().spread(function (org, valid, category) {
          request(server)
            .del(testPath + '/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.eql('true')
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })
})
