/* eslint-env mocha */
/**
 * Merchants Api
 * @name Mocha History Feature Test
 * @file history.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const Promise = require('bluebird')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const merchantFactory = require('../../helpers/factories/merchant')
const historyValidator = require('../../helpers/validators/history')
const validatorFactory = require('../../helpers/factories/validator')
const userPunchCardFactory = require('../../helpers/factories/userpunchcard')

module.exports = describe('admin/HistoryController', function () {
  set('validateHistory', function () { return historyValidator })

  describe('GET /admin/merchants/:merchant_id/history', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      const testPath = path.admin('/merchants/ABC/history')
      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      const testPath = path.admin('/merchants/')
      it('should return relevant merchant analytics data', function (done) {
        merchantFactory.model().spread((org, merch) => {
          const orgId = org.id
          const merchId = merch.id
          return Promise.join(
            userPunchCardFactory.model(orgId, merchId),
            validatorFactory.beacon(orgId, merchId),
            (userPunchStruct, validatorStruct) => {
              const merch = userPunchStruct[1]
              const userPunchCard = userPunchStruct[4]
              const beacon = validatorStruct[4]
              return Promise.join(
                models.UserPunchCardInvite.create({
                  merchantId: merch.id,
                  userPunchCardId: userPunchCard.id
                }),
                models.UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id
                }),
                (inviteStruct, validationStruct) => {
                  request(server)
                    .get(testPath + merch.id + '/history')
                    .set('Authorization', 'Bearer ' + jwtToken)
                    .expect(200)
                    .expect((res) => {
                      const data = res.body
                      expect(data).to.have.lengthOf(1)
                      const element = data[0]
                      this.validateHistory(element)
                      expect(element).to.have.property('name')
                      expect(element).to.have.property('last_names')
                      expect(element).to.have.property('email')
                    }).end(response.end(done))
                }
              )
            }
          )
        }).catch(done)
      })
    })
  })
})
