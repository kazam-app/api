/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Beacon Feature Test
 * @file test/features/admin/beacon.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const beaconFactory = require('../../helpers/factories/validator')
const merchantFactory = require('../../helpers/factories/merchant')
const beaconValidator = require('../../helpers/validators/validator')

module.exports = describe('admin/BeaconController', function () {
  set('validateBeacon', function () {
    return beaconValidator.beacon
  })

  set('validateJsonBeacon', function () {
    return beaconValidator.beaconJson
  })

  const testPath = path.admin('/beacons')

  describe('GET /admin/beacons', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return a beacon list', function (done) {
        beaconFactory.beacon()
          .spread((org, merch, shopCluster, shop, beacon) => {
            request(server)
              .get(testPath)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                const body = res.body
                expect(body).to.have.lengthOf(1)
                this.validateBeacon(body[0], beacon, true, shop)
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })

  describe('GET /admin/beacons/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail retreving a beacon with invalid id', function (done) {
        beaconFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            request(server)
              .get(testPath + '/ABC')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.badRequest)
              .expect(function (res) {
                response.badRequest(res.body, 'not_found')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should retreive a beacon with valid id', function (done) {
        beaconFactory.beacon()
          .spread((org, merch, shopCluster, shop, valid) => {
            request(server)
              .get(testPath + '/' + valid.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect((res) => {
                const body = res.body
                this.validateBeacon(body, valid, true)
                expect(body).to.have.property('shop', shop.name)
                expect(body).to.have.property('shop_id', shop.id)
                expect(body).to.have.property('merchant_id', merch.id)
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })

  describe('POST /admin/beacons', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail to create a new beacon without params', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_beacon_data')
          }).end(response.end(done))
      })

      it('should fail to create a new beacon with invalid params', function (done) {
        request(server)
          .post(testPath)
          .send({ beacon: {} })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.validation(res.body,
              {
                name: ['empty'],
                is_active: ['empty'],
                merchant_id: ['empty'],
                uid: ['empty'],
                major: ['empty'],
                minor: ['empty']
              }
            )
          }).end(response.end(done))
      })

      it('should create a new beacon with valid params', function (done) {
        merchantFactory.model().spread((org, merchant) => {
          const valid = {
            merchant_id: merchant.id,
            is_active: false,
            name: 'Test Beacon',
            uid: 'ABC',
            major: 1,
            minor: 1
          }
          request(server)
            .post(testPath)
            .send({ beacon: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.created)
            .expect((res) => {
              this.validateJsonBeacon(res.body, valid, merchant)
            }).end(response.end(done))
        })
      })
    })
  })

  describe('PUT /admin/beacons/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail updating a beacon with invalid data', function (done) {
        beaconFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            request(server)
              .put(testPath + '/ABC')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.badRequest)
              .expect(function (res) {
                response.badRequest(res.body, 'missing_beacon_data')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should fail updating a beacon with invalid id', function (done) {
        beaconFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            request(server)
              .put(testPath + '/ABC')
              .send({ beacon: {} })
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.badRequest)
              .expect(function (res) {
                response.badRequest(res.body, 'missing_beacon_data')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should update a beacon with valid id', function (done) {
        beaconFactory.beacon()
          .spread((org, merch, shopCluster, shop, beacon) => {
            const valid = { name: 'Updated Beacon' }
            request(server)
              .put(testPath + '/' + beacon.id)
              .send({ beacon: valid })
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect(function (res) {
                expect(res.body).to.have.property('id', beacon.id)
              }).end((err, res) => {
                if (err) done(err)
                else {
                  const body = res.body
                  beacon.reload().then(() => {
                    this.validateBeacon(body, beacon, true)
                    expect(body).to.have.property('shop_id', shop.id)
                    expect(body).to.have.property('merchant_id', merch.id)
                    done()
                  }).catch(done)
                }
              })
          }).catch(done)
      })
    })
  })

  describe('DELETE /admin/beacons/:id', function () {
    describe('authorization rules', function () {
      const testPath = path.admin('/beacons/ABC')

      beforeEach(session.fullReadWrite)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .del(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      const testPath = path.admin('/beacons/')

      beforeEach(session.admin)

      it('should fail removing a beacon with invalid id', function (done) {
        request(server)
          .del(testPath + 'ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should remove a beacon with valid id', function (done) {
        beaconFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            request(server)
              .del(testPath + beacon.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(response.status.success)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })
})
