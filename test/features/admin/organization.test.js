/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Organization Feature Test
 * @file test/controllers/organization.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const request = require('supertest')
const path = require('../../helpers/path')
const session = require('../../helpers/session')
const response = require('../../helpers/response')
const organizationFactory = require('../../helpers/factories/organization')
const organizationValidator = require('../../helpers/validators/organization')

module.exports = describe('admin/OrganizationController', function () {
  set('validateOrganization', function () {
    return organizationValidator.model
  })

  const testPath = path.admin('/organizations')

  describe('GET /admin/organizations', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .get(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should return an organization list', function (done) {
        organizationFactory.model().spread((org) => {
          request(server)
            .get(testPath)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateOrganization(body[0], org)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('GET /admin/organizations/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with invalid token but invalid id', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail with valid token but invalid id', function (done) {
        request(server)
          .get(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should return the current organization', function (done) {
        organizationFactory.model().spread((org) => {
          request(server)
            .get(testPath + '/' + org.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              this.validateOrganization(res.body, org)
            }).end(response.end(done))
        }).catch(done)
      })
    })
  })

  describe('POST /admin/organizations', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post(testPath)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail creating a organization with invalid params', function (done) {
        request(server)
          .post(testPath)
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.badRequest)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_organization_data')
          }).end(response.end(done))
      })

      it('should create a new organization with valid params', function (done) {
        const valid = { name: 'Test Org' }
        request(server)
          .post(testPath)
          .send({ organization: valid })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.success)
          .expect((res) => {
            this.validateOrganization(res.body, valid)
          }).end(response.end(done))
      })
    })
  })

  describe('PUT /admin/organizations/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only merchant user', function (done) {
        request(server)
          .put(testPath + '/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(response.status.unauthorized)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.admin)

      it('should fail with and invalid params', function (done) {
        organizationFactory.model().spread(function (org) {
          request(server)
            .put(testPath + '/' + org.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_organization_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail updating an organization with invalid id', function (done) {
        organizationFactory.model().spread(function (org) {
          const valid = { name: 'Updated Org' }
          request(server)
            .put(testPath + '/ABC')
            .send({ organization: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.badRequest)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_organization_data')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should update a organization with valid params', function (done) {
        organizationFactory.model().spread((org) => {
          const valid = { name: 'Updated Org' }
          request(server)
            .put(testPath + '/' + org.id)
            .send({ organization: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect(function (res) {
              expect(res.body).to.have.property('id', org.id)
            }).end((err, res) => {
              if (err) done(err)
              else {
                org.reload().then(() => {
                  this.validateOrganization(res.body, org)
                  done()
                }).catch(done)
              }
            })
        }).catch(done)
      })
    })
  })
})
