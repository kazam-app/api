/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha PunchCard Feature Test
 * @file test/features/punchard.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const i18n = require('i18n')
const set = require('mocha-let')
const moment = require('moment')
const Promise = require('bluebird')
const request = require('supertest')
// const path = require('../helpers/path')
const session = require('../helpers/session')
const response = require('../helpers/response')
const userFactory = require('../helpers/factories/user')
const merchantFactory = require('../helpers/factories/merchant')
const punchCardFactory = require('../helpers/factories/punchcard')
const punchCardValidator = require('../helpers/validators/punchcard')

module.exports = describe('PunchCardController', function () {
  set('validatePunchCard', function () {
    return punchCardValidator.model
  })

  set('validateListPunchCard', function () {
    return punchCardValidator.list
  })

  set('validateJsonPunchCard', function () {
    return punchCardValidator.json
  })

  describe('GET /punch_cards', function () {
    beforeEach(session.fullRead)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/punch_cards')
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a list of PunchCards filtered by a merchant', function (done) {
      Promise.join(
        punchCardFactory.model(),
        punchCardFactory.model(jwtOrganization.id, jwtMerchant.id),
        (invalid, valid) => {
          valid = valid[2]
          request(server)
            .get('/punch_cards')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              expect(body).to.have.lengthOf(1)
              this.validateListPunchCard(body[0], valid)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })

  describe('GET /punch_cards/active', function () {
    beforeEach(session.fullRead)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/punch_cards/active')
        .expect(response.status.unauthorized)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a published PunchCard filtered by a merchant', function (done) {
      Promise.join(
        punchCardFactory.model(jwtOrganization.id, jwtMerchant.id),
        punchCardFactory.model(
          jwtOrganization.id,
          jwtMerchant.id,
          undefined, // isPublic
          null, // identifier
          null, // catId
          null, // code,
          null, // null,
          models.PunchCard.Identity.Published,
          moment().add(1, 'days')
        ),
        (invalid, valid) => {
          valid = valid[2]
          request(server)
            .get('/punch_cards/active')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(response.status.success)
            .expect((res) => {
              const body = res.body
              this.validatePunchCard(body, valid)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })

  describe('GET /punch_cards/latest', function () {
    beforeEach(session.fullRead)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/punch_cards/latest')
        .expect(401)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a published PunchCard filtered by a merchant', function (done) {
      Promise.join(
        punchCardFactory.model(jwtOrganization.id, jwtMerchant.id),
        punchCardFactory.model(
          jwtOrganization.id,
          jwtMerchant.id,
          undefined, // isPublic
          null, // identifier
          null, // catId
          null, // code,
          null, // null,
          models.PunchCard.Identity.UnPublished,
          moment().add(1, 'days')
        ),
        (invalid, valid) => {
          valid = valid[2]
          request(server)
            .get('/punch_cards/latest')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect((res) => {
              const body = res.body
              this.validatePunchCard(body, valid)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })

  describe('POST /punch_cards', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .post('/punch_cards')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .post('/punch_cards')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail to create a new punch card with invalid params', function (done) {
        request(server)
          .post('/punch_cards')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_punch_card_data')
          }).end(response.end(done))
      })

      it('should fail to create a new punch card with invalid date', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          const invalid = {
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 3,
            redeem_code: 'REDEEMCODE5',
            expires_at: 'INVALID_DATE'
          }
          request(server)
            .post('/punch_cards')
            .send({ punch_card: invalid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(400)
            .expect(function (res) {
              response.badRequest(res.body, 'missing_punch_card_data')
            }).end(response.end(done))
        })
      })

      it('should fail to create a new punch card with invalid redeem code')

      it('should fail to create a new punch card with less than 3 punch limit', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          const invalid = {
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 1,
            expires_at: new Date()
          }
          request(server)
            .post('/punch_cards')
            .send({ punch_card: invalid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(400)
            .expect(function (res) {
              response.validation(res.body, {
                punch_limit: ['less_than']
              })
            }).end(response.end(done))
        })
      })

      it('should fail to create a new punch card with more than 15 punch limit', function (done) {
        merchantFactory.model().spread(function (org, merchant) {
          const invalid = {
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 16,
            expires_at: new Date()
          }
          request(server)
            .post('/punch_cards')
            .send({ punch_card: invalid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(400)
            .expect(function (res) {
              response.validation(res.body, {
                punch_limit: ['greater_than']
              })
            }).end(response.end(done))
        })
      })

      it('should create a new punch card with valid params', function (done) {
        merchantFactory.model().spread((org, merchant) => {
          const valid = {
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 3,
            redeem_code: 'REDEEMCODE5',
            expires_at: new Date()
          }
          request(server)
            .post('/punch_cards')
            .send({ punch_card: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect((res) => {
              const body = res.body
              this.validateJsonPunchCard(body, valid, merchant)
              expect(body).to.have.property('validation_limit', null)
            }).end(response.end(done))
        })
      })

      it('should create a new punch card with valid params and validation limit', function (done) {
        merchantFactory.model().spread((org, merchant) => {
          const valid = {
            name: 'Test PunchCard',
            prize: '2x1 Cafe',
            rules: 'Money',
            terms: 'Test terms',
            punch_limit: 3,
            redeem_code: 'REDEEMCODE5',
            expires_at: new Date(),
            validation_limit: 3
          }
          request(server)
            .post('/punch_cards')
            .send({ punch_card: valid })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect((res) => {
              const body = res.body
              this.validateJsonPunchCard(body, valid, merchant)
              expect(body).to.have.property('validation_limit', valid.validation_limit)
            }).end(response.end(done))
        })
      })
    })
  })

  describe('GET /punch_cards/:id', function () {
    beforeEach(session.fullRead)

    it('should expect valid authorization header', function (done) {
      request(server)
        .get('/punch_cards/ABC')
        .expect(401)
        .expect(function (res) {
          response.unauthorized(res.body)
        }).end(response.end(done))
    })

    it('should return a published PunchCard filtered by a merchant', function (done) {
      Promise.join(
        punchCardFactory.model(jwtOrganization.id, jwtMerchant.id),
        punchCardFactory.model(
          jwtOrganization.id,
          jwtMerchant.id,
          undefined, // isPublic
          null, // identifier
          null, // catId
          null, // code,
          null, // null,
          models.PunchCard.Identity.Published,
          moment().add(1, 'days')
        ),
        (invalid, valid) => {
          valid = valid[2]
          request(server)
            .get('/punch_cards/' + valid.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(200)
            .expect((res) => {
              const body = res.body
              this.validatePunchCard(body, valid)
            }).end(response.end(done))
        }
      ).catch(done)
    })
  })

  describe('PUT /punch_cards/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put('/punch_cards/ABC')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put('/punch_cards/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail updating a punch card with invalid id', function (done) {
        request(server)
          .put('/punch_cards/ABC')
          .send({ punch_card: {} })
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'missing_punch_card_data')
          }).end(response.end(done))
      })

      it('should fail updating a punch card with valid id from another organization', function (done) {
        punchCardFactory.model().spread(function (org, merchant, punch) {
          request(server)
            .put('/punch_cards/' + punch.id)
            .send({ punch_card: {} })
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(400)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should update a punch card with valid data', function (done) {
        const valid = { name: 'Updated PunchCard', prize: 'Updated Prize', redeem_code: 'UPDATEDCODE', validation_limit: 10 }
        punchCardFactory.model(jwtOrganization.id, jwtMerchant.id)
          .spread((org, merch, punch) => {
            request(server)
              .put('/punch_cards/' + punch.id)
              .send({ punch_card: valid })
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect(function (res) {
                expect(res.body).to.have.property('id', punch.id)
              }).end((err, res) => {
                if (err) done(err)
                else {
                  punch.reload().then(() => {
                    this.validatePunchCard(res.body, punch)
                    done()
                  })
                }
              })
          }).catch(done)
      })
    })
  })

  describe('PUT /punch_cards/:id/publish', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .put('/punch_cards/ABC/publish')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .put('/punch_cards/ABC/publish')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail publishing a punch card with invalid id', function (done) {
        request(server)
          .put('/punch_cards/ABC/publish')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should fail publishing a punch card with valid id from another organization', function (done) {
        punchCardFactory.model().spread(function (org, merchant, punch) {
          request(server)
            .put('/punch_cards/' + punch.id + '/publish')
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(400)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail publishing a punch card with valid id from with another published punch card', function (done) {
        Promise.join(
          punchCardFactory.model(
            jwtOrganization.id, // Organization
            jwtMerchant.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity
            moment().add(1, 'days') // expiresAt
          ),
          punchCardFactory.model(jwtOrganization.id, jwtMerchant.id),
          function (published, unpublished) {
            const punch = unpublished[2]
            request(server)
              .put('/punch_cards/' + punch.id + '/publish')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(400)
              .expect(function (res) {
                response.badRequest(res.body, 'active_punch_card_exists')
              }).end(response.end(done))
          }).catch(done)
      })

      it('should publish a punch card with valid id', function (done) {
        Promise.join(
          userFactory.model(),
          punchCardFactory.model(jwtOrganization.id, jwtMerchant.id),
          punchCardFactory.model( // Published from another merchant
            null, // Organization
            null, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity
            moment().add(1, 'days') // expiresAt
          ),
          (user, punchStruct) => {
            const punch = punchStruct[2]
            request(server)
              .put('/punch_cards/' + punch.id + '/publish')
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect((res) => {
                this.validatePunchCard(res.body, punch)
              }).end(response.end(done))
          }
        )
      })
    })
  })

  describe('DELETE /punch_cards/:id', function () {
    describe('authorization rules', function () {
      beforeEach(session.fullRead)

      it('should expect valid authorization header', function (done) {
        request(server)
          .del('/punch_cards/ABC')
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body)
          }).end(response.end(done))
      })

      it('should fail with valid token but only read role', function (done) {
        request(server)
          .del('/punch_cards/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(401)
          .expect(function (res) {
            response.unauthorized(res.body, i18n.__('organization_auth_fail'))
          }).end(response.end(done))
      })
    })

    describe('feature', function () {
      beforeEach(session.fullReadWrite)

      it('should fail removing a punch card with invalid id', function (done) {
        request(server)
          .del('/punch_cards/ABC')
          .set('Authorization', 'Bearer ' + jwtToken)
          .expect(400)
          .expect(function (res) {
            response.badRequest(res.body, 'not_found')
          }).end(response.end(done))
      })

      it('should fail removing a punch card with valid id from another organization', function (done) {
        punchCardFactory.model().spread(function (org, merchant, punch) {
          request(server)
            .del('/punch_cards/' + punch.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(400)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should fail removing a published punch card with valid id', function (done) {
        punchCardFactory.model(
          jwtOrganization.id, // Organization
          jwtMerchant.id, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // id
          models.PunchCard.Identity.Published // Identity
        ).spread(function (org, merchant, punch) {
          request(server)
            .del('/punch_cards/' + punch.id)
            .set('Authorization', 'Bearer ' + jwtToken)
            .expect(400)
            .expect(function (res) {
              response.badRequest(res.body, 'not_found')
            }).end(response.end(done))
        }).catch(done)
      })

      it('should remove a punch card with valid id', function (done) {
        punchCardFactory.model(jwtOrganization.id, jwtMerchant.id)
          .spread(function (org, merchant, punch) {
            request(server)
              .del('/punch_cards/' + punch.id)
              .set('Authorization', 'Bearer ' + jwtToken)
              .expect(200)
              .expect(function (res) {
                expect(res.body).to.eql('true')
              }).end(response.end(done))
          }).catch(done)
      })
    })
  })
})
