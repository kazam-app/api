/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Validator Test
 * @file test/models/validator.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const uuid = require('uuid/v4')
const Promise = require('bluebird')
const shopFactory = require('../helpers/factories/shop')
const validatorFactory = require('../helpers/factories/validator')

module.exports = describe('Validator', function () {
  describe('instanceMethods', function () {
    describe('#toRegion', function () {
      it('should return region data', function () {
        return validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const result = beacon.toRegion()
            expect(result).to.be.an('object')
            expect(result).to.have.property('uid', beacon.uid)
            expect(result).to.have.property('major', beacon.major)
            expect(result).to.have.property('minor', beacon.minor)
          })
      })
    })

    describe('#toBeacon', function () {
      it('should return beacon data', function () {
        return validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const result = beacon.toBeacon()
            expect(result).to.be.an('object')
            expect(result).to.have.property('id', beacon.id)
            expect(result).to.have.property('name', beacon.name)
            expect(result).to.have.property('is_active', beacon.isActive)
          })
      })
    })

    describe('#toAdminBeacon', function () {
      it('should return beacon data', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const result = beacon.toAdminBeacon()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', beacon.id)
          expect(result).to.have.property('merchant_id', merch.id)
          expect(result).to.have.property('shop_id', shop.id)
          expect(result).to.have.property('name', beacon.name)
          expect(result).to.have.property('uid', beacon.uid)
          expect(result).to.have.property('major', beacon.major)
          expect(result).to.have.property('minor', beacon.minor)
          expect(result).to.have.property('is_active', beacon.isActive)
        })
      })
    })

    describe('#toQr', function () {
      it('should return qr data', function () {
        return validatorFactory.qr().spread(function (org, merch, shopCluster, shop, qr) {
          const result = qr.toBeacon()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', qr.id)
          expect(result).to.have.property('name', qr.name)
          expect(result).to.have.property('is_active', qr.isActive)
        })
      })
    })

    describe('#toAdminQr', function () {
      it('should return qr data', function () {
        return validatorFactory.qr().spread(function (org, merch, shopCluster, shop, qr) {
          const result = qr.toAdminQr()
          expect(result).to.be.an('object')
          expect(result).to.have.property('merchant_id', merch.id)
          expect(result).to.have.property('shop_id', shop.id)
          expect(result).to.have.property('id', qr.id)
          expect(result).to.have.property('name', qr.name)
          expect(result).to.have.property('is_active', qr.isActive)
        })
      })
    })
  })

  describe('classMethods', function () {
    context('beacon', function () {
      describe('#createBeacon', function () {
        it('should fail to create a new Beacon with invalid data', function () {
          return models.Validator.createBeacon({}).then(function () {
            throw new Error('created a Beacon with an invalid data')
          }).catch(function (err) {
            expect(err.message).to.eql('missing_beacon_data')
          })
        })

        it('should create a new Beacon with valid data', function () {
          return shopFactory.model()
            .spread(function (org, merch, shopCluster, shop) {
              const valid = {
                merchant_id: merch.id,
                shop_id: shop.id,
                is_active: false,
                name: 'Test Beacon',
                uid: uuid(),
                major: 1,
                minor: 1
              }
              return models.Validator.createBeacon({ beacon: valid })
                .then(function (result) {
                  expect(result).to.have.property(
                    'identity',
                    models.Validator.Identity.Beacon
                  )
                  expect(result).to.have.property(
                    'merchantId',
                    valid.merchant_id
                  )
                  expect(result).to.have.property('shopId', valid.shop_id)
                  expect(result).to.have.property('name', valid.name)
                  expect(result).to.have.property('uid', valid.uid)
                  expect(result).to.have.property('major', valid.major)
                  expect(result).to.have.property('minor', valid.major)
                })
            })
        })
      })

      describe('#updateBeacon', function () {
        it('should fail to update a Beacon with invalid data', function () {
          return validatorFactory.beacon()
            .spread(function (org, merch, shopCluster, shop, beacon) {
              return models.Validator.updateBeacon('INVALID_ID', {
                beacon: { name: 'Test Beacon 2' }
              })
            }).then(function () {
              throw new Error('updated Beacon with invalid id')
            }).catch(function (err) {
              expect(err.message).to.eql('missing_beacon_data')
            })
        })

        it('should fail to update a Beacon with invalid id', function () {
          return validatorFactory.beacon()
            .spread(function (org, merch, shopCluster, shop, beacon) {
              return models.Validator.updateBeacon(-1, {
                beacon: { name: 'Test Beacon 2' }
              })
            }).then(function () {
              throw new Error('updated Beacon with invalid id')
            }).catch(function (err) {
              expect(err.message).to.eql('not_found')
            })
        })

        it('should update a Beacon show with valid data', function () {
          return validatorFactory.beacon()
            .spread(function (org, merch, shopCluster, shop, beacon) {
              const validName = 'Test Beacon 2'
              return models.Validator.updateBeacon(beacon.id, {
                beacon: { name: validName }
              }).then(function (result) {
                expect(result).to.have.property('merchantId', merch.id)
                expect(result).to.have.property('shopId', shop.id)
                expect(result).to.have.property('name', validName)
              })
            })
        })
      })

      describe('#deleteBeacon', function () {
        it('should fail to delete a Beacon with invalid data', function () {
          return validatorFactory.beacon()
            .spread(function (org, merch, shopCluster, shop, beacon) {
              return models.Validator.deleteBeacon('INVALID_ID')
            }).then(function () {
              throw new Error('deleted Beacon with invalid id')
            }).catch(function (err) {
              expect(err.message).to.eql('not_found')
            })
        })

        it('should delete a Beacon show with valid data', function () {
          return validatorFactory.beacon()
            .spread(function (org, merch, shopCluster, shop, beacon) {
              return models.Validator.deleteBeacon(beacon.id)
                .then(function (result) {
                  expect(result).to.eq(true)
                })
            })
        })
      })
    })

    context('qr', function () {
      describe('#createQr', function () {
        it('should fail to create a new QR with invalid data', function () {
          return models.Validator.createQr({}).then(function () {
            throw new Error('created a QR with an invalid data')
          }).catch(function (err) {
            expect(err.message).to.eql('missing_qr_data')
          })
        })

        it('should create a new QR with valid data', function () {
          return shopFactory.model()
            .spread(function (org, merch, shopCluster, shop) {
              const valid = {
                merchant_id: merch.id,
                shop_id: shop.id,
                is_active: false,
                name: 'Test QR'
              }
              return models.Validator.createQr({ qr: valid })
                .then(function (result) {
                  expect(result).to.have.property(
                    'merchantId',
                    valid.merchant_id
                  )
                  expect(result).to.have.property('shopId', valid.shop_id)
                  expect(result).to.have.property('name', valid.name)
                  expect(result).to.have.property(
                    'identity',
                    models.Validator.Identity.QR
                  )
                })
            })
        })
      })

      describe('#updateQr', function () {
        it('should fail to update a Qr with invalid data', function () {
          return validatorFactory.qr()
            .spread(function (org, merch, shopCluster, shop, qr) {
              return models.Validator.updateQr('INVALID_ID', {
                qr: { name: 'Test QR 2' }
              })
            }).then(function () {
              throw new Error('updated Qr with invalid id')
            }).catch(function (err) {
              expect(err.message).to.eql('missing_qr_data')
            })
        })

        it('should fail to update a Qr with invalid id', function () {
          return validatorFactory.qr()
            .spread(function (org, merch, shopCluster, shop, qr) {
              return models.Validator.updateQr(-1, {
                qr: {}
              })
            }).then(function () {
              throw new Error('updated Qr with invalid id')
            }).catch(function (err) {
              expect(err.message).to.eql('not_found')
            })
        })

        it('should update a Qr with valid data', function () {
          return Promise.join(
            shopFactory.model(),
            validatorFactory.qr()
          ).spread(function (shopStruct, qrStruct) {
            const merch = shopStruct[1]
            const shop = shopStruct[3]
            const qr = qrStruct[4]
            return models.Validator.updateQr(qr.id, {
              qr: {
                merchant_id: merch.id,
                shop_id: shop.id
              }
            }).then(function (result) {
              expect(result).to.have.property('merchantId', merch.id)
              expect(result).to.have.property('shopId', shop.id)
            })
          })
        })
      })

      describe('#deleteQr', function () {
        it('should fail to delete a QR with invalid data', function () {
          return validatorFactory.qr()
            .spread(function (org, merch, shopCluster, shop, qr) {
              return models.Validator.deleteQr('INVALID_ID')
            }).then(function () {
              throw new Error('deleted Qr with invalid id')
            }).catch(function (err) {
              expect(err.message).to.eql('not_found')
            })
        })

        it('should delete a QR show with valid data', function () {
          return validatorFactory.qr()
            .spread(function (org, merch, shopCluster, shop, qr) {
              return models.Validator.deleteQr(qr.id)
                .then(function (result) {
                  expect(result).to.eq(true)
                })
            })
        })
      })
    })
  })
})
