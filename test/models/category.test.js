/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Category Test
 * @file test/models/category.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const moment = require('moment')
const categoryFactory = require('../helpers/factories/category')
const validatorFactory = require('../helpers/factories/validator')
const userPunchCardFactory = require('../helpers/factories/userpunchcard')

module.exports = describe('Category', function () {
  describe('instanceMethods', function () {
    describe('#toShow', function () {
      it('should return show data', function () {
        return categoryFactory.model().spread(function (cat) {
          const result = cat.toShow()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', cat.id)
          expect(result).to.have.property('name', cat.name)
          expect(result).to.have.property('image_url', cat.imageUrl)
        })
      })
    })
  })

  describe('classMethods', function () {
    describe('#toShow', function () {
      it('should return toShow', function (done) {
        expect(models.Category.toShow).to.eql('toShow')
        done()
      })
    })

    describe('#toList', function () {
      it('should return toShow', function (done) {
        expect(models.Category.toList).to.eql('toShow')
        done()
      })
    })

    describe('#list', function () {
      it('should return an array of Categories', function () {
        return categoryFactory.model().spread(function (cat) {
          return models.Category.list().then(function (result) {
            expect(result).to.be.an('array')
            expect(result).to.have.lengthOf(1)
            const element = result[0]
            expect(element.id).to.eql(cat.id)
            expect(element.name).to.eql(cat.name)
            expect(element.imageUrl).to.eql(cat.imageUrl)
          })
        })
      })
    })

    describe('#createFromData', function () {
      it('should fail to create a new Category with invalid data', function () {
        return models.Category.createFromData({}).then(function () {
          throw new Error('created a Category with an invalid data')
        }).catch(function (err) {
          expect(err.message).to.eql('missing_category_data')
        })
      })

      it('should create a new Category with valid data', function () {
        const valid = { name: 'Test Category' }
        return models.Category.createFromData({ category: valid }).then(function (result) {
          expect(result).to.have.property('name', valid.name)
        })
      })
    })

    describe('#show', function () {
      it('should fail to show a Category with invalid id', function () {
        return categoryFactory.model().spread(function () {
          return models.Category.show('ABC')
        }).then(function () {
          throw new Error('found a Category via an invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should fail to show a Category with invalid id', function () {
        return categoryFactory.model().spread(function () {
          return models.Category.show(1)
        }).then(function () {
          throw new Error('found a Category via an invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should return a Category', function () {
        return categoryFactory.model().spread(function (cat) {
          return models.Category.show(cat.id).then(function (result) {
            expect(result.name).to.eql(cat.name)
            expect(result.imageUrl).to.eql(cat.imageUrl)
          })
        })
      })
    })

    describe('#updateFromData', function () {
      it('should fail to update a Category with invalid data', function () {
        return models.Category.updateFromData('INVALID_ID', {
          category: { name: 'Test Category 2' }
        }).then(function () {
          throw new Error('updated Category with invalid id')
        }).catch(function (err) {
          expect(err.message).to.eq('missing_category_data')
        })
      })

      it('should update a Category with valid data', function () {
        return categoryFactory.model().spread(function (cat) {
          let valid = { category: { name: 'Test Category 2' } }
          return models.Category.updateFromData(cat.id, valid)
        })
      })
    })

    describe('#delete', function () {
      it('should fail to remove a Category with invalid id', function () {
        return categoryFactory.model().spread(function () {
          return models.Category.delete('ABC')
        }).then(function () {
          throw new Error('found a Category via an invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should remove a Category with valid id', function () {
        return categoryFactory.model().spread(function (cat) {
          return models.Category.delete(cat.id).then(function (result) {
            expect(result).to.eql(true)
          })
        })
      })
    })

    describe('#breakdownForUser', function () {
      it('should return a Category percentage list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const UserPunchCardValidation = models.UserPunchCardValidation
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon, cat) {
          const future = moment(current).add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id, userPunchCardId: userPunchCard.id, created_at: moment(current)
            }).then(function (validation) {
              return [merch, shop, user, userPunchCard, validation, cat]
            })
          })
        }).spread(function (merch, shop, user, userPunchCard, validation, cat) {
          // #historyForUser testing
          return models.Category.breakdownForUser(user).then(function (result) {
            expect(result).to.be.an('array')
            expect(result).to.have.lengthOf(1)
            const element = result[0]
            expect(element).to.be.an('object')
            expect(element).to.have.property('value', '100.00')
            expect(element).to.have.property('name', cat.name)
          })
        })
      })
    })

    describe('#breakdownForUserByMerchant', function () {
      it('should return a Category percentage list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const UserPunchCardValidation = models.UserPunchCardValidation
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon, cat) {
          const future = moment(current).add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id, userPunchCardId: userPunchCard.id, created_at: moment(current)
            }).then(function (validation) {
              return [merch, shop, user, userPunchCard, validation, cat]
            })
          })
        }).spread(function (merch, shop, user, userPunchCard, validation, cat) {
          // #historyForUser testing
          return models.Category.breakdownForUser(user, merch).then(function (result) {
            expect(result).to.be.an('array')
            expect(result).to.have.lengthOf(1)
            const element = result[0]
            expect(element).to.be.an('object')
            expect(element).to.have.property('value', '100.00')
            expect(element).to.have.property('name', cat.name)
          })
        })
      })
    })
  })
})
