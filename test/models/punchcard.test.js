/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha PunchCard Test
 * @file test/models/punchcard.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const moment = require('moment')
const Promise = require('bluebird')
const userFactory = require('../helpers/factories/user')
const merchantFactory = require('../helpers/factories/merchant')
const punchCardFactory = require('../helpers/factories/punchcard')

module.exports = describe('PunchCard', function () {
  describe('instanceMethods', function () {
    describe('#toShow', function () {
      it('should return show apprpriate values', function () {
        return punchCardFactory.model().spread(function (org, merch, punch) {
          const result = punch.toShow()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', punch.id)
          expect(result).to.have.property('identity', punch.identity)
          expect(result).to.have.property('name', punch.name)
          expect(result).to.have.property('prize', punch.prize)
          expect(result).to.have.property('rules', punch.rules)
          expect(result).to.have.property('terms', punch.terms)
          expect(result).to.have.property('punch_limit', punch.punchLimit)
          expect(result).to.include.keys('expires_at')
        })
      })
    })

    describe('#toAdminShow', function () {
      it('should return admin show apprpriate values', function () {
        return punchCardFactory.model().spread(function (org, merch, punch) {
          const result = punch.toShow()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', punch.id)
          expect(result).to.have.property('identity', punch.identity)
          expect(result).to.have.property('name', punch.name)
          expect(result).to.have.property('prize', punch.prize)
          expect(result).to.have.property('rules', punch.rules)
          expect(result).to.have.property('terms', punch.terms)
          expect(result).to.have.property('punch_limit', punch.punchLimit)
          expect(result).to.include.keys('expires_at')
        })
      })
    })
  })

  describe('classMethods', function () {
    context('organization', function () {
      describe('#findActive', function () {
        it('should only return non expired punchards', function () {
          return Promise.join(
            punchCardFactory.model(),
            punchCardFactory.model(
              null, // orgId
              null, // merchId
              undefined, // isPublic
              null, // identifier
              null, // catId
              null, // code,
              null, // null,
              models.PunchCard.Identity.Published,
              moment().add(1, 'days')
            ),
            function (invalid, valid) {
              const punchCard = valid[2]
              return models.PunchCard.findActive().then(function (result) {
                expect(result).to.have.lengthOf(1)
                const element = result[0]
                expect(element.id).to.eql(punchCard.id)
              })
            }
          )
        })
      })

      describe('#latest', function () {
        it('should return the current published punch card for a merchant', function () {
          return merchantFactory.model().spread(function (org, merchant) {
            return Promise.join(
              punchCardFactory.model(),
              punchCardFactory.model(
                null, // orgId
                null, // merchId
                undefined, // isPublic
                null, // identifier
                null, // catId
                null, // code,
                null, // null,
                models.PunchCard.Identity.Published,
                moment().add(1, 'days')
              ),
              punchCardFactory.model(
                org.id, // orgId
                merchant.id, // merchId
                undefined, // isPublic
                null, // identifier
                null, // catId
                null, // code,
                null, // null,
                models.PunchCard.Identity.Published,
                moment().add(1, 'days')
              ),
              function (invalid, invalid2, valid) {
                const punchCard = valid[2]
                return models.PunchCard.latest(merchant).then(function (result) {
                  expect(result.id).to.eql(punchCard.id)
                  expect(result.identity).to.eql(punchCard.identity)
                  expect(result.name).to.eql(punchCard.name)
                  expect(result.prize).to.eql(punchCard.prize)
                })
              }
            )
          })
        })
      })

      describe('#active', function () {
        it('should return the current published punch card for a merchant', function (done) {
          merchantFactory.model().spread(function (org, merchant) {
            return Promise.join(
              punchCardFactory.model(),
              punchCardFactory.model(
                null, // orgId
                null, // merchId
                undefined, // isPublic
                null, // identifier
                null, // catId
                null, // code,
                null, // null,
                models.PunchCard.Identity.Published,
                moment().add(1, 'days')
              ),
              punchCardFactory.model(
                org.id, // orgId
                merchant.id, // merchId
                undefined, // isPublic
                null, // identifier
                null, // catId
                null, // code,
                null, // null,
                models.PunchCard.Identity.Published,
                moment().add(1, 'days')
              ),
              function (invalid, invalid2, valid) {
                const punchCard = valid[2]
                models.PunchCard.active(merchant).then(function (result) {
                  expect(result.id).to.eql(punchCard.id)
                  // expect(result.identity).to.eql(punchCard.identity)
                  expect(result.name).to.eql(punchCard.name)
                  expect(result.prize).to.eql(punchCard.prize)
                  done()
                })
              }
            ).catch(done)
          }).catch(done)
        })
      })

      describe('#list', function () {
        it('should only return non expired punchards', function () {
          return Promise.join(
            punchCardFactory.model(),
            punchCardFactory.model(
              null, // orgId
              null, // merchId
              undefined, // isPublic
              null, // identifier
              null, // catId
              null, // code,
              null, // null,
              models.PunchCard.Identity.Published,
              moment().add(1, 'days')
            ),
            function (invalid, valid) {
              const merchant = valid[1]
              const punchCard = valid[2]
              return models.PunchCard.list(merchant).then(function (result) {
                expect(result).to.have.lengthOf(1)
                const element = result[0]
                expect(element.id).to.eql(punchCard.id)
              })
            }
          )
        })
      })

      describe('#publish', function () {
        it('should fail with an invalid id', function () {
          return punchCardFactory.model().then(function (punchStruct) {
            return models.PunchCard.publish('INVALID_ID', punchStruct[1])
          }).then(function () {
            throw new Error('worked with invalid id')
          }).catch(function (err) {
            expect(err.message).to.eql('not_found')
          })
        })

        it('should fail with an incorrect merchant', function () {
          return Promise.join(
            merchantFactory.model(),
            punchCardFactory.model(),
            function (merchStruct, punchStruct) {
              return models.PunchCard.publish(punchStruct[2].id, merchStruct[1])
                .then(function () {
                  throw new Error('worked with invalid id')
                }).catch(function (err) {
                  expect(err.message).to.eql('not_found')
                })
            }
          )
        })

        it('should fail with a published punchcard', function () {
          return punchCardFactory.model(
            null, // orgId
            null, // merchId
            undefined, // isPublic
            null, // identifier
            null, // catId
            null, // code,
            null, // null,
            models.PunchCard.Identity.Published,
            moment().add(1, 'days')
          ).spread(function (org, merch, punch) {
            return models.PunchCard.publish(punch.id, merch)
              .then(function () {
                throw new Error('worked with invalid punchcard')
              }).catch(function (err) {
                expect(err.message).to.eql('active_punch_card_exists')
              })
          })
        })

        it('should generate user punchards when published', function () {
          return Promise.join(
            punchCardFactory.model(),
            userFactory.model(),
            userFactory.model(),
            function (punchStruct, user1Struct, user2Struct) {
              const merchant = punchStruct[1]
              const punchCard = punchStruct[2]
              return models.PunchCard.publish(punchCard.id, merchant)
            }
          ).then(function (published) {
            expect(published.identity).to.eql(models.PunchCard.Identity.Published)
            return models.UserPunchCard.findAll({ where: { punchCardId: published.id } })
          }).then(function (results) {
            expect(results).to.have.lengthOf(2)
          })
        })
      })

      describe('#createFromData', function () {
        it('should fail to create a new PunchCard with invalid date', function () {
          return merchantFactory.model().spread(function (org, merchant) {
            const invalid = {
              name: 'Test PunchCard',
              prize: '2x1 Cafe',
              rules: 'Money',
              terms: 'Test terms',
              punch_limit: 3,
              redeem_code: 'REDEEMCODE5',
              expires_at: 'INVALID_DATE'
            }
            return models.PunchCard.createFromData(invalid)
              .then(function () {
                throw new Error('created with invalid data')
              }).catch(function (err) {
                expect(err.message).to.be.eql('missing_punch_card_data')
              })
          })
        })

        it('should add a new PunchCard', function () {
          return merchantFactory.model().spread((org, merchant) => {
            const valid = {
              name: 'Test PunchCard',
              prize: '2x1 Cafe',
              rules: 'Money',
              terms: 'Test terms',
              punch_limit: 3,
              redeem_code: 'REDEEMCODE5',
              expires_at: new Date().toISOString()
            }
            return models.PunchCard.createFromData(
              { punch_card: valid },
              merchant
            ).then(function (element) {
              expect(element).to.be.an('object')
            })
          })
        })

        it('should add a new PunchCard with validation limit', function () {
          return merchantFactory.model().spread((org, merchant) => {
            const valid = {
              name: 'Test PunchCard',
              prize: '2x1 Cafe',
              rules: 'Money',
              terms: 'Test terms',
              punch_limit: 3,
              redeem_code: 'REDEEMCODE5',
              expires_at: new Date().toISOString(),
              validation_limit: 3
            }
            return models.PunchCard.createFromData({ punch_card: valid }, merchant).then(function (element) {
              expect(element).to.be.an('object')
              expect(element).to.have.a.property('validationLimit', valid.validation_limit)
            })
          })
        })

        it('should create two PunchCard with the same redeem code', function () {
          return merchantFactory.model().spread((org, merchant) => {
            const valid = {
              name: 'Test PunchCard',
              prize: '2x1 Cafe',
              rules: 'Money',
              terms: 'Test terms',
              punch_limit: 3,
              redeem_code: 'REDEEMCODE5',
              expires_at: new Date().toISOString()
            }
            return Promise.join(
              models.PunchCard.createFromData({ punch_card: valid }, merchant),
              models.PunchCard.createFromData({ punch_card: valid }, merchant)
            ).spread(function (punchCard1, punchCard2) {
              expect(punchCard1.redeemCode).to.eql(valid.redeem_code)
              expect(punchCard2.redeemCode).to.eql(valid.redeem_code)
            })
          })
        })
      })
    })

    describe('admin', function () {
      describe('#adminList', function () {
        it('should only return non expired punchards', function () {
          return Promise.join(
            punchCardFactory.model(),
            punchCardFactory.model(
              null, // orgId
              null, // merchId
              undefined, // isPublic
              null, // identifier
              null, // catId
              null, // code,
              null, // null,
              models.PunchCard.Identity.Published,
              moment().add(1, 'days')
            ),
            function (valid) {
              return models.PunchCard.adminList().then(function (result) {
                expect(result).to.be.an('array')
                expect(result).to.have.lengthOf(2)
              })
            }
          )
        })
      })

      describe('#adminUpdateFromData', function () {
        it('should not update a PunchCard with invalid date', function () {
          return punchCardFactory.model()
            .spread(function (org, merchant, punch) {
              const invalid = { expires_at: 'INVALID_DATE' }
              return models.PunchCard.adminUpdateFromData(punch.id, {
                punch_card: invalid
              }).then(function (result) {
                throw new Error('worked with invalid date')
              }).catch(function (err) {
                expect(err.name).to.be.eql('SequelizeValidationError')
                expect(err.message).to.be.eql('Validation error: invalid_date')
              })
            })
        })

        it('should fail to update a PunchCard with invalid data', function () {
          return punchCardFactory.model().spread(function (org, merchant, punch) {
            return models.PunchCard.adminUpdateFromData(punch.id, {})
              .then(function () {
                throw new Error('updated with invalid data')
              }).catch(function (err) {
                expect(err.message).to.be.eql('missing_punch_card_data')
              })
          })
        })

        it('should update a PunchCard', function () {
          const valid = { name: 'Updated Name' }
          return punchCardFactory.model().spread(function (org, merch, punch) {
            return models.PunchCard.adminUpdateFromData(punch.id, { punch_card: valid })
          }).then(function (result) {
            expect(result.name).to.eql(valid.name)
          })
        })
      })
    })
  })
})
