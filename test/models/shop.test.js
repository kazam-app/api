/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Shop Test
 * @file test/models/shop.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const session = require('../helpers/session')
const shopFactory = require('../helpers/factories/shop')
const merchantFactory = require('../helpers/factories/merchant')
const shopClusterFactory = require('../helpers/factories/shopcluster')

module.exports = describe('Shop', function () {
  describe('instanceMethods', function () {
    describe('#toAdminList', function () {
      it('should return admin list data', function () {
        return shopFactory.model().spread(function (org, merch, shopCluster, element) {
          return models.Shop.findById(element.id, { include: [{ model: models.ShopCluster }] }).then(function (shop) {
            return [shopCluster, shop]
          })
        }).spread(function (shopCluster, shop) {
          const result = shop.toAdminList()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', shop.id)
          expect(result).to.have.property('name', shop.name)
          expect(result).to.have.property('shop_cluster', shopCluster.name)
        })
      })
    })
  })

  describe('classMethods', function () {
    describe('#apiList', function () {
      context('active Merchant', function () {
        it('should return a Shop list', function () {
          return shopFactory.model().spread(function (org, merch, shopCluster, shop) {
            return models.Shop.apiList().then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element.id).to.eql(shop.id)
              expect(element.name).to.eql(shop.name)
              expect(element.merchantId).to.eql(merch.id)
              expect(element.lat).to.eql(shop.lat)
              expect(element.lng).to.eql(shop.lng)
            })
          })
        })
      })

      context('inactive Merchant', function () {
        it('should return an empty Shop array', function () {
          return merchantFactory.model(
            null, // Organization Id
            null, // id
            undefined, // isPublic
            null, // identifier
            null, // Category id
            null, // code
            false // isActive
          ).spread(function (org, merch) {
            return shopFactory.model(org.id, merch.id)
          }).spread(function (org, merch, shopCluster, shop) {
            return models.Shop.apiList().then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(0)
            })
          })
        })
      })
    })

    describe('#near', function () {
      context('active Merchant', function () {
        it('should return a Shop list without distance', function () {
          return shopFactory.model().spread(function (org, merch, shopCluster, shop) {
            return models.Shop.near().then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element.id).to.eql(shop.id)
              expect(element.name).to.eql(shop.name)
              expect(element.merchantId).to.eql(merch.id)
              expect(element.lat).to.eql(shop.lat)
              expect(element.lng).to.eql(shop.lng)
            })
          })
        })

        it('should return a Shop list with distance', function () {
          return shopFactory.model().spread(function (org, merch, shopCluster, shop) {
            const lat = 19.4286506
            const lng = -99.2159143
            const distance = 41.4205659311039
            return models.Shop.near(lat, lng).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element.id).to.eql(shop.id)
              expect(element.name).to.eql(shop.name)
              expect(element.merchantId).to.eql(merch.id)
              expect(element.lat).to.eql(shop.lat)
              expect(element.lng).to.eql(shop.lng)
              expect(element.dataValues.distance).to.eql(distance)
            })
          })
        })
      })

      context('inactive Merchant', function () {
        it('should return an empty Shop array', function () {
          return merchantFactory.model(
            null, // Organization Id
            null, // id
            undefined, // isPublic
            null, // identifier
            null, // Category id
            null, // code
            false // isActive
          ).spread(function (org, merch) {
            return shopFactory.model(org.id, merch.id)
          }).spread(function (org, merch, shopCluster, shop) {
            return models.Shop.near().then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(0)
            })
          })
        })
      })
    })

    describe('#createFromData', function () {
      beforeEach(session.fullReadWrite)

      it('should fail to create a new shop with invalid data', function () {
        return shopClusterFactory.model().spread(function (shopCluster) {
          const invalidShop = {
            location: 'Test Location',
            is_active: false,
            shop_cluster_id: shopCluster.id
          }
          return models.Shop.createFromData({ shop: invalidShop }, jwtMerchant)
        }).then(function () {
          throw new Error('created shop with invalid data')
        }).catch(function (err) {
          expect(err.name).to.eql('SequelizeValidationError')
        })
      })

      it('should create a new shop with valid data', function () {
        return shopClusterFactory.model().spread(function (shopCluster) {
          const validShop = {
            shop_cluster_id: shopCluster.id,
            is_active: false,
            name: 'Test Shop',
            location: 'Test Location',
            state: 'DF',
            country: 'MX'
          }
          return models.Shop.createFromData({ shop: validShop }, jwtMerchant)
        })
      })

      it('should create a new shop with full valid data', function () {
        return shopClusterFactory.model().spread(function (shopCluster) {
          const validShop = {
            shop_cluster_id: shopCluster.id,
            is_active: false,
            name: 'Test Shop',
            location: 'Test Location',
            city: 'CDMX',
            postal_code: '01219',
            phone: '551223446',
            state: 'DF',
            country: 'MX'
          }
          return models.Shop.createFromData({ shop: validShop }, jwtMerchant)
        })
      })
    })

    describe('#updateFromData', function () {
      it('should fail to update a shop with invalid idata', function () {
        return shopFactory.model().spread(function (org, merch, shopCluster, shop) {
          return models.Shop.updateFromData('INVALID_ID', {
            shop: { name: 'Test Shop 2' }
          }, merch)
        }).then(function () {
          throw new Error('updated shop with invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('missing_shop_data')
        })
      })

      it('should update a show with valid data', function () {
        return shopFactory.model().spread(function (org, merch, shopCluster, shop) {
          return models.Shop.updateFromData(shop.id, {
            shop: { name: 'Test Shop 2' }
          }, merch)
        })
      })
    })

    describe('#delete', function () {
      it('should fail to remove a shop with invalid id', function () {
        return shopFactory.model().spread(function (org, merch, shopCluster, shop) {
          return models.Shop.delete('INVALID_ID', merch)
        }).then(function () {
          throw new Error('removed shop with invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should remove a shop with valid id', function () {
        return shopFactory.model().spread(function (org, merch, shopCluster, shop) {
          return models.Shop.delete(shop.id, merch)
        })
      })
    })
  })
})
