/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Merchant Test
 * @file test/models/merchant.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const Promise = require('bluebird')
const session = require('../helpers/session')
const shopFactory = require('../helpers/factories/shop')
const categoryFactory = require('../helpers/factories/category')
const merchantFactory = require('../helpers/factories/merchant')
const organizationFactory = require('../helpers/factories/organization')

const imageUrl = 'https://s3-us-west-2.amazonaws.com/merchant-api-dev/no_aws.jpg'

module.exports = describe('Merchant', function () {
  describe('instanceMethods', function () {
    describe('#toShow', function () {
      it('should return show data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          return models.Merchant.findById(merch.id, { include: [{ model: models.Category }] }).then(function (merch) {
            return [merch, cat]
          })
        }).spread(function (merch, cat) {
          const result = merch.toShow()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', merch.id)
          expect(result).to.have.property('name', merch.name)
          expect(result).to.have.property('image_url', merch.imageUrl)
          expect(result).to.have.property('background_image_url', merch.backgroundImageUrl)
          expect(result).to.have.property('category_id', cat.id)
          expect(result).to.have.property('category', cat.name)
        })
      })
    })

    describe('#toApi', function () {
      it('should return api data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          return models.Merchant.findById(merch.id, { include: [{ model: models.Category }] }).then(function (merch) {
            return [merch, cat]
          })
        }).spread(function (merch, cat) {
          const result = merch.toApi()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', merch.id)
          expect(result).to.have.property('name', merch.name)
          expect(result).to.have.property('image_url', merch.imageUrl)
          expect(result).to.have.property('background_image_url', merch.backgroundImageUrl)
          expect(result).to.have.property('color', merch.color)
          expect(result).to.have.property('budget_rating', merch.budgetRating)
          expect(result).to.have.property('category', cat.name)
        })
      })
    })

    describe('#toAdminList', function () {
      it('should return admin list data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          return models.Merchant.findById(merch.id, { include: [{ model: models.Category }, { model: models.Organization }] })
            .then(function (merch) {
              return [merch, cat, org]
            })
        }).spread(function (merch, cat, org) {
          const result = merch.toAdminList()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', merch.id)
          expect(result).to.have.property('name', merch.name)
          expect(result).to.have.property('category_id', cat.id)
          expect(result).to.have.property('category', cat.name)
          expect(result).to.have.property('is_active', merch.isActive)
          expect(result).to.have.property('organization_id', org.id)
          expect(result).to.have.property('organization', org.name)
        })
      })
    })

    describe('#toAdmin', function () {
      it('should return admin data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          return models.Merchant.findById(merch.id, { include: [{ model: models.Category }, { model: models.Organization }] })
            .then(function (merch) {
              return [merch, cat, org]
            })
        }).spread(function (merch, cat, org) {
          const result = merch.toAdmin()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', merch.id)
          expect(result).to.have.property('name', merch.name)
          expect(result).to.have.property('color', merch.color)
          expect(result).to.have.property('is_active', merch.isActive)
          expect(result).to.have.property('budget_rating', merch.budgetRating)
          expect(result).to.have.property('invite_code', null)
          expect(result).to.have.property('image_url', merch.imageUrl)
          expect(result).to.have.property('background_image_url', merch.backgroundImageUrl)
          expect(result).to.have.property('category_id', cat.id)
          expect(result).to.have.property('category', cat.name)
          expect(result).to.have.property('organization_id', org.id)
          expect(result).to.have.property('organization', org.name)
        })
      })
    })

    describe('#toView', function () {
      it('should return view data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          const result = merch.toView()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', merch.id)
          expect(result).to.have.property('name', merch.name)
          expect(result).to.have.property('color', merch.color)
          expect(result).to.have.property('image_url', merch.imageUrl)
          expect(result).to.have.property('identifier', merch.identifier)
        })
      })
    })

    describe('#toApiSignUp', function () {
      it('should return api sign up data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          const result = merch.toApiSignUp()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', merch.id)
          expect(result).to.have.property('name', merch.name)
          expect(result).to.have.property('image_url', merch.imageUrl)
        })
      })
    })

    describe('#toList', function () {
      it('should return list data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          const result = merch.toList()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', merch.id)
          expect(result).to.have.property('name', merch.name)
          expect(result).to.have.property('image_url', merch.imageUrl)
          expect(result).to.include.keys(['created_at'])
        })
      })
    })
  })

  describe('classMethods', function () {
    describe('#list', function () {
      beforeEach(session.fullRead)

      it('should return a Merchant array', function () {
        return models.Merchant.list(jwtUser).then(function (result) {
          expect(result).to.be.an('array')
          expect(result).to.have.lengthOf(1)
          const element = result[0]
          expect(element.id).to.eql(jwtMerchant.id)
          expect(element.name).to.eql(jwtMerchant.name)
          expect(element.imageUrl).to.eql(jwtMerchant.imageUrl)
        })
      })
    })

    describe('#apiList', function () {
      context('active', function () {
        it('should return a Merchant array in alphabetical order', function () {
          return Promise.join(
            merchantFactory.model(),
            merchantFactory.model(),
            function (firstMerchant, secondMerchant) {
              const merch = firstMerchant[1]
              const merch2 = secondMerchant[1]
              return models.Merchant.apiList().then(function (result) {
                expect(result).to.be.an('array')
                expect(result).to.have.lengthOf(2)
                const element = result[0]
                expect(element.id).to.eql(merch.id)
                expect(element.name).to.eql(merch.name)
                expect(element.imageUrl).to.eql(merch.imageUrl)
                const element2 = result[1]
                expect(element2.id).to.eql(merch2.id)
                expect(element2.name).to.eql(merch2.name)
                expect(element2.imageUrl).to.eql(merch2.imageUrl)
              })
            }
          )
        })
      })

      context('inactive', function () {
        it('should return an empty Merchant array', function () {
          return merchantFactory.model(
            null, // Organization Id
            null, // id
            undefined, // isPublic
            null, // identifier
            null, // Category id
            null, // code
            false // isActive
          ).spread(function (org, merch) {
            return models.Merchant.apiList().then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(0)
            })
          })
        })
      })
    })

    describe('#apiNear', function () {
      context('active', function () {
        it('should fail without location points', function () {
          return merchantFactory.model().spread(function (org, merch) {
            return shopFactory.model(org.id, merch.id)
          }).spread(function () {
            return models.Merchant.apiNear()
          }).then(function () {
            throw new Error('created merchant with invalid data')
          }).catch(function (err) {
            expect(err.message).to.eql('missing_location_data')
          })
        })

        it('should return a single Merchant in distance order', function () {
          return merchantFactory.model().spread(function (org, merch) {
            return shopFactory.model(org.id, merch.id)
          }).spread(function (org, merch, shopCluster, shop) {
            const lat = 19.4286506
            const lng = -99.2159143
            return models.Merchant.apiNear(lat, lng).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element.name).to.eql(merch.name)
            })
          })
        })

        it('should return a single Merchant in distance order', function () {
          return merchantFactory.model().spread(function (org, merch) {
            return Promise.join(
              shopFactory.model(org.id, merch.id),
              shopFactory.model(org.id, merch.id)
            )
          }).spread(function (shopStruct, shopStruct2) {
            const lat = 19.4286506
            const lng = -99.2159143
            const merch = shopStruct[1]
            return models.Merchant.apiNear(lat, lng).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element).to.be.an('object')
              expect(element.name).to.eql(merch.name)
            })
          })
        })

        it('should return two Merchants in distance order', function () {
          return Promise.join(
            merchantFactory.model(),
            merchantFactory.model()
          ).spread(function (merchant1, merchant2) {
            const org1 = merchant1[0]
            const merch1 = merchant1[1]
            const org2 = merchant2[0]
            const merch2 = merchant2[1]
            return Promise.join(
              shopFactory.model(org1.id, merch1.id),
              shopFactory.model(org1.id, merch1.id),
              shopFactory.model(org2.id, merch2.id),
              shopFactory.model(org2.id, merch2.id)
            ).spread(function () {
              return [merch1, merch2]
            })
          }).spread(function (merch1, merch2) {
            const lat = 19.4286506
            const lng = -99.2159143
            return models.Merchant.apiNear(lat, lng).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(2)
              const element = result[0]
              expect(element).to.be.an('object')
              // expect(element.id).to.eql(merch1.id)
              expect(element.name).to.eql(merch1.name)
              const element2 = result[1]
              expect(element2).to.be.an('object')
              // expect(element2.id).to.eql(merch2.id)
              expect(element2.name).to.eql(merch2.name)
            })
          })
        })
      })

      context('inactive', function () {
        it('should return an empty Merchant array', function () {
          return merchantFactory.model(
            null, // Organization Id
            null, // id
            undefined, // isPublic
            null, // identifier
            null, // Category id
            null, // code
            false // isActive
          ).spread(function (org, merch) {
            const lat = 19.4286506
            const lng = -99.2159143
            return models.Merchant.apiNear(lat, lng).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(0)
            })
          })
        })
      })
    })

    describe('#adminCreateFromData', function () {
      it('should fail to create a new merchant with invalid data', function () {
        return models.Merchant.adminCreateFromData({ merchant: null }).then(function () {
          throw new Error('created merchant with invalid data')
        }).catch(function (err) {
          expect(err.message).to.eql('missing_merchant_data')
        })
      })

      it('should create a new merchant with valid data', function () {
        return Promise.join(
          categoryFactory.model(),
          organizationFactory.model(),
          function (category, organization) {
            const valid = {
              merchant: {
                color: '#000000',
                budget_rating: 1,
                image: 'NO_AWS_STREAM',
                name: 'Test Merchant',
                category_id: category[0].id,
                background_image: 'NO_AWS_STREAM',
                invite_code: 'TEST_MERCHANT_CODE',
                organization_id: organization[0].id
              }
            }
            return models.Merchant.adminCreateFromData(valid).then(function (result) {
              const merchant = valid.merchant
              expect(result.imageUrl).to.eql(imageUrl)
              expect(result.name).to.eql(merchant.name)
              expect(result.color).to.eql(merchant.color)
              expect(result.backgroundImageUrl).to.eql(imageUrl)
              expect(result.inviteCode).to.eql(merchant.invite_code)
              expect(result.categoryId).to.eql(merchant.category_id)
              expect(result.budgetRating).to.eql(merchant.budget_rating)
              expect(result.organizationId).to.eql(merchant.organization_id)
            })
          }
        )
      })
    })

    describe('#createFromData', function () {
      beforeEach(session.simple)

      it('should fail to create a new merchant with invalid data', function () {
        return models.Merchant.createFromData({ merchant: null }).then(function () {
          throw new Error('created merchant with invalid data')
        }).catch(function (err) {
          expect(err.message).to.eql('missing_merchant_data')
        })
      })

      it('should fail to create a new merchant with invalid data', function () {
        return categoryFactory.model().spread(function (category) {
          const valid = { merchant: { image: 'NO_AWS', name: 'Test Merchant', category_id: category.id, background_image: 'NO_AWS' } }
          return models.Merchant.createFromData(valid, jwtUser).then(function () {
            throw new Error('created merchant with invalid data')
          }).catch(function (err) {
            expect(err.name).to.eql('SequelizeValidationError')
            expect(err.message).to.eql('notNull Violation: Merchant.color cannot be null')
          })
        })
      })

      it('should create a new merchant with valid data', function () {
        return categoryFactory.model().spread(function (category) {
          const valid = { merchant: { image: 'NO_AWS', name: 'Test Merchant', category_id: category.id, background_image: 'NO_AWS', color: '#000000' } }
          return models.Merchant.createFromData(valid, jwtUser).then(function (result) {
            expect(result.categoryId).to.eql(category.id)
            expect(result.name).to.eql(valid.merchant.name)
            expect(result.color).to.eql(valid.merchant.color)
            expect(result.imageUrl).to.eql(imageUrl)
            expect(result.backgroundImageUrl).to.eql(imageUrl)
          })
        })
      })
    })

    describe('#updateFromData', function () {
      beforeEach(session.simple)

      it('should fail to update a merchant with invalid data', function () {
        return merchantFactory.model().spread(function (org, merch, cat) {
          const valid = { merchant: { image: 'NO_AWS_UPDATE', name: 'Test Merchant Update', background_image: 'NO_AWS_UPDATE' } }
          return models.Merchant.updateFromData('INVALID_ID', valid, jwtUser)
        }).then(function (result) {
          throw new Error('updated merchant with invalid data')
        }).catch(function (err) {
          expect(err.message).to.eql('missing_merchant_data')
        })
      })

      it('should update a merchant with valid data', function () {
        return merchantFactory.model(jwtUser.organizationId).spread(function (org, merch, cat) {
          const valid = { merchant: { image: 'NO_AWS_UPDATE', name: 'Test Merchant Update', background_image: 'NO_AWS_UPDATE', color: '#0000AA' } }
          return models.Merchant.updateFromData(merch.id, valid, jwtUser).then(function (result) {
            expect(result.id).to.eql(merch.id)
            expect(result.categoryId).to.eql(cat.id)
            expect(result.name).to.eql(valid.merchant.name)
            expect(result.color).to.eql(valid.merchant.color)
            expect(result.imageUrl).to.eql(imageUrl)
            expect(result.backgroundImageUrl).to.eql(imageUrl)
          })
        })
      })
    })

    describe('#delete', function () {
      it('should fail to remove a Merchant with invalid id', function () {
        return merchantFactory.model().spread(function () {
          return models.Merchant.delete('ABC')
        }).then(function () {
          throw new Error('found a Merchant via an invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should remove a Merchant with valid id', function () {
        return merchantFactory.model().spread(function (org, merch) {
          return models.Merchant.delete(merch.id)
        }).then(function (result) {
          expect(result).to.eql(true)
        })
      })
    })
  })
})
