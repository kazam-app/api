/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha UserPunchCardValidation Test
 * @file test/models/userpunchcardvalidation.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const moment = require('moment')
const Promise = require('bluebird')
const merchantFactory = require('../helpers/factories/merchant')
const validatorFactory = require('../helpers/factories/validator')
const userPunchCardFactory = require('../helpers/factories/userpunchcard')

module.exports = describe('UserPunchCardValidation', function () {
  describe('classMethods', function () {
    describe('#canValidate', function () {
      it('should return true if not validation limit is available', function () {
        const UserPunchCardValidation = models.UserPunchCardValidation
        const future = moment().add(1, 'd')
        return userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          null, // User
          models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
          0 // PunchCount
        ).spread(function (org, merch, punch, user, userPunchCard) {
          userPunchCard.PunchCard = punch
          return UserPunchCardValidation.canValidate(userPunchCard)
            .then(function (result) {
              expect(result).to.be.eql(true)
            })
        })
      })

      it('should return true if validation limit has not been reached', function () {
        const UserPunchCardValidation = models.UserPunchCardValidation
        const future = moment().add(1, 'd')
        const validationLimit = 3
        return userPunchCardFactory.model(
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          null, // User
          models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
          0, // PunchCount
          validationLimit
        ).spread(function (org, merch, punch, user, userPunchCard) {
          userPunchCard.PunchCard = punch
          expect(punch.validationLimit).to.eql(validationLimit)
          return UserPunchCardValidation.canValidate(userPunchCard)
            .then(function (result) {
              expect(result).to.be.eql(true)
            })
        })
      })

      it('should return false if validation limit is reached', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const UserPunchCardValidation = models.UserPunchCardValidation
          const future = moment().add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id, userPunchCardId: userPunchCard.id
            }).then(function () {
              return [punch, userPunchCard]
            })
          }).spread(function (punch, userPunchCard) {
            userPunchCard.PunchCard = punch
            expect(punch.validationLimit).to.eql(validationLimit)
            return UserPunchCardValidation.canValidate(userPunchCard).then(function (result) {
              expect(result).to.be.eql(false)
            })
          })
        })
      })
    })

    describe('#breakdown', function () {
      it('should fail without a merchant', function () {
        return models.UserPunchCardValidation.breakdown().then(function () {
          throw new Error('result returned without a merchant')
        }).catch(function (err) {
          expect(err.message).to.eq('missing_merchant')
        })
      })

      it('should fail without a range', function () {
        const current = '2018-02-20'
        return validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const future = moment(current).add(1, 'd')
            const validationLimit = 1
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
              1, // PunchCount
              validationLimit
            ).spread(function () {
              return models.UserPunchCardValidation.breakdown(merch)
                .then(function () {
                  throw new Error('result returned without a range')
                }).catch(function (err) {
                  expect(err.message).to.eq('missing_date_range')
                })
            })
          })
      })

      it('should return one validation for the current week', function () {
        const current = '2018-02-01T06:00:00.000Z'
        return validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const UserPunchCardValidation = models.UserPunchCardValidation
            const future = moment(current).add(1, 'd')
            const validationLimit = 1
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
              1, // PunchCount
              validationLimit
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: moment(current)
              })
            }).then(function () {
              const startAt = moment(current)
              const endAt = moment(current).add(1, 'M')
              return UserPunchCardValidation.breakdown(merch, startAt, endAt)
                .then(function (result) {
                  expect(result).to.be.an('object')
                  expect(result).to.have.keys(['shops', 'intervals'])
                  expect(result.intervals).to.eq(4)
                  const shops = result.shops
                  expect(shops).to.be.an('array')
                  expect(shops).to.have.lengthOf(1)
                  const resultShop = shops[0]
                  expect(resultShop).to.be.an('object')
                  expect(resultShop.id).to.eq(shop.id)
                  expect(resultShop.name).to.eq(shop.name)
                  const breakdown = resultShop.breakdown
                  expect(breakdown).to.be.an('array')
                  expect(breakdown).to.have.lengthOf(result.intervals)
                  const firstBreakdown = breakdown[0]
                  expect(firstBreakdown).to.be.an('array')
                  expect(firstBreakdown).to.have.lengthOf(1)
                  const secondBreakdown = breakdown[1]
                  expect(secondBreakdown).to.be.eq(undefined)
                  const thirdBreakdown = breakdown[2]
                  expect(thirdBreakdown).to.be.eq(undefined)
                  const fourthBreakdown = breakdown[3]
                  expect(fourthBreakdown).to.be.eq(undefined)
                })
            })
          })
      })

      it('should return one validation per week and one redeem', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const firstValidation = moment(current).add(1, 'd')
        const secondValidation = moment(current).add(2, 'w')
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const UserPunchCardValidation = models.UserPunchCardValidation
          const future = moment(current).add(1, 'M')
          const validationLimit = 2
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Redeemed, // UserPunchCard InProgress
            2, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return Promise.join(
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: firstValidation
              }),
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              UserPunchCardValidation.create({
                identity: UserPunchCardValidation.Identity.Redeem,
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              function () { return shop }
            )
          }).then(function (shop) {
            const startAt = moment(current)
            const endAt = moment(current).endOf('month')
            return UserPunchCardValidation.breakdown(merch, startAt, endAt)
              .then(function (result) {
                expect(result).to.be.an('object')
                expect(result).to.have.keys(['shops', 'intervals'])
                expect(result.intervals).to.eq(4)
                const shops = result.shops
                expect(shops).to.be.an('array')
                expect(shops).to.have.lengthOf(1)
                const resultShop = shops[0]
                expect(resultShop).to.be.an('object')
                expect(resultShop.id).to.eq(shop.id)
                expect(resultShop.name).to.eq(shop.name)
                const breakdown = resultShop.breakdown
                expect(breakdown).to.be.an('array')
                expect(breakdown).to.have.lengthOf(result.intervals)
                const firstBreakdown = breakdown[0]
                expect(firstBreakdown).to.be.an('array')
                expect(firstBreakdown).to.have.lengthOf(1)
                const secondBreakdown = breakdown[1]
                expect(secondBreakdown).to.be.an('array')
                expect(secondBreakdown).to.have.lengthOf(2)
                const thirdBreakdown = breakdown[2]
                expect(thirdBreakdown).to.be.eq(undefined)
                const fourthBreakdown = breakdown[3]
                expect(fourthBreakdown).to.be.eq(undefined)
              })
          })
        })
      })
    })

    describe('#topMerchantsForUser', function () {
      it('should return a Merchant list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const UserPunchCardValidation = models.UserPunchCardValidation
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment(current).add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id,
              userPunchCardId: userPunchCard.id,
              created_at: moment(current)
            }).then(function (validation) {
              return [merch, user, validation]
            })
          })
        }).spread(function (merch, user, validation) {
          // #topMerchantsForUser testing
          return UserPunchCardValidation.topMerchantsForUser(user)
            .then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element).to.be.an('object')
              expect(element).to.have.property('total', '1')
              expect(element).to.have.property('id', merch.id)
              expect(element).to.have.property('name', merch.name)
            })
        })
      })
    })

    describe('#topShopsForUser', function () {
      it('should return a Shop list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const UserPunchCardValidation = models.UserPunchCardValidation
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment(current).add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id,
              userPunchCardId: userPunchCard.id,
              created_at: moment(current)
            }).then(function (validation) {
              return [shop, user, validation]
            })
          })
        }).spread(function (shop, user, validation) {
          // #topMerchantsForUser testing
          return UserPunchCardValidation.topShopsForUser(user)
            .then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element).to.be.an('object')
              expect(element).to.have.property('total', '1')
              expect(element).to.have.property('id', shop.id)
              expect(element).to.have.property('name', shop.name)
            })
        })
      })
    })

    describe('#topShopsForUserByMerchant', function () {
      it('should return a Shop list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const UserPunchCardValidation = models.UserPunchCardValidation
        return Promise.join(merchantFactory.model(), validatorFactory.beacon(), function (merchantStruct, validatorStruct) {
          const org = validatorStruct[0]
          const merch = validatorStruct[1]
          const shop = validatorStruct[3]
          const beacon = validatorStruct[4]
          const future = moment(current).add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id,
              userPunchCardId: userPunchCard.id,
              created_at: moment(current)
            }).then(function (validation) {
              return [merch, shop, user, validation]
            })
          })
        }).spread(function (merch, shop, user, validation) {
          // #topShopsForUserByMerchant testing
          return UserPunchCardValidation.topShopsForUserByMerchant(user, merch)
            .then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element).to.be.an('object')
              expect(element).to.have.property('total', '1')
              expect(element).to.have.property('id', shop.id)
              expect(element).to.have.property('name', shop.name)
            })
        })
      })
    })

    describe('#historyForUser', function () {
      context('admin', function () {
        it('should return an Event list', function () {
          const current = '2018-02-01T06:00:00.000Z'
          const UserPunchCardValidation = models.UserPunchCardValidation
          return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
            const future = moment(current).add(1, 'd')
            const validationLimit = 1
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
              1, // PunchCount
              validationLimit
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: moment(current)
              }).then(function (validation) {
                return [merch, shop, user, userPunchCard, validation]
              })
            })
          }).spread(function (merch, shop, user, userPunchCard, validation) {
            // #historyForUser testing
            return UserPunchCardValidation.historyForUser(user, true)
              .then(function (result) {
                expect(result).to.be.an('array')
                expect(result).to.have.lengthOf(1)
                const element = result[0]
                expect(element).to.be.an('object')
                expect(element).to.have.property('id', validation.id)
                expect(element).to.have.property('identifier', validation.identifier)
                expect(element).to.have.property('identity', validation.identity)
                expect(element).to.include.keys('created_at')
                expect(element).to.have.property('Validator.Shop.name', shop.name)
                expect(element).to.have.property('Validator.Shop.Merchant.id', merch.id)
                expect(element).to.have.property('Validator.Shop.Merchant.name', merch.name)
              })
          })
        })
      })
    })

    describe('#historyForUserByMerchant', function () {
      it('should return an Event list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const UserPunchCardValidation = models.UserPunchCardValidation
        return Promise.join(merchantFactory.model(), validatorFactory.beacon(), function (merchantStruct, validatorStruct) {
          const org = validatorStruct[0]
          const merch = validatorStruct[1]
          const shop = validatorStruct[3]
          const beacon = validatorStruct[4]
          const future = moment(current).add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id,
              userPunchCardId: userPunchCard.id,
              created_at: moment(current)
            }).then(function (validation) {
              return [merch, shop, user, validation]
            })
          })
        }).spread(function (merch, shop, user, validation) {
          // #historyForUserByMerchant testing
          return UserPunchCardValidation.historyForUserByMerchant(user, merch)
            .then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element).to.be.an('object')
              expect(element).to.have.property('id', validation.id)
              expect(element).to.have.property(
                'identifier',
                validation.identifier
              )
              expect(element).to.have.property('identity', validation.identity)
              expect(element).to.include.keys('created_at')
              expect(element).to.have.property('Validator.Shop.name', shop.name)
            })
        })
      })
    })

    describe('#averageBreakdownForUser', function () {
      it('should return a weekly and monthly breakdown', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const firstValidation = moment(current).add(1, 'd')
        const secondValidation = moment(current).add(2, 'w')
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const UserPunchCardValidation = models.UserPunchCardValidation
          const future = moment(current).add(1, 'M')
          const validationLimit = 2
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Redeemed, // UserPunchCard InProgress
            2, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return Promise.join(
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: firstValidation
              }),
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              UserPunchCardValidation.create({
                identity: UserPunchCardValidation.Identity.Redeem,
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              function () { return [merch, shop, user, userPunchCard] }
            )
          }).spread(function (merch, shop, user, userPunchCard) {
            return UserPunchCardValidation.averageBreakdownForUser(user)
              .then(function (result) {
                expect(result).to.be.an('array')
                expect(result).to.have.lengthOf(2)
                const element = result[0]
                expect(element).to.be.an('object')
                expect(element).to.include.keys(
                  ['identity', 'weekly_avg', 'monthly_avg']
                )
              })
          })
        })
      })
    })

    describe('#weeklyAverageForUserByMerchant', function () {
      it('should return a weekly and monthly breakdown', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const firstValidation = moment(current).add(1, 'd')
        const secondValidation = moment(current).add(2, 'w')
        const UserPunchCardValidation = models.UserPunchCardValidation
        return Promise.join(merchantFactory.model(), validatorFactory.beacon(), function (merchantStruct, validatorStruct) {
          const org = validatorStruct[0]
          const merch = validatorStruct[1]
          const shop = validatorStruct[3]
          const beacon = validatorStruct[4]
          const future = moment(current).add(1, 'd')
          const validationLimit = 2
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Redeemed, // UserPunchCard InProgress
            2, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return Promise.join(
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: firstValidation
              }),
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              UserPunchCardValidation.create({
                identity: UserPunchCardValidation.Identity.Redeem,
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              function () { return [merch, shop, user, userPunchCard] }
            )
          }).spread(function (merch, shop, user, userPunchCard) {
            return UserPunchCardValidation.weeklyAverageForUserByMerchant(user, merch)
              .then(function (result) {
                expect(result).to.be.an('array')
                expect(result).to.have.lengthOf(1)
                const element = result[0]
                expect(element).to.be.an('object')
                expect(element).to.have.property('average', '1.50')
              })
          })
        })
      })
    })

    describe('#monthlyAverageForUserByMerchant', function () {
      it('should return a weekly and monthly breakdown', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const firstValidation = moment(current).add(1, 'd')
        const secondValidation = moment(current).add(2, 'w')
        const UserPunchCardValidation = models.UserPunchCardValidation
        return Promise.join(merchantFactory.model(), validatorFactory.beacon(), function (merchantStruct, validatorStruct) {
          const org = validatorStruct[0]
          const merch = validatorStruct[1]
          const shop = validatorStruct[3]
          const beacon = validatorStruct[4]
          const future = moment(current).add(1, 'd')
          const validationLimit = 2
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Redeemed, // UserPunchCard InProgress
            2, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return Promise.join(
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: firstValidation
              }),
              UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              UserPunchCardValidation.create({
                identity: UserPunchCardValidation.Identity.Redeem,
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: secondValidation
              }),
              function () { return [merch, shop, user, userPunchCard] }
            )
          }).spread(function (merch, shop, user, userPunchCard) {
            return UserPunchCardValidation.monthlyAverageForUserByMerchant(user, merch).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element).to.be.an('object')
              expect(element).to.have.property('average', '3.00')
            })
          })
        })
      })
    })

    describe('#apiHistoryForUser', function () {
      it('should return an Event list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        return validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const UserPunchCardValidation = models.UserPunchCardValidation
            const future = moment(current).add(1, 'd')
            const validationLimit = 1
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              // UserPunchCard InProgress
              models.UserPunchCard.Identity.InProgress,
              1, // PunchCount
              validationLimit
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: moment(current)
              }).then(function (result) {
                return [merch, shop, user, punch, userPunchCard, result]
              })
            }).spread(function (merch, shop, user, punch, userPunchCard, validation) {
              return UserPunchCardValidation.apiHistoryForUser(user)
                .then(function (result) {
                  console.log(result)
                  expect(result).to.be.an('array')
                  expect(result).to.have.lengthOf(1)
                  const element = result[0]
                  expect(element).to.be.an('object')
                  expect(element).to.have.property('id', validation.id)
                  expect(element).to.have.property('identifier',
                    validation.identifier
                  )
                  expect(element).to.have.property('identity',
                    validation.identity
                  )
                  expect(element).to.have.property('created_at')
                  expect(element).to.have.property('Validator.Shop.name',
                    shop.name
                  )
                  expect(element).to.have.property('Validator.Merchant.name',
                    merch.name
                  )
                  expect(element).to.have.property(
                    'Validator.Merchant.imageUrl',
                    'test.jpg'
                  )
                  expect(element).to.have.property('UserPunchCard.identity',
                    userPunchCard.identity
                  )
                  expect(element).to.have.property('UserPunchCard.identifier',
                    userPunchCard.identifier
                  )
                  expect(element).to.have.property(
                    'UserPunchCard.PunchCard.expiresAt'
                  )
                  const expiredAt = moment(
                    element['UserPunchCard.PunchCard.expiresAt']
                  ).toISOString()
                  expect(expiredAt).to.eq(punch.expiresAt.toISOString())
                })
            })
          })
      })
    })
  })
})
