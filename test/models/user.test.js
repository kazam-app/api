/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha User Test
 * @file test/models/user.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const set = require('mocha-let')
const moment = require('moment')
const Promise = require('bluebird')
const error = require('../helpers/error')
const session = require('../helpers/session')
const userFactory = require('../helpers/factories/user')
const merchantFactory = require('../helpers/factories/merchant')
const validatorFactory = require('../helpers/factories/validator')
const expiryNotifications = require('../../lib/expiry-notification')
const mailingListFactory = require('../helpers/factories/mailinglist')
const userPunchCardFactory = require('../helpers/factories/userpunchcard')

module.exports = describe('User', function () {
  set('validateCommon', function () {
    return function (result) {
      expect(result).to.be.an('object')
      expect(result).to.have.property('id')
      expect(result).to.have.property('gender')
      expect(result).to.have.property('birthdate')
      expect(result).to.have.property('created_at')
    }
  })

  set('validateSignUp', function () {
    return function (result, validUser) {
      expect(result).to.have.property('name', validUser.name)
      expect(result).to.have.property('last_names', validUser.last_names)
      expect(result).to.have.property('email', validUser.email)
      expect(result).to.have.property('token')
    }
  })

  set('validateLogin', function () {
    return function (result, user) {
      expect(result).to.have.property('name', user.name)
      expect(result).to.have.property('last_names', user.lastNames)
      expect(result).to.have.property('email', user.email)
      expect(result).to.have.property('gender', user.gender)
      expect(result).to.have.property('birthdate', user.birthdate)
      expect(result).to.have.property('token')
    }
  })

  describe('instanceMethods', function () {
    describe('#toShow', function () {
      beforeEach(session.user)

      it('should return only show values', function (done) {
        const result = jwtUser.toShow()
        this.validateCommon(result)
        expect(result).to.have.property('name', jwtUser.name)
        expect(result).to.have.property('last_names', jwtUser.lastNames)
        expect(result).to.have.property('email', jwtUser.email)
        expect(result).to.have.property('gender', jwtUser.gender)
        expect(result).to.have.property('birthdate', jwtUser.birthdate)
        done()
      })
    })

    describe('#verification', function () {
      context('verified', function () {
        beforeEach(session.user)

        it('should fail if verified', function () {
          return jwtUser.verification().then(function (result) {
            throw new Error('returned with verified user')
          }).catch(function (err) {
            expect(err.message).to.be.eq('user_verified')
          })
        })
      })

      context('not verified', function () {
        beforeEach(session.unVerifiedUser)

        it('should return a user an token', function () {
          return jwtUser.verification().spread((user, token) => {
            this.validateCommon(user)
            expect(user).to.have.property('isVerified', jwtUser.isVerified)
            expect(token).to.be.a('string')
          })
        })
      })
    })

    describe('#updatePermissions', function () {
      beforeEach(session.unVerifiedUser)

      it('should reject invalid params', function () {
        return jwtUser.updatePermissions('INVALID').then(function (result) {
          throw new Error('validated with invalid data')
        }).catch(function (err) {
          expect(err.message).to.be.eq('missing_user_data')
        })
      })

      it('should update permissions', function () {
        const update = { user: { has_expiry_notify_permission: false } }
        return jwtUser.updatePermissions(update).then(function (result) {
          expect(result.hasExpiryNotifyPermission).to.be.eq(false)
        })
      })
    })

    describe('#toVerified', function () {
      beforeEach(session.user)

      it('should return only if verified', function (done) {
        const result = jwtUser.toVerified()
        expect(result).to.have.property('is_verified', jwtUser.isVerified)
        done()
      })
    })

    describe('#checkAuth', function () {
      beforeEach(session.user)

      it('should reject an incorrect password', function () {
        return jwtUser.checkAuth('INVALID').then(function (result) {
          throw new Error('validated with invalid data')
        }).catch(function (err) {
          expect(err.message).to.be.eq('login_fail')
        })
      })

      it('should resolve with a correct password', function () {
        return jwtUser.checkAuth(jwtPass).then(function (result) {
          expect(result).to.have.property(
            'identity',
            models.UserToken.Identity.Login
          )
          expect(result).to.have.property('userId', jwtUser.id)
        })
      })
    })

    describe('#fbMe', function () {
      it('should reject an incorrect access token')
      it('should resolve with a correct access token')
    })

    describe('#toMailchimp', function () {
      beforeEach(session.user)

      it('should sync to Mailchimp', function (done) {
        const result = jwtUser.toMailchimp()
        expect(result).to.have.property('email_address', jwtUser.email)
        expect(result).to.have.property('status', 'subscribed')
        done()
      })
    })

    describe('#toAdminList', function () {
      beforeEach(session.user)

      it('should return only admin list values', function (done) {
        const result = jwtUser.toAdminList()
        expect(result).to.be.an('object')
        expect(result).to.have.property('name', jwtUser.name)
        expect(result).to.have.property('last_names', jwtUser.lastNames)
        expect(result).to.have.property('email', jwtUser.email)
        expect(result).to.have.property('is_verified', jwtUser.isVerified)
        done()
      })
    })

    // skip
    xdescribe('#mailchimpSignUp', function () {
      it('should sync to Mailchimp', function () {
        return mailingListFactory.model().then(function () {
          return userFactory.unSynced()
        }).then(function (user) {
          return user.mailchimpSignUp()
        }).then(function (user) {
          expect(user.mailchimpId).to.eq('MAILCHIMP_NOT_ACTIVE')
        })
      })
    })

    // TODO: capture a mailchimp request cassette
    // skip
    xdescribe('#churn', function () {
      it('should update Mailchimp', function () {
        return mailingListFactory.model().then(function () {
          return userFactory.unSynced('joel.cano.avila@gmail.com')
        }).then(function (user) {
          return user.mailchimpSignUp()
        }).then(function (user) {
          return user.churn()
        }).then(function (result) {
          console.log(result)
          // expect(user.mailchimpId).to.eq('MAILCHIMP_NOT_ACTIVE')
        })
      })
    })

    describe('#toAdmin', function () {
      beforeEach(session.user)

      it('should return only admin list values', function () {
        return jwtUser.toAdmin().then(function (result) {
          expect(result).to.be.an('object')
          expect(result).to.have.property('name', jwtUser.name)
          expect(result).to.have.property('last_names', jwtUser.lastNames)
          expect(result).to.have.property('email', jwtUser.email)
          expect(result).to.have.property('is_verified', jwtUser.isVerified)
          expect(result).to.have.property('sign_up_type', jwtUser.signUpType())
          expect(result).to.have.property('gender', jwtUser.gender)
          expect(result).to.have.property('birthdate', jwtUser.birthdate)
          expect(result).to.include.keys([
            'created_at',
            'top_merchants',
            'top_shops',
            'history',
            'categories',
            'averages'
          ])
          expect(result.top_merchants).to.be.an('array')
          expect(result.top_shops).to.be.an('array')
          expect(result.history).to.be.an('array')
          expect(result.categories).to.be.an('array')
          expect(result.averages).to.be.an('object')
        })
      })
    })

    describe('#toAdminMerchant', function () {
      beforeEach(session.user)

      it('should return only admin list values', function () {
        return merchantFactory.model().spread(function (org, merch) {
          return jwtUser.toAdminMerchant(merch).then(function (result) {
            expect(result).to.be.an('object')
            expect(result).to.have.property('name', jwtUser.name)
            expect(result).to.have.property('last_names', jwtUser.lastNames)
            expect(result).to.have.property('email', jwtUser.email)
            expect(result).to.have.property('birthdate', jwtUser.birthdate)
            expect(result).to.have.property('merchant_total', 0)
            expect(result).to.have.property('weekly_average', 0)
            expect(result).to.have.property('monthly_average', 0)
            expect(result).to.include.keys(['top_shops', 'history'])
            expect(result.top_shops).to.be.an('array')
            expect(result.history).to.be.an('array')
          })
        })
      })
    })

    describe('#apiHistory', function () {
      set('validateExpiry', function () {
        return function (item, merch, userPunchCard, expiresAt) {
          expect(item).to.be.an('object')
          expect(item).to.have.property('id', userPunchCard.id)
          expect(item).to.have.property('identity', userPunchCard.identity)
          expect(item).to.have.property('identifier', userPunchCard.identifier)
          expect(item).to.have.property('PunchCard.expiresAt')
          const expiredAt = moment(item['PunchCard.expiresAt']).toISOString()
          expect(expiredAt).to.eq(expiresAt.toISOString())
          expect(item).to.have.property(
            'PunchCard.Merchant.name',
            merch.name
          )
          expect(item).to.have.property(
            'PunchCard.Merchant.imageUrl',
            'test.jpg'
          )
        }
      })

      set('validateComplete', function () {
        return function (item, merch, punch, userPunchCard, lastValidationAt) {
          expect(item).to.be.an('object')
          expect(item).to.have.property('id', userPunchCard.id)
          expect(item).to.have.property('name', merch.name)
          expect(item).to.have.property('image_url', 'test.jpg')
          expect(item).to.have.property('identity', userPunchCard.identity)
          expect(item).to.have.property('identifier', userPunchCard.identifier)
          expect(item).to.have.property('created_at')
          const completedAt = moment(item.created_at).toISOString()
          expect(completedAt).to.eq(lastValidationAt.toISOString())
          expect(item).to.have.property('expires_at')
          const expiresAt = moment(item.expires_at).toISOString()
          expect(expiresAt).to.eq(punch.expiresAt.toISOString())
        }
      })

      set('validateValidation', function () {
        return function (
          item, merch, shop, userPunchCard, validation, validationAt
        ) {
          expect(item).to.be.an('object')
          expect(item).to.have.property('id', validation.id)
          expect(item).to.have.property(
            'identifier',
            validation.identifier
          )
          expect(item).to.have.property('identity', validation.identity)
          expect(item).to.have.property('created_at')
          const validatedAt = moment(item.created_at).toISOString()
          expect(validatedAt).to.eq(validationAt.toISOString())
          expect(item).to.have.property(
            'Validator.Shop.name',
            shop.name
          )
          expect(item).to.have.property(
            'Validator.Merchant.name',
            merch.name
          )
          expect(item).to.have.property(
            'Validator.Merchant.imageUrl',
            'test.jpg'
          )
          expect(item).to.have.property(
            'UserPunchCard.identity',
            userPunchCard.identity
          )
          expect(item).to.have.property('UserPunchCard.identifier',
            userPunchCard.identifier
          )
        }
      })

      it('should return an expiry and a punch Event list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const punchAt = moment(current)
        return validatorFactory.beacon()
          .spread((org, merch, shopCluster, shop, beacon) => {
            const UserPunchCardValidation = models.UserPunchCardValidation
            const future = moment(current).add(1, 'd')
            const validationLimit = 1
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              // UserPunchCard InProgress
              models.UserPunchCard.Identity.InProgress,
              1, // PunchCount
              validationLimit
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return UserPunchCardValidation.create({
                validatorId: beacon.id,
                userPunchCardId: userPunchCard.id,
                created_at: punchAt
              }).then(function (result) {
                return [merch, shop, user, punch, userPunchCard, result, beacon]
              })
            }).spread((
              merch, shop, user, punch, userPunchCard, validation, beacon
            ) => {
              return user.apiHistory().then((result) => {
                expect(result).to.be.an('array')
                expect(result).to.have.lengthOf(2)
                this.validateExpiry(result[0], merch, userPunchCard, future)
                this.validateValidation(
                  result[1],
                  merch,
                  shop,
                  userPunchCard,
                  validation,
                  punchAt
                )
              })
            })
          })
      })

      it('should return three punches, a complete and an expiry Event list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const future = moment(current).add(1, 'M')
        const firstPunch = moment(current)
        const secondPunch = moment(current).add(1, 'd')
        const thirdPunch = moment(current).add(2, 'd')
        return validatorFactory.beacon()
          .spread((org, merch, shopCluster, shop, beacon) => {
            const UserPunchCardValidation = models.UserPunchCardValidation
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              // UserPunchCard InProgress
              models.UserPunchCard.Identity.InProgress,
              3 // PunchCount
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return Promise.join(
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: firstPunch
                }),
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: secondPunch
                }),
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: thirdPunch
                }),
                function (punch1, punch2, punch3) {
                  return [
                    merch,
                    shop,
                    user,
                    punch,
                    userPunchCard,
                    beacon,
                    punch1,
                    punch2,
                    punch3
                  ]
                }
              )
            }).spread((merch, shop, user, punch, userPunchCard, beacon, punch1, punch2, punch3) => {
              return user.apiHistory().then((result) => {
                expect(result).to.be.an('array')
                expect(result).to.have.lengthOf(5)
                this.validateExpiry(result[0], merch, userPunchCard, future)
                this.validateComplete(
                  result[1],
                  merch,
                  punch,
                  userPunchCard,
                  thirdPunch
                )
                this.validateValidation(
                  result[2],
                  merch,
                  shop,
                  userPunchCard,
                  punch3,
                  thirdPunch
                )
                this.validateValidation(
                  result[3],
                  merch,
                  shop,
                  userPunchCard,
                  punch2,
                  secondPunch
                )
                this.validateValidation(
                  result[4],
                  merch,
                  shop,
                  userPunchCard,
                  punch1,
                  firstPunch
                )
              })
            })
          })
      })
    })

    describe('#toExpiryNotification', function () {
      context('first notification', function () {
        set('flag', function () {
          return models.UserPunchCard.AlertNotificationSent.First
        })

        it('should return the notification for a single merchant', function () {
          return Promise.join(
            userFactory.model(
              null, // id
              undefined, // isValid
              null, // validateEmail
              'player_id' // OneSignal
            ),
            validatorFactory.beacon(),
            function (userStruct, validatorStruct) {
              const user = userStruct[0]
              const org = validatorStruct[0]
              const merch = validatorStruct[1]
              // const shopCluster = validatorStruct[2]
              // const shop = validatorStruct[3]
              const beacon = validatorStruct[4]
              const UserPunchCardValidation = models.UserPunchCardValidation
              const expiresAt = moment().add(
                process.env.USER_FIRST_EXPIRY_WARNING_AT,
                'd'
              )
              return userPunchCardFactory.model(
                org.id, // Organization
                merch.id, // Merchant
                undefined, // isPublic
                null, // identifier
                null, // Category
                null, // code
                null, // PunchCard
                models.PunchCard.Identity.Published, // identity published
                expiresAt, // expiresAt
                user.id, // User
                // UserPunchCard InProgress
                models.UserPunchCard.Identity.InProgress,
                1 // PunchCount
              ).spread(function (org, merch, punch, user, userPunchCard) {
                return UserPunchCardValidation.create({
                  validatorId: beacon.id, userPunchCardId: userPunchCard.id
                }).then(function () {
                  return [merch, punch, userPunchCard, user]
                })
              })
            }
          ).spread((merch, punch, userPunchCard, user) => {
            return models.User.forFirstExpiryNotification()
              .then((users) => {
                const user = users[0]
                const notifications = expiryNotifications[this.flag]
                return user.toExpiryNotification(notifications)
              })
              .then(function (result) {
                expect(result).to.be.an('object')
                expect(result.headings).to.have.property(
                  'en',
                  '🚨¡Ojo! Tarjeta a punto de vencer. 😱'
                )
                expect(result.contents).to.have.property(
                  'en',
                  'Tu tarjeta de Test Merchant vence en menos de un mes ⏳. Asegúrate de completarla para evitar perder tus premios. 🎁'
                )
              })
          })
        })

        it('should return the notification for two merchants', function () {
          return Promise.join(
            userFactory.model(
              null, // id
              undefined, // isValid
              null, // validateEmail
              'player_id' // OneSignal
            ),
            validatorFactory.beacon(),
            validatorFactory.beacon(),
            function (userStruct, validatorStruct, validatorStruct2) {
              const user = userStruct[0]
              const org = validatorStruct[0]
              const merch = validatorStruct[1]
              const beacon = validatorStruct[4]
              const org2 = validatorStruct2[0]
              const merch2 = validatorStruct2[1]
              const beacon2 = validatorStruct2[4]
              const UserPunchCardValidation = models.UserPunchCardValidation
              const expiresAt = moment().add(
                process.env.USER_FIRST_EXPIRY_WARNING_AT,
                'd'
              )
              return Promise.join(
                userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                userPunchCardFactory.model(
                  org2.id, // Organization
                  merch2.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                function (userPunchCardStruct, userPunchCardStruct2) {
                  const userPunchCard = userPunchCardStruct[4]
                  const userPunchCard2 = userPunchCardStruct2[4]
                  return Promise.join(
                    UserPunchCardValidation.create({
                      validatorId: beacon.id, userPunchCardId: userPunchCard.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon2.id, userPunchCardId: userPunchCard2.id
                    })
                  )
                }
              )
            }
          ).then(() => {
            return models.User.forFirstExpiryNotification()
              .then((users) => {
                const user = users[0]
                const notifications = expiryNotifications[this.flag]
                return user.toExpiryNotification(notifications)
              })
              .then(function (result) {
                expect(result).to.be.an('object')
                expect(result.headings).to.have.property(
                  'en',
                  '🚨¡Ojo! Tarjetas a punto de vencer. 😱'
                )
                expect(result.contents).to.have.property(
                  'en',
                  'Tus tarjetas de Test Merchant y Test Merchant vence en menos de un mes ⏳. Asegúrate de completarla para evitar perder tus premios. 🎁'
                )
              })
          })
        })

        it('should return the notification for three merchants', function () {
          return Promise.join(
            userFactory.model(
              null, // id
              undefined, // isValid
              null, // validateEmail
              'player_id' // OneSignal
            ),
            validatorFactory.beacon(),
            validatorFactory.beacon(),
            validatorFactory.beacon(),
            function (userStruct, validatorStruct, validatorStruct2, validatorStruct3) {
              const user = userStruct[0]
              const org = validatorStruct[0]
              const merch = validatorStruct[1]
              const beacon = validatorStruct[4]
              const org2 = validatorStruct2[0]
              const merch2 = validatorStruct2[1]
              const beacon2 = validatorStruct2[4]
              const org3 = validatorStruct3[0]
              const merch3 = validatorStruct3[1]
              const beacon3 = validatorStruct3[4]
              const UserPunchCardValidation = models.UserPunchCardValidation
              const expiresAt = moment().add(
                process.env.USER_FIRST_EXPIRY_WARNING_AT,
                'd'
              )
              return Promise.join(
                userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                userPunchCardFactory.model(
                  org2.id, // Organization
                  merch2.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                userPunchCardFactory.model(
                  org3.id, // Organization
                  merch3.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                function (userPunchCardStruct, userPunchCardStruct2, userPunchCardStruct3) {
                  const userPunchCard = userPunchCardStruct[4]
                  const userPunchCard2 = userPunchCardStruct2[4]
                  const userPunchCard3 = userPunchCardStruct3[4]
                  return Promise.join(
                    UserPunchCardValidation.create({
                      validatorId: beacon.id, userPunchCardId: userPunchCard.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon2.id, userPunchCardId: userPunchCard2.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon3.id, userPunchCardId: userPunchCard3.id
                    })
                  )
                }
              )
            }
          ).then(() => {
            return models.User.forFirstExpiryNotification()
              .then((users) => {
                const user = users[0]
                const notifications = expiryNotifications[this.flag]
                return user.toExpiryNotification(notifications)
              })
              .then(function (result) {
                expect(result).to.be.an('object')
                expect(result.headings).to.have.property(
                  'en',
                  '🚨¡Ojo! Tarjetas a punto de vencer. 😱'
                )
                expect(result.contents).to.have.property(
                  'en',
                  '3 de tus tarjetas vencen en menos de 30 días ⏳. Asegúrate de completarlas para no perder tus premios. 🎁'
                )
              })
          })
        })
      })

      context('second notification', function () {
        set('flag', function () {
          return models.UserPunchCard.AlertNotificationSent.Second
        })

        it('should return the notification for a single merchant', function () {
          return Promise.join(
            userFactory.model(
              null, // id
              undefined, // isValid
              null, // validateEmail
              'player_id' // OneSignal
            ),
            validatorFactory.beacon(),
            function (userStruct, validatorStruct) {
              const user = userStruct[0]
              const org = validatorStruct[0]
              const merch = validatorStruct[1]
              // const shopCluster = validatorStruct[2]
              // const shop = validatorStruct[3]
              const beacon = validatorStruct[4]
              const UserPunchCardValidation = models.UserPunchCardValidation
              const expiresAt = moment().add(
                process.env.USER_SECOND_EXPIRY_WARNING_AT,
                'd'
              )
              return userPunchCardFactory.model(
                org.id, // Organization
                merch.id, // Merchant
                undefined, // isPublic
                null, // identifier
                null, // Category
                null, // code
                null, // PunchCard
                models.PunchCard.Identity.Published, // identity published
                expiresAt, // expiresAt
                user.id, // User
                // UserPunchCard InProgress
                models.UserPunchCard.Identity.InProgress,
                1 // PunchCount
              ).spread(function (org, merch, punch, user, userPunchCard) {
                return UserPunchCardValidation.create({
                  validatorId: beacon.id, userPunchCardId: userPunchCard.id
                }).then(function () {
                  return [merch, punch, userPunchCard, user]
                })
              })
            }
          ).spread((merch, punch, userPunchCard, user) => {
            return models.User.forSecondExpiryNotification()
              .then((users) => {
                const user = users[0]
                const notifications = expiryNotifications[this.flag]
                return user.toExpiryNotification(notifications)
              })
              .then(function (result) {
                expect(result).to.be.an('object')
                expect(result.headings).to.have.property(
                  'en',
                  '🚨¡Cuidado! Tarjeta a punto de expirar.🚨'
                )
                expect(result.contents).to.have.property(
                  'en',
                  'Tu tarjeta de Test Merchant está a punto de expirar 😱. Asegúrate de completarla para evitar perder tus premios. 🎁'
                )
              })
          })
        })

        it('should return the notification for two merchants', function () {
          return Promise.join(
            userFactory.model(
              null, // id
              undefined, // isValid
              null, // validateEmail
              'player_id' // OneSignal
            ),
            validatorFactory.beacon(),
            validatorFactory.beacon(),
            function (userStruct, validatorStruct, validatorStruct2) {
              const user = userStruct[0]
              const org = validatorStruct[0]
              const merch = validatorStruct[1]
              const beacon = validatorStruct[4]
              const org2 = validatorStruct2[0]
              const merch2 = validatorStruct2[1]
              const beacon2 = validatorStruct2[4]
              const UserPunchCardValidation = models.UserPunchCardValidation
              const expiresAt = moment().add(
                process.env.USER_SECOND_EXPIRY_WARNING_AT,
                'd'
              )
              return Promise.join(
                userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                userPunchCardFactory.model(
                  org2.id, // Organization
                  merch2.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                function (userPunchCardStruct, userPunchCardStruct2) {
                  const userPunchCard = userPunchCardStruct[4]
                  const userPunchCard2 = userPunchCardStruct2[4]
                  return Promise.join(
                    UserPunchCardValidation.create({
                      validatorId: beacon.id, userPunchCardId: userPunchCard.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon2.id, userPunchCardId: userPunchCard2.id
                    })
                  )
                }
              )
            }
          ).then(() => {
            return models.User.forSecondExpiryNotification()
              .then((users) => {
                const user = users[0]
                const notifications = expiryNotifications[this.flag]
                return user.toExpiryNotification(notifications)
              })
              .then(function (result) {
                expect(result).to.be.an('object')
                expect(result.headings).to.have.property(
                  'en',
                  '🚨¡Cuidado! Tarjetas a punto de expirar.🚨'
                )
                expect(result.contents).to.have.property(
                  'en',
                  'Tus tarjetas de Test Merchant y Test Merchant expiran en menos de 15 días 😱. Complétalas y canjea tus premios. 🎁'
                )
              })
          })
        })

        it('should return the notification for three merchants', function () {
          return Promise.join(
            userFactory.model(
              null, // id
              undefined, // isValid
              null, // validateEmail
              'player_id' // OneSignal
            ),
            validatorFactory.beacon(),
            validatorFactory.beacon(),
            validatorFactory.beacon(),
            function (userStruct, validatorStruct, validatorStruct2, validatorStruct3) {
              const user = userStruct[0]
              const org = validatorStruct[0]
              const merch = validatorStruct[1]
              const beacon = validatorStruct[4]
              const org2 = validatorStruct2[0]
              const merch2 = validatorStruct2[1]
              const beacon2 = validatorStruct2[4]
              const org3 = validatorStruct3[0]
              const merch3 = validatorStruct3[1]
              const beacon3 = validatorStruct3[4]
              const UserPunchCardValidation = models.UserPunchCardValidation
              const expiresAt = moment().add(
                process.env.USER_SECOND_EXPIRY_WARNING_AT,
                'd'
              )
              return Promise.join(
                userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                userPunchCardFactory.model(
                  org2.id, // Organization
                  merch2.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                userPunchCardFactory.model(
                  org3.id, // Organization
                  merch3.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ),
                function (userPunchCardStruct, userPunchCardStruct2, userPunchCardStruct3) {
                  const userPunchCard = userPunchCardStruct[4]
                  const userPunchCard2 = userPunchCardStruct2[4]
                  const userPunchCard3 = userPunchCardStruct3[4]
                  return Promise.join(
                    UserPunchCardValidation.create({
                      validatorId: beacon.id, userPunchCardId: userPunchCard.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon2.id, userPunchCardId: userPunchCard2.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon3.id, userPunchCardId: userPunchCard3.id
                    })
                  )
                }
              )
            }
          ).then(() => {
            return models.User.forSecondExpiryNotification()
              .then((users) => {
                const user = users[0]
                const notifications = expiryNotifications[this.flag]
                return user.toExpiryNotification(notifications)
              })
              .then(function (result) {
                expect(result).to.be.an('object')
                expect(result.headings).to.have.property(
                  'en',
                  '🚨¡Cuidado! Tarjetas a punto de expirar.🚨'
                )
                expect(result.contents).to.have.property(
                  'en',
                  '¡No pierdas tus sellos! 3 de tus tarjetas van a expirar muy pronto 😫, no olvides completarlas para llevarte tus premios. 🎁'
                )
              })
          })
        })
      })
    })
  })

  describe('classMethods', function () {
    describe('#login', function () {
      beforeEach(session.user)

      it('should fail to login with invalid data', function () {
        return models.User.login({ email: 'foo', password: 'bar' })
          .then(function (result) {
            throw new Error('logged in with invalid data')
          }).catch(function (err) {
            expect(err.message).to.be.eq('login_fail')
          })
      })

      it('should login with valid data', function () {
        return models.User.login({ email: jwtUser.email, password: jwtPass })
          .then((result) => {
            this.validateCommon(result)
            this.validateLogin(result, jwtUser)
          })
      })
    })

    context('new user', function () {
      describe('#signUp', function () {
        it('should fail to signup a User with invalid data', function () {
          return models.User.signUp({}).spread(function () {
            throw new Error('created user with invalid data')
          }).catch(function (err) {
            expect(err.message).to.be.eq('missing_user_data')
          })
        })

        it(
          'should create a new User with valid data and accept empty gender',
          function () {
            const pass = '12345678'
            const validUser = {
              name: 'Name',
              last_names: 'Last Names',
              email: 'foo@bar.com',
              gender: '',
              password: pass,
              password_confirmation: pass
            }
            return models.User.signUp({ user: validUser })
              .spread((user, token) => {
                expect(user.gender).to.be.eq(null)
                this.validateCommon(user)
                this.validateSignUp(user, validUser)
              })
          }
        )

        it(
          'should create a new User with valid data and accept NaN gender',
          function () {
            const pass = '12345678'
            const validUser = {
              name: 'Name',
              last_names: 'Last Names',
              email: 'foo@bar.com',
              gender: 'NaN',
              password: pass,
              password_confirmation: pass
            }
            return models.User.signUp({ user: validUser })
              .spread((user, token) => {
                expect(user.gender).to.be.eq(null)
                this.validateCommon(user)
                this.validateSignUp(user, validUser)
              })
          }
        )

        it(
          'should create a new User with valid data and accept null gender',
          function () {
            const pass = '12345678'
            const validUser = {
              name: 'Name',
              last_names: 'Last Names',
              email: 'foo@bar.com',
              gender: null,
              password: pass,
              password_confirmation: pass
            }
            return models.User.signUp({ user: validUser }).spread((user, token) => {
              expect(user.gender).to.be.eq(null)
              this.validateCommon(user)
              this.validateSignUp(user, validUser)
            })
          }
        )

        it('should create a new User with valid data', function () {
          const pass = '12345678'
          const validUser = {
            name: 'Name',
            last_names: 'Last Names',
            email: 'foo@bar.com',
            gender: 1,
            password: pass,
            password_confirmation: pass
          }
          return models.User.signUp({ user: validUser }).spread((user, token) => {
            expect(user).to.have.property('gender', validUser.gender)
            this.validateCommon(user)
            this.validateSignUp(user, validUser)
          })
        })
      })

      describe('#generatePunchCards', function () {
        it('should create a new user punch card')
      })
    })

    describe('#updateFromData', function () {
      beforeEach(session.user)

      it('should update a User with valid data and passwords', function () {
        const data = {
          name: 'Name', password: jwtPass, password_confirmation: jwtPass
        }
        return jwtUser.updateFromData({ user: data }).then(function (result) {
          expect(result.name).to.be.eql(data.name)
        })
      })

      it('should update a User with null gender and passwords', function () {
        const data = {
          gender: '', password: jwtPass, password_confirmation: jwtPass
        }
        return jwtUser.updateFromData({ user: data }).then(function (result) {
          expect(result.gender).to.be.eql(null)
        })
      })
    })

    describe('#verify', function () {
      beforeEach(session.user)

      it('should fail without a token', function () {
        return models.User.verify({}).then(function () {
          throw new Error('verified user with invalid data')
        }).catch(function (err) {
          expect(err.message).to.be.eq('invalid_token')
        })
      })

      it('should throw an InvalidError with an invalid token', function () {
        return models.User.verify({ token: 'INVALID' }).then(function () {
          throw new Error('verified user with invalid token')
        }).catch(function (err) {
          error.invalidError(err, 'invalid_token')
        })
      })

      it('should throw an ExpiredError with an expired token', function () {
        return models.UserToken.create({
          userId: jwtUser.id,
          identity: models.UserToken.Identity.Verify,
          expiresAt: new Date()
        }).then(function (token) {
          return models.User.verify({ token: token.jwt() })
        }).then(function () {
          throw new Error('verified user with expired token')
        }).catch(function (err) {
          error.expiredError(err, 'expired_token')
        })
      })

      it('should verify a user with a valid token', function () {
        return models.UserToken.create({
          userId: jwtUser.id,
          identity: models.UserToken.Identity.Verify
        }).then(function (token) {
          return models.User.verify({ token: token.jwt() })
        }).then(function (result) {
          expect(result).to.eq(true)
        })
      })
    })

    xdescribe('#mailchimpSignUpSync', function () {
      it('should sync to Mailchimp', function () {
        return userFactory.unSynced().then(function (user) {
          expect(user.mailchimpId).to.be.eq(null)
          return models.User.mailchimpSignUpSync().then(function (users) {
            expect(users).to.be.an('array')
            const element = users[0]
            expect(element.id).to.eq(user.id)
            expect(element.mailchimpId).to.eq('MAILCHIMP_NOT_ACTIVE')
          })
        })
      })
    })

    context('expiry notification', function () {
      describe('#forFirstExpiryNotification', function () {
        context('empty results', function () {
          beforeEach(session.user)

          it('should return an empty array', function () {
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const days = process.env.USER_FIRST_EXPIRY_WARNING_AT
                const expiresAt = moment().add(days + 1, 'd')
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  jwtUser.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard]
                  })
                }).spread(function (merch, punch, userPunchCard) {
                  return models.User.forFirstExpiryNotification()
                    .then(function (result) {
                      expect(result).to.be.an('array')
                      expect(result).to.have.lengthOf(0)
                    })
                })
              })
          })

          it('should not return a User with missing playerId', function () {
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_FIRST_EXPIRY_WARNING_AT,
                  'd'
                )
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  jwtUser.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard]
                  })
                }).spread(function (merch, punch, userPunchCard) {
                  return models.User.forFirstExpiryNotification()
                    .then(function (result) {
                      expect(result).to.be.an('array')
                      expect(result).to.have.lengthOf(0)
                    })
                })
              })
          })
        })

        context('with User results', function () {
          it('should return a User with playerId', function () {
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                'player_id' // OneSignal
              ),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                // const shopCluster = validatorStruct[2]
                // const shop = validatorStruct[3]
                const beacon = validatorStruct[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_FIRST_EXPIRY_WARNING_AT,
                  'd'
                )
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard, user]
                  })
                })
              }
            ).spread(function (merch, punch, userPunchCard, user) {
              return models.User.forFirstExpiryNotification()
                .then(function (result) {
                  expect(result).to.be.an('array')
                  expect(result).to.have.lengthOf(1)
                  const element = result[0]
                  expect(element.id).to.eq(user.id)
                  expect(element.playerId).to.eq(user.playerId)
                })
            })
          })
        })
      })

      describe('#forSecondExpiryNotification', function () {
        beforeEach(session.user)

        context('without player id', function () {
          it('should return an empty array', function () {
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const days = process.env.USER_SECOND_EXPIRY_WARNING_AT
                const expiresAt = moment().add(days + 1, 'd')
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  jwtUser.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard]
                  })
                }).spread(function (merch, punch, userPunchCard) {
                  return models.User.forSecondExpiryNotification()
                    .then(function (result) {
                      expect(result).to.be.an('array')
                      expect(result).to.have.lengthOf(0)
                    })
                })
              })
          })
        })

        context('with player id', function () {
          it('should return a User with playerId', function () {
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                'player_id' // OneSignal
              ),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                // const shopCluster = validatorStruct[2]
                // const shop = validatorStruct[3]
                const beacon = validatorStruct[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_SECOND_EXPIRY_WARNING_AT,
                  'd'
                )
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard, user]
                  })
                })
              }
            ).spread(function (merch, punch, userPunchCard, user) {
              return models.User.forSecondExpiryNotification()
                .then(function (result) {
                  expect(result).to.be.an('array')
                  expect(result).to.have.lengthOf(1)
                  const element = result[0]
                  expect(element.id).to.eq(user.id)
                  expect(element.playerId).to.eq(user.playerId)
                })
            })
          })
        })
      })

      describe('#forExpiryNotification', function () {
        context('without player id', function () {
          beforeEach(session.user)

          it('should return an empty array', function () {
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const days = process.env.USER_FIRST_EXPIRY_WARNING_AT
                const expiresAt = moment().add(days + 1, 'd')
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  jwtUser.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard]
                  })
                }).spread(function (merch, punch, userPunchCard) {
                  const queryAt = moment().add(days, 'd')
                  return models.User.forExpiryNotification(queryAt)
                    .then(function (result) {
                      expect(result).to.be.an('array')
                      expect(result).to.have.lengthOf(0)
                    })
                })
              })
          })

          it('should return an empty array without a player id', function () {
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const days = process.env.USER_FIRST_EXPIRY_WARNING_AT
                const expiresAt = moment().add(days + 1, 'd')
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  jwtUser.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard]
                  })
                }).spread(function (merch, punch, userPunchCard) {
                  return models.User.forExpiryNotification(expiresAt)
                    .then(function (result) {
                      expect(result).to.be.an('array')
                      expect(result).to.have.lengthOf(0)
                    })
                })
              })
          })

          it('should return a single User with multiple cards', function () {
            const expiresAt = moment().add(
              process.env.USER_FIRST_EXPIRY_WARNING_AT,
              'd'
            )
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const Published = models.PunchCard.Identity.Published
                return Promise.join(
                  userPunchCardFactory.model(
                    org.id, // Organization
                    merch.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    Published, // identity published
                    expiresAt, // expiresAt
                    jwtUser.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org.id, // Organization
                    merch.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    Published, // identity published
                    expiresAt, // expiresAt
                    jwtUser.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  )
                ).spread(function (userPunchCardStruct1, userPunchCardStruct2) {
                  const UserPunchCardValidation = models.UserPunchCardValidation
                  const userPunchCard1 = userPunchCardStruct1[4]
                  const userPunchCard2 = userPunchCardStruct2[4]
                  return Promise.join(
                    UserPunchCardValidation.create({
                      validatorId: beacon.id, userPunchCardId: userPunchCard1.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon.id, userPunchCardId: userPunchCard2.id
                    })
                  )
                }).spread(function () {
                  return models.User.forExpiryNotification(expiresAt)
                    .then(function (result) {
                      expect(result).to.be.an('array')
                      expect(result).to.have.lengthOf(0)
                    })
                })
              })
          })

          it(
            'should return a single User with cards from multiple merchants',
            function () {
              const expiresAt = moment().add(
                process.env.USER_FIRST_EXPIRY_WARNING_AT,
                'd'
              )
              return Promise.join(
                validatorFactory.beacon(),
                validatorFactory.beacon()
              ).spread(function (validatorStruct1, validatorStruct2) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const org1 = validatorStruct1[0]
                const merch1 = validatorStruct1[1]
                const beacon1 = validatorStruct1[4]
                const org2 = validatorStruct2[0]
                const merch2 = validatorStruct2[1]
                const beacon2 = validatorStruct2[4]
                return Promise.join(
                  userPunchCardFactory.model(
                    org1.id, // Organization
                    merch1.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    jwtUser.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org2.id, // Organization
                    merch2.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    jwtUser.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  )
                ).spread(function (userPunchCardStruct1, userPunchCardStruct2) {
                  const userPunchCard1 = userPunchCardStruct1[4]
                  const userPunchCard2 = userPunchCardStruct2[4]
                  return Promise.join(
                    UserPunchCardValidation.create({
                      validatorId: beacon1.id, userPunchCardId: userPunchCard1.id
                    }),
                    UserPunchCardValidation.create({
                      validatorId: beacon2.id, userPunchCardId: userPunchCard2.id
                    })
                  )
                })
              }).spread(function () {
                return models.User.forExpiryNotification(expiresAt)
                  .then(function (result) {
                    expect(result).to.be.an('array')
                    expect(result).to.have.lengthOf(0)
                  })
              })
            }
          )
        })

        context('with player id', function () {
          it('should return a User with playerId', function () {
            const expiresAt = moment().add(
              process.env.USER_FIRST_EXPIRY_WARNING_AT,
              'd'
            )
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                'player_id' // OneSignal
              ),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                // const shopCluster = validatorStruct[2]
                // const shop = validatorStruct[3]
                const beacon = validatorStruct[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard, user]
                  })
                })
              }
            ).spread(function (merch, punch, userPunchCard, user) {
              return models.User.forExpiryNotification(expiresAt)
                .then(function (result) {
                  expect(result).to.be.an('array')
                  expect(result).to.have.lengthOf(1)
                  const element = result[0]
                  expect(element.id).to.eq(user.id)
                  expect(element.playerId).to.eq(user.playerId)
                  const userPunchCards = element.UserPunchCards
                  expect(userPunchCards).to.be.an('array')
                  expect(userPunchCards).to.have.lengthOf(1)
                  const userCard = userPunchCards[0]
                  expect(userCard.id).to.eq(userPunchCard.id)
                  expect(userCard.alertNotifcationSent).to.eq(userPunchCard.alertNotifcationSent)
                  expect(userCard.punchCount).to.eq(userPunchCard.punchCount)
                  const card = userCard.PunchCard
                  expect(card.punchLimit).to.eq(punch.punchLimit)
                  const merchant = card.Merchant
                  expect(merchant.id).to.eq(merch.id)
                  expect(merchant.name).to.eq(merch.name)
                })
            })
          })
        })
      })

      // TODO: mock requests
      xdescribe('#expiryNotify', function () {
        beforeEach(session.user)

        set('playerId', function () {
          // OneSignal playerId
          return '0c52a000-8a5f-4e40-97c2-15d79802d4b7'
        })

        context('first notification', function () {
          it('should not notify without player id', function () {
            const User = models.User
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_FIRST_EXPIRY_WARNING_AT,
                  'd'
                )
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  jwtUser.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard]
                  })
                }).spread(function (merch, punch, userPunchCard) {
                  return User.forFirstExpiryNotification()
                }).then(function (users) {
                  return models.User.expiryNotify(
                    models.UserPunchCard.AlertNotificationSent.First
                  )(users)
                }).then(function (notified) {
                  expect(notified).to.be.an('array')
                  expect(notified).to.have.lengthOf(0)
                })
              })
          })

          it('should notify for a single merchant', function () {
            const expiresAt = moment().add(
              process.env.USER_FIRST_EXPIRY_WARNING_AT,
              'd'
            )
            const notificationFlag = models.UserPunchCard.AlertNotificationSent.First
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                this.playerId // OneSignal playerId
              ),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                // const shopCluster = validatorStruct[2]
                // const shop = validatorStruct[3]
                const beacon = validatorStruct[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard, user]
                  })
                })
              }
            ).spread(function (merch, punch, userPunchCard, user) {
              return models.User.forFirstExpiryNotification()
            }).then(function (users) {
              return models.User.expiryNotify(notificationFlag)(users)
            }).then(function (notified) {
              expect(notified).to.be.an('array')
              expect(notified).to.have.lengthOf(1)
              return models.UserPunchCard.findAll()
            }).then(function (cards) {
              expect(cards).to.be.an('array')
              expect(cards).to.have.lengthOf(1)
              const element = cards[0]
              expect(element).to.have.property(
                'alertNotifcationSent',
                notificationFlag
              )
            })
          })

          it('should notify for two merchants', function () {
            const notificationFlag = models.UserPunchCard.AlertNotificationSent.First
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                this.playerId // OneSignal
              ),
              validatorFactory.beacon(),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct, validatorStruct2) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                const beacon = validatorStruct[4]
                const org2 = validatorStruct2[0]
                const merch2 = validatorStruct2[1]
                const beacon2 = validatorStruct2[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_FIRST_EXPIRY_WARNING_AT,
                  'd'
                )
                return Promise.join(
                  userPunchCardFactory.model(
                    org.id, // Organization
                    merch.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org2.id, // Organization
                    merch2.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  function (userPunchCardStruct, userPunchCardStruct2) {
                    const userPunchCard = userPunchCardStruct[4]
                    const userPunchCard2 = userPunchCardStruct2[4]
                    return Promise.join(
                      UserPunchCardValidation.create({
                        validatorId: beacon.id, userPunchCardId: userPunchCard.id
                      }),
                      UserPunchCardValidation.create({
                        validatorId: beacon2.id,
                        userPunchCardId: userPunchCard2.id
                      })
                    )
                  }
                )
              }
            ).then(function () {
              return models.User.forFirstExpiryNotification()
            }).then(function (users) {
              return models.User.expiryNotify(notificationFlag)(users)
            }).then(function (notified) {
              expect(notified).to.be.an('array')
              expect(notified).to.have.lengthOf(1)
              return models.UserPunchCard.findAll()
            }).then(function (cards) {
              expect(cards).to.be.an('array')
              expect(cards).to.have.lengthOf(2)
              const element = cards[0]
              expect(element).to.have.property(
                'alertNotifcationSent',
                notificationFlag
              )
            })
          })

          it('should notify for three merchants', function () {
            const notificationFlag = models.UserPunchCard.AlertNotificationSent.First
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                this.playerId // OneSignal
              ),
              validatorFactory.beacon(),
              validatorFactory.beacon(),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct, validatorStruct2, validatorStruct3) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                const beacon = validatorStruct[4]
                const org2 = validatorStruct2[0]
                const merch2 = validatorStruct2[1]
                const beacon2 = validatorStruct2[4]
                const org3 = validatorStruct3[0]
                const merch3 = validatorStruct3[1]
                const beacon3 = validatorStruct3[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_FIRST_EXPIRY_WARNING_AT,
                  'd'
                )
                return Promise.join(
                  userPunchCardFactory.model(
                    org.id, // Organization
                    merch.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org2.id, // Organization
                    merch2.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org3.id, // Organization
                    merch3.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  function (userPunchCardStruct, userPunchCardStruct2, userPunchCardStruct3) {
                    const userPunchCard = userPunchCardStruct[4]
                    const userPunchCard2 = userPunchCardStruct2[4]
                    const userPunchCard3 = userPunchCardStruct3[4]
                    return Promise.join(
                      UserPunchCardValidation.create({
                        validatorId: beacon.id, userPunchCardId: userPunchCard.id
                      }),
                      UserPunchCardValidation.create({
                        validatorId: beacon2.id,
                        userPunchCardId: userPunchCard2.id
                      }),
                      UserPunchCardValidation.create({
                        validatorId: beacon3.id,
                        userPunchCardId: userPunchCard3.id
                      })
                    )
                  }
                )
              }
            ).then(function () {
              return models.User.forFirstExpiryNotification()
            }).then(function (users) {
              return models.User.expiryNotify(notificationFlag)(users)
            }).then(function (notified) {
              expect(notified).to.be.an('array')
              expect(notified).to.have.lengthOf(1)
              return models.UserPunchCard.findAll()
            }).then(function (cards) {
              expect(cards).to.be.an('array')
              expect(cards).to.have.lengthOf(3)
              const element = cards[0]
              expect(element).to.have.property(
                'alertNotifcationSent',
                notificationFlag
              )
            })
          })
        })

        context('second notification', function () {
          it('should not notify without player id', function () {
            const User = models.User
            return validatorFactory.beacon()
              .spread(function (org, merch, shopCluster, shop, beacon) {
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_SECOND_EXPIRY_WARNING_AT,
                  'd'
                )
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  jwtUser.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard]
                  })
                }).spread(function (merch, punch, userPunchCard) {
                  return User.forSecondExpiryNotification()
                }).then(function (users) {
                  return models.User.expiryNotify(
                    models.UserPunchCard.AlertNotificationSent.Second
                  )(users)
                }).then(function (notified) {
                  expect(notified).to.be.an('array')
                  expect(notified).to.have.lengthOf(0)
                })
              })
          })

          it('should notify for a single merchant', function () {
            const expiresAt = moment().add(
              process.env.USER_SECOND_EXPIRY_WARNING_AT,
              'd'
            )
            const notificationFlag = models.UserPunchCard.AlertNotificationSent.Second
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                this.playerId // OneSignal playerId
              ),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                // const shopCluster = validatorStruct[2]
                // const shop = validatorStruct[3]
                const beacon = validatorStruct[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                return userPunchCardFactory.model(
                  org.id, // Organization
                  merch.id, // Merchant
                  undefined, // isPublic
                  null, // identifier
                  null, // Category
                  null, // code
                  null, // PunchCard
                  models.PunchCard.Identity.Published, // identity published
                  expiresAt, // expiresAt
                  user.id, // User
                  // UserPunchCard InProgress
                  models.UserPunchCard.Identity.InProgress,
                  1 // PunchCount
                ).spread(function (org, merch, punch, user, userPunchCard) {
                  return UserPunchCardValidation.create({
                    validatorId: beacon.id, userPunchCardId: userPunchCard.id
                  }).then(function () {
                    return [merch, punch, userPunchCard, user]
                  })
                })
              }
            ).spread(function (merch, punch, userPunchCard, user) {
              return models.User.forSecondExpiryNotification()
            }).then(function (users) {
              return models.User.expiryNotify(notificationFlag)(users)
            }).then(function (notified) {
              expect(notified).to.be.an('array')
              expect(notified).to.have.lengthOf(1)
              return models.UserPunchCard.findAll()
            }).then(function (cards) {
              expect(cards).to.be.an('array')
              expect(cards).to.have.lengthOf(1)
              const element = cards[0]
              expect(element).to.have.property(
                'alertNotifcationSent',
                notificationFlag
              )
            })
          })

          it('should notify for two merchants', function () {
            const notificationFlag = models.UserPunchCard.AlertNotificationSent.First
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                this.playerId // OneSignal
              ),
              validatorFactory.beacon(),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct, validatorStruct2) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                const beacon = validatorStruct[4]
                const org2 = validatorStruct2[0]
                const merch2 = validatorStruct2[1]
                const beacon2 = validatorStruct2[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_FIRST_EXPIRY_WARNING_AT,
                  'd'
                )
                return Promise.join(
                  userPunchCardFactory.model(
                    org.id, // Organization
                    merch.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org2.id, // Organization
                    merch2.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  function (userPunchCardStruct, userPunchCardStruct2) {
                    const userPunchCard = userPunchCardStruct[4]
                    const userPunchCard2 = userPunchCardStruct2[4]
                    return Promise.join(
                      UserPunchCardValidation.create({
                        validatorId: beacon.id, userPunchCardId: userPunchCard.id
                      }),
                      UserPunchCardValidation.create({
                        validatorId: beacon2.id,
                        userPunchCardId: userPunchCard2.id
                      })
                    )
                  }
                )
              }
            ).then(function () {
              return models.User.forFirstExpiryNotification()
            }).then(function (users) {
              return models.User.expiryNotify(notificationFlag)(users)
            }).then(function (notified) {
              expect(notified).to.be.an('array')
              expect(notified).to.have.lengthOf(1)
              return models.UserPunchCard.findAll()
            }).then(function (cards) {
              expect(cards).to.be.an('array')
              expect(cards).to.have.lengthOf(2)
              const element = cards[0]
              expect(element).to.have.property(
                'alertNotifcationSent',
                notificationFlag
              )
            })
          })

          it('should notify for three merchants', function () {
            const notificationFlag = models.UserPunchCard.AlertNotificationSent.Second
            return Promise.join(
              userFactory.model(
                null, // id
                undefined, // isValid
                null, // validateEmail
                this.playerId // OneSignal
              ),
              validatorFactory.beacon(),
              validatorFactory.beacon(),
              validatorFactory.beacon(),
              function (userStruct, validatorStruct, validatorStruct2, validatorStruct3) {
                const user = userStruct[0]
                const org = validatorStruct[0]
                const merch = validatorStruct[1]
                const beacon = validatorStruct[4]
                const org2 = validatorStruct2[0]
                const merch2 = validatorStruct2[1]
                const beacon2 = validatorStruct2[4]
                const org3 = validatorStruct3[0]
                const merch3 = validatorStruct3[1]
                const beacon3 = validatorStruct3[4]
                const UserPunchCardValidation = models.UserPunchCardValidation
                const expiresAt = moment().add(
                  process.env.USER_FIRST_EXPIRY_WARNING_AT,
                  'd'
                )
                return Promise.join(
                  userPunchCardFactory.model(
                    org.id, // Organization
                    merch.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org2.id, // Organization
                    merch2.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  userPunchCardFactory.model(
                    org3.id, // Organization
                    merch3.id, // Merchant
                    undefined, // isPublic
                    null, // identifier
                    null, // Category
                    null, // code
                    null, // PunchCard
                    models.PunchCard.Identity.Published, // identity published
                    expiresAt, // expiresAt
                    user.id, // User
                    // UserPunchCard InProgress
                    models.UserPunchCard.Identity.InProgress,
                    1 // PunchCount
                  ),
                  function (userPunchCardStruct, userPunchCardStruct2, userPunchCardStruct3) {
                    const userPunchCard = userPunchCardStruct[4]
                    const userPunchCard2 = userPunchCardStruct2[4]
                    const userPunchCard3 = userPunchCardStruct3[4]
                    return Promise.join(
                      UserPunchCardValidation.create({
                        validatorId: beacon.id, userPunchCardId: userPunchCard.id
                      }),
                      UserPunchCardValidation.create({
                        validatorId: beacon2.id,
                        userPunchCardId: userPunchCard2.id
                      }),
                      UserPunchCardValidation.create({
                        validatorId: beacon3.id,
                        userPunchCardId: userPunchCard3.id
                      })
                    )
                  }
                )
              }
            ).then(function () {
              return models.User.forFirstExpiryNotification()
            }).then(function (users) {
              return models.User.expiryNotify(notificationFlag)(users)
            }).then(function (notified) {
              expect(notified).to.be.an('array')
              expect(notified).to.have.lengthOf(1)
              return models.UserPunchCard.findAll()
            }).then(function (cards) {
              expect(cards).to.be.an('array')
              expect(cards).to.have.lengthOf(3)
              const element = cards[0]
              expect(element).to.have.property(
                'alertNotifcationSent',
                notificationFlag
              )
            })
          })
        })
      })
    })
  })
})
