/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha UserPunchCard Test
 * @file test/models/userpunchcard.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const moment = require('moment')
const Promise = require('bluebird')
const userFactory = require('../helpers/factories/user')
const merchantFactory = require('../helpers/factories/merchant')
const validatorFactory = require('../helpers/factories/validator')
const userPunchCardFactory = require('../helpers/factories/userpunchcard')

module.exports = describe('UserPunchCard', function () {
  describe('classMethods', function () {
    describe('#apiList', function () {
      context('active Merchant', function () {
        it('should return a UserPunchCard array', function () {
          return userPunchCardFactory.model(
            null, // Organization
            null, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            moment().add(1, 'days') // expiresAt
          ).spread(function (org, merch, punch, user, userPunchCard) {
            const UserPunchCard = models.UserPunchCard
            return UserPunchCard.apiList(user).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element.id).to.eql(userPunchCard.id)
              expect(element.identity).to.eql(UserPunchCard.Identity.InProgress)
            })
          })
        })
      })

      context('inactive Merchant', function () {
        it('should return an empty array', function () {
          return merchantFactory.model(
            null, // Organization Id
            null, // id
            undefined, // isPublic
            null, // identifier
            null, // Category id
            null, // code
            false // isActive
          ).spread(function (org, merch) {
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              moment().add(1, 'days') // expiresAt
            )
          }).spread(function (org, merch, punch, user, userPunchCard) {
            return models.UserPunchCard.apiList(user).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(0)
            })
          })
        })
      })
    })

    describe('#ready', function () {
      context('active Merchant', function () {
        it('should return a ready UserPunchCard array', function () {
          const UserPunchCard = models.UserPunchCard
          const Ready = UserPunchCard.Identity.Ready
          return userPunchCardFactory.model(
            null, // Organization
            null, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            moment().add(1, 'days'), // expiresAt
            null, // User
            Ready, // identity ready
            3 // UserPunchCard punchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCard.ready(user).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(1)
              const element = result[0]
              expect(element.id).to.eql(userPunchCard.id)
              expect(element.identity).to.eql(Ready)
            })
          })
        })

        it('should return a list of ready user punch cards for a user', function () {
          const PunchCard = models.PunchCard
          const future = moment().add(1, 'days')
          const UserPunchCard = models.UserPunchCard
          const Ready = UserPunchCard.Identity.Ready
          const Published = PunchCard.Identity.Published
          return Promise.join(
            userPunchCardFactory.model(
              null, // Organization
              null, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              Published, // identity published
              future, // expiresAt
              null, // User
              Ready, // identity ready
              4 // UserPunchCard punchCount
            ),
            userPunchCardFactory.model(
              null, // Organization
              null, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              Published, // identity published
              future, // expiresAt
              null, // User
              Ready, // identity ready
              3 // UserPunchCard punchCount
            ),
            function (invalid, valid) {
              const user = valid[3]
              const userPunchCard = valid[4]
              return UserPunchCard.ready(user).then(function (result) {
                expect(result).to.have.lengthOf(1)
                const element = result[0]
                expect(element.id).to.eql(userPunchCard.id)
                expect(element.identity).to.eql(Ready)
                expect(element.punchCount).to.eql(userPunchCard.punchCount)
              })
            }
          )
        })
      })

      context('inactive Merchant', function () {
        it('should return an empty array', function () {
          const UserPunchCard = models.UserPunchCard
          return merchantFactory.model(
            null, // Organization Id
            null, // id
            undefined, // isPublic
            null, // identifier
            null, // Category id
            null, // code
            false // isActive
          ).spread(function (org, merch) {
            const Ready = UserPunchCard.Identity.Ready
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              moment().add(1, 'days'), // expiresAt
              null, // User
              Ready, // identity ready
              3 // UserPunchCard punchCount
            )
          }).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCard.ready(user).then(function (result) {
              expect(result).to.be.an('array')
              expect(result).to.have.lengthOf(0)
            })
          })
        })
      })
    })

    describe('#addValidationDate', function () {
      it('should not add a validation date if the validation limit not reached', function () {
        const future = moment().add(1, 'd')
        return userPunchCardFactory.model( // Valid new punch card
          null, // Organization
          null, // Merchant
          undefined, // isPublic
          null, // identifier
          null, // Category
          null, // code
          null, // PunchCard
          models.PunchCard.Identity.Published, // identity published
          future, // expiresAt
          null, // User
          models.UserPunchCard.Identity.Ready, // UserPunchCard ready
          3 // PunchCount
        ).spread(function (org, merch, punch, user, userPunchCard) {
          expect(punch.validationLimit).to.eql(null)
          userPunchCard.PunchCard = punch
          return models.UserPunchCard.addValidationDate([userPunchCard]).then(function (elements) {
            expect(elements).to.be.an('array')
            expect(elements[0]).to.not.include.keys('validationDate')
          })
        })
      })

      it('should add a validation date if the validation limit is reached', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const UserPunchCardValidation = models.UserPunchCardValidation
          const future = moment().add(1, 'd')
          const validationLimit = 1
          return userPunchCardFactory.model(
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1, // PunchCount
            validationLimit
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return UserPunchCardValidation.create({
              validatorId: beacon.id, userPunchCardId: userPunchCard.id
            }).then(function () {
              return [punch, userPunchCard]
            })
          }).spread(function (punch, userPunchCard) {
            expect(punch.validationLimit).to.eql(validationLimit)
            userPunchCard.PunchCard = punch
            return models.UserPunchCard.addValidationDate([userPunchCard]).then(function (elements) {
              expect(elements).to.be.an('array')
              const element = elements[0]
              expect(element).to.include.keys('validationDate')
              expect(element.validationDate).to.be.eql(punch.validationLimitEnd())
            })
          })
        })
      })
    })

    describe('#punch', function () {
      it('should fail with a ready punch card', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          return models.UserPunchCard.punch(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function (result) {
            throw new Error('a ready punch card was found')
          })
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should fail with a validator from another merchant', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            null, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          return models.UserPunchCard.punch(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function () {
            throw new Error('a validator from an incorrect merchant was found')
          })
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should fail when validation limit is reached', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            0, // PunchCount
            0
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          return models.UserPunchCard.punch(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function (result) {
            throw new Error('validated over the limit')
          }).catch(function (err) {
            expect(err.message).to.eql('validation_limit')
          })
        })
      })

      it('should add a punch via a beacon validator', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            0 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          return models.UserPunchCard.punch(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function (result) {
            expect(result.punchCount).to.eql(++userPunchCard.punchCount)
          })
        })
      })

      it('should add a punch via a qr validator', function () {
        return validatorFactory.qr().spread(function (org, merch, shopCluster, shop, qr) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard ready
            0 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, qr]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, qr) {
          return models.UserPunchCard.punch(userPunchCard.id, merch.id, {
            token: qr.token
          }, user).then(function (result) {
            expect(result.punchCount).to.eql(++userPunchCard.punchCount)
          })
        })
      })
    })

    describe('#redeem', function () {
      it('should fail with a incomplete punch card with beacon', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          return models.UserPunchCard.redeem(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function () {
            throw new Error('an incomplete punch card was found with beacon')
          })
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should fail with a incomplete punch card with qr', function () {
        return validatorFactory.qr().spread(function (org, merch, shopCluster, shop, qr) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.InProgress, // UserPunchCard InProgress
            1 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, qr]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, qr) {
          return models.UserPunchCard.redeem(userPunchCard.id, merch.id, {
            token: qr.token
          }, user).then(function () {
            throw new Error('an incomplete punch card was found with qr')
          })
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should fail with a validator from another merchant', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            null, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          return models.UserPunchCard.redeem(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function (result) {
            throw new Error('a ready punch card was found')
          })
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should fail with an unverified user and set env', function () {
        return Promise.join(
          validatorFactory.beacon(),
          userFactory.unVerified(),
          function (validator, user) {
            const future = moment().add(1, 'd')
            const org = validator[0]
            const merch = validator[1]
            const beacon = validator[4]
            return userPunchCardFactory.model( // Valid new punch card
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              user.id, // User
              models.UserPunchCard.Identity.Ready, // UserPunchCard ready
              3 // PunchCount
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return [org, merch, punch, user, userPunchCard, beacon]
            })
          }
        ).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          process.env.KZM_CHECK_USER_VERIFICATION = 'true'
          return models.UserPunchCard.redeem(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function (result) {
            throw new Error('redeemed with an unVerified user')
          })
        }).catch(function (err) {
          expect(err.message).to.eql('not_verified')
        })
      })

      it('should redeem via a beacon validator with an unverified user without the env var', function () {
        return Promise.join(
          validatorFactory.beacon(),
          userFactory.unVerified(),
          function (validator, user) {
            const future = moment().add(1, 'd')
            const org = validator[0]
            const merch = validator[1]
            const beacon = validator[4]
            return userPunchCardFactory.model( // Valid new punch card
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              user.id, // User
              models.UserPunchCard.Identity.Ready, // UserPunchCard ready
              3 // PunchCount
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return [org, merch, punch, user, userPunchCard, beacon]
            })
          }
        ).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          process.env.KZM_CHECK_USER_VERIFICATION = null
          return models.UserPunchCard.redeem(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function (result) {
            expect(result.punchCount).to.eql(punch.punchLimit)
          })
        })
      })

      it('should redeem via a beacon validator', function () {
        return validatorFactory.beacon().spread(function (org, merch, shopCluster, shop, beacon) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, beacon]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, beacon) {
          return models.UserPunchCard.redeem(userPunchCard.id, merch.id, {
            uid: beacon.uid,
            major: beacon.major,
            minor: beacon.minor
          }, user).then(function (result) {
            expect(result.punchCount).to.eql(punch.punchLimit)
          })
        })
      })

      it('should redeem via a qr validator', function () {
        return validatorFactory.qr().spread(function (org, merch, shopCluster, shop, qr) {
          const future = moment().add(1, 'd')
          return userPunchCardFactory.model( // Valid new punch card
            org.id, // Organization
            merch.id, // Merchant
            undefined, // isPublic
            null, // identifier
            null, // Category
            null, // code
            null, // PunchCard
            models.PunchCard.Identity.Published, // identity published
            future, // expiresAt
            null, // User
            models.UserPunchCard.Identity.Ready, // UserPunchCard ready
            3 // PunchCount
          ).spread(function (org, merch, punch, user, userPunchCard) {
            return [org, merch, punch, user, userPunchCard, qr]
          })
        }).spread(function (org, merch, punch, user, userPunchCard, qr) {
          return models.UserPunchCard.redeem(userPunchCard.id, merch.id, {
            token: qr.token
          }, user).then(function (result) {
            expect(result.punchCount).to.eql(punch.punchLimit)
          })
        })
      })
    })

    describe('#expired', function () {
      it('should return an Event list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const firstValidation = moment(current).add(1, 'd')
        const secondValidation = moment(current).add(2, 'w')
        return validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const UserPunchCardValidation = models.UserPunchCardValidation
            const future = moment(current).add(1, 'd')
            const validationLimit = 2
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              // UserPunchCard InProgress
              models.UserPunchCard.Identity.InProgress,
              2, // PunchCount
              validationLimit
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return Promise.join(
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: firstValidation
                }),
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: secondValidation
                }),
                UserPunchCardValidation.create({
                  identity: UserPunchCardValidation.Identity.Redeem,
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: secondValidation
                }),
                function () { return [merch, shop, user, userPunchCard] }
              )
            }).spread(function (merch, shop, user, userPunchCard) {
              return models.UserPunchCard.expired(user)
                .then(function (result) {
                  expect(result).to.be.an('array')
                  expect(result).to.have.lengthOf(1)
                  const element = result[0]
                  expect(element).to.be.an('object')
                  expect(element).to.have.property('id', userPunchCard.id)
                  expect(element).to.have.property('identifier',
                    userPunchCard.identifier
                  )
                  expect(element).to.have.property('identity',
                    userPunchCard.identity
                  )
                  expect(element).to.include.keys('PunchCard.expiresAt')
                  expect(element).to.have.property('PunchCard.Merchant.name',
                    merch.name
                  )
                  expect(element).to.have.property(
                    'PunchCard.Merchant.imageUrl',
                    'test.jpg'
                  )
                })
            })
          })
      })
    })

    describe('#completed', function () {
      it('should return an Event list', function () {
        const current = '2018-02-01T06:00:00.000Z'
        const firstValidation = moment(current).clone().add(1, 'd')
        const secondValidation = moment(current).clone().add(2, 'w')
        return validatorFactory.beacon()
          .spread(function (org, merch, shopCluster, shop, beacon) {
            const UserPunchCardValidation = models.UserPunchCardValidation
            const future = moment(current).add(1, 'd')
            return userPunchCardFactory.model(
              org.id, // Organization
              merch.id, // Merchant
              undefined, // isPublic
              null, // identifier
              null, // Category
              null, // code
              null, // PunchCard
              models.PunchCard.Identity.Published, // identity published
              future, // expiresAt
              null, // User
              // UserPunchCard InProgress
              models.UserPunchCard.Identity.InProgress,
              3 // PunchCount
            ).spread(function (org, merch, punch, user, userPunchCard) {
              return Promise.join(
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: firstValidation
                }),
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: secondValidation
                }),
                UserPunchCardValidation.create({
                  validatorId: beacon.id,
                  userPunchCardId: userPunchCard.id,
                  created_at: secondValidation
                }),
                function () { return [merch, shop, user, userPunchCard] }
              )
            }).spread(function (merch, shop, user, userPunchCard) {
              return models.UserPunchCard.completed(user)
                .then(function (result) {
                  console.log(result)
                  expect(result).to.be.an('array')
                  expect(result).to.have.lengthOf(1)
                  const element = result[0]
                  expect(element).to.be.an('object')
                  expect(element).to.have.property('id', userPunchCard.id)
                  expect(element).to.have.property('identifier',
                    userPunchCard.identifier
                  )
                  expect(element).to.have.property('identity',
                    userPunchCard.identity
                  )
                  expect(element).to.include.keys('created_at')
                  expect(element).to.have.property('name', merch.name)
                  expect(element).to.have.property('image_url', 'test.jpg')
                })
            })
          })
      })
    })
  })
})
