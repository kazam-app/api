/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha ShopCluster Test
 * @file test/models/shopcluster.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const shopClusterFactory = require('../helpers/factories/shopcluster')

module.exports = describe('ShopCluster', function () {
  describe('instanceMethods', function () {
    describe('#toShow', function () {
      it('should return show data', function () {
        return shopClusterFactory.model().spread(function (cluster) {
          const result = cluster.toShow()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', cluster.id)
          expect(result).to.have.property('name', cluster.name)
          expect(result).to.have.property('location', cluster.location)
          expect(result).to.have.property('postal_code', cluster.postalCode)
          expect(result).to.have.property('city', cluster.city)
          expect(result).to.have.property('state', cluster.state)
          expect(result).to.have.property('country', cluster.country)
          expect(result).to.have.property('lat', cluster.lat)
          expect(result).to.have.property('lng', cluster.lng)
        })
      })
    })
  })

  describe('classMethods', function () {
    describe('#createFromData', function () {
      it('should fail to create a new shop cluster with invalid data', function () {
        return models.ShopCluster.createFromData({ shop_cluster: null })
          .then(function () {
            throw new Error('created shop cluster with invalid data')
          }).catch(function (err) {
            expect(err.message).to.eql('missing_shop_cluster_data')
          })
      })

      it('should create a new merchant with valid data', function () {
        return models.ShopCluster.createFromData({
          shop_cluster: {
            name: 'Test ShopCluster',
            location: 'Test Location',
            city: 'CDMX',
            postal_code: '01217',
            state: 'DF'
          }
        })
      })
    })

    describe('#updateFromData', function () {
      it('should fail to update a shop cluster with invalid id', function () {
        return models.ShopCluster.updateFromData('INVALID_ID', {
          shop_cluster: {
            name: 'Test ShopCluster 2',
            location: 'Test Location',
            city: 'CDMX',
            postal_code: '01217',
            state: 'DF'
          }
        }).then(function () {
          throw new Error('updated shop cluster with invalid id')
        }).catch(function (err) {
          expect(err.message).to.eq('missing_shop_cluster_data')
        })
      })
    })

    describe('#delete', function () {
      it('should fail to remove a Category with invalid id', function () {
        return shopClusterFactory.model().spread(function () {
          return models.ShopCluster.delete('ABC')
        }).then(function () {
          throw new Error('found a Category via an invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should remove a Category with valid id', function () {
        return shopClusterFactory.model().spread(function (cluster) {
          return models.ShopCluster.delete(cluster.id).then(function (result) {
            expect(result).to.eql(true)
          })
        })
      })
    })
  })
})
