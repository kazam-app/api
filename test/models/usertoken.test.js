/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha UserToken Test
 * @file test/models/usertoken.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const error = require('../helpers/error')
const session = require('../helpers/session')

module.exports = describe('UserToken', function () {
  describe('classMethods', function () {
    describe('#findByJwtAndVerify', function () {
      beforeEach(session.user)

      it('should throw an InvalidError with an invalid jwt', function () {
        return models.UserToken.findByJwtAndVerify('INVALID_TOKEN', models.UserToken.Identity.Login)
          .then(function () {
            throw new Error('found a token with invalid data')
          }).catch(function (err) {
            error.invalidError(err, 'invalid_token')
          })
      })

      it('should throw an ExpiredError with an expired jwt', function () {
        const identity = models.UserToken.Identity.Validate
        return models.UserToken.create({ userId: jwtUser.id, identity: identity, expiresAt: new Date() })
          .then(function (token) {
            return models.UserToken.findByJwtAndVerify(token.jwt(), identity)
          })
          .then(function () {
            throw new Error('found an expired token')
          })
          .catch(function (err) {
            error.expiredError(err, 'expired_token')
          })
      })

      it('should throw an NotFoundError with another identity', function () {
        const identity = models.UserToken.Identity.Verify
        return models.UserToken.findByJwtAndVerify(jwtToken, identity)
          .then(function () {
            throw new Error('found another token')
          })
          .catch(function (err) {
            error.notFoundError(err, 'token_not_found')
          })
      })

      it('should return a verified jwt and UserToken', function () {
        const identity = models.UserToken.Identity.Login
        return models.UserToken.findByJwtAndVerify(jwtToken, identity)
          .spread(function (token, element) {
            expect(token).to.be.an('object')
            expect(element).to.have.a.property('userId', jwtUser.id)
            expect(element).to.have.a.property('token')
            expect(element).to.have.a.property('secret')
          })
      })
    })
  })
})
