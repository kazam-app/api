/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Organization Test
 * @file test/models/organization.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const organizationFactory = require('../helpers/factories/organization')

module.exports = describe('Organization', function () {
  describe('instanceMethods', function () {
    describe('#toAdmin', function () {
      it('should return admin data', function () {
        return organizationFactory.model().spread(function (org) {
          const result = org.toAdmin()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', org.id)
          expect(result).to.have.property('name', org.name)
        })
      })
    })
  })

  describe('classMethods', function () {
    describe('#list', function () {
      it('should return an array of Organizations', function () {
        return organizationFactory.model().spread(function (org) {
          return models.Organization.list().then(function (result) {
            expect(result).to.be.an('array')
            expect(result).to.have.lengthOf(1)
            const element = result[0]
            expect(element.id).to.eql(org.id)
            expect(element.name).to.eql(org.name)
          })
        })
      })
    })

    describe('#createFromData', function () {
      it('should fail to create a new Organization with invalid data', function () {
        return models.Organization.createFromData({}).then(function () {
          throw new Error('created a Organization with an invalid data')
        }).catch(function (err) {
          expect(err.message).to.eql('missing_organization_data')
        })
      })

      it('should create a new Organization with valid data', function () {
        const valid = { name: 'Test Organization' }
        return models.Organization.createFromData({ organization: valid }).then(function (result) {
          expect(result).to.have.property('name', valid.name)
        })
      })
    })

    describe('#updateFromData', function () {
      it('should fail to update a Organization with invalid data', function () {
        return models.Organization.updateFromData('INVALID_ID', {
          organization: { name: 'Test Organization 2' }
        }).then(function () {
          throw new Error('updated Organization with invalid id')
        }).catch(function (err) {
          expect(err.message).to.eq('missing_organization_data')
        })
      })

      it('should update a Organization with valid data', function () {
        return organizationFactory.model().spread(function (cat) {
          let valid = { organization: { name: 'Test Organization 2' } }
          return models.Organization.updateFromData(cat.id, valid)
        })
      })
    })

    describe('#show', function () {
      it('should fail to show a Organization with invalid id', function () {
        return organizationFactory.model().spread(function () {
          return models.Organization.show('ABC')
        }).then(function () {
          throw new Error('found a Organization via an invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should fail to show a Organization with invalid id', function () {
        return organizationFactory.model().spread(function () {
          return models.Organization.show(1)
        }).then(function () {
          throw new Error('found a Organization via an invalid id')
        }).catch(function (err) {
          expect(err.message).to.eql('not_found')
        })
      })

      it('should return a Organization', function () {
        return organizationFactory.model().spread(function (cat) {
          return models.Organization.show(cat.id).then(function (result) {
            expect(result.name).to.eql(cat.name)
            expect(result.imageUrl).to.eql(cat.imageUrl)
          })
        })
      })
    })
  })
})
