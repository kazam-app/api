/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha OrganizationUser Test
 * @file test/models/organizationuser.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const organizationFactory = require('../helpers/factories/organization')
const organizationUserFactory = require('../helpers/factories/organizationuser')

module.exports = describe('OrganizationUser', function () {
  describe('#login', function () {
    beforeEach(require('../helpers/session').simple)

    it('should fail with invalid data', function () {
      return models.OrganizationUser.login('foo', 'bar')
        .then(function () {
          throw new Error('logged in with invalid data')
        }).catch(function (err) {
          expect(err.message).to.eql('login_fail')
        })
    })

    it('should return a user and token with valid data', function () {
      return models.OrganizationUser.login(jwtUser.email, jwtPass)
    })
  })

  describe('#signUp', function () {
    it('should fail with invalid data', function () {
      return models.OrganizationUser.signUp({})
        .then(function () {
          throw new Error('created user with invalid data')
        }).catch(function (err) {
          expect(err.message).to.eql('missing_user_data')
        })
    })

    it('should create a new user with correct invite params', function () {
      return organizationUserFactory.model().spread(function (org, user) {
        return models.OrganizationUserInvite.create({
          organizationUserId: user.id,
          email: 'test@u360.test'
        })
      }).then(function (invite) {
        const pass = '12345678'
        const validUser = {
          name: 'Name',
          last_names: 'Last Names',
          password: pass,
          password_confirmation: pass
        }
        const params = {
          user: validUser,
          token: invite.jwt()
        }
        return models.OrganizationUser.signUp(params)
      })
    })

    it('should create a new user and organization and return a token', function () {
      const pass = '12345678'
      const validUser = {
        organization: { name: 'Test Organization' },
        user: {
          name: 'Name',
          last_names: 'Last Names',
          email: 'foo@bar.com',
          password: pass,
          password_confirmation: pass
        }
      }
      return models.OrganizationUser.signUp(validUser)
    })
  })

  describe('#generateUser', function () {
    it('should fail with missing_password_confirmation', function (done) {
      try {
        models.OrganizationUser.generateUser({})
          .then(function () {
            throw new Error('generate user with invalid data')
          })
      } catch (err) {
        expect(err.message).to.eql('missing_password_confirmation')
        done()
      }
    })

    it('should fail with password_does_not_match', function (done) {
      try {
        models.OrganizationUser.generateUser({
          password: '12345678',
          password_confirmation: 'no_match'
        }).then(function () {
          throw new Error('generate user with invalid data')
        })
      } catch (err) {
        expect(err.message).to.eql('password_does_not_match')
        done()
      }
    })

    it('should fail with invalid method data', function () {
      return models.OrganizationUser.generateUser({
        password: '12345678',
        password_confirmation: '12345678'
      }).then(function () {
        throw new Error('generate user with invalid data')
      }).catch(function () {
        // expect(err.message).to.eql('invalid_method')
      })
    })

    it('should fail with invalid email', function (done) {
      organizationFactory.model().spread(function (org) {
        models.OrganizationUser.generateUser({
          password: '12345678',
          password_confirmation: '12345678'
        }, '', org.id).then(function () {
          done(new Error('generate user with invalid data'))
        }).catch(function () {
          done()
        })
      }).catch(done)
    })

    it('should fail with invalid data', function (done) {
      organizationFactory.model().spread(function (org) {
        models.OrganizationUser.generateUser({
          password: '12345678',
          password_confirmation: '12345678'
        }, 'test@wallet.mx', org.id).then(function () {
          done(new Error('generate user with invalid data'))
        }).catch(function () {
          done()
        })
      }).catch(done)
    })

    it('should generate an OrganizationUser instance', function (done) {
      organizationFactory.model().spread(function (org) {
        return models.OrganizationUser.generateUser({
          name: 'Name',
          last_names: 'Last Names',
          password: '12345678',
          password_confirmation: '12345678'
        }, 'test@wallet.mx', org.id).then(function (result) {
          done()
        })
      }).catch(done)
    })
  })
})
