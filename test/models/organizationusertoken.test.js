/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha OrganizationUserToken Test
 * @file test/models/organizationusertoken.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const orgUserFactory = require('../helpers/factories/organizationuser')
const orgUserTokenFactory = require('../helpers/factories/organizationusertoken')

module.exports = describe('OrganizationUserToken', function () {
  describe('instanceMethods', function () {
    describe('#logout', function () {
      it('should add an expiresAt date', function (done) {
        orgUserTokenFactory.model().spread(function (org, user, token) {
          return token.logout().then(function (result) {
            expect(result).to.not.eql(null)
            done()
          })
        }).catch(done)
      })
    })

    describe('#usableSecret', function () {
      it('should return a decoded string')
    })
  })

  describe('classMethods', function () {
    describe('#refreshSession', function () {
      it('should refresh a session token without a previous token', function (done) {
        orgUserFactory.model().spread(function (org, user) {
          return models.OrganizationUserToken.refreshSession(user)
        }).then(function () {
          done()
        }).catch(done)
      })

      it('should refresh a session token with a previous token', function (done) {
        orgUserTokenFactory.model().spread(function (org, user, token) {
          return models.OrganizationUserToken.refreshSession(user)
        }).then(function () {
          done()
        }).catch(done)
      })
    })

    describe('#findSessionToken', function () {
      it('should not find a session token if non exist', function (done) {
        models.OrganizationUserToken.findSessionToken('ABC').then(function (token) {
          expect(token).to.be.eql(null)
          done()
        }).catch(done)
      })

      it('should refresh a session token with a previous token', function (done) {
        orgUserTokenFactory.model().spread(function (org, user, token) {
          return models.OrganizationUserToken.findSessionToken(token.token)
            .then(function (result) {
              return [token, result]
            })
        }).spread(function (valid, token) {
          expect(token).to.have.property('id', valid.id)
          expect(token).to.have.property('organizationUserId', valid.organizationUserId)
          expect(token).to.have.property('token', valid.token)
          expect(token).to.have.property('secret', valid.secret)
          done()
        }).catch(done)
      })
    })
  })
})
