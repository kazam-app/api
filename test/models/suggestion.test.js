/* eslint-env mocha */
/**
 * Kazam Api
 * @name Mocha Suggestion Test
 * @file test/models/suggestion.test.js
 * @author Joel Cano
 */

'use strict'

// callbacks
require('../callbacks')

const session = require('../helpers/session')
const suggestionFactory = require('../helpers/factories/suggestion')

module.exports = describe('Suggestion', function () {
  describe('instanceMethods', function () {
    describe('#toApi', function () {
      it('should return api data', function () {
        return suggestionFactory.model().spread(function (user, suggestion) {
          const result = suggestion.toApi()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', result.id)
          expect(result).to.have.property('name', result.name)
        })
      })
    })

    describe('#toAdmin', function () {
      it('should return admin data', function () {
        return suggestionFactory.model().spread(function (user, suggestion) {
          return models.Suggestion.findById(suggestion.id, { include: [{ model: models.User }] }).then(function (result) {
            return [user, result]
          })
        }).spread(function (user, suggestion) {
          const result = suggestion.toAdmin()
          expect(result).to.be.an('object')
          expect(result).to.have.property('id', result.id)
          expect(result).to.have.property('name', result.name)
          expect(result).to.have.property('user')
          const userObj = result.user
          expect(userObj).to.be.an('object')
          expect(userObj).to.have.property('identifier', user.identifier)
          expect(userObj).to.have.property('name', user.name)
          expect(userObj).to.have.property('email', user.email)
        })
      })
    })
  })

  describe('classMethods', function () {
    describe('#adminList', function () {
      it('should return an empty array', function () {
        return models.Suggestion.adminList().then(function (suggestions) {
          expect(suggestions).to.be.an('array')
          expect(suggestions).to.have.lengthOf(0)
        })
      })

      it('should return an array with a Suggestion', function () {
        return suggestionFactory.model().spread(function (user, suggestion) {
          return models.Suggestion.adminList().then(function (suggestions) {
            expect(suggestions).to.be.an('array')
            expect(suggestions).to.have.lengthOf(1)
            const element = suggestions[0]
            expect(element).to.have.property('id', suggestion.id)
            expect(element).to.have.property('name', suggestion.name)
          })
        })
      })
    })

    describe('#createFromData', function () {
      beforeEach(session.user)

      it('should fail to create a new Suggestion with invalid data', function () {
        const invalid = {}
        return models.Suggestion.createFromData({ suggestion: invalid }, jwtUser).then(function () {
          throw new Error('created a suggestion with invalid data')
        }).catch(function (err) {
          expect(err.name).to.eql('SequelizeValidationError')
        })
      })

      it('should create a new Suggestion with valid data', function () {
        const valid = { name: 'Test Merchant Suggestion' }
        return models.Suggestion.createFromData({ suggestion: valid }, jwtUser).then(function (result) {
          expect(result.name).to.be.eq(valid.name)
        })
      })
    })
  })
})
