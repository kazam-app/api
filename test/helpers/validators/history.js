/**
 * Merchant Api
 * @name Mocha History Validators
 * @file history.js
 * @author Joel Cano
 */

'use strict'

/**
 * @description Validates if the element matches the provided json object
 * @param {object} element - json object
 * @param {Category} model - sequelize instance
 *
 * @static
 */
module.exports = function (element) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id')
  expect(element).to.have.property('identity')
  expect(element).to.have.property('created_at')
  expect(element).to.have.property('shop')
  expect(element).to.have.property('user')
}
