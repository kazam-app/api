/**
 * Merchant Api
 * @name Mocha Validator Validators
 * @file category.js
 * @author Joel Cano
 */

'use strict'
/**
 * @function beacon
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Validator} model - sequelize instance
 *
 * @static
 */
module.exports.beacon = function (element, model, isActive) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('is_active', isActive)
}
/**
 * @function beaconJson
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Validator} model - sequelize instance
 *
 * @static
 */
module.exports.beaconJson = function (element, object, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('merchant_id', object.merchant_id)
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('is_active', object.is_active)
  expect(element).to.have.property('uid', object.uid)
  expect(element).to.have.property('major', object.major)
  expect(element).to.have.property('minor', object.minor)
  expect(element).to.include.keys('id')
}
/**
 * @function qr
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Validator} model - sequelize instance
 *
 * @static
 */
module.exports.qr = function (element, model, isActive) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('is_active', isActive)
}
/**
 * @function qrJson
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Validator} model - sequelize instance
 *
 * @static
 */
module.exports.qrJson = function (element, object, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('is_active', object.is_active)
  expect(element).to.have.property('merchant_id', merchant.id)
  expect(element).to.include.keys('id')
}
