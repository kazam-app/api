/**
 * Merchant Api
 * @name Mocha Organization User Validators
 * @file organizationuser.js
 * @author Joel Cano
 */

'use strict'

/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {OrganizationUser} model - sequelize instance
 *
 * @static
 */
module.exports.model = function (element, model) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('last_names', model.last_names)
  expect(element).to.have.property('email', model.email)
  expect(element).to.include.keys('id')
  expect(element).to.not.include.keys('password')
  expect(element).to.include.keys('token')
}
/**
 * @function profile
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {OrganizationUser} model - sequelize instance
 *
 * @static
 */
module.exports.profile = function (element, model, organization) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('last_names', model.lastNames)
  expect(element).to.have.property('email', model.email)
  expect(element).to.have.property('organization', organization.name)
  expect(element).to.include.keys(['id', 'merchants'])
  expect(element.merchants).to.be.an('array')
  expect(element).to.not.include.keys('password')
}
/**
 * @function json
 * @description Validates if the element matches the provided json values
 * @param {object} element - json object
 * @param {object} object - valid json object
 *
 * @static
 */
module.exports.json = function (element, object) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('last_names', object.lastNames)
  expect(element).to.have.property('email', object.email)
  expect(element).to.include.keys('id')
  expect(element).to.not.include.keys('password')
  expect(element).to.include.keys('token')
}
