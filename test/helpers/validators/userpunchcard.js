/**
 * Merchant Api
 * @name Mocha UserPunchCard Validators
 * @file userpunchcard.js
 * @author Joel Cano
 */

'use strict'
/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {UserPunchCard} model - sequelize instance
 * @param {PunchCard} punch - sequelize instance
 * @param {Merchant} merchant - sequelize instance
 *
 * @static
 */
module.exports.model = function (element, model, punch, merchant, category) {
  expect(element).to.be.an('object')
  // console.log('modelValidator', element)
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('punch_count', model.punchCount)
  expect(element).to.have.property('prize', punch.prize)
  expect(element).to.have.property('punch_limit', punch.punchLimit)
  expect(element).to.have.property('merchant_id', merchant.id)
  expect(element).to.have.property('merchant_name', merchant.name)
  expect(element).to.have.property('merchant_image_url', merchant.imageUrl)
  expect(element).to.have.property('merchant_color', merchant.color)
  expect(element).to.have.property('category', category.name)
}
/**
 * @function merchant
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {UserPunchCard} model - sequelize instance
 * @param {PunchCard} punch - sequelize instance
 *
 * @static
 */
module.exports.merchant = function (element, model, punch) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('punch_count', model.punchCount)
  expect(element).to.have.property('prize', punch.prize)
  expect(element).to.have.property('rules', punch.rules)
  expect(element).to.have.property('terms', punch.terms)
  expect(element).to.have.property('punch_limit', punch.punchLimit)
  expect(element).to.include.keys('expires_at')
}
/**
 * @function merchant
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {UserPunchCard} model - sequelize instance
 * @param {PunchCard} punch - sequelize instance
 *
 * @static
 */
module.exports.punch = function (element, model, punch) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('identity', model.identity)
  expect(element).to.have.property('punch_count', model.punchCount + 1)
  expect(element).to.have.property('punch_limit', punch.punchLimit)
}
/**
 * @function redeem
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {UserPunchCard} model - sequelize instance
 * @param {PunchCard} punch - sequelize instance
 *
 * @static
 */
module.exports.redeem = function (element, model, punch) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('punch_count', model.punchCount)
  expect(element).to.have.property('prize', punch.prize)
  expect(element).to.have.property('rules', punch.rules)
  expect(element).to.have.property('terms', punch.terms)
  expect(element).to.include.keys('expires_at')
}
