/**
 * Kazam Api
 * @name Mocha User Validators
 * @file test/helpers/validators/user.js
 * @author Joel Cano
 */

'use strict'

/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {User} model - sequelize instance
 *
 * @static
 */
module.exports.model = function (element, model) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('is_verified', model.isVerified)
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('last_names', model.lastNames)
  expect(element).to.have.property('email', model.email)
  expect(element).to.have.property('gender', model.gender)
  expect(element).to.have.property('birthdate', model.birthdate)
  expect(element).to.not.include.keys('password')
  expect(element).to.include.keys(['token', 'created_at'])
}
/**
 * @function json
 * @description Validates if the element matches the provided json values
 * @param {object} element - json object
 * @param {object} object - valid json object
 *
 * @static
 */
module.exports.json = function (element, object) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('last_names', object.last_names)
  expect(element).to.have.property('email', object.email)
  expect(element).to.not.include.keys('password')
  expect(element).to.include.keys(['token', 'id', 'is_verified', 'created_at'])
}
/**
 * @function current
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {User} model - sequelize instance
 *
 * @static
 */
module.exports.current = function (element, model) {
  expect(element).to.have.property('id', model.id)
  expect(element).to.have.property('identifier', model.identifier)
  expect(element).to.have.property('email', model.email)
}
