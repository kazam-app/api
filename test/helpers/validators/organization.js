/**
 * Merchant Api
 * @name Mocha Organization Validators
 * @file organization.js
 * @author Joel Cano
 */

'use strict'

/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Organization} model - sequelize instance
 *
 * @static
 */
module.exports.model = function (element, model) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.include.keys('id')
}
