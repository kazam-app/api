/**
 * Merchant Api
 * @name Mocha PunchCard Validators
 * @file punchcard.js
 * @author Joel Cano
 */

'use strict'

/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Shop} model - sequelize instance
 * @param {Merchant} merchant - sequelize merchant instance
 *
 * @static
 */
module.exports.model = function (element, model) {
  expect(element).to.be.an('object')
  // console.log(element.identity)
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('prize', model.prize)
  expect(element).to.have.property('rules', model.rules)
  expect(element).to.have.property('terms', model.terms)
  expect(element).to.have.property('punch_limit', model.punchLimit)
  expect(element).to.have.property('redeem_code', model.redeemCode)
  expect(element).to.include.keys('expires_at')
}
/**
 * @function list
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Shop} model - sequelize instance
 * @param {Merchant} merchant - sequelize merchant instance
 *
 * @static
 */
module.exports.adminList = function (element, model, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('identity', model.identity)
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('punch_limit', model.punchLimit)
  expect(element).to.have.property('merchant', merchant.name)
  expect(element).to.include.keys('expires_at')
}
/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Shop} model - sequelize instance
 * @param {Merchant} merchant - sequelize merchant instance
 *
 * @static
 */
module.exports.list = function (element, model) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('prize', model.prize)
  expect(element).to.have.property('rules', model.rules)
  expect(element).to.have.property('terms', model.terms)
  expect(element).to.have.property('punch_limit', model.punchLimit)
  expect(element).to.include.keys('expires_at')
}
/**
 * @function json
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Shop} model - sequelize instance
 * @param {Merchant} merchant - sequelize merchant instance
 *
 * @static
 */
module.exports.json = function (element, model) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('prize', model.prize)
  expect(element).to.have.property('rules', model.rules)
  expect(element).to.have.property('terms', model.terms)
  expect(element).to.have.property('punch_limit', model.punch_limit)
  expect(element).to.have.property('validation_limit_type', 0)
  expect(element).to.include.keys('expires_at')
}
