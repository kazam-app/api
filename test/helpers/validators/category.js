/**
 * Kazam Api
 * @name Mocha Category Validators
 * @file test/helpers/validtors/category.js
 * @author Joel Cano
 */

'use strict'

/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Category} model - sequelize instance
 *
 * @static
 */
module.exports.model = function (element, model) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('image_url', model.imageUrl)
  expect(element).to.include.keys('id')
}
/**
 * @function json
 * @description Validates if the element matches the provided json object
 * @param {object} element - json object
 * @param {object} object - valid object
 *
 * @static
 */
module.exports.json = function (element, object) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('image_url', object.image_url)
  expect(element).to.include.keys('id')
}
