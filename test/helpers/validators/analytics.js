/**
 * Kazam Api
 * @name Mocha Analytics Validators
 * @file test/helpers/validators/analytics.js
 * @author Joel Cano
 */

'use strict'

const moment = require('moment')

/**
 * @description Validates if the body matches the expected analytics structure
 * @param {object} body - response json object
 *
 * @static
 */
module.exports = function (body) {
  expect(body).to.be.an('object')
  expect(body).to.have.property('active', 2)
  expect(body).to.have.property('punches', 7)
  expect(body).to.have.property('ready', 0)
  expect(body).to.have.property('redeems', 0)
  // outstanding
  expect(body).to.have.property('outstanding')
  const outstanding = body.outstanding
  expect(outstanding).to.be.an('object')
  expect(outstanding).to.have.property('limit', 3)
  expect(outstanding).to.have.property('unspecified')
  const date = moment()
  const month = date.format('MM')
  const year = date.format('YYYY')
  // shops
  expect(body).to.have.property('shops')
  const shops = body.shops
  expect(shops).to.have.property('punches')
  expect(shops.punches).to.be.an('array')
  expect(shops).to.have.property('redeems')
  expect(shops.redeems).to.be.an('array')
  // invites
  expect(body).to.have.property('invites')
  const invites = body.invites
  expect(invites).to.be.an('array')
  expect(invites).to.have.lengthOf(1)
  const invite = invites[0]
  expect(invite).to.be.an('object')
  expect(invite).to.have.property('total', '1')
  expect(invite).to.have.property('month', month)
  expect(invite).to.have.property('year', year)
  // actives
  expect(body).to.have.property('actives')
  const actives = body.actives
  expect(actives).to.be.an('array')
  expect(actives).to.have.lengthOf(1)
  const active = actives[0]
  expect(active).to.be.an('object')
  expect(active).to.have.property('total', '2')
  expect(active).to.have.property('month', month)
  expect(active).to.have.property('year', year)
  // validations
  expect(body).to.have.property('validations')
  const validations = body.validations
  expect(validations).to.be.an('array')
  expect(validations).to.have.lengthOf(1)
  const validation = validations[0]
  expect(validation).to.be.an('object')
  expect(validation).to.have.property('total', '1')
  expect(validation).to.have.property('month', month)
}
