/**
 * Kazam Api
 * @name Mocha Merchant Validators
 * @file test/helpers/validators/merchant.js
 * @author Joel Cano
 */

'use strict'

/**
 * @function list
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Merchant} model - sequelize instance
 *
 * @static
 */
module.exports.list = function (element, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', merchant.name)
  expect(element).to.have.property('color', merchant.color)
  expect(element).to.have.property('image_url', merchant.imageUrl)
  expect(element).to.have.property('budget_rating', merchant.budgetRating)
  expect(element).to.include.keys(['id', 'category', 'organization', 'organization_id', 'category_id'])
}
/**
 * @function adminList
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Merchant} model - sequelize instance
 *
 * @static
 */
module.exports.adminList = function (element, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', merchant.name)
  expect(element).to.include.keys(['id', 'category', 'organization', 'organization_id', 'category_id'])
}
/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Merchant} model - sequelize instance
 *
 * @static
 */
module.exports.model = function (element, merchant, org, cat) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('organization_id', org.id)
  expect(element).to.have.property('category_id', cat.id)
  expect(element).to.have.property('color', merchant.color)
  expect(element).to.have.property('name', merchant.name)
  expect(element).to.have.property('color', merchant.color)
  expect(element).to.have.property('image_url', merchant.imageUrl)
  expect(element).to.have.property('budget_rating', merchant.budgetRating)
  expect(element).to.include.keys('id')
}
/**
 * @function json
 * @description Validates if the element matches the provided json object
 * @param {object} element - json object
 * @param {object} object - valid object
 *
 * @static
 */
module.exports.json = function (element, object, category, org) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('color', object.color || null)
  expect(element).to.have.property('category_id', category.id)
  expect(element).to.have.property('organization_id', org.id)
  expect(element).to.have.property('budget_rating', object.budget_rating || 1)
  expect(element).to.include.keys(['id', 'image_url'])
}
