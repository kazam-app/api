/**
 * Merchant Api
 * @name Mocha Shop Validators
 * @file shop.js
 * @author Joel Cano
 */

'use strict'
/**
 * @function model
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Shop} model - sequelize instance
 * @param {Merchant} merchant - sequelize merchant instance
 *
 * @static
 */
module.exports.model = function (element, model, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('merchant_id', merchant.id)
  expect(element).to.have.property('is_active', model.isActive)
  expect(element).to.have.property('location', model.location)
  expect(element).to.have.property('postal_code', model.postalCode)
  expect(element).to.have.property('phone', model.phone)
  expect(element).to.have.property('city', model.city)
  expect(element).to.have.property('state', model.state)
  expect(element).to.have.property('country', model.country)
  expect(element).to.have.property('lat')
  expect(element).to.have.property('lng')
  // expect(element).to.have.property('image_url', shop.imageUrl);
  expect(element).to.include.keys('id')
}
/**
 * @function api
 * @description Validates if the element matches the provided model instance
 * @param {object} element - json object
 * @param {Shop} model - sequelize instance
 * @param {Merchant} merchant - sequelize merchant instance
 *
 * @static
 */
module.exports.api = function (element, model, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', model.name)
  expect(element).to.have.property('merchant_id', merchant.id)
  expect(element).to.have.property('is_active', model.isActive)
  expect(element).to.have.property('location', model.location)
  expect(element).to.have.property('postal_code', model.postalCode)
  expect(element).to.have.property('phone', model.phone)
  expect(element).to.have.property('city', model.city)
  expect(element).to.have.property('state', model.state)
  expect(element).to.have.property('country', model.country)
  expect(element).to.have.property('lat')
  expect(element).to.have.property('lng')
  expect(element).to.include.keys('id')
}
/**
 * @function json
 * @description Validates if the element matches the provided json values
 * @param {object} element - json object
 * @param {object} object - valid json object
 * @param {Merchant} merchant - sequelize merchant instance
 *
 * @static
 */
module.exports.json = function (element, object, merchant) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('merchant_id', merchant.id)
  expect(element).to.have.property('shop_cluster_id', null)
  expect(element).to.have.property('location', object.location)
  expect(element).to.have.property('postal_code', object.postal_code)
  expect(element).to.have.property('phone', object.phone)
  expect(element).to.have.property('city', object.city)
  expect(element).to.have.property('state', object.state)
  expect(element).to.have.property('lat')
  expect(element).to.have.property('lng')
  expect(element).to.include.keys('id', 'is_active')
}
/**
 * @function adminList
 * @description Validates if the element matches the provided
 *              Shop and ShopCluster instance
 * @param {object} element - json object
 * @param {Shop} shop - sequelize Shop instance
 * @param {Shop} sc - sequelize ShopCluster instance
 *
 * @static
 */
module.exports.adminList = function (element, shop, sc) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', shop.id)
  expect(element).to.have.property('name', shop.name)
  expect(element).to.have.property('shop_cluster', sc.name)
}
/**
 * @function adminJson
 * @description Validates if the element matches the provided
 *              Shop and ShopCluster instance
 * @param {object} element - json object
 * @param {Shop} shop - sequelize Shop instance
 * @param {ShopCluster} sc - sequelize ShopCluster instance
 *
 * @static
 */
module.exports.adminJson = function (element, object) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('name', object.name)
  expect(element).to.have.property('merchant_id', object.merchant_id)
  expect(element).to.have.property('shop_cluster_id', object.shop_cluster_id)
  expect(element).to.have.property('is_active', object.is_active)
  expect(element).to.have.property('location', object.location)
  expect(element).to.have.property('postal_code', object.postal_code)
  expect(element).to.have.property('phone', object.phone)
  expect(element).to.have.property('city', object.city)
  expect(element).to.have.property('state', object.state)
  expect(element).to.include.keys(['id', 'country', 'lat', 'lng'])
}
/**
 * @function admin
 * @description Validates if the element matches the provided Shop instance
 * @param {object} element - json object
 * @param {Shop} shop - sequelize Shop instance
 *
 * @static
 */
module.exports.admin = function (element, shop) {
  expect(element).to.be.an('object')
  expect(element).to.have.property('id', shop.id)
  expect(element).to.have.property('name', shop.name)
  expect(element).to.have.property('merchant_id', shop.merchantId)
  expect(element).to.have.property('shop_cluster_id', shop.shopClusterId)
  expect(element).to.have.property('is_active', shop.isActive)
  expect(element).to.have.property('location', shop.location)
  expect(element).to.have.property('postal_code', shop.postalCode)
  expect(element).to.have.property('phone', shop.phone)
  expect(element).to.have.property('city', shop.city)
  expect(element).to.have.property('state', shop.state)
  expect(element).to.have.property('country', shop.country)
  expect(element).to.have.property('lat', shop.lat)
  expect(element).to.have.property('lng', shop.lng)
}
