/**
 * Merchant Api
 * @name OrganizationUserToken Factories
 * @file organizationusertoken.js
 * @author Joel Cano
 */

'use strict'

const orgUserFactory = require('./organizationuser')

module.exports = {
  /**
   * @method model
   * @description Creates or finds a OrganizationUserToken and its dependencies
   * @param {object}  orgId - Organization id
   * @param {object}  id - OrganizationUser id
   * @param {boolean} isValid - validity flag
   * @return {Promise<Organization, OrganizationUser>}
   *  returns a Organization and OrganizationUser Promise
   *
   * @static
   */
  model: function (orgId, userId, isValid, id, identity) {
    return orgUserFactory.model(orgId, userId, isValid)
      .spread(function (org, user) {
        const valid = {
          organizationUserId: user.id,
          identity: identity
        }
        return models.OrganizationUserToken.findOrCreate({
          where: { id: id, organizationUserId: user.id },
          defaults: valid
        }).spread(function (token, created) {
          return [org, user, token]
        })
      })
  }
}
