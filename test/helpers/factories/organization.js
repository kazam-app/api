/**
 * Kazam Api
 * @name Organization Factories
 * @file test/helpers/factories/organization.js
 * @author Joel Cano
 */

'use strict'

const uuid = require('uuid/v4')

module.exports = {
  valid: { name: 'Test Organization ' },
  /**
   * @method model
   * @description Creates or finds an Organization
   * @param {object}  id - Organization id
   * @return {Promise<Organization>} returns a Organization Promise
   *
   * @static
   */
  model: function (id) {
    const valid = { name: this.valid.name + uuid() }
    return models.Organization.findOrCreate({
      where: { id: id },
      defaults: valid
    })
  }
}
