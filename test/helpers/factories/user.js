/**
 * Kazam Api
 * @name User Factories
 * @file test/helpers/factories/user.js
 * @author Joel Cano
 */

'use strict'

const uuid = require('uuid/v4')

module.exports = {
  pass: '12345678',
  /**
   * @method model
   * @description Creates or finds a User and its dependencies
   * @param {object}  id - User id
   * @param {boolean} isValid - validity flag
   * @param {string} validateEmail - validateEmail flag
   * @return {Promise<boolean, User>} returns a boolean and User Promise
   *
   * @static
   */
  model: function (id, isValid, validateEmail, playerId) {
    if (isValid === undefined) isValid = true
    let valid = {
      isValid: isValid,
      isVerified: true,
      name: 'Name',
      lastNames: 'Last Names',
      email: uuid() + '@testKzm.com',
      password: models.User.hash(this.pass),
      validateEmail: validateEmail,
      playerId: playerId
    }
    return models.User.findOrCreate({ where: { id: id }, defaults: valid })
  },
  /**
   * @method unVerified
   * @description Creates an unVerified User and its dependencies
   * @return {Promise<User>} returns a User Promise
   *
   * @static
   */
  unVerified: function () {
    let valid = {
      isValid: true,
      name: 'Name',
      lastNames: 'Last Names',
      email: uuid() + '@testKzm.com',
      password: models.User.hash(this.pass)
    }
    return models.User.create(valid)
  },
  /**
   * @method unSynced
   * @description Creates an un synced User and its dependencies
   * @return {Promise<User>} returns a User Promise
   *
   * @static
   */
  unSynced: function (email) {
    if (!email) email = uuid() + '@testKzm.com'
    let valid = {
      isValid: true,
      name: 'Name',
      lastNames: 'Last Names',
      email: email,
      password: models.User.hash(this.pass)
    }
    return models.User.create(valid, { hooks: false })
  }
}
