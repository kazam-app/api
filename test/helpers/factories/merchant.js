/**
 * Kazam Api
 * @name Merchant Factories
 * @file test/helpers/factories/merchant.js
 * @author Joel Cano
 */

'use strict'

const _ = require('lodash/object')
const Promise = require('bluebird')
const catFactory = require('./category')
const orgFactory = require('./organization')

module.exports = {
  valid: {
    name: 'Test Merchant',
    imageUrl: 'test.jpg',
    backgroundImageUrl: 'background_test.jpg'
  },
  /**
   * @method model
   * @description Creates or finds an Merchant
   * @param {integer}  orgId - Organization id
   * @param {integer}  id - Merchant id
   * @param {boolean}  isPublic - public flag
   * @param {string}  identifier - identifing string
   * @param {integer}  catId - Category id
   * @param {string}  code - invite code
   * @return {Promise<Organization, Merchant>}
   *  returns a Organization and Merchant Promise
   *
   * @static
   */
  model: function (orgId, id, isPublic, identifier, catId, code, isActive) {
    return Promise.join(orgFactory.model(orgId), catFactory.model(catId), (orgStuct, catStruct) => {
      const org = orgStuct[0]
      const cat = catStruct[0]
      let valid = _.assign({}, this.valid, {
        categoryId: cat.id,
        organizationId: org.id,
        identifier: identifier,
        inviteCode: code,
        color: '#AAAFFF'
      })
      if (isPublic !== undefined) valid.isPublic = isPublic
      if (isActive !== undefined) valid.isActive = isActive
      return models.Merchant.findOrCreate({
        where: { id: id, organizationId: org.id },
        defaults: valid
      }).spread(function (merchant, created) {
        return [org, merchant, cat]
      })
    }
    )
  }
}
