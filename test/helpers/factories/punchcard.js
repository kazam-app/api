/**
 * Kazam Api
 * @name PunchCard Factories
 * @file test/helpers/factories/punchcard.js
 * @author Joel Cano
 */

'use strict'

const uuid = require('uuid/v4')
const _ = require('lodash/object')
const merchFactory = require('./merchant')

module.exports = {
  valid: {
    name: 'Test PunchCard',
    prize: 'Test Prize',
    rules: 'Test Rules',
    terms: 'Test Terms',
    punchLimit: 3
  },
  /**
   * @method model
   * @description Creates or finds a PunchCard
   * @param {integer} orgId - Organization id
   * @param {integer} merchId - Merchant id
   * @param {boolean} isPublic - public flag
   * @param {String} identifier - identifing string
   * @param {integer} catId - Category id
   * @param {String} code - invite code
   * @param {integer} id - PunchCard id
   * @param {integer} identity - PunchCard identity
   * @param {Date} expiresAt - PunchCard expiresAt
   * @param {integer} limit - PunchCard validationLimit
   * @return {Promise<Organization, Merchant, PunchCard>}
   *  returns a Organization, Merchant, and PunchCard Promise
   *
   * @static
   */
  model: function (orgId, merchId, isPublic, identifier, catId, code, id, identity, expiresAt, limit) {
    if (!expiresAt) expiresAt = new Date()
    return merchFactory.model(orgId, merchId, isPublic, identifier, catId, code)
      .spread((org, merchant, cat) => {
        let valid = _.assign({}, this.valid, {
          identity: identity,
          redeemCode: uuid(),
          expiresAt: expiresAt,
          merchantId: merchant.id,
          validationLimit: limit
        })
        return models.PunchCard.findOrCreate({
          where: { id: id, merchantId: merchant.id },
          defaults: valid
        }).spread(function (punch, created) {
          return [org, merchant, punch, cat]
        })
      })
  }
}
