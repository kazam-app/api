/**
 * Kazam Api
 * @name Shop Factories
 * @file test/helpers/factories/shop.js
 * @author Joel Cano
 */

'use strict'

const _ = require('lodash/object')
const Promise = require('bluebird')
const merchFactory = require('./merchant')
const shopClusterFactory = require('./shopcluster')

module.exports = {
  valid: {
    isActive: true,
    name: 'Test Shop',
    location: 'Test Location',
    city: 'Test City',
    postalCode: '01219',
    phone: '1234567890',
    state: 'DF',
    country: 'MX',
    lat: 19.4283926,
    lng: -99.2161986
  },
  /**
   * @method model
   * @description Creates or finds a Shop
   * @param {integer} orgId - Organization id
   * @param {integer} merchid - Merchant id
   * @param {boolean} isPublic - public flag
   * @param {string} identifier - identifing string
   * @param {integer} scId - ShopCluster id
   * @param {integer} id - Shop id
   * @return {Promise<Organization, Merchant, ShopCluster, Shop>}
   *  returns a Organization, Merchant, ShopCluster and Shop Promise
   *
   * @static
   */
  model: function (orgId, merchId, isPublic, identifier, scId, id, lat, lng) {
    return Promise.join(
      merchFactory.model(orgId, merchId, isPublic, identifier),
      shopClusterFactory.model(scId),
      (merchStruct, scStruct) => {
        const org = merchStruct[0]
        const merch = merchStruct[1]
        const cat = merchStruct[2]
        const shopCluster = scStruct[0]
        let valid = _.assign({}, this.valid, { merchantId: merch.id, shopClusterId: shopCluster.id })
        if (lat) valid.lat = lat
        if (lng) valid.lng = lng
        return models.Shop.findOrCreate({ where: { id: id, merchantId: merch.id }, defaults: valid }).spread(function (shop, created) {
          return [org, merch, shopCluster, shop, cat]
        })
      }
    )
  }
}
