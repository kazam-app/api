/**
 * Kazam Api
 * @name UserPunchCard Factories
 * @file test/helpers/factories/userpunchcard.js
 * @author Joel Cano
 */

'use strict'

const Promise = require('bluebird')
const userFactory = require('./user')
const punchFactory = require('./punchcard')

module.exports = {
  /**
   * @method model
   * @description Creates or finds a UserPunchCard
   * @param {integer}  orgId - Organization id
   * @param {integer}  merchId - Merchant id
   * @param {boolean}  isPublic - public flag
   * @param {string}  identifier - identifing string
   * @param {integer} catId - Category id
   * @param {string}  code - invite code
   * @param {integer} id - PunchCard id
   * @param {integer} pIdentity - PunchCard identity
   * @param {Date} expiresAt - PunchCard expiresAt
   * @param {integer} userId - User id
   * @param {integer} identity - UserPunchCard identity
   * @param {integer} punches - UserPunchCard punchCount
   * @param {integer} limit - PunchCard validationLimit
   * @return {Promise<Organization, Merchant, PunchCard, User, UserPunchCard>}
   *  returns a Organization, Merchant, PunchCard, User and UserPunchCard
   *  Promise
   *
   * @static
   */
  model: function (
    orgId,
    merchId,
    isPublic,
    identifier,
    catId,
    code,
    punchCardId,
    pIdentity,
    expiresAt,
    userId,
    identity,
    punches,
    limit,
    createdAt
  ) {
    return Promise.join(
      punchFactory.model(
        orgId,
        merchId,
        isPublic,
        identifier,
        catId,
        code,
        punchCardId,
        pIdentity,
        expiresAt,
        limit
      ),
      userFactory.model(userId),
      function (punchStruct, userStruct) {
        const punch = punchStruct[2]
        const user = userStruct[0]
        let insert = {
          punchCardId: punch.id,
          userId: user.id,
          identity: identity
        }
        if (punches) insert.punchCount = punches
        if (createdAt) insert.created_at = createdAt
        return models.UserPunchCard.create(insert)
          .then(function (userPunchCard) {
            return [
              punchStruct[0],
              punchStruct[1],
              punch,
              user,
              userPunchCard,
              punchStruct[3]
            ]
          })
      }
    )
  }
}
