/**
 * Merchants Api
 * @name Shop Factories
 * @file shop.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method model
   * @description Creates or finds a ShopCluster
   * @param {integer} id - ShopCluster id
   * @return {Promise<ShopCluster>}
   *  returns a ShopCluster Promise
   *
   * @static
   */
  model: function (id) {
    const defaults = {
      name: 'Test ShopCluster',
      location: 'Test Location',
      city: 'CDMX',
      postalCode: '01210',
      state: 'DF'
    }
    return models.ShopCluster.findOrCreate({
      where: { id: id },
      defaults: defaults
    })
  }
}
