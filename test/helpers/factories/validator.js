/**
 * Kazam Api
 * @name Validator Factories
 * @file test/helpers/factories/validator.js
 * @author Joel Cano
 */

'use strict'

const uuid = require('uuid/v4')
const shopFactory = require('./shop')

module.exports = {
  /**
   * @method beacon
   * @description Creates or finds a Beacon
   * @param {integer} orgId       Organization id
   * @param {integer} merchid     Merchant id
   * @param {boolean} isPublic    public flag
   * @param {string} identifier   identifing string
   * @param {integer} scId        ShopCluster id
   * @param {integer} shopId      Shop id
   * @param {integer} id          Validator id
   * @param {boolean} isActive    Validator isActive
   * @param {string} uid          Validator uid
   * @param {integer} major       Validator major
   * @param {integer} minor       Validator minor
   * @return {Promise<Organization, Merchant, ShopCluster, Shop, Validator, Category>}
   *  returns a Organization, Merchant, ShopCluster, Shop and Validator Promise
   *
   * @static
   */
  beacon: function (orgId, merchId, isPublic, identifier, scId, shopId, id, isActive, uid, major, minor) {
    return shopFactory.model(orgId, merchId, isPublic, identifier, scId, shopId)
      .spread(function (org, merch, shopCluster, shop, cat) {
        const identity = models.Validator.Identity.Beacon
        const code = uid || uuid()
        const valid = {
          isActive: true,
          merchantId: merch.id,
          shopId: shop.id,
          identity: identity,
          name: code,
          uid: code,
          major: major || 0,
          minor: minor || 0
        }
        if (isActive !== undefined) valid.isActive = isActive
        return models.Validator.findOrCreate({
          where: { id: id, identity: identity },
          defaults: valid
        }).spread(function (beacon, created) {
          return [org, merch, shopCluster, shop, beacon, cat]
        })
      })
  },
  /**
   * @method unAssignedBeacon
   * @description Creates or finds a Beacon
   * @param {integer} orgId - Organization id
   * @param {integer} merchid - Merchant id
   * @param {boolean} isPublic - public flag
   * @param {string} identifier - identifing string
   * @param {integer} scId - ShopCluster id
   * @param {integer} id - Shop id
   * @return {Promise<Organization, Merchant, ShopCluster, Shop, Validator>}
   *  returns a Organization, Merchant, ShopCluster, Shop and Validator Promise
   *
   * @static
   */
  unAssignedBeacon: function (orgId, merchId, isPublic, identifier, scId, shopId, id, isActive, uid, major, minor) {
    return shopFactory.model(orgId, merchId, isPublic, identifier, scId, shopId)
      .spread(function (org, merch, shopCluster, shop) {
        const identity = models.Validator.Identity.Beacon
        const code = uid || uuid()
        const valid = {
          isActive: true,
          merchantId: merch.id,
          // shopId: shop.id,
          identity: identity,
          name: code,
          uid: code,
          major: major || 0,
          minor: minor || 0
        }
        if (isActive !== undefined) valid.isActive = isActive
        return models.Validator.findOrCreate({
          where: { id: id, identity: identity },
          defaults: valid
        }).spread(function (beacon, created) {
          return [org, merch, shopCluster, shop, beacon]
        })
      })
  },
  /**
   * @method qr
   * @description Creates or finds a QR
   * @param {integer} orgId - Organization id
   * @param {integer} merchid - Merchant id
   * @param {boolean} isPublic - public flag
   * @param {string} identifier - identifing string
   * @param {integer} scId - ShopCluster id
   * @param {integer} id - Shop id
   * @return {Promise<Organization, Merchant, ShopCluster, Shop, Validator, Category>}
   *  returns a Organization, Merchant, ShopCluster, Shop, Validator and
   *  Category Promise
   *
   * @static
   */
  qr: function (orgId, merchId, isPublic, identifier, scId, shopId, id, isActive) {
    return shopFactory.model(orgId, merchId, isPublic, identifier, scId, shopId)
      .spread(function (org, merch, shopCluster, shop, cat) {
        return models.Validator.generateQr(shop, merch).then(function (qr) {
          if (isActive !== undefined) {
            qr.isActive = isActive
            return qr.save()
          } else return qr
        }).then(function (qr) {
          return [org, merch, shopCluster, shop, qr, cat]
        })
      })
  }
}
