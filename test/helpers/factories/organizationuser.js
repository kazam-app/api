/**
 * Merchant Api
 * @name OrganizationUser Factories
 * @file organizationuser.js
 * @author Joel Cano
 */

'use strict'

const uuid = require('uuid/v4')
const _ = require('lodash/object')
const orgFactory = require('./organization')

module.exports = {
  pass: '12345678',
  valid: {
    isValid: true,
    name: 'Name',
    lastNames: 'Last Names'
  },
  /**
   * @method model
   * @description Creates or finds a OrganizationUser and its dependencies
   * @param {object}  orgId - Organization id
   * @param {object}  id - OrganizationUser id
   * @param {boolean} isValid - validity flag
   * @return {Promise<Organization, OrganizationUser>}
   *  returns a Organization and OrganizationUser Promise
   *
   * @static
   */
  model: function (orgId, id, isValid) {
    return orgFactory.model(orgId).spread((org, created) => {
      let valid = _.assign({}, this.valid, {
        organizationId: org.id,
        email: uuid() + '@test.com',
        password: models.OrganizationUser.hash(this.pass)
      })
      if (isValid !== undefined) valid.isValid = isValid
      return models.OrganizationUser.findOrCreate({
        where: { id: id, organizationId: orgId },
        defaults: valid
      }).spread(function (user, created) {
        return [org, user]
      })
    })
  }
}
