/**
 * Kazam Api
 * @name Category Factories
 * @file test/helpers/factories/category.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  valid: { name: 'Test Category' },
  /**
   * @method model
   * @description Creates or finds an Category
   * @param {object}  id - Category id
   * @return {Promise<Category>} returns a Category Promise
   *
   * @static
   */
  model: function (id) {
    const valid = { name: this.valid.name, imageUrl: 'test.jpg' }
    return models.Category.findOrCreate({ where: { id: id }, defaults: valid })
  }
}
