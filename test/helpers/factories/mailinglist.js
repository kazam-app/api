/**
 * Kazam Api
 * @name MailingList Factories
 * @file test/helpers/factories/mailinglist.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  valid: { name: 'Test List' },
  /**
   * @method model
   * @description Creates or finds an MailingList
   * @param {object}  id - MailingList id
   * @return {Promise<MailingList>} returns a MailingList Promise
   *
   * @static
   */
  model: function (id) {
    const list = process.env.MAILCHIMP_LIST || 'e24cfef922'
    const valid = { name: this.valid.name, mailchimpId: list }
    return models.MailingList.findOrCreate({
      where: { id: id },
      defaults: valid
    })
  }
}
