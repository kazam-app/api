/**
 * Kazam Api
 * @name Suggestion Factories
 * @file test/helpers/factories.js
 * @author Joel Cano
 */

'use strict'

const userFactory = require('./user')

module.exports = {
  valid: {
    name: 'Test Suggestion'
  },
  /**
   * @method model
   * @description Creates or finds an Suggestion
   * @param {integer}  id           User id
   * @param {integer}  id           Suggestion id
   * @return {Promise<Suggestion>}  returns a Suggestion Promise
   *
   * @static
   */
  model: function (userId, id) {
    return userFactory.model(userId).spread((user) => {
      this.valid.userId = user.id
      return models.Suggestion.findOrCreate({
        where: { id: id },
        defaults: this.valid
      }).spread(function (suggestion) {
        return [user, suggestion]
      })
    })
  }
}
