/**
 * Kazam Api
 * @description Path Helper Functions
 * @file test/helpers/path.js
 * @author Joel Cano
 */

'use strict'

module.exports.api = function (path) {
  return '/v1' + path
}

module.exports.org = function (path) {
  return path
}

module.exports.admin = function (path) {
  return '/admin' + path
}
