/**
 * Kazam Api
 * @description Mocha Error Helper Functions
 * @file test/helpers/session.js
 * @author Joel Cano
 */

'use strict'

function standardError (err, name, message) {
  expect(err).to.be.an('error')
  expect(err.name).to.be.a('string').that.eql(name)
  expect(err.message).to.be.a('string').that.eql(message)
}

module.exports = {
  /**
   * @function expiredError
   * @description Validates if the error is an ExpiredError instance
   * @param {object} err -  ExpiredError instance
   * @param {string} msg -  error message
   *
   * @static
   */
  expiredError: function (err, message) {
    standardError(err, 'ExpiredError', message)
  },
  /**
   * @function invalidError
   * @description Validates if the error is an ExpiredError instance
   * @param {object} err -  ExpiredError instance
   * @param {string} msg -  error message
   *
   * @static
   */
  invalidError: function (err, message) {
    standardError(err, 'InvalidError', message)
  },
  /**
   * @function invalidError
   * @description Validates if the error is an ExpiredError instance
   * @param {object} err -  ExpiredError instance
   * @param {string} msg -  error message
   *
   * @static
   */
  notFoundError: function (err, message) {
    standardError(err, 'NotFoundError', message)
  }
}
