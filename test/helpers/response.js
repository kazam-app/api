/**
 * Kazam Api
 * @description Mocha Response Helper Functions
 * @file test/helpers/response.js
 * @author Joel Cano
 */

'use strict'

const i18n = require('i18n')

function standardError (body, code, message) {
  expect(body).to.have.all.keys('message', 'code')
  expect(body).to.deep.equal({ code: code, message: i18n.__(message) })
}

module.exports.status = {
  // success
  success: 200,
  created: 201,
  // error
  gone: 410,
  tooMany: 429,
  notFound: 404,
  forbbiden: 403,
  badRequest: 400,
  unauthorized: 401,
  unprocessableEntity: 422
}
/**
 * @function badRequest
 * @description Validates if the response body is a BadRequest template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.badRequest = function (body, msg) {
  standardError(body, 'BadRequest', msg)
}
/**
 * @function unauthorized
 * @description Validates if the response body is an Unauthorized template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.unauthorized = function (body, msg) {
  if (!msg) msg = 'No authorization token was found'
  expect(body).to.have.all.keys('message', 'code')
  expect(body.code).to.equal('InvalidCredentials')
  expect(body.message).to.equal(msg)
}
/**
 * @function forbbiden
 * @description Validates if the response body is a Forbbiden template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.forbbiden = function (body, msg) {
  standardError(body, 'Forbidden', msg)
}
/**
 * @function notFound
 * @description Validates if the response body is a BadRequest template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.notFound = function (body, msg) {
  standardError(body, 'NotFound', msg)
}
/**
 * @function gone
 * @description Validates if the response body is a Gone template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.gone = function (body, msg) {
  standardError(body, 'Gone', msg)
}
/**
 * @function tooMany
 * @description Validates if the response body is a TooManyRequests template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.tooMany = function (body, msg) {
  standardError(body, 'TooManyRequests', msg)
}
/**
 * @function validation
 * @description Validates if the response body is a BadRequest validation template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.validation = function (body, msgs) {
  expect(body).to.have.all.keys('message', 'code')
  expect(body).to.have.property('code', 'ValidationError')
  const messages = body.message
  let keys = Object.keys(msgs)
  for (let i = 0; i < keys.length; ++i) {
    let key = keys[i]
    let errors = msgs[key]
    expect(messages).to.have.property(key)
    let values = messages[key]
    expect(values).to.be.an('array')
    for (let j = 0; j < errors.length; ++j) {
      expect(values).to.include(errors[j])
    }
  }
}
/**
 * @function end
 * @description Validates if the response body is an Unauthorized template
 * @param {object} body - response body
 * @param {string} msg -  response message
 *
 * @static
 */
module.exports.end = function (done) {
  return function (err, res) {
    if (err) {
      console.error(res.body)
      done(err)
    } else done()
  }
}
