/**
 * Kazam Api
 * @name Mocha Session Helper Functions
 * @file test/helpers/session.js
 * @author Joel Cano
 */

'use strict'

const userFactory = require('./factories/user')
const merchantFactory = require('./factories/merchant')
const organizationUserFactory = require('./factories/organizationuser')

module.exports = {
  /**
   * @method user
   * @description Sets an User session
   * @param {function} done - callback function
   *
   * @static
   */
  user: function (done) {
    global.jwtPass = userFactory.pass
    userFactory.model().spread(function (user) {
      return user.checkAuth(jwtPass).then(function (token) {
        return [token, user]
      })
    }).spread(function (token, user) {
      global.jwtUser = user
      global.jwtToken = token.jwt()
      done()
    }).catch(done)
  },
  /**
   * @method unVerifiedUser
   * @description Sets an User session
   * @param {function} done - callback function
   *
   * @static
   */
  unVerifiedUser: function (done) {
    global.jwtPass = userFactory.pass
    userFactory.unVerified().then(function (user) {
      return user.checkAuth(jwtPass).then(function (token) {
        return [token, user]
      })
    }).spread(function (token, user) {
      global.jwtUser = user
      global.jwtToken = token.jwt()
      done()
    }).catch(done)
  },
  /**
   * @method simple
   * @description Sets authentication specific global variables with a minimum
   *  merchant user
   * @param {function} done - callback function
   *
   * @static
   */
  simple: function (done) {
    global.jwtPass = organizationUserFactory.pass
    organizationUserFactory.model()
      .spread(function (org, user) {
        return user.checkAuth(jwtPass).then(function (token) {
          return [token, user, org]
        })
      })
      .spread(function (token, user, organization) {
        global.jwtUser = user
        global.jwtToken = token.jwt()
        global.jwtOrganization = organization
        done()
      }).catch(done)
  },
  /**
   * @method fullRead
   * @description Sets authentication specific global variables with the a full
   *  merchant user.
   * @param {function} done - callback function
   *
   * @static
   */
  fullRead: function (done) {
    organizationUserFactory.model()
      .spread(function (org, user) {
        return merchantFactory.model(org.id)
          .spread(function (org, merchant) {
            return [org, user, merchant]
          })
      })
      .spread(function (org, user, merchant) {
        global.jwtOrganization = org
        global.jwtMerchant = merchant
        global.jwtPass = organizationUserFactory.pass
        const attrs = { merchantId: merchant.id, organizationUserId: user.id }
        return models.OrganizationUserMerchant.create(attrs)
          .then(function (userMerchant) {
            return [userMerchant, user]
          })
      })
      .spread(function (userMerchant, user) {
        global.jwtUser = user
        return user.getPermissions(userMerchant.merchantId)
      })
      .then(function (token) {
        global.jwtToken = token.token
        done()
      })
  },
  /**
   * @method fullReadWrite
   * @description Sets authentication specific global variables with the a full
   *  merchant user.
   * @param {function} done - callback function
   *
   * @static
   */
  fullReadWrite: function (done) {
    organizationUserFactory.model()
      .spread(function (org, user) {
        return merchantFactory.model(org.id)
          .spread(function (org, merchant) {
            return [org, user, merchant]
          })
      })
      .spread(function (org, user, merchant) {
        global.jwtOrganization = org
        global.jwtMerchant = merchant
        global.jwtPass = organizationUserFactory.pass
        const attrs = {
          merchantId: merchant.id,
          organizationUserId: user.id,
          role: models.OrganizationUserMerchant.Role.ReadWrite
        }
        return models.OrganizationUserMerchant.create(attrs)
          .then(function (userMerchant) {
            return [userMerchant, user]
          })
      })
      .spread(function (userMerchant, user) {
        global.jwtUser = user
        return user.getPermissions(userMerchant.merchantId)
      })
      .then(function (token) {
        global.jwtToken = token.token
        done()
      })
      .catch(done)
  },
  /**
   * @method admin
   * @description Sets authentication specific global variables with an admin
   *  user.
   * @param {function} done - callback function
   *
   * @static
   */
  admin: function (done) {
    global.jwtPass = '12345678'
    const validUser = {
      isValid: true,
      name: 'Name',
      lastNames: 'Last Names',
      email: 'foo@bar.com',
      password: models.OrganizationUser.hash(jwtPass)
    }
    models.OrganizationUser.create(validUser, { validate: false })
      .then(function (result) {
        global.jwtUser = result
        return result.checkAuth(jwtPass).then(function (token) {
          return token
        })
      }).then(function (result) {
        global.jwtToken = result.jwt()
        done()
      }).catch(done)
  }
}
