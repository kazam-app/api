/**
 * Kazam Api
 * @name Suggestion Controller
 * @file controllers/api/suggestion.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method create
   * @description Create a Suggestion via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Suggestion = req.models.Suggestion
    Suggestion.createFromData(req.params, req.jwtUser)
      .then(Suggestion.decorate(Suggestion.toApi))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
