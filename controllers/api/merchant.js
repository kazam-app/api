/**
 * Kazam Api
 * @name Merchant Controller
 * @file controllers/admin/merchant.js
 * @author Joel Cano
 */

'use strict'

const NotFoundError = require('../../lib/error').NotFoundError

module.exports = {
  /**
   * @method index
   * @description Returns all merchants for the api
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.apiList()
      .then(Merchant.decorate(Merchant.toApi))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method near
   * @description Returns all merchants for the api
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  near: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.apiNear(req.params.lat, req.params.lng)
      .then(Merchant.decorate(Merchant.toApiNear))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Return merchant information by id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    req.models.Merchant.apiShow(req.params.id)
      .then(function (result) {
        res.success(result.toApi())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method beacon
   * @description Return merchant information by beacon
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  beacon: function (req, res, next) {
    req.models.Merchant.findByBeacon(req.params)
      .then(function (result) {
        res.success(result.toApi())
      })
      .catch(NotFoundError, res.notFound)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method qr
   * @description Return merchant information by qr token
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  qr: function (req, res, next) {
    req.models.Merchant.findByQr(req.params.token)
      .then(function (result) {
        res.success(result.toApi())
      })
      .catch(res.badRequest)
      .finally(next)
  }
}
