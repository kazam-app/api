/**
 * Kazam Api
 * @name User Controller
 * @file controllers/api/user.js
 * @author Joel Cano
 */

'use strict'

const Mailer = require('../../mailer')
const errors = require('../../lib/error')
const ExpiredError = errors.ExpiredError
// template urls
const env = process.env
const recoveryUrl = env.CORS_RECOVERY || 'https://recovery.kazamapp.mx'
const validationUrl = env.CORS_VALIDATION || 'https://validation.kazamapp.mx'
const verificationUrl = env.CORS_VERIFICATION || 'https://verify.kazamapp.mx'

function tokenUrl (url, token) {
  return url + '?token=' + token
}

module.exports = {
  /**
   * @method login
   * @description Returns a jwt if login successful
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  login: function (req, res, next) {
    req.models.User.login(req.params)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method signup
   * @description Returns a jwt if signup successful
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  signup: function (req, res, next) {
    req.models.User.signUp(req.params)
      .spread(function (user, token) {
        return Mailer.mailgunSend(user.email, 'welcome_user', {
          subject: req.__('welcome_user_subject'),
          content: tokenUrl(verificationUrl, token)
        }, req.log).then(function (result) {
          return user
        }).catch(function (err) {
          return res.mailerError(err, user)
        }).finally(function () {
          return user
        })
      })
      .then(function (user) {
        res.success(user)
      })
      .catch(req.ValidationError, res.validation)
      .catch(function (err) {
        res.badRequest(err.message)
      })
      .finally(next)
  },
  /**
   * @method check
   * @description Check if values are valid
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  check: function (req, res, next) {
    req.models.User.checkFields(req.params)
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method recovery
   * @description Send recovery email once
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  recovery: function (req, res, next) {
    req.models.User.recovery(req.params.email)
      .spread(function (user, token) {
        return Mailer.mailgunSend(user.email, 'recovery_user', {
          subject: req.__('recovery_subject'),
          content: tokenUrl(recoveryUrl, token)
        }, req.log).then(function (result) {
          return user
        })
      })
      .then(function () {
        res.success(true)
      })
      .catch(function (err) {
        req.log.error({ message: err.message, email: req.params.email })
        res.success(true)
      })
      .finally(next)
  },
  /**
   * @method recover
   * @description Recover a user via a token and set their password
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  recover: function (req, res, next) {
    req.models.User.recover(req.params)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method logout
   * @description Returns true if user logged out
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  logOut: function (req, res, next) {
    req.jwtUser.logout(req.jwtToken)
      .then(function () {
        res.success(true)
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method profile
   * @description Returns profile information
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  profile: function (req, res, next) {
    res.success(req.jwtUser.toShow())
    next()
  },
  /**
   * @method isVerified
   * @description Returns the users verified status
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  isVerified: function (req, res, next) {
    res.success(req.jwtUser.toVerified())
    next()
  },
  /**
   * @method update
   * @description Update a user via params
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  update: function (req, res, next) {
    req.jwtUser.updateFromData(req.params)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method email
   * @description Update a user email via params
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  email: function (req, res, next) {
    req.jwtUser.updateEmail(req.params)
      .spread(function (user, token) {
        return Mailer.mailgunSend(user.validateEmail, 'validate', {
          subject: req.__('validate_subject'),
          content: tokenUrl(validationUrl, token)
        }, req.log).then(function () {
          return user
        })
      })
      .then(function () {
        res.success(true)
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method validate
   * @description Validate a user email via a token
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  validate: function (req, res, next) {
    req.models.User.validate(req.params)
      .then(res.success)
      .catch(ExpiredError, res.gone)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method verification
   * @description Send a verification email to the current user
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  verification: function (req, res, next) {
    req.jwtUser.verification()
      .spread(function (user, token) {
        return Mailer.mailgunSend(user.email, 'verification', {
          subject: req.__('verification_subject'),
          content: tokenUrl(verificationUrl, token)
        }, req.log).then(function () {
          return { sent: true }
        }).catch(function (err) {
          return res.mailerError(err, { sent: false })
        })
      })
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method verify
   * @description Verify a user via a token
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  verify: function (req, res, next) {
    req.models.User.verify(req.params)
      .then(res.success)
      .catch(ExpiredError, res.gone)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method password
   * @description Update passwords
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  password: function (req, res, next) {
    const params = req.params
    req.jwtUser.updatePassword(
      params.old_password,
      params.password,
      params.password_confirmation
    ).then(function (user) {
      // TODO mailer template
      return Mailer.mailgunSend(user.email, 'password_changed', {
        subject: req.__('password_update_subject')
      }, req.log).then(function (result) {
        return user
      })
    }).then(function () {
      res.success(true)
    }).catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method profile
   * @description Returns permissions information
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  getPermissions: function (req, res, next) {
    res.success(req.jwtUser.toShowPermissions())
    next()
  },
  /**
   * @method permissions
   * @description Update notification permissions
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  permissions: function (req, res, next) {
    const params = req.params
    req.jwtUser.updatePermissions(params).then(function () {
      res.success(true)
    }).catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method history
   * @description Return the users recent history
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  history: function (req, res, next) {
    const User = req.models.User
    req.jwtUser.apiHistory(req.params)
      .then(User.rawDecorate(User.toApiHistory))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method player
   * @description Update player id from data
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  player: function (req, res, next) {
    req.jwtUser.updatePlayerFromData(req.params).then(function (result) {
      res.success(true)
    }).catch(res.badRequest)
      .finally(next)
  }
}
