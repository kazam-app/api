/**
 * Kazam Api
 * @name About Controller
 * @file controllers/api/about.js
 * @author Joel Cano
 */

'use strict'

const about = require('../../lib/about')

const version = {
  ios: process.env.IOS_VERSION || '1.0.0',
  android: process.env.ANDROID_VERSION || '1.0.0'
}

module.exports = {
  /**
   * @method version
   * @description Returns if the app should update
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  version: function (req, res, next) {
    let result = false
    const params = req.params
    const platform = params.platform || 'ios'
    if (params.version) result = params.version === version[platform]
    res.success({ update: !result })
    next()
  },
  /**
   * @method faq
   * @description Returns frequently asked question
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  faq: function (req, res, next) {
    res.success(about.faq)
    next()
  },
  /**
   * @method terms
   * @description Returns terms
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  terms: function (req, res, next) {
    res.success(about.terms)
    next()
  },
  /**
   * @method policy
   * @description Returns polciy
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  policy: function (req, res, next) {
    res.success(about.policy)
    next()
  }
}
