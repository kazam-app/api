/**
 * Kazam Api
 * @name Shop Controller
 * @file controllers/api/shop.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all shops with an optional distance order.
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.apiList(req.params.lat, req.params.lng)
      .then(Shop.decorate(Shop.toApiList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method near
   * @description Return all shops that are close
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  near: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.near(req.params.lat, req.params.lng)
      .then(Shop.decorate(Shop.toApiList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method merchant
   * @description Return all shops filtered by merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  merchant: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.merchant(req.params.merchant_id, req.params.lat, req.params.lng)
      .then(Shop.decorate(Shop.toApiMerchant))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
