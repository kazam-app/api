/**
 * Kazam Api
 * @name UserPunchCard API Controller
 * @file controllers/api/user-punch-card.js
 * @author Joel Cano
 */

'use strict'

const errors = require('../../lib/error')
const UnVerifiedError = errors.UnVerifiedError
const LimitReachedError = errors.LimitReachedError

module.exports = {
  /**
   * @method index
   * @description Returns all PunchCards for the api
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const UserPunchCard = req.models.UserPunchCard
    UserPunchCard.apiList(req.jwtUser)
      .then(UserPunchCard.addValidationDate)
      .then(UserPunchCard.decorate(UserPunchCard.toApiList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method ready
   * @description Returns all ready PunchCards for the api
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  ready: function (req, res, next) {
    const UserPunchCard = req.models.UserPunchCard
    req.models.UserPunchCard.ready(req.jwtUser)
      .then(UserPunchCard.decorate(UserPunchCard.toApiList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method merchant
   * @description Return all punchcards filtered by merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  merchant: function (req, res, next) {
    const UserPunchCard = req.models.UserPunchCard
    UserPunchCard.merchant(req.params.merchant_id, req.jwtUser)
      .then(UserPunchCard.addValidationDate)
      .then(UserPunchCard.decorate(UserPunchCard.toApiMerchant))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method punch
   * @description Add a punch via a validator
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  punch: function (req, res, next) {
    const params = req.params
    req.models.UserPunchCard.punch(params.id, params.merchant_id, params, req.jwtUser)
      .then(function (result) {
        res.success(result.toPunch())
      })
      .catch(LimitReachedError, res.tooMany)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method redeem
   * @description Redeem a punch card via a validator
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  redeem: function (req, res, next) {
    const params = req.params
    req.models.UserPunchCard.redeem(params.id, params.merchant_id, params, req.jwtUser)
      .then(function (result) {
        res.success(result.toRedeem())
      })
      .catch(UnVerifiedError, res.forbidden)
      .catch(res.badRequest)
      .finally(next)
  }
}
