/**
 * Kazam Api
 * @name Beacon API Controller
 * @file controllers/api/beacon.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method region
   * @description Returns current beacon regions
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  regions: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.regions()
      .then(Validator.decorate(Validator.toRegion))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method merchant
   * @description Returns all active beacons for merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  merchant: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.merchant(req.params.merchant_id, Validator.Identity.Beacon)
      .then(Validator.decorate(Validator.toRegion))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
