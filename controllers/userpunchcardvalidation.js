/**
 * Kazam Api
 * @name UserPunchCardValidation Controller
 * @file controllers/userpunchcardvalidation.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method breakdown
   * @description Return all punch and redeem breakdown for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  breakdown: function (req, res, next) {
    const UserPunchCardValidation = req.models.UserPunchCardValidation
    UserPunchCardValidation.breakdown(req.merchant, req.params.starts_at, req.params.ends_at)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
