/**
 * Kazam Api
 * @name Shop Controller
 * @file controllers/shop.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Returns all Shops related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param  {Function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.list(req.merchant)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Get a Shop via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.show(req.params.id, req.merchant)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a Shop via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.createFromData(req.params, req.merchant)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update Shop via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.updateFromData(req.params.id, req.params, req.merchant)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Delete Shop via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.delete(req.params.id, req.merchant)
      .then(function (result) {
        res.success(result)
      })
      .catch(res.badRequest)
      .finally(next)
  }
}
