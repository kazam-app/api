/**
 * Kazam Api
 * @name PunchCard Controller
 * @file controllers/punchcard.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all PunchCards related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param  {Function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.list(req.merchant)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method active
   * @description Returns an active PunchCard related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param  {Function} next callback
   *
   * @static
   */
  active: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.active(req.merchant)
      .then(function (result) {
        res.success(result || {})
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method latest
   * @description Returns an latest PunchCard related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  latest: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.latest(req.merchant)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Returns an active PunchCard related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.show(req.params.id, req.merchant)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a PunchCard via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.createFromData(req.params, req.merchant)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Update a PunchCard via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.updateFromData(req.params.id, req.params, req.merchant)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method publish
   * @description Publishes a PunchCard related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  publish: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.publish(req.params.id, req.merchant)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Delete PunchCard via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.delete(req.params.id, req.merchant)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
