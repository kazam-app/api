/**
 * Kazam Api
 * @name QR Controller
 * @file controllers/qr.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all Beacons related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param  {Function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.list(Validator.Identity.QR, req.merchant)
      .then(Validator.decorate(Validator.toQr))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method toggle
   * @description Toggle beacon isActive
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param  {Function} next callback
   *
   * @static
   */
  toggle: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.toggle(req.params.id, Validator.Identity.QR, req.merchant)
      .then(function (result) {
        res.success(result.toQr())
      })
      .catch(res.badRequest)
      .finally(next)
  }
}
