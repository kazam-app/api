/**
 * Kazam Api
 * @name History Controller
 * @file controllers/history.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Restusn all validation history for a merchants
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const UserPunchCardValidation = req.models.UserPunchCardValidation
    UserPunchCardValidation.history(req.merchant, req.urbanity, req.params)
      .then(
        UserPunchCardValidation.rawDecorator(UserPunchCardValidation.toHistory)
      ).then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
