/**
 * Kazam Api
 * @name Analytics Controller
 * @file controllers/analytics.js
 * @author Joel Cano
 */

'use strict'

const Promise = require('bluebird')

module.exports = {
  /**
   * @method index
   * @description Returns analytics data for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const merchant = req.merchant
    const models = req.models
    const User = models.User
    const Shop = models.Shop
    const UserPunchCard = models.UserPunchCard
    const UserPunchCardValidation = models.UserPunchCardValidation
    const UserPunchCardInvite = models.UserPunchCardInvite
    Promise.join(
      User.activeForMerchant(merchant),
      UserPunchCard.activePunchCountForMerchant(merchant),
      UserPunchCard.readyPunchCountForMerchant(merchant),
      UserPunchCardValidation.redeemsForMerchant(merchant),
      UserPunchCard.outstandingByGender(merchant),
      Shop.topShops(merchant),
      UserPunchCardInvite.countByMonthForMerchant(merchant),
      User.activeForMerchantByMonth(merchant),
      UserPunchCardValidation.activeForMerchantPerMonth(merchant),
      function (active, activePunches, ready, redeems, outstanding, shops, invites, actives, validations) {
        return {
          active: parseInt(active[0].total),
          punches: parseInt(activePunches[0].total),
          ready: parseInt(ready[0].total),
          redeems: parseInt(redeems[0].total),
          outstanding: outstanding,
          shops: shops,
          invites: invites,
          actives: actives,
          validations: validations
        }
      })
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
