/**
 * Kazam Api
 * @name Analytics Admin Controller
 * @file controllers/admin/analytics.js
 * @author Joel Cano
 */

'use strict'

const analytics = require('../analytics')

module.exports = {
  /**
   * @method index
   * @description Returns analytics data for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: analytics.index
}
