/**
 * Kazam Api
 * @name User Admin Controller
 * @file controllers/admin/user.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all Users
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const User = req.models.User
    User.adminList()
      .then(User.decorate(User.toAdminList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Return a User via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const User = req.models.User
    User.adminShow(req.params.id)
      .then(User.decorate(User.toAdmin))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method merchant
   * @description Return a User information via id filtered for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  merchant: function (req, res, next) {
    const User = req.models.User
    User.adminShow(req.params.id)
      .then(function (user) {
        return user.toAdminMerchant(req.merchant)
      })
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
