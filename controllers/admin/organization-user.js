/**
 * Kazam Api
 * @name OrganizationUser Admin Controller
 * @file controllers/admin/organization-user.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method login
   * @description Returns a jwt if login successful
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  login: function (req, res, next) {
    const data = req.params
    req.models.OrganizationUser.adminLogin(data.email, data.password)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method logOut Method
   * @description Returns true if user logged out
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  logOut: function (req, res, next) {
    req.jwtUser.logout().then(function () {
      res.success(true)
    }).catch(res.badRequest)
      .finally(next)
  }
}
