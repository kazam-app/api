/**
 * Kazam Api
 * @name MailingList Admin Controller
 * @file controllers/admin/mailinglist.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description return all mailing lists
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const MailingList = req.models.MailingList
    MailingList.adminList()
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a MailingList via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const MailingList = req.models.MailingList
    MailingList.createFromData(req.params)
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  }
}
