/**
 * Kazam Api
 * @name OrganizationUserMerchant Admin Controller
 * @file controllers/admin/organizationuser.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description return all OrganizationUserMerchants for a Merchant
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  index: function (req, res, next) {
    const OrganizationUserMerchant = req.models.OrganizationUserMerchant
    OrganizationUserMerchant.adminList(req.params.merchant_id)
      .then(OrganizationUserMerchant.decorate(OrganizationUserMerchant.toAdminList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create an OrganizationUserMerchant via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const OrganizationUserMerchant = req.models.OrganizationUserMerchant
    OrganizationUserMerchant.adminCreateFromData(req.params)
      .then(function (result) {
        res.success(result.toAdmin())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  }
}
