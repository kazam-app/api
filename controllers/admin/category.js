/**
 * Kazam Api
 * @name Category Controller
 * @file controllers/admin/category.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all categories
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Category = req.models.Category
    Category.list()
      .then(Category.decorate(Category.toList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Returns a Category via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Category = req.models.Category
    Category.show(req.params.id)
      .then(Category.decorate(Category.toShow))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a Category via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Category = req.models.Category
    Category.createFromData(req.params)
      .then(Category.decorate(Category.toShow))
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update Category via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Category = req.models.Category
    Category.updateFromData(req.params.id, req.params)
      .then(Category.decorate(Category.toShow))
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Remove a Category via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const Category = req.models.Category
    Category.delete(req.params.id)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
