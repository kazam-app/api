/**
 * Kazam Api
 * @name Shop Admin Controller
 * @file shop.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all shops for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.adminList(req.merchant)
      .then(Shop.decorate(Shop.toAdminList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Returns a Shop via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.adminShow(req.params.id, req.merchant)
      .then(function (result) {
        res.success(result.toAdminShow())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a new shop for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.adminCreateFromData(req.params, req.merchant)
      .then(function (result) {
        res.success(result.toAdminShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update a shop for a merchant via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.adminUpdateFromData(req.params.id, req.params, req.merchant)
      .then(function (result) {
        res.success(result.toAdminShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Delete a shop for a merchant via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const Shop = req.models.Shop
    Shop.adminDelete(req.params.id, req.merchant)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
