/**
 * Kazam Api
 * @name History Admin Controller
 * @file controllers/admin/history.js
 * @author Joel Cano
 */

'use strict'

const history = require('../history')

module.exports = {
  /**
   * @method index
   * @description Returns all validation history data for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: history.index
}
