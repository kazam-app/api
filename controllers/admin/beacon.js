/**
 * Kazam Api
 * @name Beacon Controller
 * @file controllers/admin/beacon.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all beacons
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.list(Validator.Identity.Beacon)
      .then(Validator.decorate(Validator.toAdminBeacon))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Returns a Beacon via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.show(req.params.id, Validator.Identity.Beacon)
      .then(function (result) {
        res.success(result.toAdminBeacon())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a Beacon via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.createBeacon(req.params)
      .then(function (result) {
        res.created(result.toAdminBeacon())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update Beacon via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.updateBeacon(req.params.id, req.params)
      .then(function (result) {
        res.success(result.toAdminBeacon())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Remove a Beacon via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.deleteBeacon(req.params.id)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
