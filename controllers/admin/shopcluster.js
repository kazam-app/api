/**
 * Kazam Api
 * @name Shop Cluster Admin Controller
 * @file controllers/admin/shopcluster.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all shop clusters
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const ShopCluster = req.models.ShopCluster
    ShopCluster.list()
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Return a shop cluster
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const ShopCluster = req.models.ShopCluster
    ShopCluster.show(req.params.id)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create shop cluster via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const ShopCluster = req.models.ShopCluster
    ShopCluster.createFromData(req.params)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update shop cluster via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const ShopCluster = req.models.ShopCluster
    ShopCluster.updateFromData(req.params.id, req.params)
      .then(function (result) {
        res.success(result.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Remove a shop cluster via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const ShopCluster = req.models.ShopCluster
    ShopCluster.delete(req.params.id)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
