/**
 * Kazam Api
 * @name Suggestion Admin Controller
 * @file controllers/admin/suggestion.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all Suggestions
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Suggestion = req.models.Suggestion
    Suggestion.adminList()
      .then(Suggestion.decorate(Suggestion.toAdmin))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
