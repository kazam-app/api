/**
 * Kazam Api
 * @name Organiztion Controller
 * @file controllers/admin/organiztion.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all organizations
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Organization = req.models.Organization
    Organization.list()
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Return a organiztion via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Organization = req.models.Organization
    Organization.show(req.params.id)
      .then(function (result) {
        res.success(result.toAdmin())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a merchant via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Organization = req.models.Organization
    Organization.createFromData(req.params)
      .then(function (result) {
        res.success(result.toAdmin())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update merchant via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Organization = req.models.Organization
    Organization.updateFromData(req.params.id, req.params)
      .then(function (result) {
        res.success(result.toAdmin())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  }
}
