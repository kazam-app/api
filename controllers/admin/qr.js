/**
 * Kazam Api
 * @name Qr Admin Controller
 * @file controllers/admin/qr.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all qrs
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.list(Validator.Identity.QR)
      .then(Validator.decorate(Validator.toAdminQr))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Returns a QR via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.show(req.params.id, Validator.Identity.QR)
      .then(function (result) {
        res.success(result.toAdminQr())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a QR via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.createQr(req.params)
      .then(function (result) {
        res.success(result.toAdminQr())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update QR via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.updateQr(req.params.id, req.params)
      .then(function (result) {
        res.success(result.toAdminQr())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Remove a QR via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const Validator = req.models.Validator
    Validator.deleteQr(req.params.id)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
