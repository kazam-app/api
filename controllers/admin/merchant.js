/**
 * Kazam Api
 * @name Merchant Admin Controller
 * @file controllers/admin/merchant.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description return all merchants
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.adminList()
      .then(Merchant.decorate(Merchant.toAdminList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Return a merchant via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.adminShow(req.params.id)
      .then(Merchant.decorate(Merchant.toAdmin))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a merchant via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.adminCreateFromData(req.params)
      .then(Merchant.decorate(Merchant.toAdmin))
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update merchant via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.adminUpdateFromData(req.params.id, req.params)
      .then(Merchant.decorate(Merchant.toAdmin))
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Remove a merchant via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.delete(req.params.id)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
