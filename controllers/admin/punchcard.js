/**
 * Kazam Api
 * @name PunchCard Admin Controller
 * @file controllers/admin/punchcard.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all punch cards
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.adminList()
      .then(PunchCard.decorate(PunchCard.toAdminList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Returns a PunchCard via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.adminShow(req.params.id)
      .then(function (result) {
        res.success(result.toAdminShow())
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a PunchCard via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.adminCreateFromData(req.params)
      .then(function (result) {
        res.success(result.toAdminShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update PunchCard via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.adminUpdateFromData(req.params.id, req.params)
      .then(function (result) {
        res.success(result.toAdminShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method publish
   * @description Publishes a PunchCard related to a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {Function} next callback
   *
   * @static
   */
  publish: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.adminPublish(req.params.id)
      .then(function (result) {
        res.success(result.toAdminShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Remove a PunchCard via id
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  delete: function (req, res, next) {
    const PunchCard = req.models.PunchCard
    PunchCard.adminDelete(req.params.id)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
