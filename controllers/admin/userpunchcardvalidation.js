/**
 * Kazam Api
 * @name UserPunchCardValidation Admin Controller
 * @file controllers/admin/userpunchcardvalidation.js
 * @author Joel Cano
 */

'use strict'

const userpunchcardvalidation = require('../userpunchcardvalidation')

module.exports = {
  /**
   * @method breakdown
   * @description Return all punch and redeem breakdown for a merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  breakdown: userpunchcardvalidation.breakdown
}
