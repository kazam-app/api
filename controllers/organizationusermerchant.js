/**
 * Kazam Api
 * @name Organization User Merchant Controller
 * @file controller/organizationusermerchant.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method create
   * @description Creates a new OrganizationUserMerchant via params
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  create: function (req, res, next) {
    const params = req.params
    const OrganizationUserMerchant = req.models.OrganizationUserMerchant
    OrganizationUserMerchant.createFromData(
      params.id, // url component
      params, // request parameters
      req.jwtUser, // auth user
      req.merchant // auth user merchant
    ).then(function (result) {
      res.success(result.toShow())
    }).catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  }
}
