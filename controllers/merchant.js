/**
 * Kazam Api
 * @name Merchant Controller
 * @file controllers/merchant.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description return all merchants for a user
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.list(req.jwtUser)
      .then(Merchant.decorate(Merchant.toList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method show
   * @description Return the current merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  show: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.show(req.params.id, req.jwtUser)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method create
   * @description Create a merchant via params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  create: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.createFromData(req.params, req.jwtUser)
      .then(Merchant.decorate(Merchant.toView))
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method copy
   * @description Reuse banking information via ids
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  copy: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.copy(req.params.id, req.merchant, req.jwtUser)
      .then(Merchant.decorate(Merchant.toView))
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update merchant via id and params
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  update: function (req, res, next) {
    const Merchant = req.models.Merchant
    Merchant.updateFromData(req.params.id, req.params, req.jwtUser)
      .then(Merchant.decorate(Merchant.toView))
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  }
}
