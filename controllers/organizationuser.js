/**
 * Kazam Api
 * @name OrganizationUser Controller
 * @file controllers/organizationuser.js
 * @author Joel Cano
 */

'use strict'

const Mailer = require('../mailer')
const errors = require('../lib/error')
const ExpiredError = errors.ExpiredError
// template urls
const inviteUrl = process.env.CORS_INVITE || 'https://merchants.kazamapp.mx/invite'
const recoveryUrl = process.env.CORS_RECOVER || 'https://merchants.kazamapp.mx/auth/recover'
const validationUrl = process.env.CORS_MERCHANT_VALIDATION || 'https://merchants.kazamapp.mx/auth/validate'

function tokenUrl (url, token) {
  return url + '/' + token
}

function tokenQuery (url, token) {
  return url + '?token=' + token
}

function sendWelcomeEmail (req, email, url, token) {
  return Mailer.mailgunSend(email, 'welcome', {
    subject: req.__('welcome_subject'),
    content: tokenUrl(url, token)
  }, req.log)
}

module.exports = {
  /**
   * @method login
   * @description Returns a jwt if login successful
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  login: function (req, res, next) {
    const data = req.params
    const OrganizationUser = req.models.OrganizationUser
    OrganizationUser.login(data.email, data.password)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method signup
   * @description Returns a jwt if signup successful
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  signup: function (req, res, next) {
    const OrganizationUser = req.models.OrganizationUser
    OrganizationUser.signUp(req.params)
      .spread(function (user, validationToken, signUpToken) {
        return sendWelcomeEmail(req, user.email, validationUrl, validationToken)
          .then(function () {
            return signUpToken
          })
      })
      .then(function (token) {
        res.success({ token: token })
      })
      .catch(req.ValidationError, res.validation)
      .catch(function (err) {
        res.badRequest(err.message)
      })
      .finally(next)
  },
  /**
   * @method resend
   * @description Resend the validation token
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  resend: function (req, res, next) {
    req.models.OrganizationUser.resend(req.params)
      .spread(function (user, token) {
        return sendWelcomeEmail(req, user.email, validationUrl, token)
          .then(function () {
            return user
          })
      })
      .then(function () {
        res.success(true)
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method refresh
   * @description Refresh the validation token
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  refresh: function (req, res, next) {
    req.models.OrganizationUser.refresh(req.params)
      .spread(function (user, token) {
        return sendWelcomeEmail(req, user.email, validationUrl, token)
          .then(function () {
            return user
          })
      })
      .then(function () {
        res.success(true)
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method validate
   * @description Validate a user via a token and set their password
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  validate: function (req, res, next) {
    req.models.OrganizationUser.validate(req.params)
      .then(res.success)
      .catch(ExpiredError, res.gone)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method permissions Method
   * @description Returns a jwt if with resources array
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  permissions: function (req, res, next) {
    req.jwtUser.getPermissions(req.params.relation)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method logOut Method
   * @description Returns true if user logged out
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  logOut: function (req, res, next) {
    req.jwtUser.logout()
      .then(function () {
        res.success(true)
      })
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method request
   * @description Request a new password
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  request: function (req, res, next) {
    req.models.OrganizationUser.request(req.params.email)
      .spread(function (user, token) {
        return Mailer.mailgunSend(user.email, 'recovery', {
          subject: req.__('recovery_subject'),
          content: tokenQuery(recoveryUrl, token)
        }, req.log).then(function () {
          return user
        })
      })
      .then(function () {
        res.success(true)
      })
      .catch(function () {
        res.success(true)
      })
      .finally(next)
  },
  /**
   * @method recover
   * @description Recover a user via a token and set their password
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  recover: function (req, res, next) {
    req.models.OrganizationUser.recover(req.params)
      .then(res.success)
      .catch(ExpiredError, res.gone)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method delete
   * @description Remove a user via id
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  delete: function (req, res, next) {
    req.models.OrganizationUser.delete(req.params.id, req.jwtUser)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method invite
   * @description Invite another email to join organization
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  invite: function (req, res, next) {
    req.jwtUser.invite(req.params.email)
      .spread(function (invite, created) {
        if (created) {
          return Mailer.mailgunSend(invite.email, 'invite', {
            subject: req.__('invite_subject'),
            content: inviteUrl + '?token=' + invite.jwt()
          }, req.log).then(function () {
            return true
          })
        } else return false
      })
      .then(res.success)
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method profile
   * @description View the current user
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  profile: function (req, res, next) {
    req.jwtUser.toProfile(req.organization)
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method update
   * @description Update user via and params
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  update: function (req, res, next) {
    req.jwtUser.updateFromData(req.params)
      .then(function (user) {
        res.success(user.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  },
  /**
   * @method password
   * @description Update User's password
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  password: function (req, res, next) {
    req.jwtUser.updatePassword(req.params)
      .then(function (user) {
        res.success(user.toShow())
      })
      .catch(req.ValidationError, res.validation)
      .catch(res.badRequest)
      .finally(next)
  }
}
