/**
 * Kazam Api
 * @name Category Controller
 * @file controllers/category.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method index
   * @description Return all categories
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  index: function (req, res, next) {
    const Category = req.models.Category
    Category.list()
      .then(Category.decorate(Category.toList))
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
