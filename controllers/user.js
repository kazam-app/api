/**
 * Kazam Api
 * @name User Controller
 * @file controllers/user.js
 * @author Joel Cano
 */

'use strict'

module.exports = {
  /**
   * @method merchant
   * @description Return a User information via id filtered for the current
   *  merchant
   * @param {object} req - Restify request instance
   * @param {object} res - Restify response instance
   * @param {function} next callback
   *
   * @static
   */
  merchant: function (req, res, next) {
    const User = req.models.User
    User.show(req.params.id)
      .then(function (user) {
        return user.toMerchant(req.merchant)
      })
      .then(res.success)
      .catch(res.badRequest)
      .finally(next)
  }
}
