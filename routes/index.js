/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/index.js
 * @author Joel Cano
 */

'use strict'

// const dashboard = function (path) {
//   return '/dash' + path
// }

const admin = function (path) {
  return '/admin' + path
}

const v1 = function (path) {
  return '/v1' + path
}

module.exports = function (server) {
  // API Routes
  require('./api')(server, v1)
  // OrganizationUser Routes
  require('./organization')(server)
  // Admin Routes
  require('./admin')(server, admin)
}
