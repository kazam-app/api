/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file controllers/category.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

module.exports = function (server, prefix) {
  // Category
  const category = require('../../controllers/category')
  /**
   * @api {get} /categories List
   * @apiName List
   * @apiGroup Category
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         Category id
   * @apiSuccess {String} name        Category name
   * @apiSuccess {String} image_url   Category imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Category',
   *     image_url: 'test.jpg'
   *   }]
   */
  server.get(prefix('/categories'), jwtAuth, category.index)
}
