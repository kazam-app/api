/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/organization/userpunchcardvalidation.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  // UserPunchCardValidation
  const userpunchcardvalidation = require('../../controllers/userpunchcardvalidation')
  /**
   * @api {get} /breakdown Breakdown
   * @apiName Breakdown
   * @apiGroup UserPunchCardValidation
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {object} shops             Breakdown shops
   * @apiSuccess {integer} intervals        Breakdown intervals
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     shops: [{
   *        id: '2150',
   *        name: 'Test Shop',
   *        breakdown: [[{ total: '1', identity: 0, id: '2152', name: 'Test Shop' }], null, null, null],
   *        punch_total: 1,
   *        redeem_total: 0
   *     }],
   *     intervals: 4
   *   }]
   */
  server.get(prefix('/breakdown'), jwtAuth, orgAuth.read, userpunchcardvalidation.breakdown)
}
