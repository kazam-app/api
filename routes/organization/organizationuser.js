/**
 * Merchants Api
 * @name Restify Routing Logic
 * @file organizationuser.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  /**
   * OrganizationUser with Token
   * @apiDefine OrganizationUserJWT
   * @apiSuccess {String} id          OrganizationUser id
   * @apiSuccess {String} name        OrganizationUser name
   * @apiSuccess {String} last_names  OrganizationUser last_names
   * @apiSuccess {String} email       OrganizationUser email
   * @apiSuccess {String} token       OrganizationUser jwt
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: '6af22c60-8858-4617-b372-03642f9d9be8@test.com',
   *     token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI0YTVlNDViN2JkOTFmYzVkZTFkNzU2MWQwNWM1MzA0MDU5NzZjNGIwNzZmMmI2YjM5ODJkYTMxN2VhZTU1M2E0IiwiZXhwIjoxNTI0NzU2Nzg2LCJpYXQiOjE0OTMyMjA3ODZ9._KY5vDcIZumq4Jf0OZrHpdstJmvP_C5gBcliTSrUhqA'
   *   }
   */
  /**
   * OrganizationUser
   * @apiDefine OrganizationUserSuccess
   * @apiSuccess {String} id                OrganizationUser id
   * @apiSuccess {String} name              OrganizationUser name
   * @apiSuccess {String} last_names        OrganizationUser last_names
   * @apiSuccess {String} email             OrganizationUser email
   * @apiSuccess {String} organization      Organization name
   * @apiSuccess {Array} merchants          Merchant data
   * @apiSuccess {String} merchants.name    Merchant name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: '6af22c60-8858-4617-b372-03642f9d9be8@test.com',
   *     organization: 'Test Organization',
   *     merchants: [{ name: 'Test Merchant' }]
   *   }
   */
  // Organization User
  const organizationuser = require('../../controllers/organizationuser')
  /**
   * @api {post} /login Login
   * @apiName Login
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio regresa la información del usuario y un token si los datos son correctos.
   *
   * @apiParam {String} email      OrganizationUser email
   * @apiParam {String} password   OrganizationUser password
   *
   * @apiUse OrganizationUserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'login_fail'
   *   }
   */
  server.post('/login', organizationuser.login)
  /**
   * @api {post} /sign_up SignUp Invite
   * @apiName SignUpInvite
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio funciona para poder registrar a un usuario para entrar al dashboard de marcas
   *  utilizando un token de invitación.
   *
   * @apiParam {String} token                       OrganizationUserInvite token
   * @apiParam {Object} user                        OrganizationUser data
   * @apiParam {String} user.name                   OrganizationUser name
   * @apiParam {String} user.last_names             OrganizationUser last_names
   * @apiParam {String} user.password               OrganizationUser password
   * @apiParam {String} user.password_confirmation  OrganizationUser password_confirmation
   * @apiParam {Object} organization                Organization data
   * @apiParam {String} organization.name           Organization name
   *
   * @apiSuccess {boolean} success Signup Result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   { token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIwMTYwMTEwYWVjYmQ5OTk3NzIzNmM1YWI4NmVjMmNiYTc5MWY5MzU4NzlkNTI5ODVlNzVjMWYyNzZlMDlhMWYwIiwiZXhwIjoxNTI5NjE1Mzk5LCJpYXQiOjE0OTgwNzkzOTl9.fF2O0aZ7xRf-owQcRfwIoj31rFvswPNq_MJmHTnn5Us' }
   * @apiErrorExample {json} Missing User:
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   * @apiErrorExample {json} Missing Organization
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_organization_data'
   *   }
   */
  /**
   * @api {post} /sign_up SignUp
   * @apiName SignUp
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio funciona para poder registrar a un usuario para entrar al dashboard de marcas.
   *
   * @apiParam {Object} user                        OrganizationUser data
   * @apiParam {String} user.name                   OrganizationUser name
   * @apiParam {String} user.last_names             OrganizationUser last_names
   * @apiParam {String} user.email                  OrganizationUser email
   * @apiParam {String} user.password               OrganizationUser password
   * @apiParam {String} user.password_confirmation  OrganizationUser password_confirmation
   * @apiParam {Object} organization                Organization data
   * @apiParam {String} organization.name           Organization name
   *
   * @apiSuccess {boolean} success Signup Result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   { token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIwMTYwMTEwYWVjYmQ5OTk3NzIzNmM1YWI4NmVjMmNiYTc5MWY5MzU4NzlkNTI5ODVlNzVjMWYyNzZlMDlhMWYwIiwiZXhwIjoxNTI5NjE1Mzk5LCJpYXQiOjE0OTgwNzkzOTl9.fF2O0aZ7xRf-owQcRfwIoj31rFvswPNq_MJmHTnn5Us' }
   * @apiErrorExample {json} Missing User:
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   * @apiErrorExample {json} Missing Organization
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_organization_data'
   *   }
   */
  server.post('/sign_up', organizationuser.signup)
  /**
   * @api {post} /resend Resend
   * @apiName Resend
   * @apiGroup OrganizationUser
   *
   * @apiParam {String} token OrganizationUserToken jwt
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   */
  server.post('/resend', organizationuser.resend)
  /**
   * @api {post} /refresh Refresh
   * @apiName Refresh
   * @apiGroup OrganizationUser
   *
   * @apiParam {String} token OrganizationUserToken jwt
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   */
  server.post('/refresh', organizationuser.refresh)
  /**
   * @api {post} /validate Validate
   * @apiName Validate
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio es para poder validar el nuevo correo del usuario.
   *
   * @apiParam {String} token OrganizationUserToken jwt
   *
   * @apiUse OrganizationUserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'invalid_token'
   *   }
   */
  server.post('/validate', organizationuser.validate)
  /**
   * @api {post} /request Request
   * @apiName Request
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio es para iniciar la recuperación de la contraseña del usuario.
   *
   * @apiParam {String} email OrganizationUser data
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   */
  server.post('/request', organizationuser.request)
  /**
   * @api {put} /recover Recover
   * @apiName Recover
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio es para poder establecer una contraseña nueva como parte del flujo de
   *  recuperación de la contraseña del usuario.
   *
   * @apiParam {String} token                   OrganizationUser jwt
   * @apiParam {String} password                OrganizationUser password
   * @apiParam {String} password_confirmation   OrganizationUser password_confirmation
   *
   * @apiUse OrganizationUserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'invalid_token'
   *   }
   */
  server.put('/recover', organizationuser.recover)
  // Sessionful Routes
  /**
   * @api {get} /permissions/:relation Permissions
   * @apiName Permissions
   * @apiGroup OrganizationUser
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} relation Merchant id
   *
   * @apiUse OrganizationUserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'login_fail'
   *   }
   */
  server.get('/permissions/:relation', jwtAuth, organizationuser.permissions)
  /**
   * @api {delete} /log_out Log Out
   * @apiName Log Out
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio elimina los datos de sesión del usuario autenticado.
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'login_fail'
   *   }
   */
  server.del('/log_out', jwtAuth, organizationuser.logOut)
  /**
   * @api {get} /profile Profile
   * @apiName Profile
   * @apiGroup OrganizationUser
   * @apiDescription Este servicio regresa los datos del usuario autenticado.
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiUse OrganizationUserSuccess
   */
  server.get('/profile', jwtAuth, orgAuth.read, organizationuser.profile)
  /**
   * @api {put} /profile Update
   * @apiName Update
   * @apiGroup OrganizationUser
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Object} user                        OrganizationUser data
   * @apiParam {String} user.name                   OrganizationUser name
   * @apiParam {String} user.last_names             OrganizationUser last_names
   * @apiParam {String} user.email                  OrganizationUser email
   *
   * @apiUse OrganizationUserSuccess
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.put('/profile', jwtAuth, orgAuth.read, organizationuser.update)
  /**
   * @api {put} /profile/password UpdatePassword
   * @apiName UpdatePassword
   * @apiGroup OrganizationUser
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Object} user                        OrganizationUser data
   * @apiParam {String} user.password               OrganizationUser password
   * @apiParam {String} user.password_confirmation  OrganizationUser password_confirmation
   *
   * @apiUse OrganizationUserSuccess
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.put('/profile/password', jwtAuth, orgAuth.read, organizationuser.password)
  /**
   * @api {delete} /user/:id Remove user
   * @apiName Remove
   * @apiGroup OrganizationUser
   *
   * @apiUse JwtAuth
   *
   * @apiParam {integer} id   OrganizationUser id
   *
   * @apiSuccess {String} succcess Remove success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del('/users/:id', jwtAuth, orgAuth.readWrite, organizationuser.delete)
  /**
   * @api {post} /users/:email Invite
   * @apiName Invite
   * @apiGroup OrganizationUser
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} relation Merchant id
   *
   * @apiSuccess {String} succcess Invite success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'email_error'
   *   }
   */
  server.post('/users/:email', jwtAuth, orgAuth.readWrite, organizationuser.invite)
}
