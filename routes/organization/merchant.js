/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/organization/merchant.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  /**
   * @apiDefine MerchantSuccess
   * @apiSuccess {integer} id          Merchant id
   * @apiSuccess {String} name         Merchant name
   * @apiSuccess {String} image_url    Merchant imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Merchant',
   *     image_url: 'default.jpg'
   *   }
   */
  /**
   * @apiDefine MerchantParams
   * @apiParam {Object} merchant                   Merchant data
   * @apiParam {String} merchant.category_id       Merchant categoryId
   * @apiParam {String} merchant.name              Merchant name
   * @apiParam {String} merchant.color              Merchant color
   * @apiParam {String} merchant.image             Base64 image stream
   * @apiParam {String} merchant.background_image  Base64 image stream
   */
  /**
   * @apiDefine MerchantError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Missing User
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_merchant_data'
   *   }
   */
  const merchant = require('../../controllers/merchant')
  /**
   * @api {get} /merchants List
   * @apiName List
   * @apiGroup Merchant
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         Merchant id
   * @apiSuccess {String} name        Merchant name
   * @apiSuccess {String} image_url   Merchant imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Merchant',
   *     image_url: 'test.jpg'
   *   }]
   */
  server.get('/merchants', jwtAuth, merchant.index)
  /**
   * @api {get} /merchants/:id Show
   * @apiName Show
   * @apiGroup Merchant
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id    Merchant name
   *
   * @apiSuccess {Integer} id          Merchant id
   * @apiSuccess {String} name         Merchant name
   * @apiSuccess {Integer} category    Category id
   * @apiSuccess {String} category     Category name
   * @apiSuccess {String} image_url    Merchant imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Merchant',
   *     category_id: 1,
   *     category: 'Category name',
   *     image_url: 'default.jpg'
   *   }
   *
   * @apiUse MerchantError
   */
  server.get('/merchants/:id', jwtAuth, orgAuth.read, merchant.show)
  /**
   * @api {post} /merchants Create
   * @apiName Create
   * @apiGroup Merchant
   *
   * @apiUse JwtAuth
   *
   * @apiUse MerchantParams
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantError
   */
  server.post('/merchants', jwtAuth, merchant.create)
  /**
   * @api {put} /merchants/:id Edit
   * @apiName Edit
   * @apiGroup Merchant
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id  Merchant id
   * @apiUse MerchantParams
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantError
   */
  server.put('/merchants/:id', jwtAuth, merchant.update)
  /**
   * @api {put} /merchants/:id/copy Copy
   * @apiName Copy
   * @apiGroup Merchant
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id  Merchant id
   * @apiUse MerchantParams
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantError
   */
  server.put('/merchants/:id/copy', jwtAuth, orgAuth.readWrite, merchant.copy)
  // server.del('/merchants/:id', jwtAuth, orgAuth, merchant.delete)
}
