/**
 * Merchants Api
 * @name Restify Routing Logic
 * @file beacon.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  // Beacon
  const beacon = require('../../controllers/beacon')
  /**
   * @api {get} /beacons List
   * @apiName List
   * @apiGroup Beacon
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         Beacon id
   * @apiSuccess {String} name        Beacon name
   * @apiSuccess {String} isActive    Beacon isActive(Active: true, Paused: false)
   * @apiSuccess {String} shop        Shop name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'XYZ',
   *     is_active: false,
   *     shop: 'Test Shop'
   *   }]
   */
  server.get('/beacons', jwtAuth, orgAuth.read, beacon.index)
  /**
   * @api {put} /beacons/:id/toggle Toggle
   * @apiName Toggle
   * @apiGroup Beacon
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id             Beacon id
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Beacon not found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.put('/beacons/:id/toggle', jwtAuth, orgAuth.readWrite, beacon.toggle)
}
