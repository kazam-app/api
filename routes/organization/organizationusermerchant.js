/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/organization/organizationusermerchant.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  // OrganizationUserMerchant
  const organizationusermerchant = require('../../controllers/organizationusermerchant')
  /**
   * @api {post} /users/:id/merchant Create
   * @apiName Create
   * @apiGroup OrganizationUserMerchant
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {integer} id           OrganizationUser id
   * @apiParam {String} merchant_id   Merchant id
   * @apiParam {String} role          OrganizationUserMerchant role
   *
   * @apiSuccess {integer} id          OrganizationUserMerchant id
   * @apiSuccess {integer} role        OrganizationUserMerchant role
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '2',
   *     role: 1
   *   }
   * @apiErrorExample {json} Missing User
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_organization_user_merchant_data'
   *   }
   */
  server.post('/users/:id/merchant', jwtAuth, orgAuth.readWrite, organizationusermerchant.create)
}
