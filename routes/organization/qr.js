/**
 * Merchants Api
 * @name Restify Routing Logic
 * @file beacon.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  // QR
  const qr = require('../../controllers/qr')
  /**
   * @api {get} /qrs List
   * @apiName List
   * @apiGroup QR
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         QR id
   * @apiSuccess {String} name        QR name
   * @apiSuccess {String} isActive    QR isActive(Active: true, Paused: false)
   * @apiSuccess {String} shop        Shop name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'XYZ',
   *     is_active: false,
   *     shop: 'Test Shop'
   *   }]
   */
  server.get('/qrs', jwtAuth, orgAuth.read, qr.index)
  /**
   * @api {put} /qrs/:id/toggle Toggle
   * @apiName Toggle
   * @apiGroup QR
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id             QR id
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} QR not found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.put('/qrs/:id/toggle', jwtAuth, orgAuth.readWrite, qr.toggle)
}
