/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/organization/shop.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  /**
   * @apiDefine ShopSuccess
   * @apiSuccess {Integer} id           Shop id
   * @apiSuccess {String} name          Shop name
   * @apiSuccess {Boolean} is_active    Shop isActive
   * @apiSuccess {String} location      Shop location
   * @apiSuccess {String} postal_code   Shop postalCode
   * @apiSuccess {String} phone         Shop phone
   * @apiSuccess {String} city          Shop city
   * @apiSuccess {String} state         Shop state
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     merchant_id: '1',
   *     shop_cluster_id: '1',
   *     name: 'Test PunchCard',
   *     is_active: false,
   *     location: 'Test Prize',
   *     postal_code: '01219',
   *     phone: '551223446',
   *     city: 'CDMX',
   *     state: 'DF',
   *     country: 'MX'
   *   }
   */
  /**
   * @apiDefine ShopParams
   * @apiParam {Object} shop                      Shop data
   * @apiParam {Boolean} shop.is_active           Shop is_active
   * @apiParam {Integer} shop.shop_cluster_id     Shop shop_cluster_id
   * @apiParam {String} shop.name                 Shop name
   * @apiParam {String} shop.location             Shop location
   * @apiParam {String} shop.city                 Shop city
   * @apiParam {String} shop.postal_code          Shop postal_code
   * @apiParam {String} shop.phone                Shop phone
   * @apiParam {String} shop.state                Shop state
   * @apiParam {String} shop.country              Shop country
   */
  /**
   * @apiDefine ShopError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Missing Shop data
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_shop_data'
   *   }
   */
  /**
   * @apiDefine ShopNotFound
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Invalid Shop id
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  // Shop
  const shop = require('../../controllers/shop')
  /**
   * @api {get} /shops List
   * @apiName List
   * @apiGroup Shop
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {integer} id           Shop id
   * @apiSuccess {String} name          Shop name
   * @apiSuccess {Boolean} is_active    Shop isActive
   * @apiSuccess {String} location      Shop location
   * @apiSuccess {String} postal_code   Shop postalCode
   * @apiSuccess {String} phone         Shop phone
   * @apiSuccess {String} city          Shop city
   * @apiSuccess {String} state         Shop state
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     merchant_id: '1',
   *     shop_cluster_id: '1',
   *     name: 'Test PunchCard',
   *     is_active: false,
   *     location: 'Test Prize',
   *     postal_code: '01219',
   *     phone: '551223446',
   *     city: 'CDMX',
   *     state: 'DF',
   *     country: 'MX'
   *   }]
   */
  server.get('/shops', jwtAuth, orgAuth.read, shop.index)
  /**
   * @api {get} /shops/:id Show
   * @apiName Show
   * @apiGroup Shop
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      Shop id
   *
   * @apiUse ShopSuccess
   *
   * @apiUse ShopNotFound
   */
  server.get('/shops/:id', jwtAuth, orgAuth.read, shop.show)
  /**
   * @api {post} /shops Create
   * @apiName Create
   * @apiGroup Shop
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiUse ShopParams
   *
   * @apiUse ShopSuccess
   *
   * @apiUse ShopError
   */
  server.post('/shops', jwtAuth, orgAuth.readWrite, shop.create)
  /**
   * @api {put} /shops/:id Update
   * @apiName Update
   * @apiGroup Shop
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiUse ShopParams
   *
   * @apiUse ShopSuccess
   *
   * @apiUse ShopError
   */
  server.put('/shops/:id', jwtAuth, orgAuth.readWrite, shop.update)
  /**
   * @api {delete} /shops/:id Delete
   * @apiName Delete
   * @apiGroup Shop
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      Shop id
   *
   * @apiUse ShopSuccess
   *
   * @apiUse ShopNotFound
   */
  server.del('/shops/:id', jwtAuth, orgAuth.readWrite, shop.delete)
}
