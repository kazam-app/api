/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file controllers/analytics.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  // Analytics
  const analytics = require('../../controllers/analytics')
  /**
   * @api {get} /analytics Analytics
   * @apiName Analytics
   * @apiGroup Analytics
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} active                    Active Users
   * @apiSuccess {Integer} punches                   Total Punches
   * @apiSuccess {Integer} redeems                   Total Redeems
   * @apiSuccess {Object} outstanding                Outstaning punch cards by gender
   * @apiSuccess {Integer} outstanding.limit         PunchCard punchLimit
   * @apiSuccess {Object} outstanding.unspecified    Outstaning punches unspecified gender (1 - 15 punches)
   * @apiSuccess {Object} outstanding.female         Outstaning punches female (3 - 15 punches)
   * @apiSuccess {Object} outstanding.male           Outstaning punches male (3 - 15 punches)
   * @apiSuccess {Object} shops                      Top Shops lists
   * @apiSuccess {Array} shops.punches       Top shops by punches
   * @apiSuccess {Array} shops.redeems       Top shops by redeems
   * @apiSuccess {Array} invites             Total invites per month
   * @apiSuccess {Array} actives             Total active users per month
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     active: 2,
   *     punches: 7,
   *     redeems: 1,
   *     outstanding: {
   *        limit: 15
   *        unspecified: { '3': 7 },
   *        female: { '15': 90 },
   *        male: { '3': 8, '10': 6, '15': 44 }
   *     },
   *     shops: {
   *        punches: [{ id: '1', name: 'Test Shop', total: '1' }],
   *        redeems: [{ id: '1', name: 'Test Shop', total: '1' }]
   *     },
   *     invites: [{ month: '06', total: 1 }],
   *     actives: [{ month: '06', total: 1 }]
   *   }]
   */
  server.get(prefix('/analytics'), jwtAuth, orgAuth.read, analytics.index)
}
