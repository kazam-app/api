/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/organization/user.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  const user = require('../../controllers/user')
  /**
   * @api {get} /admin/users/:id Show
   * @apiName List
   * @apiGroup User
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id               User identifier
   * @apiParam {Integer} merchant_id     Merchant id
   *
   * @apiSuccess {String} id              User identifier
   * @apiSuccess {String} name            User name
   * @apiSuccess {String} last_names      User lastNames (First char)
   * @apiSuccess {String} email           User email
   * @apiSuccess {Boolean} is_verified    User isVerified
   * @apiSuccess {String} sign_up_type    User signUpType
   * @apiSuccess {String} sign_up_type    User signUpType
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '645a9172-d145-4bee-abe9-fa9cb9873fca',
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: 'e1be2edf-c633-4b6d-b3d7-9362167844b0@testKzm.com',
   *     is_verified: true,
   *     sign_up_type: 'email',
   *     gender: null,
   *     birthdate: null,
   *     created_at: '2018-03-07T00:51:47.801Z',
   *     merchant_total: 0,
   *     top_shops: [],
   *     history: [],
   *     weekly_average: 0,
   *     monthly_average: 0,
   *   }
   */
  server.get('/users/:id', jwtAuth, orgAuth.read, user.merchant)
}
