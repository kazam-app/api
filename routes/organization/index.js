/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/organization/index.js
 * @author Joel Cano
 */

'use strict'

module.exports = function (server, prefix) {
  /**
   * OrganizationUser API definitions
   */
  if (!prefix) prefix = function (path) { return path }
  /**
   * Public Definition
   * @apiDefine public Public
   *  Public access
   */
  /**
   * OrganizationUserMerchant.Role.Read Permission
   * @apiDefine Read Read
   *  OrganizationUsers with read access for that merchant
   */
  /**
   * OrganizationUserMerchant.Role.ReadWrite Permission
   * @apiDefine ReadWrite Read and Write
   *  OrganizationUsers with read and write access for that merchant
   */
  /**
   * Jwt Authentication
   * @apiDefine JwtAuth
   * @apiHeader {String} Authorization Bearer JWT
   * @apiHeaderExample {String} Authorization
   *   Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxYzI2MmViYjAxZWMwOTQzOWQ0MjJhZGE3NGIyYjc1NTM2MDJmMWRmODBlMjFkMWU5Y2RhMmNmM2Y0YjY3M2U2IiwiZXhwIjoxNTI0NzY1ODQxLCJyZWwiOiIxIiwiaWF0IjoxNDkzMjI5ODQxfQ.ujVPcwpEhMH_3f56bQO2DPxxkJboAnVcQmz1j6et5yw
   */
  // Organization User
  require('./organizationuser')(server, prefix)
  // OrganizationUserMerchant
  require('./organizationusermerchant')(server, prefix)
  // Merchant
  require('./merchant')(server, prefix)
  // Shop
  require('./shop')(server, prefix)
  // Beacon
  require('./beacon')(server, prefix)
  // Beacon
  require('./qr')(server, prefix)
  // Punchard
  require('./punchcard')(server, prefix)
  // Category
  require('./category')(server, prefix)
  // History
  require('./history')(server, prefix)
  // Analytics
  require('./analytics')(server, prefix)
  // UserPunchCardValidation
  require('./userpunchcardvalidation')(server, prefix)
  // User
  require('./user')(server, prefix)
}
