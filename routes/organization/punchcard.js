/**
 * Merchants Api
 * @name Restify Routing Logic
 * @file punchcard.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = jwt({ secret: jwtSecret })

const orgAuth = require('../../lib/filters/organization')

module.exports = function (server, prefix) {
  /**
   * @apiDefine PunchCardSuccess
   * @apiSuccess {Integer} id           PunchCard id
   * @apiSuccess {String} name          PunchCard name
   * @apiSuccess {String} prize         PunchCard prize
   * @apiSuccess {String} rules         PunchCard rules
   * @apiSuccess {String} terms         PunchCard terms
   * @apiSuccess {String} redeem_code   PunchCard redeemCode
   * @apiSuccess {Integer} punch_limit  PunchCard punchLimit
   * @apiSuccess {Date} expires_at      PunchCard expiresAt
   * @apiSuccess {Integer} validation_limit  PunchCard validationLimit
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test PunchCard',
   *     prize: 'Test Prize',
   *     rules: 'Test Rules',
   *     terms: 'Test Terms',
   *     redeem_code: 'TEST1',
   *     punch_limit: 3,
   *     expires_at: '2017-05-25T19:07:39.007Z',
   *     validation_limit: null
   *   }
   */
  /**
   * @apiDefine PunchCardParams
   * @apiParam {Object} punch_card                    PunchCard data
   * @apiParam {String} punch_card.name               PunchCard name*
   * @apiParam {String} punch_card.prize              PunchCard prize*
   * @apiParam {String} punch_card.rules              PunchCard rules*
   * @apiParam {String} punch_card.terms              PunchCard terms*
   * @apiParam {String} punch_card.redeem_code        PunchCard redeemCode*
   * @apiParam {Integer} punch_card.punch_limit       PunchCard punchLimit (3 - 15)*
   * @apiParam {Date} punch_card.expires_at           PunchCard expiresAt (ISO 8601 Date)*
   * @apiParam {Integer} punch_card.validation_limit  PunchCard validationLimit
   */
  /**
   * @apiDefine PunchCardError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Missing PunchCard data
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_punch_card_data'
   *   }
   */
  // PunchCard
  const punchcard = require('../../controllers/punchcard')
  /**
   * @api {get} /punch_cards List
   * @apiName List
   * @apiGroup PunchCard
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {integer} id           PunchCard id
   * @apiSuccess {String} name          PunchCard name
   * @apiSuccess {String} prize         PunchCard prize
   * @apiSuccess {String} rules         PunchCard rules
   * @apiSuccess {String} terms         PunchCard terms
   * @apiSuccess {Integer} punch_limit  PunchCard punchLimit
   * @apiSuccess {Date} expires_at      PunchCard expiresAt (ISO 8601 Date)
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test PunchCard',
   *     prize: 'Test Prize',
   *     rules: 'Test Rules',
   *     terms: 'Test Terms',
   *     punch_limit: 3,
   *     expires_at: '2017-05-25T19:07:39.007Z'
   *   }]
   */
  server.get('/punch_cards',
    jwtAuth,
    orgAuth.read,
    punchcard.index
  )
  /**
   * @api {get} /punch_cards/latest Latest
   * @apiName Latest
   * @apiGroup PunchCard
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {String} identity    Identity enum (UnPublished: 0, Published: 1)
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.get('/punch_cards/latest',
    jwtAuth,
    orgAuth.read,
    punchcard.latest
  )
  /**
   * @api {get} /punch_cards/active Active
   * @apiName Active
   * @apiGroup PunchCard
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.get('/punch_cards/active',
    jwtAuth,
    orgAuth.read,
    punchcard.active
  )
  /**
   * @api {get} /punch_cards/:id Show
   * @apiName Show
   * @apiGroup PunchCard
   * @apiPermission Read
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.get('/punch_cards/:id',
    jwtAuth,
    orgAuth.read,
    punchcard.show
  )
  /**
   * @api {post} /punch_cards Create
   * @apiName Create
   * @apiGroup PunchCard
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiUse PunchCardParams
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.post('/punch_cards',
    jwtAuth,
    orgAuth.readWrite,
    punchcard.create
  )
  /**
   * @api {put} /punch_cards/:id Update
   * @apiName Update
   * @apiGroup PunchCard
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   * @apiUse PunchCardParams
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.put('/punch_cards/:id',
    jwtAuth,
    orgAuth.readWrite,
    punchcard.update
  )
  /**
   * @api {put} /punch_cards/:id/publish Publish
   * @apiName Publish
   * @apiGroup PunchCard
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.put('/punch_cards/:id/publish',
    jwtAuth,
    orgAuth.readWrite,
    punchcard.publish
  )
  /**
   * @api {delete} /punch_cards/:id Delete
   * @apiName Delete
   * @apiGroup PunchCard
   * @apiPermission ReadWrite
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   *
   * @apiUse PunchCardError
   */
  server.del('/punch_cards/:id', jwtAuth, orgAuth.readWrite, punchcard.delete)
}
