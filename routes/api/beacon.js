/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/beacon.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').user
const jwtUserAuth = jwt({ secret: jwtSecret })

module.exports = function (server, prefix) {
  // Beacon
  const beacon = require('../../controllers/api/beacon')
  /**
   * @api {get} /v1/beacons/regions Regions
   * @apiName Regions
   * @apiGroup Beacon
   * @apiPermission User
   *
   * @apiSuccess {Array} regions              Region array
   * @apiSuccess {String} regions.uid         Beacon uid
   * @apiSuccess {String} regions.major       Beacon major(opcional)
   * @apiSuccess {String} regions.minor       Beacon minor(opcional)
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     uid: '2469ec70-508d-4e36-8d32-f7dd44bd2896',
   *     major: null,
   *     minor: null
   *   }]
   */
  server.get(prefix('/beacons/regions'), jwtUserAuth, beacon.regions)
  /**
   * @api {get} /v1/merchants/:merchant_id/beacons Merchant
   * @apiName Merchant
   * @apiGroup Beacon
   * @apiPermission User
   *
   * @apiSuccess {Array} beacons              Beacon array
   * @apiSuccess {String} beacon.uid         Beacon uid
   * @apiSuccess {String} beacon.major       Beacon major(opcional)
   * @apiSuccess {String} beacon.minor       Beacon minor(opcional)
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     uid: '2469ec70-508d-4e36-8d32-f7dd44bd2896',
   *     major: null,
   *     minor: null
   *   }]
   */
  server.get(prefix('/merchants/:merchant_id/beacons'), jwtUserAuth, beacon.merchant)
}
