/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/about.js
 * @author Joel Cano
 */

'use strict'

module.exports = function (server, prefix) {
  // About
  const about = require('../../controllers/api/about')
  /**
   * @api {get} /v1/version Version
   * @apiName Version
   * @apiGroup About
   *
   * @apiParam {String} version      Version ej: 1.0.0
   * @apiParam {String} platform     Version ej: ios | android
   *
   * @apiSuccess {Boolean} update          Should the app require update
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     update: false
   *   }
   */
  server.get(prefix('/version'), about.version)
  /**
   * @api {get} /v1/faq FAQ
   * @apiName FAQ
   * @apiGroup About
   *
   * @apiSuccess {Array} array               Text Paragraphs
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   ['Question 1', 'Question 2', 'Question 3']
   */
  server.get(prefix('/faq'), about.faq)
  /**
   * @api {get} /v1/terms Terms
   * @apiName Terms
   * @apiGroup About
   *
   * @apiSuccess {Array} array               Text Paragraphs
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   ['Paragraph', 'Paragraph 2', 'Paragraph 3']
   */
  server.get(prefix('/terms'), about.terms)
  /**
   * @api {get} /v1/policy Policy
   * @apiName Policy
   * @apiGroup About
   *
   * @apiSuccess {Array} array               Text Paragraphs
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   ['Paragraph', 'Paragraph 2', 'Paragraph 3']
   */
  server.get(prefix('/policy'), about.policy)
}
