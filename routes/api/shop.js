/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/shop.js
 * @author Joel Cano
 */

'use strict'

module.exports = function (server, prefix) {
  /**
   * @apiDefine ShopSuccess
   * @apiSuccess {Integer} id                       Shop id
   * @apiSuccess {String} name                      Shop name
   * @apiSuccess {Decimal} distance                 Shop distance(Kilometers)
   * @apiSuccess {String} category                  Category name
   * @apiSuccess {String} shop_cluster              ShopCluster name
   * @apiSuccess {Integer} merchant_id              Shop merchantId
   * @apiSuccess {String} merchant_name             Merchant name
   * @apiSuccess {String} merchant_color            Merchant color
   * @apiSuccess {String} merchant_image_url        Merchant imageUrl
   * @apiSuccess {Integer} merchant_budget_rating   Merchant budgetRating
   * @apiSuccess {Float} lat                        Shop latitude
   * @apiSuccess {Float} lng                        Shop longitud
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Shop',
   *     distance: '1.1',
   *     category: 'Test Category',
   *     shop_cluster: 'Test Cluster',
   *     merchant_id: '1',
   *     merchant_name: 'Test Merchant',
   *     merchant_color: null,
   *     merchant_image_url:
   *      'https://s3-us-west-2.amazonaws.com/merchant-api-dev/2469ec70-508d-4e36-8d32-f7dd44bd2896.jpg'
   *     merchant_budget_rating: 2,
   *     lat: 19.4283926,
   *     lng: -99.2161986
   *   }]
   */
  // Shop
  const shop = require('../../controllers/api/shop')
  /**
   * @api {get} /v1/shops List
   * @apiName List
   * @apiGroup Shop
   * @apiDescription Este servicio regresa un lista de tiendas.
   *  Opcionalmente se puede enviar un punto de coordenadas y los
   *  resultados vendran ordenados por distancia.
   *
   * @apiParam {Float} lat   Current latitude (optional)
   * @apiParam {Float} lng   Current longitud (optional)
   *
   * @apiUse ShopSuccess
   */
  server.get(prefix('/shops'), shop.index)
  /**
   * @api {get} /v1/shops/near Near
   * @apiName Near
   * @apiGroup Shop
   * @apiDescription Este servicio regresa un lista de tiendas.
   *  Opcionalmente se puede enviar un punto de coordenadas y los
   *  resultados vendran ordenados y filtrados por distancia.
   *
   * @apiParam {Float} lat   Current latitude (optional)
   * @apiParam {Float} lng   Current longitud (optional)
   *
   * @apiUse ShopSuccess
   */
  server.get(prefix('/shops/near'), shop.near)
  /**
   * @api {get} /v1/merchants/:merchant_id/shops Merchant
   * @apiName Merchant
   * @apiGroup Shop
   *
   * @apiParam {Integer} merchant_id   Merchant id*
   * @apiParam {Float} lat   Current latitude (optional)
   * @apiParam {Float} lng   Current longitud (optional)
   *
   * @apiSuccess {Integer} id                  Shop id
   * @apiSuccess {String} name                 Shop name
   * @apiSuccess {String} distance             Shop distance(Kilometers)
   * @apiSuccess {String} category             Category name
   * @apiSuccess {String} shop_cluster         ShopCluster name
   * @apiSuccess {String} merchant_name        Merchant name
   * @apiSuccess {Float} lat                   Shop latitude
   * @apiSuccess {Float} lng                   Shop longitud
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Shop',
   *     distance: '1.1',
   *     category: 'Test Category',
   *     shop_cluster: 'Test Cluster',
   *     merchant_name: 'Test Merchant',
   *     lat: 19.4283926,
   *     lng: -99.2161986
   *   }]
   */
  server.get(prefix('/merchants/:merchant_id/shops'), shop.merchant)
}
