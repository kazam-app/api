/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/suggestion.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').user
const jwtUserAuth = jwt({ secret: jwtSecret })

module.exports = function (server, prefix) {
  // Suggestion
  const suggestion = require('../../controllers/api/suggestion')
  /**
   * @api {post} /v1/suggestions Create
   * @apiName Create
   * @apiGroup Suggestion
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Object} suggestion                Suggestion data
   * @apiParam {String} suggestion.name           Suggestion name
   *
   * @apiSuccess {Integer} id                     Suggestion id
   * @apiSuccess {String} name                    Suggestion name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Suggestion'
   *   }
   */
  server.post(prefix('/suggestions'), jwtUserAuth, suggestion.create)
}
