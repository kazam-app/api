/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/user.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').user
const jwtUserAuth = jwt({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine UserJWT
   * @apiSuccess {Integer} id                           User id
   * @apiSuccess {String} identifier                    User identifier
   * @apiSuccess {Boolean} is_verified                  User isVerified
   * @apiSuccess {String} name                          User name
   * @apiSuccess {String} last_names                    User lastNames
   * @apiSuccess {String} email                         User email
   * @apiSuccess {Integer} gender
   *  Gender enum (Female: 0, Male: 1)
   * @apiSuccess {Date} birthdate
   *  User birthdate(ISO 8601 Date)
   * @apiSuccess {Date} created_at
   *  User created_at(ISO 8601 Date)
   * @apiSuccess {String} token                          User JWT
   * @apiSuccess {Boolean} has_expiry_notify_permission
   *  User hasExpiryNotifyPermission
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: 1,
   *     identifier: '6af22c60-8858-4617-b372-03642f9d9be8',
   *     is_verified: false,
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: '6af22c60-8858-4617-b372-03642f9d9be8@test.com',
   *     gender: null,
   *     birthdate: null,
   *     created_at: '2017-05-25T19:07:39.007Z',
   *     token:
   *      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI0YTVlNDViN2JkOTFmYzVkZTFkNzU2MWQwNWM1MzA0MDU5NzZjNGIwNzZmMmI2YjM5ODJkYTMxN2VhZTU1M2E0IiwiZXhwIjoxNTI0NzU2Nzg2LCJpYXQiOjE0OTMyMjA3ODZ9._KY5vDcIZumq4Jf0OZrHpdstJmvP_C5gBcliTSrUhqA',
   *     has_expiry_notify_permission: true
   *   }
   */
  // User
  const user = require('../../controllers/api/user')
  /**
   * @api {post} /v1/login Login
   * @apiName Login
   * @apiGroup User
   * @apiDescription Este servicio regresa un token valido si los datos son
   *  correctos.
   *
   * @apiParam {String} email          User email
   * @apiParam {String} password       User password
   * @apiParam {String} fb_id          Facebook id
   * @apiParam {String} fb_token       Facebook token
   * @apiParam {String} fb_expires_at  Facebook token expiration (ISO 8601 Date)
   *
   * @apiUse UserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'login_fail'
   *   }
   */
  server.post(prefix('/login'), user.login)
  /**
   * @api {post} /v1/validate Validate
   * @apiName Validate Email
   * @apiGroup User
   * @apiDescription Este servicio es para poder validar el nuevo correo del
   *  usuario.
   *
   * @apiParam {String} token User jwt
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'invalid_token'
   *   }
   */
  server.post(prefix('/validate'), user.validate)
  /**
   * @api {post} /v1/verification Verification
   * @apiName Verification
   * @apiGroup User
   * @apiPermission User
   * @apiDescription Este servicio es para poder enviar el correo para que los
   *  usuarios verifiquen su cuenta. Regresa un error si el usuario ya esta
   *  verificado.
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {boolean} sent        Email sent
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     sent: true
   *   }
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'user_verified'
   *   }
   */
  server.post(prefix('/verification'), jwtUserAuth, user.verification)
  /**
   * @api {post} /v1/verify Verify
   * @apiName Verify
   * @apiGroup User
   * @apiDescription Este servicio es para poder verificar la cuenta del usuario.
   *  Utilizando el token que normalmente se envia por correo.
   *
   * @apiParam {String} token User jwt
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'invalid_token'
   *   }
   */
  server.post(prefix('/verify'), user.verify)
  /**
   * @api {post} /v1/recovery Recovery
   * @apiName Recovery
   * @apiGroup User
   * @apiDescription Este servicio es para iniciar la recuperación de la
   *  contraseña del usuario.
   *
   * @apiParam {String} email User data
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   */
  server.post(prefix('/recovery'), user.recovery)
  /**
   * @api {post} /v1/recover Recover
   * @apiName Recover
   * @apiGroup User
   * @apiDescription Este servicio es para poder establecer una contraseña nueva
   *  como parte del flujo de recuperación de la contraseña del usuario.
   *
   * @apiParam {String} token                   User jwt
   * @apiParam {String} password                User password
   * @apiParam {String} password_confirmation   User password_confirmation
   *
   * @apiUse UserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'invalid_token'
   *   }
   */
  server.post(prefix('/recover'), user.recover)
  /**
   * @api {post} /v1/sign_up SignUp
   * @apiName SignUp
   * @apiGroup User
   *
   * @apiParam {String} name                    User name
   * @apiParam {String} last_names              User lastNames
   * @apiParam {String} email                   User email
   * @apiParam {String} password                User password
   * @apiParam {String} password_confirmation   Password confirmation
   * @apiParam {Integer} gender                 Gender enum (Female: 0, Male: 1)
   * @apiParam {Date} birthdate                 User birthdate(ISO 8601 Date)
   * @apiParam {String} invite_code             Merchant inviteCode
   * @apiParam {String} fb_id                   Facebook id
   * @apiParam {String} fb_token                Facebook token
   * @apiParam {Date}   fb_expires_at           Facebook token expiration (ISO 8601 Date)
   * @apiParam {String} player_id               OneSignal PlayerId (optional)
   *
   * @apiSuccess {Integer} id          User id
   * @apiSuccess {String} name        User name
   * @apiSuccess {String} last_names  User token
   * @apiSuccess {String} email       User email
   * @apiSuccess {Integer} gender     Gender enum (Female: 0, Male: 1)
   * @apiSuccess {Date} birthdate     User birthdate (ISO 8601 Date)
   * @apiSuccess {Date} created_at    User created_at (ISO 8601 Date)
   * @apiSuccess {String} token       User jwt
   * @apiSuccess {Object} merchant             Merchant data (Only with valid invite_code)
   * @apiSuccess {String} merchant.id          Merchant id
   * @apiSuccess {String} merchant.name        Merchant name
   * @apiSuccess {String} merchant.image_url   Merchant imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '876',
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: '6af22c60-8858-4617-b372-03642f9d9be8@test.com',
   *     gender: null,
   *     birthdate: null,
   *     created_at: '2017-05-25T19:07:39.007Z',
   *     token: '05b1ceca6020410988e18390b4be0890c9c37e962cf6a0fa2e0c9c476383db6c'
   *   }
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.post(prefix('/sign_up'), user.signup)
  /**
   * @api {post} /v1/check Check
   * @apiName Check
   * @apiGroup User
   * @apiDescription Este servicio permite validar los valores antes de
   *  registrarlos en la base de datos.
   *
   * @apiParam {String} name                    User name
   * @apiParam {String} last_names              User lastNames
   * @apiParam {String} email                   User email
   * @apiParam {String} password                User password
   * @apiParam {String} password_confirmation   Password confirmation
   * @apiParam {Integer} gender                 Gender enum (Female: 0, Male: 1)
   * @apiParam {Date} birthdate                 User birthdate(ISO 8601 Date)
   * @apiParam {String} fb_id                   Facebook id
   * @apiParam {String} fb_token                Facebook token
   * @apiParam {Date}   fb_expires_at           Facebook token expiration (ISO 8601 Date)
   *
   * @apiSuccess {Boolean} success      Validation result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.post(prefix('/check'), user.check)
  /**
   * @api {delete} /v1/log_out Log Out
   * @apiName LogOut
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Boolean} success  Logout result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.del(prefix('/log_out'), jwtUserAuth, user.logOut)
  /**
   * @api {get} /v1/profile Profile
   * @apiName Profile
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiUse UserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.get(prefix('/profile'), jwtUserAuth, user.profile)
  /**
   * @api {put} /v1/profile Update
   * @apiName Update
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} user                     User data
   * @apiParam {String} user.name                User name
   * @apiParam {String} user.last_names          User lastNames
   * @apiParam {Integer} user.gender             Gender enum
   *  (Female: 0, Male: 1)
   * @apiParam {Date} user.birthdate             User birthdate(ISO 8601 Date)
   *
   * @apiUse UserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.put(prefix('/profile'), jwtUserAuth, user.update)
  /**
   * @api {get} /v1/profile/verified isVerified
   * @apiName isVerified
   * @apiGroup User
   * @apiPermission User
   * @apiDescription Este servicio regresa si el usuario con sessión esta
   *  verificado.
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Boolean} is_verified  Verified status
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     is_verified: false
   *   }
   */
  server.get(prefix('/profile/verified'), jwtUserAuth, user.isVerified)
  /**
   * @api {put} /v1/profile/email Email
   * @apiName UpdateEmail
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Object} user                     User data
   * @apiParam {String} user.email               User email
   * @apiParam {String} user.password            User password
   *
   * @apiSuccess {Boolean} success  Email result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.put(prefix('/profile/email'), jwtUserAuth, user.email)
  /**
   * @api {put} /v1/profile/password Password
   * @apiName UpdatePassword
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} old_password                User old_password
   * @apiParam {String} password                    User password
   * @apiParam {String} password_confirmation       User password_confirmation
   *
   * @apiSuccess {Boolean} success  Password result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.put(prefix('/profile/password'), jwtUserAuth, user.password)
  /**
   * @api {get} /v1/profile/permissions GetPermissions
   * @apiName GetPermissions
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Boolean} has_expiry_notify_permission
   *  User.hasExpiryNotifyPermission
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     has_expiry_notify_permission: false
   *   }
   */
  server.get(prefix('/profile/permissions'), jwtUserAuth, user.getPermissions)
  /**
   * @api {put} /v1/profile/permissions Permissions
   * @apiName UpdatePermissions
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Object} user                     User data
   * @apiParam {Boolean} user.has_expiry_notify_permission   User
   *  has_expiry_notify_permission
   *
   * @apiSuccess {Boolean} success  Permission result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.put(prefix('/profile/permissions'), jwtUserAuth, user.permissions)
  /**
   * @api {get} /v1/profile/history History
   * @apiName History
   * @apiGroup User
   * @apiPermission User
   * @apiDescription Este serivcio regresa la lista de eventos mas recientes
   *  para un usuario. Regresa un arreglo de eventos. Los eventos pueden ser de
   *  dos tipos, de validación(sellos y canjes) o de tarjeta (expiración o
   *  completada). La información de la tienda es opcional debido a que solo se
   *  regresa en los eventos de validación.
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Array}                       Events array
   * @apiSuccess {String} id                    Event id
   * @apiSuccess {Date} event_at                Event event_at(ISO 8601 Date)
   * @apiSuccess {Integer} event_type           Event event_type
   *  (Validation: 0, Tarjeta: 1)
   * @apiSuccess {Object} validation            EventType Validation data
   *  (optional, only if event_type is 0)
   * @apiSuccess {Integer} validation.identity  EventType Validation identity
   *  (Punch: 0, Redeem: 1)
   * @apiSuccess {Object} card                  EventType Card data
   *  (optional, only if event_type is 1)
   * @apiSuccess {Integer} card.identity        EventType Card identity
   *  (Expiry: 0, Completed: 1)
   * @apiSuccess {Object} punch_card            Event PunchCard data
   * @apiSuccess {String} punch_card.id         Event PunchCard identifier
   * @apiSuccess {String} punch_card.identity   Event PunchCard identity
   *  (Active: 0, Completed: 1, Redeemed: 2, Expired: 3)
   * @apiSuccess {Object} merchant              Event Merchant data
   * @apiSuccess {String} merchant.name         Event Merchant name
   * @apiSuccess {String} merchant.image_url    Event Merchant imageUrl
   * @apiSuccess {Object} shop                  Event Shop data (optional)
   * @apiSuccess {String} shop.name             Event Shop name
   * @apiSuccessExample {json} Validation Success
   *   HTTP/1.1 200 OK
   *   [
   *     {
   *       id: 'c1947c9c-4244-495b-b846-2be79e3aa0dd',
   *       identity: 0,
   *       event_at: '2018-02-01T06:00:00.000Z',
   *       event_type: 0,
   *       validation: { identity: 0 },
   *       punch_card: {
   *         id: 'c3ba88df-061d-42b4-a793-b968a59aeb09',
   *         identity: 0
   *       },
   *       merchant: {
   *         name: 'Test Merchant',
   *         image_url:
   *          'https://s3-us-west-2.amazonaws.com/merchant-api-dev/test.jpg'
   *       },
   *       shop: { name: 'Test Shop' } }
   *     }
   *   ]
   * @apiSuccessExample {json} Card Success
   *   HTTP/1.1 200 OK
   *   [
   *     {
   *       id: 'c1947c9c-4244-495b-b846-2be79e3aa0dd',
   *       identity: 0,
   *       event_at: '2018-02-01T06:00:00.000Z',
   *       event_type: 1,
   *       card: { identity: 0 },
   *       punch_card: {
   *         id: 'c3ba88df-061d-42b4-a793-b968a59aeb09',
   *         identity: 0
   *       },
   *       merchant: {
   *         name: 'Test Merchant',
   *         image_url:
   *          'https://s3-us-west-2.amazonaws.com/merchant-api-dev/test.jpg'
   *       }
   *     }
   *   ]
   */
  server.get(prefix('/profile/history'), jwtUserAuth, user.history)
  /**
   * @api {put} /v1/profile/player Player
   * @apiName UpdatePlayer
   * @apiGroup User
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Object} user                User data
   * @apiParam {Boolean} user.player_id     User playerId
   *
   * @apiSuccess {Boolean} success  Update result
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_user_data'
   *   }
   */
  server.put(prefix('/profile/player'), jwtUserAuth, user.player)
}
