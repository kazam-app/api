/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/merchant.js
 * @author Joel Cano
 */

'use strict'

module.exports = function (server, prefix) {
  /**
   * @apiDefine MerchantListNearSuccess
   * @apiSuccess {Integer} id                     Merchant id
   * @apiSuccess {String} name                    Merchant name
   * @apiSuccess {String} image_url               Merchant imageUrl
   * @apiSuccess {String} background_image_url    Merchant backgroundImageUrl
   * @apiSuccess {Integer} budget_rating          Merchant budgetRating
   * @apiSuccess {String} color                   Merchant color(HEX ex: #000000)
   * @apiSuccess {String} category                Category name
   * @apiSuccess {Integer} shop_id                Shop id
   * @apiSuccess {String} shop_name               Shop name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Merchant',
   *     image_url: 'test.jpg',
   *     background_image_url: 'test.jpg',
   *     budget_rating: 0,
   *     category: 'Retail',
   *     shop_id: '1',
   *     shop_name: 'Test Shop'
   *   }]
   */
  /**
   * @apiDefine MerchantListSuccess
   * @apiSuccess {Integer} id                     Merchant id
   * @apiSuccess {String} name                    Merchant name
   * @apiSuccess {String} image_url               Merchant imageUrl
   * @apiSuccess {String} background_image_url    Merchant backgroundImageUrl
   * @apiSuccess {Integer} budget_rating          Merchant budgetRating
   * @apiSuccess {String} color                   Merchant color(HEX ex: #000000)
   * @apiSuccess {String} category                Category name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Merchant',
   *     image_url: 'test.jpg',
   *     background_image_url: 'test.jpg',
   *     budget_rating: 0,
   *     category: 'Retail'
   *   }]
   */
  /**
   * @apiDefine MerchantSuccess
   * @apiSuccess {Integer} id                   Merchant id
   * @apiSuccess {String} name                  Merchant name
   * @apiSuccess {String} image_url             Merchant imageUrl
   * @apiSuccess {String} background_image_url  Merchant backgroundImageUrl
   * @apiSuccess {String} color                 Merchant color(HEX ex: #000000)
   * @apiSuccess {String} category              Category name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Merchant',
   *     image_url: 'test.jpg',
   *     background_image_url: 'test.jpg',
   *     budget_rating: 0,
   *     color: '#000000',
   *     category: 'Retail'
   *   }
   */
  /**
   * @apiDefine MerchantNotFound
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} MerchantNotFound
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  // Merchant
  const merchant = require('../../controllers/api/merchant')
  /**
   * @api {get} /v1/merchants List
   * @apiName List
   * @apiGroup Merchant
   *
   * @apiUse MerchantListSuccess
   */
  server.get(prefix('/merchants'), merchant.index)
  /**
   * @api {get} /v1/merchants/beacon Beacon
   * @apiName Beacon
   * @apiGroup Merchant
   *
   * @apiParam {String} uid       Beacon uid
   * @apiParam {String} major     Beacon major
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse NotFoundError
   */
  server.get(prefix('/merchants/beacon'), merchant.beacon)
  /**
   * @api {get} /v1/merchants/qr/:token QR
   * @apiName QR
   * @apiGroup Merchant
   *
   * @apiParam {String} token       QR token
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantNotFound
   */
  server.get(prefix('/merchants/qr/:token'), merchant.qr)
  /**
   * @api {get} /v1/merchants/near Near
   * @apiName Near
   * @apiGroup Merchant
   * @apiDescription Este servicio regresa las marcas mas cercanas o un error si
   *  no se envía la ubicación.
   *
   * @apiParam {Float} lat   Current latitude
   * @apiParam {Float} lng   Current longitud
   *
   * @apiUse MerchantListNearSuccess
   *
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} MissingLocationData
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_location_data'
   *   }
   */
  server.get(prefix('/merchants/near'), merchant.near)
  /**
   * @api {get} /v1/merchants/:id Show
   * @apiName Show
   * @apiGroup Merchant
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantNotFound
   */
  server.get(prefix('/merchants/:id'), merchant.show)
}
