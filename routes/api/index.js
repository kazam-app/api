/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/index.js
 * @author Joel Cano
 */

'use strict'

module.exports = function (server, prefix) {
  /**
   * API definitions
   */
  if (!prefix) prefix = function (path) { return path }
  /**
   * API definitions
   */
  /**
   * User Access
   * @apiDefine User User
   *  User token access
   */
  /**
   * Jwt Authentication
   * @apiDefine JwtAuth
   * @apiHeader {String} Authorization Bearer JWT
   * @apiHeaderExample {String} Authorization
   *   Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxYzI2MmViYjAxZWMwOTQzOWQ0MjJhZGE3NGIyYjc1NTM2MDJmMWRmODBlMjFkMWU5Y2RhMmNmM2Y0YjY3M2U2IiwiZXhwIjoxNTI0NzY1ODQxLCJyZWwiOiIxIiwiaWF0IjoxNDkzMjI5ODQxfQ.ujVPcwpEhMH_3f56bQO2DPxxkJboAnVcQmz1j6et5yw
   */
  /**
   * @apiDefine NotFoundError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} NotFound
   *   HTTP/1.1 404 Not Found
   *   {
   *     code: 'NotFound',
   *     message: 'not_found'
   *   }
   */
  // User
  require('./user')(server, prefix)
  // Merchant
  require('./merchant')(server, prefix)
  // Shop
  require('./shop')(server, prefix)
  // UserPunchCard
  require('./user-punch-card')(server, prefix)
  // Beacon
  require('./beacon')(server, prefix)
  // Suggestion
  require('./suggestion')(server, prefix)
  // About
  require('./about')(server, prefix)
}
