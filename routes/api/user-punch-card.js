/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/api/user-punch-card.js
 * @author Joel Cano
 */

'use strict'

const jwt = require('restify-jwt-community')
const jwtSecret = require('../../lib/filters/jwt').user
const jwtUserAuth = jwt({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine PunchCardSuccess
   * @apiSuccess {Integer} id                    UserPunchCard id
   * @apiSuccess {Integer} identity              UserPunchCard identity
   *  (InProgress: 0, Ready: 1, Redeemed: 2)
   * @apiSuccess {Integer} punch_count           UserPunchCard punchCount
   * @apiSuccess {String} prize                  PunchCard prize
   * @apiSuccess {String} punch_limit            PunchCard punch_limit
   * @apiSuccess {Date} validation_date          PunchCard validationDate ISO
   *  8061(opcional, solo cuando se ha llegado al limite)
   * @apiSuccess {String} merchant_name          Merchant name
   * @apiSuccess {String} merchant_image_url     Merchant imageUrl
   * @apiSuccess {String} merchant_color         Merchant color
   * @apiSuccess {String} category               Category name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     identity: 0,
   *     prize: 'Test Prize',
   *     punch_count: 0,
   *     punch_limit: 3,
   *     category: 'Test Category',
   *     merchant_name: 'Test Merchant',
   *     merchant_image_url: 'test.jpg',
   *     merchant_color: '#AAAFFF'
   *   }]
   */
  /**
   * @apiDefine ValidationParams
   * @apiParam {Integer} id                    UserPunchCard id
   * @apiParam {Integer} merchant_id           Merchant id
   * @apiParam {String} uid                    Beacon uid
   * @apiParam {String} major                  Beacon major
   * @apiParam {String} minor                  Beacon minor
   * @apiParam {String} token                  QR token
   */
  /**
   * @apiDefine ValidatorError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} ValidatorError
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_validator_data'
   *   }
   */
  /**
   * @apiDefine ValidationLimitError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} ValidationLimitError
   *   HTTP/1.1 429 Too Many Requests
   *   {
   *     code: 'TooManyRequests',
   *     message: 'validation_limit'
   *   }
   */
  /**
   * @apiDefine NotFound
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} MerchantNotFound
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  // UserPunchCard
  const userPunchCard = require('../../controllers/api/user-punch-card')
  /**
   * @api {get} /v1/punch_cards List
   * @apiName List
   * @apiGroup PunchCard
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiUse PunchCardSuccess
   */
  server.get(prefix('/punch_cards'), jwtUserAuth, userPunchCard.index)
  /**
   * @api {get} /v1/punch_cards Ready
   * @apiName Ready
   * @apiGroup PunchCard
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id                    UserPunchCard id
   * @apiSuccess {Integer} identity              UserPunchCard identity
   *  (InProgress: 0, Ready: 1, Redeemed: 2)
   * @apiSuccess {Integer} punch_count           UserPunchCard punchCount
   * @apiSuccess {String} prize                  PunchCard prize
   * @apiSuccess {String} punch_limit            PunchCard punch_limit
   * @apiSuccess {String} merchant_name          Merchant name
   * @apiSuccess {String} merchant_image_url     Merchant imageUrl
   * @apiSuccess {String} merchant_color         Merchant color
   * @apiSuccess {String} category               Category name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     identity: 0,
   *     prize: 'Test Prize',
   *     punch_count: 0,
   *     punch_limit: 3,
   *     category: 'Test Category',
   *     merchant_name: 'Test Merchant',
   *     merchant_image_url: 'test.jpg',
   *     merchant_color: '#AAAFFF'
   *   }]
   */
  server.get(prefix('/punch_cards/ready'), jwtUserAuth, userPunchCard.ready)
  /**
   * @api {get} /v1/merchants/:merchant_id/punch_cards Merchant
   * @apiName Merchant
   * @apiGroup PunchCard
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} merchant_id   Merchant id
   *
   * @apiSuccess {Integer} id                    UserPunchCard id
   * @apiSuccess {Integer} identity              UserPunchCard identity
   *  (InProgress: 0, Ready: 1, Redeemed: 2)
   * @apiSuccess {Integer} punch_count           UserPunchCard punchCount
   * @apiSuccess {String} prize                  PunchCard prize
   * @apiSuccess {String} punch_limit            PunchCard punchLimit
   * @apiSuccess {Date} expires_at               PunchCard expiresAt (ISO 8061)
   * @apiSuccess {Date} validation_date          PunchCard validationDate
   *  ISO 8061(opcional, solo cuando se ha llegado al limite)
   * @apiSuccess {String} terms                  PunchCard terms
   * @apiSuccess {String} rules                  PunchCard rules
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     identity: 1,
   *     prize: 'Test Prize',
   *     punch_count: 0,
   *     punch_limit: 3,
   *     expires_at: '2017-08-03T17:19:40.425Z',
   *     terms: 'Terms',
   *     rules: 'Rules'
   *   }]
   */
  server.get(prefix('/merchants/:merchant_id/punch_cards'),
    jwtUserAuth,
    userPunchCard.merchant
  )
  /**
   * @api {post} /v1/merchants/:merchant_id/punch/:id Punch
   * @apiName Punch
   * @apiGroup PunchCard
   * @apiPermission User
   *
   * @apiUse JwtAuth
   *
   * @apiUse ValidationParams
   *
   * @apiSuccess {Integer} id            UserPunchCard id
   * @apiSuccess {Integer} identity      UserPunchCard identity
   *  (InProgress: 0, Ready: 1)
   * @apiSuccess {Integer} punch_count   UserPunchCard punchCount
   * @apiSuccess {Integer} punch_limit   PunchCard punchLimit
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     identity: 0,
   *     punch_count: 0,
   *     punch_limit: 10
   *   }
   *
   * @apiUse NotFound
   *
   * @apiUse ValidatorError
   * @apiUse ValidationLimitError
   */
  server.post(prefix('/merchants/:merchant_id/punch/:id'),
    jwtUserAuth,
    userPunchCard.punch
  )
  /**
   * @api {post} /v1/merchants/:merchant_id/redeem/:id Redeem
   * @apiName Redeem
   * @apiGroup PunchCard
   * @apiPermission User
   * @apiDescription Este servicio se usa para canjear la tarjeta de un comercio
   *  especifico. Se requiere saber el identificador del comercio, el
   *  identificador de la tarejta y los valores completos de un método de
   *  validación.
   *
   * @apiUse JwtAuth
   *
   * @apiUse ValidationParams
   *
   * @apiSuccess {Integer} id               UserPunchCard id
   * @apiSuccess {Integer} punch_count      UserPunchCard punchCount
   * @apiSuccess {String} prize             PunchCard prize
   * @apiSuccess {String} redeem_code       PunchCard redeemCode
   * @apiSuccess {Date} expires_at          PunchCard expiresAt (ISO 8061)
   * @apiSuccess {String} terms             PunchCard terms
   * @apiSuccess {String} rules             PunchCard rules
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     punch_count: 0,
   *     redeem_code: 'URBANPROMO10',
   *     prize: 'Test Prize',
   *     expires_at: '2017-08-03T17:19:40.425Z',
   *     terms: 'Terms',
   *     rules: 'Rules'
   *   }
   *
   * @apiUse NotFound
   *
   * @apiUse ValidatorError
   * @apiErrorExample {json} NotVerified
   *   HTTP/1.1 403 Forbbiden
   *   {
   *     code: 'Forbidden',
   *     message: 'not_verified'
   *   }
   */
  server.post(prefix('/merchants/:merchant_id/redeem/:id'),
    jwtUserAuth,
    userPunchCard.redeem
  )
}
