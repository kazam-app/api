/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/merchant.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine MerchantParams
   * @apiParam {Object} merchant                   Merchant data
   * @apiParam {String} merchant.name              Merchant name
   * @apiParam {String} merchant.image             Base64 image stream
   * @apiParam {String} merchant.background_image  Base64 image stream
   * @apiParam {String} merchant.budget_rating     Merchant budgetRating
   * @apiParam {String} merchant.color             Merchant color
   * @apiParam {String} merchant.invite_code       Merchant inviteCode
   * @apiParam {Integer} merchant.category_id      Category id
   * @apiParam {Integer} merchant.organization_id  Organization id
   */
  /**
   * @apiDefine MerchantSuccess
   * @apiSuccess {Integer} id                      Merchant id
   * @apiSuccess {Integer} category_id             Category id
   * @apiSuccess {Integer} organization_id         Organization id
   * @apiSuccess {String} name                     Merchant name
   * @apiSuccess {String} image_url                Merchant imageUrl
   * @apiSuccess {String} background_image_url     Merchant backgroundImageUrl
   * @apiSuccess {String} color                    Merchant color
   * @apiSuccess {String} invite_code              Merchant inviteCode
   * @apiSuccess {String} category                 Category name
   * @apiSuccess {String} organization             Organization name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     category_id: '1',
   *     organization_id: '1',
   *     name: 'Test Merchant',
   *     color: '#000000',
   *     image_url: 'test.jpg',
   *     background_image_url: 'test.jpg',
   *     category: 'Test Category',
   *     organization: 'Test Organization'
   *   }
   */
  /**
   * @apiDefine MerchantError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_merchant_data'
   *   }
   */
  // Merchant
  const merchant = require('../../controllers/admin/merchant')
  /**
   * @api {get} /admin/merchants List
   * @apiName List
   * @apiGroup Merchant
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id                      Merchant id
   * @apiSuccess {Integer} category_id             Category id
   * @apiSuccess {Integer} organization_id         Organization id
   * @apiSuccess {String} name                     Merchant name
   * @apiSuccess {String} category                 Category name
   * @apiSuccess {String} organization             Organization name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     category_id: '1',
   *     organization_id: '1',
   *     name: 'Test Merchant',
   *     category: 'Test Category',
   *     organization: 'Test Organization'
   *   }]
   */
  server.get(prefix('/merchants'), jwtAuth, adminAuth, merchant.index)
  /**
   * @api {get} /admin/merchants/:id Show
   * @apiName Show
   * @apiGroup Merchant
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id  Merchant id
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantError
   */
  server.get(prefix('/merchants/:id'), jwtAuth, adminAuth, merchant.show)
  /**
   * @api {post} /admin/merchants Create
   * @apiName Create
   * @apiGroup Merchant
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse MerchantParams
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantError
   */
  server.post(prefix('/merchants'), jwtAuth, adminAuth, merchant.create)
  /**
   * @api {put} /admin/merchants/:id Edit
   * @apiName Edit
   * @apiGroup Merchant
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id        Merchant id
   * @apiUse MerchantParams
   *
   * @apiUse MerchantSuccess
   *
   * @apiUse MerchantError
   */
  server.put(prefix('/merchants/:id'), jwtAuth, adminAuth, merchant.update)
  /**
   * @api {delete} /admin/merchants/:id Delete
   * @apiName Delete
   * @apiGroup Merchant
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id             Merchant id
   *
   * @apiSuccess {boolean} success      Removal success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del(prefix('/merchants/:id'), jwtAuth, adminAuth, merchant.delete)
}
