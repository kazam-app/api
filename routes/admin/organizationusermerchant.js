/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/organizationusermerchant.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  // OrganizationUserMerchant
  const organizationusermerchant = require('../../controllers/admin/organizationusermerchant')
  /**
   * @api {get} /admin/merchants/:merchant_id/users List
   * @apiName List
   * @apiGroup OrganizationUserMerchant
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id                      OrganizationUserMerchant id
   * @apiSuccess {String} name                     OrganizationUser name
   * @apiSuccess {String} last_names               OrganizationUser lastNames
   * @apiSuccess {String} email                    OrganizationUser email
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Name',
   *     last_names: 'Test LastName',
   *     email: 'test@kazam.xyz'
   *   }]
   */
  server.get(prefix('/merchants/:merchant_id/users'), jwtAuth, adminAuth, organizationusermerchant.index)
  /**
   * @api {post} /admin/merchants/:merchant_id/users Create
   * @apiName Create
   * @apiGroup OrganizationUserMerchant
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id                      OrganizationUserMerchant id
   * @apiSuccess {String} name                     OrganizationUser name
   * @apiSuccess {String} last_names               OrganizationUser lastNames
   * @apiSuccess {String} email                    OrganizationUser email
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Name',
   *     last_names: 'Test LastName',
   *     email: 'test@kazam.xyz'
   *   }
   */
  server.post(prefix('/merchants/:merchant_id/users'), jwtAuth, adminAuth, organizationusermerchant.create)
}
