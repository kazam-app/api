/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/organization/shop.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const merchantAuth = require('../../lib/filters/merchant')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine ShopParams
   * @apiParam {Object} shop                      Shop data
   * @apiParam {Boolean} shop.is_active           Shop is_active
   * @apiParam {Integer} shop.shop_cluster_id     Shop shop_cluster_id
   * @apiParam {String} shop.name                 Shop name
   * @apiParam {String} shop.location             Shop location
   * @apiParam {String} shop.city                 Shop city
   * @apiParam {String} shop.postal_code          Shop postal_code
   * @apiParam {String} shop.phone                Shop phone
   * @apiParam {String} shop.state                Shop state
   * @apiParam {String} shop.country              Shop country
   * @apiParam {Double} shop.lat                  Shop lat
   * @apiParam {Double} shop.lng                  Shop lng
   */
  /**
   * @apiDefine ShopSuccess
   * @apiSuccess {Integer} id                Shop id
   * @apiSuccess {Integer} merchant_id       Shop merchantId
   * @apiSuccess {Integer} shop_cluster_id   Shop shopClusterId
   * @apiSuccess {String} name               Shop name
   * @apiSuccess {Boolean} is_active         Shop isActive
   * @apiSuccess {String} location           Shop location
   * @apiSuccess {String} postal_code        Shop postalCode
   * @apiSuccess {String} phone              Shop phone
   * @apiSuccess {String} city               Shop city
   * @apiSuccess {String} state              Shop state
   * @apiSuccess {String} country            Shop country
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     merchant_id: '1',
   *     shop_cluster_id: '1',
   *     name: 'Test Shop',
   *     is_active: false,
   *     location: 'Test Prize',
   *     postal_code: '01219',
   *     phone: '551223446',
   *     city: 'CDMX',
   *     state: 'DF',
   *     country: 'MX'
   *   }
   */
  /**
   * @apiDefine ShopError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_shop_data'
   *   }
   */
  // Shop
  const shop = require('../../controllers/admin/shop')
  /**
   * @api {get} /admin/merchants/:merchant_id/shops List
   * @apiName List
   * @apiGroup Shop
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id              Shop id
   * @apiParam {Integer} merchant_id     Merchant id
   *
   * @apiSuccess {integer} id            Shop id
   * @apiSuccess {String} name           Shop name
   * @apiSuccess {String} shop_cluster   ShopCluster name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     shop_cluster: 'Test ShopCluster',
   *     name: 'Test Shop'
   *   }]
   */
  server.get(prefix('/merchants/:merchant_id/shops'), jwtAuth, adminAuth, merchantAuth, shop.index)
  /**
   * @api {get} /admin/merchants/:merchant_id/shops/:id Show
   * @apiName Show
   * @apiGroup Shop
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id             Shop id
   * @apiParam {Integer} merchant_id    Merchant id
   *
   * @apiUse ShopSuccess
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.get(prefix('/merchants/:merchant_id/shops/:id'), jwtAuth, adminAuth, merchantAuth, shop.show)
  /**
   * @api {post} /admin/merchants/:merchant_id/shops Create
   * @apiName Create
   * @apiGroup Shop
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} merchant_id    Merchant id
   * @apiUse ShopParams
   *
   * @apiUse ShopSuccess
   *
   * @apiUse ShopError
   */
  server.post(prefix('/merchants/:merchant_id/shops'), jwtAuth, adminAuth, merchantAuth, shop.create)
  /**
   * @api {put} /admin/merchants/:merchant_id/shops/:id Update
   * @apiName Update
   * @apiGroup Shop
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id             Shop id
   * @apiParam {Integer} merchant_id    Merchant id
   * @apiUse ShopParams
   *
   * @apiUse ShopSuccess
   *
   * @apiUse ShopError
   */
  server.put(prefix('/merchants/:merchant_id/shops/:id'), jwtAuth, adminAuth, merchantAuth, shop.update)
  /**
   * @api {delete} /admin/merchants/:merchant_id/shops/:id Delete
   * @apiName Delete
   * @apiGroup Shop
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id             Shop id
   * @apiParam {Integer} merchant_id    Merchant id
   *
   * @apiSuccess {Boolean} success      Removal success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Shop not_found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del(prefix('/merchants/:merchant_id/shops/:id'), jwtAuth, adminAuth, merchantAuth, shop.delete)
}
