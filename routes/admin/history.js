/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/history.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const merchantAuth = require('../../lib/filters/merchant')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  // History
  const history = require('../../controllers/history')
  /**
   * @api {get} /admin/merchants/:merchant_id/history List
   * @apiName List
   * @apiGroup History
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} page            Page number
   * @apiParam {Integer} per_page        Results per page (default: 20)
   *
   * @apiSuccess {Integer} total            UserPunchCardValidation count
   * @apiSuccess {Array} data               UserPunchCardValidation array
   * @apiSuccess {Integer} data.id          UserPunchCardValidation identifier
   * @apiSuccess {Date} data.created_at     UserPunchCardValidation CreatedAt (ISO 8601 Date)
   * @apiSuccess {Integer} data.identity    UserPunchCardValidation identity (Punch: 0, Redeem: 1)
   * @apiSuccess {String} data.shop         Shop name
   * @apiSuccess {String} data.user         User identifier
   * @apiSuccess {String} data.name         User name
   * @apiSuccess {String} data.last_names   User lastNames
   * @apiSuccess {String} data.email        User email
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     total: 1,
   *     data: [{
   *       id: 'dc2868bc-483f-45b4-a635-cd77049c527e',
   *       created_at: '2017-06-19T20:44:50.904Z',
   *       identity: 0,
   *       shop: 'Test Shop',
   *       user: '48ace1a0-aaec-4078-9106-7f4750d37473',
   *       name: 'Name',
   *       last_names: 'Last Names',
   *       email: 'f471ec37-3d74-4d6b-9b1d-1c77b77f5797@test360.com'
   *     }]
   *   }
   */
  server.get(prefix('/merchants/:merchant_id/history'),
    jwtAuth, adminAuth, merchantAuth, history.index
  )
}
