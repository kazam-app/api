/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/suggestion.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  const suggestion = require('../../controllers/admin/suggestion')
  /**
   * @api {get} /admin/suggestions List
   * @apiName List
   * @apiGroup Suggestion
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id                 Suggestion id
   * @apiSuccess {String} name                Suggestion name
   * @apiSuccess {Date} created_at            Suggestion created_at
   * @apiSuccess {Object} user                User data
   * @apiSuccess {String} user.identifier     User identifier
   * @apiSuccess {String} user.name           User name
   * @apiSuccess {String} user.email          User email
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Suggestion',
   *     created_at: '2017-05-25T19:07:39.007Z',
   *     user: {
   *       identifier: '6af22c60-8858-4617-b372-03642f9d9be8',
   *       name: 'User',
   *       email: '6af22c60-8858-4617-b372-03642f9d9be8@test.com'
   *     }
   *   }]
   */
  server.get(prefix('/suggestions'), jwtAuth, adminAuth, suggestion.index)
}
