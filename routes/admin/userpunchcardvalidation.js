/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/userpunchcardvalidation.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const merchantAuth = require('../../lib/filters/merchant')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  const userpunchcardvalidation = require('../../controllers/admin/userpunchcardvalidation')
  /**
   * @api {get} /admin/merchants/:merchant_id/breakdown Breakdown
   * @apiName Breakdown
   * @apiGroup UserPunchCardValidation
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {object} shops             Breakdown shops
   * @apiSuccess {integer} intervals        Breakdown intervals
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     shops: [{
   *        id: '2150',
   *        name: 'Test Shop',
   *        breakdown: [
   *          [
   *            { total: '1', identity: 0, id: '2152', name: 'Test Shop' }
   *          ],
   *          null,
   *          null,
   *          null
   *        ],
   *        punch_total: 1,
   *        redeem_total: 0
   *     }],
   *     intervals: 4
   *   }]
   */
  server.get(prefix('/merchants/:merchant_id/breakdown'),
    jwtAuth, adminAuth, merchantAuth, userpunchcardvalidation.breakdown
  )
}
