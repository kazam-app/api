/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/organization.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine OrganizationParams
   * @apiParam {Object} organization         Organization data
   * @apiParam {String} organization.name    Organization name
   */
  /**
   * @apiDefine OrganizationSuccess
   * @apiSuccess {Integer} id                Organization id
   * @apiSuccess {String} organization       Organization name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Org'
   *   }
   */
  /**
   * @apiDefine OrganizationError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_organization_data'
   *   }
   */
  // Organization
  const organization = require('../../controllers/admin/organization')
  /**
   * @api {get} /admin/organizations List
   * @apiName List
   * @apiGroup Organization
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {String} id      Organization id
   * @apiSuccess {String} name    Organization name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Organization'
   *   }]
   * @apiErrorExample {json} Unauthorized
   *   HTTP/1.1 401 Unauthorized
   *   {
   *     code: 'InvalidCredentials',
   *     message: 'No authorization token was found'
   *   }
   */
  server.get(prefix('/organizations'), jwtAuth, adminAuth, organization.index)
  /**
   * @api {get} /admin/organizations/:id Show
   * @apiName Show
   * @apiGroup Organization
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id  Organization id
   *
   * @apiUse OrganizationSuccess
   *
   * @apiUse OrganizationError
   */
  server.get(prefix('/organizations/:id'), jwtAuth, adminAuth, organization.show)
  /**
   * @api {post} /admin/organizations Create
   * @apiName Create
   * @apiGroup Organization
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse OrganizationParams
   *
   * @apiUse OrganizationSuccess
   *
   * @apiUse OrganizationError
   */
  server.post(prefix('/organizations'), jwtAuth, adminAuth, organization.create)
  /**
   * @api {put} /admin/organizations/:id Edit
   * @apiName Edit
   * @apiGroup Organization
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id        Organization id
   * @apiUse OrganizationParams
   *
   * @apiUse OrganizationSuccess
   *
   * @apiUse OrganizationError
   */
  server.put(prefix('/organizations/:id'), jwtAuth, adminAuth, organization.update)
}
