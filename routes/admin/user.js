/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/user.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const merchantAuth = require('../../lib/filters/merchant')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  const user = require('../../controllers/admin/user')
  /**
   * @api {get} /admin/users List
   * @apiName List
   * @apiGroup User
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {String} id              User identifier
   * @apiSuccess {String} name            User name
   * @apiSuccess {String} last_names      User lastName
   * @apiSuccess {String} email           User email
   * @apiSuccess {Boolean} is_verified    User isVerified
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '645a9172-d145-4bee-abe9-fa9cb9873fca',
   *     name: 'Name',
   *     last_names: 'LastName'
   *     email: '6af22c60-8858-4617-b372-03642f9d9be8@test.com',
   *     is_verified: false
   *   }]
   */
  server.get(prefix('/users'), jwtAuth, adminAuth, user.index)
  /**
   * @api {get} /admin/users/:id List
   * @apiName List
   * @apiGroup User
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id               User identifier
   *
   * @apiSuccess {String} id              User identifier
   * @apiSuccess {String} name            User name
   * @apiSuccess {String} last_names      User lastNames
   * @apiSuccess {String} email           User email
   * @apiSuccess {Boolean} is_verified    User isVerified
   * @apiSuccess {String} sign_up_type    User signUpType
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '645a9172-d145-4bee-abe9-fa9cb9873fca',
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: 'e1be2edf-c633-4b6d-b3d7-9362167844b0@testKzm.com',
   *     is_verified: true,
   *     sign_up_type: 'email',
   *     gender: null,
   *     birthdate: null,
   *     created_at: '2018-03-07T00:51:47.801Z',
   *     top_merchants: [],
   *     top_shops: [],
   *     history: [],
   *     categories: [],
   *     averages: {}
   *   }
   */
  server.get(prefix('/users/:id'), jwtAuth, adminAuth, user.show)
  /**
   * @api {get} /admin/merchants/:merchant_id/users/:id Show
   * @apiName List
   * @apiGroup User
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {String} id               User identifier
   * @apiParam {Integer} merchant_id     Merchant id
   *
   * @apiSuccess {String} id              User identifier
   * @apiSuccess {String} name            User name
   * @apiSuccess {String} last_names      User lastNames
   * @apiSuccess {String} email           User email
   * @apiSuccess {Boolean} is_verified    User isVerified
   * @apiSuccess {String} sign_up_type    User signUpType
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '645a9172-d145-4bee-abe9-fa9cb9873fca',
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: 'e1be2edf-c633-4b6d-b3d7-9362167844b0@testKzm.com',
   *     is_verified: true,
   *     sign_up_type: 'email',
   *     gender: null,
   *     birthdate: null,
   *     created_at: '2018-03-07T00:51:47.801Z',
   *     top_merchants: [],
   *     top_shops: [],
   *     history: [],
   *     categories: [],
   *     averages: {}
   *   }
   */
  server.get(prefix('/merchants/:merchant_id/users/:id'), jwtAuth, adminAuth, merchantAuth, user.merchant)
}
