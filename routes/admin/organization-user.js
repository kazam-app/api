/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/organization-user.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  // Organization User
  const organizationuser = require('../../controllers/admin/organization-user')
  /**
   * OrganizationUser with Token
   * @apiDefine OrganizationUserJWT
   * @apiSuccess {String} id          OrganizationUser id
   * @apiSuccess {String} name        OrganizationUser name
   * @apiSuccess {String} last_names  OrganizationUser last_names
   * @apiSuccess {String} email       OrganizationUser email
   * @apiSuccess {String} token       OrganizationUser jwt
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Name',
   *     last_names: 'Last Names',
   *     email: '6af22c60-8858-4617-b372-03642f9d9be8@test.com',
   *     token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiI0YTVlNDViN2JkOTFmYzVkZTFkNzU2MWQwNWM1MzA0MDU5NzZjNGIwNzZmMmI2YjM5ODJkYTMxN2VhZTU1M2E0IiwiZXhwIjoxNTI0NzU2Nzg2LCJpYXQiOjE0OTMyMjA3ODZ9._KY5vDcIZumq4Jf0OZrHpdstJmvP_C5gBcliTSrUhqA'
   *   }
   */
  /**
   * @api {post} /admin/login Login
   * @apiName Login
   * @apiGroup OrganizationUser
   *
   * @apiParam {String} email      OrganizationUser email
   * @apiParam {String} password   OrganizationUser password
   *
   * @apiUse OrganizationUserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'login_fail'
   *   }
   */
  server.post(prefix('/login'), organizationuser.login)
  /**
   * @api {delete} /admin/log_out Log Out
   * @apiName Log Out
   * @apiGroup OrganizationUser
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'login_fail'
   *   }
   */
  server.del(prefix('/log_out'), jwtAuth, adminAuth, organizationuser.logOut)
  /**
   * @api {get} /admin/merchants/:merchant_id/users/unassigned Unassigned
   * @apiName Unassigned
   * @apiGroup OrganizationUser
   * @apiPermission Admin
   *
   * @apiParam {String} merchant_id      Merchant id
   *
   * @apiUse OrganizationUserJWT
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'login_fail'
   *   }
   */
  // server.get(prefix('/merchants/:merchant_id/users/unassigned'), organizationuser.unassigned)
}
