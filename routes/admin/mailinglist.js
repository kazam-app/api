/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/mailinglist.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine MailingListParams
   * @apiParam {Object} mailing_list           MailingList data
   * @apiParam {String} mailing_lists.name     MailingList name
   */
  /**
   * @apiDefine MailingListSuccess
   * @apiSuccess {Integer} id            MailingList id
   * @apiSuccess {String} name           MailingList name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Lists',
   *     image_url: 'test.jpg'
   *   }
   */
  /**
   * @apiDefine MailingListError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_mailing_list_data'
   *   }
   */
  // MailingList
  const mailingList = require('../../controllers/admin/mailinglist')
  /**
   * @api {get} /admin/mailing_lists List
   * @apiName List
   * @apiGroup MailingList
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         MailingList id
   * @apiSuccess {String} name        MailingList name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Lists'
   *   }]
   */
  server.get(prefix('/mailing_lists'), jwtAuth, adminAuth, mailingList.index)
  /**
   * @api {post} /admin/mailing_lists Create
   * @apiName Create
   * @apiGroup MailingList
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse MailingListParams
   *
   * @apiUse MailingListSuccess
   *
   * @apiUse MailingListError
   */
  server.post(prefix('/mailing_lists'), jwtAuth, adminAuth, mailingList.create)
}
