/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/qr.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine QRParams
   * @apiParam {Object} qr                    QR data
   * @apiParam {Integer} qr.merchant_id       QR merchantId
   * @apiParam {Integer} qr.shop_id           QR shopId
   * @apiParam {String} qr.name               QR name
   * @apiParam {Boolean} qr.is_active         QR isActive
   */
  /**
   * @apiDefine QRSuccess
   * @apiSuccess {Integer} id               QR id
   * @apiSuccess {Integer} merchant_id      QR merchant_id
   * @apiSuccess {Integer} shop_id          QR shop_id
   * @apiSuccess {Boolean} is_active        QR isActive
   * @apiSuccess {String} name              QR name
   * @apiSuccess {String} image_url         QR imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     merchant_id: 1,
   *     shop_id: null,
   *     name: 'QR 1',
   *     is_active: true,
   *     image_url: 'test.jpg'
   *   }
   */
  /**
   * @apiDefine QRError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Missing QR data
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_qr_data'
   *   }
   */
  // QR
  const qr = require('../../controllers/admin/qr')
  /**
   * @api {get} /admin/qrs List
   * @apiName List
   * @apiGroup QR
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         QR id
   * @apiSuccess {String} name        QR name
   * @apiSuccess {String} image_url   QR imageUrl
   * @apiSuccess {String} shop        Shop name
   * @apiSuccess {String} merchant    Merchant name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'QR 1',
   *     is_active: true,
   *     image_url: 'test.jpg',
   *     shop: 'Test Shop',
   *     merchant: 'Test Merchant'
   *   }]
   */
  server.get(prefix('/qrs'), jwtAuth, adminAuth, qr.index)
  /**
   * @api {get} /admin/qrs/:id Show
   * @apiName Show
   * @apiGroup QR
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse QRSuccess
   * @apiSuccess {String} shop        Shop name
   * @apiSuccess {String} merchant    Merchant name
   *
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} QR not_found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.get(prefix('/qrs/:id'), jwtAuth, adminAuth, qr.show)
  /**
   * @api {post} /admin/qrs Create
   * @apiName Create
   * @apiGroup QR
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse QRParams
   *
   * @apiUse QRSuccess
   *
   * @apiUse QRError
   */
  server.post(prefix('/qrs'), jwtAuth, adminAuth, qr.create)
  /**
   * @api {put} /admin/qrs/:id Update
   * @apiName Update
   * @apiGroup QR
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      QR id
   * @apiUse QRParams
   *
   * @apiUse QRSuccess
   *
   * @apiUse QRError
   */
  server.put(prefix('/qrs/:id'), jwtAuth, adminAuth, qr.update)
  /**
   * @api {delete} /admin/qrs/:id Delete
   * @apiName Delete
   * @apiGroup QR
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id              QR id
   *
   * @apiSuccess {Boolean} success       Removal success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} QR not_found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del(prefix('/qrs/:id'), jwtAuth, adminAuth, qr.delete)
}
