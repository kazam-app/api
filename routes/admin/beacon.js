/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/beacon.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine BeaconParams
   * @apiParam {Object} beacon                    Beacon data
   * @apiParam {Integer} beacon.merchant_id        Beacon merchantId
   * @apiParam {Integer} beacon.shop_id            Beacon shopId
   * @apiParam {String} beacon.name               Beacon name
   * @apiParam {Boolean} beacon.is_active         Beacon isActive
   * @apiParam {String} beacon.uid                Beacon uid
   * @apiParam {String} beacon.major              Beacon major
   * @apiParam {String} beacon.minor              Beacon minor
   */
  /**
   * @apiDefine BeaconSuccess
   * @apiSuccess {Integer} id           Beacon id
   * @apiSuccess {String} name          Beacon name
   * @apiSuccess {Boolean} is_active    Beacon isActive
   * @apiSuccess {String} uid           Beacon uid
   * @apiSuccess {String} major         Beacon major
   * @apiSuccess {String} minor         Beacon minor
   * @apiSuccess {String} shop_id       Shop id
   * @apiSuccess {String} merchant_id   Merchant id
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Beacon 1',
   *     is_active: true,
   *     uid: '6c799c42-ebe0-4866-b492-9fb2930e7bbe',
   *     major: '01',
   *     minor: '1',
   *     shop_id: '1',
   *     merchant_id: '1'
   *   }
   */
  /**
   * @apiDefine BeaconError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Missing Beacon data
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_beacon_data'
   *   }
   */
  // Beacon
  const beacon = require('../../controllers/admin/beacon')
  /**
   * @api {get} /admin/beacons List
   * @apiName List
   * @apiGroup Beacon
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         Beacon id
   * @apiSuccess {String} name        Beacon name
   * @apiSuccess {Boolean} is_active  Beacon isActive
   * @apiSuccess {String} uid         Beacon uid
   * @apiSuccess {String} major       Beacon major
   * @apiSuccess {String} minor       Beacon minor
   * @apiSuccess {String} shop        Shop name
   * @apiSuccess {String} merchant    Merchant name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Beacon 1',
   *     is_active: true,
   *     uid: '6c799c42-ebe0-4866-b492-9fb2930e7bbe',
   *     major: '01',
   *     minor: '1',
   *     shop: 'Test Shop',
   *     merchant: 'Test Merchant'
   *   }]
   */
  server.get(prefix('/beacons'), jwtAuth, adminAuth, beacon.index)
  /**
   * @api {get} /admin/beacons/:id Show
   * @apiName Show
   * @apiGroup Beacon
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id             Beacon id
   *
   * @apiUse BeaconSuccess
   * @apiSuccess {String} shop        Shop name
   * @apiSuccess {String} merchant    Merchant name
   *
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Beacon not_found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.get(prefix('/beacons/:id'), jwtAuth, adminAuth, beacon.show)
  /**
   * @api {post} /admin/beacons Create
   * @apiName Create
   * @apiGroup Beacon
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse BeaconParams
   *
   * @apiUse BeaconSuccess
   *
   * @apiUse BeaconError
   */
  server.post(prefix('/beacons'), jwtAuth, adminAuth, beacon.create)
  /**
   * @api {put} /admin/beacons/:id Update
   * @apiName Update
   * @apiGroup Beacon
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id         Beacon id
   * @apiUse BeaconParams
   *
   * @apiUse BeaconSuccess
   *
   * @apiUse BeaconError
   */
  server.put(prefix('/beacons/:id'), jwtAuth, adminAuth, beacon.update)
  /**
   * @api {delete} /admin/beacons/:id Delete
   * @apiName Delete
   * @apiGroup Beacon
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id               Beacon id
   *
   * @apiSuccess {Boolean} success        Removal success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Beacon not_found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del(prefix('/beacons/:id'), jwtAuth, adminAuth, beacon.delete)
}
