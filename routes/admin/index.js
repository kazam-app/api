/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/index.js
 * @author Joel Cano
 */

'use strict'

module.exports = function (server, prefix) {
  /**
   * Admin API definitions
   */
  /**
   * @apiDefine Admin Urbanity User access only
   * Only authorized Urbanity Users belong to this group.
   */
  /**
   * Jwt Authentication
   * @apiDefine JwtAuth
   * @apiHeader {String} Authorization Bearer JWT
   * @apiHeaderExample {String} Authorization
   *   Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiIxYzI2MmViYjAxZWMwOTQzOWQ0MjJhZGE3NGIyYjc1NTM2MDJmMWRmODBlMjFkMWU5Y2RhMmNmM2Y0YjY3M2U2IiwiZXhwIjoxNTI0NzY1ODQxLCJyZWwiOiIxIiwiaWF0IjoxNDkzMjI5ODQxfQ.ujVPcwpEhMH_3f56bQO2DPxxkJboAnVcQmz1j6et5yw
   */
  if (!prefix) prefix = function (path) { return path }
  // Organization
  require('./organization')(server, prefix)
  // Organization User
  require('./organization-user')(server, prefix)
  // Shop Cluster
  require('./shopcluster')(server, prefix)
  // Category
  require('./category')(server, prefix)
  // Merchant
  require('./merchant')(server, prefix)
  // Shop
  require('./shop')(server, prefix)
  // Beacon
  require('./beacon')(server, prefix)
  // Qr
  require('./qr')(server, prefix)
  // PunchCard
  require('./punchcard')(server, prefix)
  // Analytics
  require('./analytics')(server, prefix)
  // History
  require('./history')(server, prefix)
  // Suggestion
  require('./suggestion')(server, prefix)
  // OrganizationUserMerchant
  require('./organizationusermerchant')(server, prefix)
  // UserPunchCardValidation
  require('./userpunchcardvalidation')(server, prefix)
  // User
  require('./user')(server, prefix)
  // MailingLists
  require('./mailinglist')(server, prefix)
}
