/**
 * Kazam Api
 * @name Restify Routing Logic
 * @file routes/admin/category.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine CategoryParams
   * @apiParam {Object} category          Category data
   * @apiParam {String} category.name     Category name
   * @apiParam {String} category.image    Base64 image stream
   */
  /**
   * @apiDefine CategorySuccess
   * @apiSuccess {Integer} id            Category id
   * @apiSuccess {String} name           Category name
   * @apiSuccess {String} image_url      Category imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test Category',
   *     image_url: 'test.jpg'
   *   }
   */
  /**
   * @apiDefine CategoryError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_category_data'
   *   }
   */
  // Category
  const category = require('../../controllers/admin/category')
  /**
   * @api {get} /admin/categories List
   * @apiName List
   * @apiGroup Category
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id         Category id
   * @apiSuccess {String} name        Category name
   * @apiSuccess {String} image_url   Category imageUrl
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test Category',
   *     image_url: 'test.jpg'
   *   }]
   */
  server.get(prefix('/categories'), jwtAuth, adminAuth, category.index)
  /**
   * @api {get} /admin/categories/:id Show
   * @apiName Show
   * @apiGroup Category
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      Category id
   *
   * @apiUse CategorySuccess
   *
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.get(prefix('/categories/:id'), jwtAuth, adminAuth, category.show)
  /**
   * @api {post} /admin/categories Create
   * @apiName Create
   * @apiGroup Category
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse CategoryParams
   *
   * @apiUse CategorySuccess
   *
   * @apiUse CategoryError
   */
  server.post(prefix('/categories'), jwtAuth, adminAuth, category.create)
  /**
   * @api {put} /admin/categories/:id Edit
   * @apiName Edit
   * @apiGroup Category
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      Category id
   * @apiUse CategoryParams
   *
   * @apiUse CategorySuccess
   *
   * @apiUse CategoryError
   */
  server.put(prefix('/categories/:id'), jwtAuth, adminAuth, category.update)
  /**
   * @api {delete} /admin/categories/:id Delete
   * @apiName Delete
   * @apiGroup Category
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      Category id
   *
   * @apiSuccess {boolean} success    Removal success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del(prefix('/categories/:id'), jwtAuth, adminAuth, category.delete)
}
