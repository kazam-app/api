/**
 * Merchants Api
 * @name Restify Routing Logic
 * @file shopcluster.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  /**
   * @apiDefine ShopClusterParams
   * @apiParam {Object} shop_cluster               ShopCluster data
   * @apiParam {String} shop_cluster.name          ShopCluster name
   * @apiParam {String} shop_cluster.location      ShopCluster location
   * @apiParam {String} shop_cluster.city          ShopCluster city
   * @apiParam {String} shop_cluster.postal_code   ShopCluster postalCode
   * @apiParam {String} shop_cluster.state         ShopCluster state
   * @apiParam {String} shop_cluster.lat           ShopCluster lat
   * @apiParam {String} shop_cluster.lng           ShopCluster lng
   */
  /**
   * @apiDefine ShopClusterSuccess
   * @apiSuccess {Integer} id             ShopCluster id
   * @apiSuccess {String} name            ShopCluster name
   * @apiSuccess {String} location        ShopCluster location
   * @apiSuccess {String} postal_code     ShopCluster postalCode
   * @apiSuccess {String} city            ShopCluster city
   * @apiSuccess {String} state           ShopCluster state
   * @apiSuccess {String} country         ShopCluster country
   * @apiSuccess {Double} lat             ShopCluster lat
   * @apiSuccess {Double} lng             ShopCluster lng
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     name: 'Test ShopCluster',
   *     location: 'Test Location',
   *     postal_code: '01217',
   *     city: 'CDMX',
   *     state: 'DF',
   *     lat: null,
   *     lng: null
   *   }
   */
  /**
   * @apiDefine ShopClusterError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_shop_cluster_data'
   *   }
   */
  // Shop Cluster
  const shopCluster = require('../../controllers/admin/shopcluster')
  /**
   * @api {get} /admin/shop_clusters List
   * @apiName List
   * @apiGroup ShopCluster
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {String} id          ShopCluster id
   * @apiSuccess {String} name        ShopCluster name
   * @apiSuccess {String} location    ShopCluster location
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     name: 'Test ShopCluster',
   *     location: 'Test Location'
   *   }]
   * @apiErrorExample {json} Unauthorized
   *   HTTP/1.1 401 Unauthorized
   *   {
   *     code: 'InvalidCredentials',
   *     message: 'No authorization token was found'
   *   }
   */
  server.get(prefix('/shop_clusters'), jwtAuth, adminAuth, shopCluster.index)
  /**
   * @api {get} /admin/shop_clusters/:id Show
   * @apiName Show
   * @apiGroup ShopCluster
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id               ShopCluster id
   *
   * @apiUse ShopClusterSuccess
   *
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Invalid id
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.get(prefix('/shop_clusters/:id'), jwtAuth, adminAuth, shopCluster.show)
  /**
   * @api {post} /admin/shop_clusters Create
   * @apiName Create
   * @apiGroup ShopCluster
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse ShopClusterParams
   *
   * @apiUse ShopClusterSuccess
   *
   * @apiUse ShopClusterError
   */
  server.post(prefix('/shop_clusters'), jwtAuth, adminAuth, shopCluster.create)
  /**
   * @api {put} /admin/shop_clusters/:id Edit
   * @apiName Edit
   * @apiGroup ShopCluster
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      ShopCluster id
   * @apiUse ShopClusterParams
   *
   * @apiUse ShopClusterSuccess
   *
   * @apiUse ShopClusterError
   */
  server.put(prefix('/shop_clusters/:id'), jwtAuth, adminAuth, shopCluster.update)
  /**
   * @api {delete} /admin/shop_clusters/:id Delete
   * @apiName Delete
   * @apiGroup ShopCluster
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id           ShopCluster id
   *
   * @apiSuccess {boolean} success    Removal success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiErrorExample {json} BadRequest
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del(prefix('/shop_clusters/:id'), jwtAuth, adminAuth, shopCluster.delete)
}
