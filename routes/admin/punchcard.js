/**
 * Merchants Api
 * @name Restify Routing Logic
 * @file punchcard.js
 * @author Joel Cano
 */

'use strict'

const adminAuth = require('../../lib/filters/admin')
const jwtSecret = require('../../lib/filters/jwt').organization
const jwtAuth = require('restify-jwt-community')({ secret: jwtSecret })

module.exports = function (server, prefix) {
  // PunchCard
  /**
   * @apiDefine PunchCardParams
   * @apiParam {Object} punch_card                     PunchCard data
   * @apiParam {String} punch_card.name                PunchCard name
   * @apiParam {String} punch_card.prize               PunchCard prize
   * @apiParam {String} punch_card.rules               PunchCard rules
   * @apiParam {String} punch_card.terms               PunchCard terms
   * @apiParam {String} punch_card.redeem_code         PunchCard redeemCode
   * @apiParam {Integer} punch_card.punch_limit        PunchCard punchLimit (3 - 15)
   * @apiParam {Date} punch_card.expires_at            PunchCard expiresAt (ISO 8601 Date)
   * @apiParam {Integer} punch_card.validation_limit   PunchCard validationLimit
   */
  /**
   * @apiDefine PunchCardSuccess
   * @apiSuccess {Integer} id                PunchCard id
   * @apiSuccess {Integer} identity          PunchCard identity (Unpublished: 0, Published: 1, Finished: 2)
   * @apiSuccess {String} name               PunchCard name
   * @apiSuccess {String} prize              PunchCard prize
   * @apiSuccess {String} rules              PunchCard rules
   * @apiSuccess {String} terms              PunchCard terms
   * @apiSuccess {String} redeem_code        PunchCard redeemCode
   * @apiSuccess {Integer} punch_limit       PunchCard punchLimit
   * @apiSuccess {Date} expires_at           PunchCard expiresAt
   * @apiSuccess {Integer} validation_limit  PunchCard validationLimit
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     id: '1',
   *     identity: 0,
   *     name: 'Test PunchCard',
   *     prize: 'Test Prize',
   *     rules: 'Test Rules',
   *     terms: 'Test Terms',
   *     punch_limit: 3,
   *     redeem_code: 'TEST1',
   *     expires_at: '2017-05-25T19:07:39.007Z',
   *     validation_limit: null
   *   }
   */
  /**
   * @apiDefine PunchCardError
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} Missing PunchCard data
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'missing_punch_card_data'
   *   }
   */
  const punchcard = require('../../controllers/admin/punchcard')
  /**
   * @api {get} /admin/punch_cards List
   * @apiName List
   * @apiGroup PunchCard
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiSuccess {Integer} id           PunchCard id
   * @apiSuccess {Integer} identity     PunchCard identity
   * @apiSuccess {String} name          PunchCard name
   * @apiSuccess {String} punch_limit   PunchCard punchLimit
   * @apiSuccess {String} expires_at    PunchCard expires_at(ISO 8601 date)
   * @apiSuccess {String} merchant      Merchant name
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   [{
   *     id: '1',
   *     identity: 1,
   *     name: 'PunchCard 1',
   *     punch_limit: 15,
   *     expires_at: '2018-07-01',
   *     merchant: 'Test Merchant'
   *   }]
   */
  server.get(prefix('/punch_cards'), jwtAuth, adminAuth, punchcard.index)
  /**
   * @api {get} /admin/punch_cards/:id Show
   * @apiName Show
   * @apiGroup PunchCard
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiUse PunchCardSuccess
   * @apiErrorExample {json} PunchCard not_found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.get(prefix('/punch_cards/:id'), jwtAuth, adminAuth, punchcard.show)
  /**
   * @api {post} /admin/punch_cards Create
   * @apiName Create
   * @apiGroup PunchCard
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.post(prefix('/punch_cards'), jwtAuth, adminAuth, punchcard.create)
  /**
   * @api {put} /admin/punch_cards/:id Update
   * @apiName Update
   * @apiGroup PunchCard
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   * @apiUse PunchCardParams
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.put(prefix('/punch_cards/:id'), jwtAuth, adminAuth, punchcard.update)
  /**
   * @api {put} /punch_cards/:id/publish Publish
   * @apiName Publish
   * @apiGroup PunchCard
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   *
   * @apiUse PunchCardSuccess
   *
   * @apiUse PunchCardError
   */
  server.put(prefix('/punch_cards/:id/publish'),
    jwtAuth,
    adminAuth,
    punchcard.publish
  )
  /**
   * @api {delete} /admin/punch_cards/:id Delete
   * @apiName Delete
   * @apiGroup PunchCard
   * @apiPermission Admin
   *
   * @apiUse JwtAuth
   *
   * @apiParam {Integer} id      PunchCard id
   *
   * @apiSuccess {Boolean} success
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   true
   * @apiError {String} code      HTTP Error
   * @apiError {String} message   Error message
   * @apiErrorExample {json} PunchCard not_found
   *   HTTP/1.1 400 Bad Request
   *   {
   *     code: 'BadRequest',
   *     message: 'not_found'
   *   }
   */
  server.del(prefix('/punch_cards/:id'), jwtAuth, adminAuth, punchcard.delete)
}
