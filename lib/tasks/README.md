# Tasks

Repetitive or time sensitive tasks are stored in this folder.

## Install

All script installation and configuration should be in the `lib/tasks/index.js`
file. A CRON like npm is used to execute the scripts via `setInterval` to avoid
OS dependent implementations.

## Run

The tasks are started and stopped when the server is initialized or stopped in
`index.js`.

## List

- churned
- expiry-notification
