/**
 * Kazam Api
 * @name Churned User task
 * @file lib/tasks/churned.js
 * @author Joel Cano
 */

'use strict'

const moment = require('moment')

const dayInterval = process.env.CHURNED_DAY_INTERVAL || 30

module.exports = function (models, log) {
  // search for all the mailingLists for users
  return models.MailingList.scope('users').findAll()
    .then(function (results) {
      const pointer = moment()
      const startAt =
        pointer.clone().subtract(dayInterval, 'days').startOf('day')
      // find users that have not been active for the past `dayInterval`
      return models.User.newChurned(startAt.toDate(), pointer.toDate())
    }).then(function (users) {
      let updates = []
      for (let i = 0; i < users.length; ++i) {
        updates.push(users[i].churn())
      }
      log.info({ task: 'churned' }, { updating: updates.length })
      // update
      return Promise.all(updates)
    }).then(function (result) {
      log.debug({ task: 'churned' }, { script: result })
      log.info({ task: 'churned' }, 'script success')
    }).catch(function (err) {
      log.fatal({ task: 'churned' }, 'script failed: ' + err.message)
    })
}
