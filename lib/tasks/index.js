/**
 * Kazam Api
 * @name Index
 * @description Task configuration and functions
 * @file lib/tasks/index.js
 * @author Joel Cano
 */

'use strict'

/**
 * NOTE: Configure all repetitive tasks here. Reasoning: multiple environment
 *  deployments require deploy dependant configuration. Timed tasks are handled
 *  with cron npm to avoid `shipit` or `EBS` dependant scripts.
 *  Jobs are configured but not started by default. They should be started when
 *  the server is initialized and stoped when server is shut down.
 */
const CronJob = require('cron').CronJob
// tasks
const churned = require('./churned')
// const expiryNotification = require('./expiry-notification')

module.exports = {
  /**
   * @method configure
   * @description Configure cron tasks
   * @param {array} models - Sequelize model instance array
   * @param {object} log - Logger instance
   * @returns {array} configured job array
   *
   * @static
   */
  configure: function (models, log) {
    log.info({ task: 'general' }, 'configuring')
    let jobs = []
    /**
     * Churned users task configuration. Run once every day.
     */
    const churnTaskTag = { task: 'churned' }
    jobs.push(
      new CronJob('0 23 * * * *',
        function () {
          log.info(churnTaskTag, 'started')
          // execute task
          churned(models, log).then(function () {
            log.info(churnTaskTag, 'finished')
          })
        },
        function () {
          log.info(churnTaskTag, 'stopped')
        }
      )
    )
    /**
     * Expiry notification task configuration. Run once every day.
     */
    // const expiryTaskTag = { task: 'expiry-notification' }
    // jobs.push(
    //   new CronJob('0 22 * * * *',
    //     function () {
    //       log.info(expiryTaskTag, 'started')
    //       // execute task
    //       expiryNotification(models, log).then(function () {
    //         log.info(expiryTaskTag, 'notified')
    //       })
    //     },
    //     function () {
    //       log.info(expiryTaskTag, 'stopped')
    //     }
    //   )
    // )
    log.info({ task: 'general' }, { configured: jobs.length })
    return jobs
  },
  /**
   * @method start
   * @description Start CronJob tasks
   * @param {array} jobs - CronJob instance array
   * @param {object} log - Logger instance
   *
   * @static
   */
  start: function (jobs, log) {
    log.debug({ task: 'general' }, 'starting')
    for (let i = 0; i < jobs.length; ++i) {
      let job = jobs[0]
      job.start()
    }
  },
  /**
   * @method stop
   * @description Stop CronJob tasks
   * @param {array} jobs - CronJob instance array
   * @param {object} log - Logger instance
   *
   * @static
   */
  stop: function (jobs, log) {
    log.debug({ task: 'general' }, 'stopping')
    for (let i = 0; i < jobs.length; ++i) {
      let job = jobs[0]
      job.stop()
    }
  }
}
