/**
 * Kazam Api
 * @name ExpiryNotification
 * @description Expiry notification task
 * @file lib/tasks/expiry-notification.js
 * @author Joel Cano
 */

'use strict'

/**
 * @method first
 * @description Search for and notify users
 * @param {array<object>} models - moment date instance
 * @param {object} log - moment date instance
 * @return {Promise<User[]>} Returns a User array promise
 *
 * @static
 */
module.exports.first = function (models, log) {
  const User = models.User
  return User.forFirstExpiryNotification()
    .then(User.expiryNotify(models.UserPunchCard.AlertNotificationSent.First))
}
/**
 * @method second
 * @description Search for and notify users
 * @param {array<object>} models - moment date instance
 * @param {object} log - moment date instance
 * @return {Promise<User[]>} Returns a User array promise
 *
 * @static
 */
module.exports.second = function (models, log) {
  const User = models.User
  return User.forSecondExpiryNotification()
    .then(User.expiryNotify(models.UserPunchCard.AlertNotificationSent.Second))
}
