/**
 * Kazam Api
 * @name Sentry Instalation
 * @file lib/sentry.js
 * @author Joel Cano
 */

'use strict'

const Raven = require('raven')
const dsn = process.env.SENTRY_DSN

module.exports.install = function () {
  Raven.config(dsn).install()
}
