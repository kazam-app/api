/**
 * Kazam Api
 * @name Raw Query Decorator Helper
 * @file lib/rawdecorate.js
 * @author Joel Cano
 */

'use strict'

const map = require('async/map')
const Promise = require('bluebird')

module.exports = function (decorator) {
  return function (elements) {
    return new Promise(function (resolve, reject) {
      map(elements,
        function (item, cbk) { cbk(null, decorator(item)) },
        function (err, results) {
          if (err) reject(err)
          else resolve(results)
        }
      )
    })
  }
}
