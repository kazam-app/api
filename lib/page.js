/**
 * Kazam Api
 * @name Sequelize Pagination definition
 * @file lib/page.js
 * @author Joel Cano
 */

'use strict'

const Promise = require('bluebird')

/**
 * @description String model definition structure
 * @type {Object}
 *
 */
module.exports = function (total, list) {
  return Promise.join(total, list, function (total, data) {
    return {
      total: parseInt(total[0].total),
      data: data
    }
  })
}
