/**
 * Kazam Api
 * @name Error Definitions
 * @file lib/error.js
 * @author Joel Cano
 */

'use strict'

class NotFoundError extends Error {
  constructor (message) {
    super(message)
    this.name = 'NotFoundError'
    this.message = message
    Error.captureStackTrace(this, this.constructor)
  }
}
exports.NotFoundError = NotFoundError

class LimitReachedError extends Error {
  constructor (message) {
    super(message)
    this.name = 'LimitError'
    this.message = message
    Error.captureStackTrace(this, this.constructor)
  }
}
exports.LimitReachedError = LimitReachedError

class UnVerifiedError extends Error {
  constructor (message) {
    super(message)
    this.name = 'UnVerifiedError'
    this.message = message
    Error.captureStackTrace(this, this.constructor)
  }
}
exports.UnVerifiedError = UnVerifiedError

class InvalidError extends Error {
  constructor (message) {
    super(message)
    this.name = 'InvalidError'
    this.message = message
    Error.captureStackTrace(this, this.constructor)
  }
}
exports.InvalidError = InvalidError

class ExpiredError extends Error {
  constructor (message) {
    super(message)
    this.name = 'ExpiredError'
    this.message = message
    Error.captureStackTrace(this, this.constructor)
  }
}
exports.ExpiredError = ExpiredError
