/**
 * Kazam Api
 * @name Password Helper
 * @file lib/password.js
 * @author Joel Cano
 */

'use strict'

const salts = 11
const minLength = 8
const bcrypt = require('bcrypt')

const hash = function (value) {
  return bcrypt.hashSync(value, salts)
}

module.exports.hash = hash

module.exports.generate = function (password, confirmation) {
  // check values
  if ((password && confirmation)) {
    // check confirmation
    if (password === confirmation) {
      // check length
      if (password.length < minLength) return hash(password)
      else throw new Error('password_min_length')
    } else throw new Error('password_does_not_match')
  } else throw new Error('missing_password_confirmation')
}
