/**
 * Kazam Api
 * @name OneSignal
 * @description OneSignal Initialize and Helper methods
 * @file lib/one-signal.js
 * @author Joel Cano
 */

'use strict'

const OneSignal = require('onesignal-node')
const env = process.env

const client = new OneSignal.Client({
  userAuthKey: env.ONE_SIGNAL_AUTH,
  app: { appAuthKey: env.ONE_SIGNAL_APP_AUTH, appId: env.ONE_SIGNAL_APP_ID }
})

module.exports = {
  api: client,
  notify: {
    /**
     * @method notify.email
     * @description Send a notification via email
     * @param {string} email - email
     * @returns {Promise} returns a promise with a OneSignal API Response
     *
     * @static
     */
    email: function (email, contents) {
      let notification = new OneSignal.Notification({ contents: contents })
      notification.setParameter('!include_email_tokens', email)
      return client.sendNotification(notification)
    },
    /**
     * @method notify.player
     * @description Send a notification via player id
     * @param {string} playerId - OneSignal player id
     * @param {object} message - Object with notification related data
     * @returns {Promise} returns a promise with a OneSignal API Response
     *
     * @static
     */
    player: function (playerId, message) {
      let notification = new OneSignal.Notification(message)
      notification.setTargetDevices([playerId])
      return client.sendNotification(notification)
    }
  }
}
