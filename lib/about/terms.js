/**
 * Kazam Api
 * @name Terms and Conditions
 * @file lib/about/terms.js
 * @author Joel Cano
 */

'use strict'

module.exports = [
  'TÉRMINOS Y CONDICIONES KAZAM',
  // 1
  '1. INTRODUCCIÓN',
  'El presente documento (en lo sucesivo, “Términos y Condiciones”) regula la operación del Programa de Lealtad Kazam (en lo sucesivo, “Programa de Lealtad”), por lo que respecta, de manera enunciativa más no limitativa, a lo siguiente: (i) la afiliación o desafiliación al mismo; y (ii) la acumulación o canje de Sellos (conforme este término se define más adelante) por los Usuarios (conforme este término se define más adelante).',
  // 2
  '2. PROGRAMA DE LEALTAD',
  '2.1 El Programa de Lealtad es operado y administrado por G30.Holdings, S.A.P.I. de C.V. (“Kazam”), con domicilio en Tampico No. 42 – PB3, Colonia Roma, Cuauhtémoc, Ciudad de México, Código Postal 06700, en la Ciudad de México, en cooperación con las empresas o grupos que tienen celebrado con Kazam un contrato para la participación en el Programa de Lealtad (en lo sucesivo, las “Socios Comerciales”).',
  'El Programa de Lealtad se opera en la República Mexicana. La información sobre la identidad y estado de los Socios Comerciales que de tiempo en tiempo participen en el Programa de Lealtad estará disponible en la página de Internet www.kzm.mx (en lo sucesivo, la “Página del Programa”). El registro al Programa de Lealtad será sin costo alguno.',
  '2.2 Los sellos digitales serán obtenidos por los Usuarios como resultado de acciones específicas, como son la compra de bienes y/o servicios en los establecimientos de las Socios Comerciales (en lo sucesivo, los “Sellos”). Los Sellos se acumularan en la tarjeta digital de lealtad  (la “Tarjeta de Lealtad”) perteneciente a cada uno de los Socios Comerciales en los que se haya realizado la acción específica.',
  '2.3 Los Sellos se podrán canjear, sujeto a los términos más abajo señalados, en el consumo de bienes y/o servicios con los Socios Comerciales.',
  '2.4 Los Sellos no tienen valor monetario y no pueden canjearse por dinero en efectivo.',
  // 3
  '3. USUARIOS',
  '3.1 Podrá participar en el Programa de Lealtad cualquier persona física que sea mayor de 18 años de edad, con capacidad legal para obligarse, que resida en el territorio mexicano, y que haya creado una Cuenta (cuyo término se define más adelante), en lo sucesivo, “Usuario(s)”).',
  '3.2 Las Cuentas se podrán obtener a través de la plataforma digital denominada “Kazam” desarrollada específicamente para dispositivos móviles por Kazam para efectos del Programa de Lealtad (en lo sucesivo la “Aplicación Kazam”).',
  '3.3 El Usuario manifiesta y acepta de conformidad expresamente a estos Términos y Condiciones al registrarse en el Programa de Lealtad. Cada Usuario es responsable de estar al tanto de los Términos y Condiciones, así como de sus modificaciones, una vez que hayan sido notificadas dichas modificaciones a través de la Aplicación Kazam o a través de correo electrónico.',
  '3.4 Kazam no aceptará registros múltiples al Programa de Lealtad realizados por el mismo Usuario.',
  '3.5 El Usuario debe asegurarse que sus datos son precisos y actualizados. La rectificación o cambio de los datos personales se pueden realizar a través de la Aplicación Kazam.',
  // 4
  '4. ACUMULACIÓN DE SELLOS.',
  '4.1 El Usuario acumulará Sellos por consumos en establecimientos de los Socios Comerciales, en el entendido, de que cada una de los Socios Comerciales establecerá las condiciones, para el otorgamiento de dichos Sellos, las cuales podrán ser consultadas en la sección “Vencimiento y Condiciones” en cada Tarjeta de Lealtad que aparezca en la Aplicación Kazam.',
  '4.2 Para acumular Sellos el Usuario deberá mostrar su Tarjeta de Lealtad (la cual se encuentra dentro de la Aplicación Kazam y solicitar que se acumulen los Sellos correspondientes a ese consumo en su Cuenta (dicha solicitud debe ser realizada durante el proceso de cobro).',
  'En el supuesto de que no se haga lo anterior, no se podrán acumular con posterioridad Sellos en relación al consumo realizado (no habrá acumulaciones retroactivas). En el caso de que por fallas en el sistema del establecimiento correspondiente no se pueda realizar la acumulación el día del consumo, el mismo día del consumo se tendrá que enviar un correo electrónico a ayuda@kzm.mx , detallando la situación, el usuario, la fecha, la hora, nombre del Socio Comercial y una foto del comprobante de compra.',
  'Kazam se reserva el derecho de otorgar a su discreción los Sellos a los Usuarios relacionados a los casos descritos en el párrafo anterior.',
  '4.3 En el caso de que diversos Usuarios participen en un mismo consumo y quisieran todos recibir o c Sellos por dicho Consumo, se tendrá que atener a los términos de cobro de cada Socio Comercial.',
  '4.4 Los Socios Comerciales serán los responsables de la emisión precisa de los Sellos.',
  '4.5 Los Socios Comerciales definirán los bienes y servicios elegibles de tiempo en tiempo para la acumulación de Sellos, en el entendido que salvo que se indique lo contrario en la Aplicación Kazam conforme el mecanismo indicado en el párrafo siguiente, en algunos casos serán sujetos a acumulación consumos mediante entrega a domicilio.',
  '4.6 El Usuario podrá obtener información sobre los Socios Comerciales, los establecimientos de los Socios Comerciales que participan en el Programa de Lealtad, los bienes y servicios de cada Socio Comercial elegibles para acumulación o canje, y otros detalles del Programa de Lealtad, en cualquier momento a través de la Aplicación Kazam.',
  '4.7 Los pagos realizados en combinación de pago en efectivo o tarjeta de crédito/débito y mediante canje de Sellos, sólo acumularán Sellos en lo que corresponde a la porción pagada en efectivo o tarjeta de crédito/ débito.',
  '4.8 El canje de una Tarjeta de Lealtad no generará el otorgamiento de Sellos.',
  // 5
  '5. CUENTA KAZAM',
  'Los Sellos que haya acumulado el Usuario se registrarán bajo la Tarjeta de Lealtad de cada uno de los Socios Comerciales en su Cuenta en la Aplicación Kazam (en lo sucesivo, la “Cuenta”). El Usuario podrá verificar el saldo actual las Tarjetas de Lealtad de su Cuenta a través de la Aplicación Kazam.',
  // 6
  '6. CANJEO DE SELLOS.',
  '6.1 Los Sellos acumulados por un Usuario podrán ser utilizados por el mismo para el pago total o parcial de consumos de bienes y/o servicios en los establecimientos de las Socios Comerciales (de aquellos bienes y/o servicios elegibles conforme lo establecido más abajo), en términos de los dispuesto en las condiciones de cada una de las Tarjetas de Lealtad..',
  '6.2 Para canjear Sellos, el Usuario deberá mostrar su Tarjeta de Lealtad relacionada a ese Socio Comercial dentro de la Aplicación Kazam, y solicitar al momento de pago que se canjeen los Sellos.',
  '6.3 En el caso de que por fallas en el sistema del establecimiento correspondiente no se pueda realizar el canje de la Tarjeta de Lealtad, el mismo día se tendrá que enviar un correo electrónico a ayuda@kzm.mx, detallando el usuario, la situación, la fecha, la hora, nombre del Socio Comercial, y se buscará una solución en apoyo al Usuario.',
  '6.4 Los Socios Comerciales serán responsables del canje preciso de los Sellos.',
  '6.5 Las Socios Comerciales definirán los bienes y servicios elegibles de tiempo en tiempo para el canje de Sellos, mismos que se publicarán en las Tarjeta de Lealtad, en el entendido que salvo que se indique lo contrario conforme lo anteriormente señalado, el canje de Sellos no será aplicable para servicios de entrega a domicilio.',
  // 7
  '7. PROMOCIONES DEL PROGRAMA KAZAM',
  'Kazam y las Socios Comerciales establecerán de tiempo en tiempo promociones como parte del programa, que serán publicadas en la Aplicación Kazam.',
  // 8
  '8. RESTRICCIONES DE COMBINACIÓN:',
  '8.1. Salvo que se indique lo contrario en la Tarjeta de Lealtad, la acumulación y el canje de Sellos no podrá combinarse con otros programas de lealtad que en su caso manejen las Socios Comerciales.',
  '8.2.Salvo que se indique lo contrario en la Tarjeta de Lealtad, los Sellos podrán canjearse en combinación con promociones bancarias, otros descuentos (incluyendo aquellos derivados de encuestas de satisfacción de servicios), o productos en precio promocional.',
  // 9
  '9. VENCIMIENTO DE SELLOS KAZAM',
  'La vigencia de cada uno de los Sellos, la establecerá la Socio Comercial a su discreción y podrá ser consultada dentro de la sección “Vencimiento y Condiciones” de la Tarjeta de Lealtad en la Aplicación Kazam.',
  // 10
  '10. COMUNICACIONES',
  'Todas las comunicaciones que Kazam dirija a un Usuario se enviarán a la dirección de correo electrónico que el mismo haya proporcionado al registrarse en el Programa de Lealtad (o que hubiera actualizado posteriormente a su inscripción); o bien a través de la Aplicación Kazam.',
  // 11
  '11. TERMINACIÓN DE LA PARTICIPACIÓN EN EL PROGRAMA DE LEALTAD',
  '11.1 El Usuario podrá cancelar su participación al Programa de Lealtad en cualquier momento, enviando un correo a ayuda@kzm.mx indicando las razones de la cancelación de la participación en el Programa.',
  '11.2 Kazam podrá cancelar a cualquier Usuario su participación en el Programa de Lealtad en cualquier de los siguientes escenarios: (a) en caso de que el Usuario proporcione a una persona no autorizada acceso a su Cuenta, (b) en caso de que Kazam determine que se están realizando transacciones de acumulación o de canje fraudulentas o (c) de cualquier otro modo, se violen estos Términos y Condiciones. Se enviará al Usuario el aviso de cancelación, explicando la razón para dicha cancelación.',
  '11.3 La cancelación de la participación de un Usuario en el Programa de Lealtad resultará en la pérdida y cancelación de los Sellos acumulados por éste.',
  // 12
  '12. CAMBIO A LOS TÉRMINOS Y CONDICIONES',
  'Estos Términos y Condiciones podrán ser modificados por Kazam en cualquier momento. Dichos cambios serán informados a los Usuarios a través de un anuncio vía la Página del Programa, correo electrónico o la Aplicación Kazam (sin perjuicio de la responsabilidad de los Usuarios de revisar continuamente la Página del Programa, la Aplicación Kazam y su correo electrónico).',
  // 13
  '13. CANCELACIÓN DEL PROGRAMA',
  '13.1 Kazam podrá, a su completa discreción, suspender o terminar el Programa de Lealtad. Esa decisión será comunicada a los Usuarios a través de la Página del Programa, con por lo menos con 30 (treinta) días naturales antes de fecha efectiva de la suspensión o cancelación del Programa Kazam.',
  '13.2 Después que sea efectiva la cancelación del Programa Kazam, todos los Sellos acumulados por los Usuarios se perderán, sin incurrir Kazam (ni las Socios Comerciales) en responsabilidad alguna.',
  // 14
  '14. INDEMNIZACIÓN.',
  'Usted acepta que será personalmente responsable del uso del Programa de Lealtad, y usted acepta defender, indemnizar y mantener indemne a Kazam y sus funcionarios, directores, empleados, consultores, filiales, subsidiarias, clientes, proveedores y agentes, de y contra cualquier y todo reclamo, responsabilidades, daños, pérdidas y gastos, incluyendo los abogados y honorarios de contabilidad y costos, que surjan de o relacionados de alguna manera con (i) el acceso, uso o supuesto uso la Cuenta; (ii) su violación de los Términos o cualquier representación, garantía o acuerdos mencionados en este documento, o cualquier ley o regulación aplicable; (iii) su violación de cualquier derecho de terceros, incluyendo sin limitación cualquier derecho de propiedad intelectual, la publicidad, la confidencialidad, la propiedad o derecho de privacidad; o (iv) cualquier disputa o problema entre usted y cualquier tercero. Nos reservamos el derecho, a nuestro propio costo, de asumir la defensa exclusiva y control de cualquier asunto sujeto a indemnización por usted, y en tal caso, se compromete a cooperar con nuestra defensa de dicha reclamación.',
  // 15
  '15. DISPOSICIONES GENERALES',
  '15.1 Todos los derechos y obligaciones que resulten de estos Términos y Condiciones se regirán por las leyes de la República Mexicana.',
  '15.2 Para cualquier controversia derivada de estos Términos y Condiciones las partes se someten a las leyes y jurisdicción de los tribunales competentes de la Ciudad de México.',
  '15.3 Kazam no será responsable de los cambios, descontinuación de los servicios y/o productos de las Socios Comerciales.',
  '15.4 Kazam no tendrá responsabilidad en caso de que no pueda cumplir con estos Términos y Condiciones debido a casos de fuerza mayor o caso fortuito u otro cambio en la ley o por cualesquiera otras razones fuera de su control.',
  '15.5 Las Socios Comerciales pueden cambiar cualquiera de los términos y condiciones de su negocio en cualquier momento sin dar previo aviso al Usuario. Kazam no tiene la responsabilidad de informar a los Usuarios sobre dichos cambios. De la misma manera, Kazam no será responsable de los actos u omisiones de las Socios Comerciales.',
  // 16
  '16. AVISO DE PRIVACIDAD',
  'G30.Holdings, S.A.P.I. de C.V. con domicilio en Tampico 42 – 3PB, Colonia Roma, Cuauhtémoc, Código Postal 06700, en la Ciudad de México, es responsable del tratamiento de sus datos personales. Los datos personales recabados se utilizarán para la operación del Programa de Lealtad, incluyendo comunicación al Usuario de los beneficios y promociones bajo el mismo. Adicionalmente, sus datos personales, salvo que usted manifieste negativa, mediante comunicado al correo ayuda@kzm.mx, serán utilizados para la siguiente finalidad primaria: envío de nuestras promociones y descuentos.',
  'Asimismo, salvo que usted manifieste negativa mediante comunicado al correo anteriormente señalado, sus datos personales serán transferidos a las Socios Comerciales para que puedan ser utilizados con fines de estudios de mercadotecnia, estadísticos y de análisis del mercado, incluyendo la preparación y la distribución de información personalizada sobre los bienes y/o servicios de las Socios Comerciales.',
  'Si requiere mayor información puede acceder a nuestro aviso de privacidad integral en el siguiente link www.kzm.mx'
]
