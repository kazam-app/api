/**
 * Kazam Api
 * @name Frequently Asked Questions
 * @file lib/about/faq.js
 * @author Joel Cano
 */

'use strict'

module.exports = [
  // Pregunta 1
  '¿Tu tarjeta de sellos ya no aparece?',
  'Si no encuentras tu tarjeta de sellos de alguna marca es por que ya expiró y estas al cumplir su fecha de vencimiento desaparecen.',
  // Pregunta 2
  '¿Para qué pedimos tu fecha de nacimiento y género?',
  'Odiamos mandar mensajes poco relevantes, te pedimos estos datos para conocerte un poco mejor y así brindarte una mejor experiencia en el app.',
  // Pregunta 3
  '¿Qué hacer en caso de no recibir un sello o canjear un premio?',
  'Si intentaste sellar o canjear un premio y simplemente no se pudo no te preocupes. Tomale una foto a tu recibo y mandanos un correo a ayuda@kzm.mx contandonos lo que paso para que te podamos ayudar.',
  // Pregunta 4
  '¿Tiene algún costo?',
  'No :), usar Kazam es gratis.',
  // Pregunta 5
  '¿Para qué necesitan permiso de notificación?',
  'Pedimos este permiso para recordarte cuando tengas premios por canjear así como cuando cuando tengamos alguna promoción especial para ti.',
  // Pregunta 6
  '¿Usar bluetooth con Kazam gasta la bateria de mi celular?',
  'No :) en Kazam utilizamos BLE4.0 o Bluetooth 4.0 lo que nos permite utilizar tu bluetooth sin consumir la pila de tu celular como sucedía cuando usabas un aparato de manos libres hace un par de años.',
  // Pregunta 7
  '¿Para qué necesitan saber mi ubicación?',
  'Pedimos el permiso de ubicación para poder enseñarte la tarjeta correcta del establecimiento en el que te encuentras, así como para mandarte ofertas personalizadas. Pero no te preocupes no guardamos tus ubicaciones ni tus rutas, y nunca compartimos este dato con nadie.',
  // Pregunta 8
  '¿Tengo que recoger alguna tarjeta de lealtad física?',
  'No, nuestro programa es 100% digital.',
  // Pregunta 9
  '¿Como canjear mis premios?',
  '¡Completa tus tarjetas para recibir tu premio! Para canjear, pide al encargado de la tienda que acerque el sello digital a tu celular, igual que cuando sellas. Si no tienes tu Bluetooth prendido, también puedes escanear el código QR.',
  // Pregunta 10
  '¿Cómo sellar mis tarjetas?',
  'Para sellar tus tarjetas, pide al encargado de la tienda que acerque el sello digital a tu celular. ¡Igual que si sellaras una tarjeta normal! En caso de que no tengas tu Bluetooth prendido puedes escanear el código QR de la tienda para recibir tu sello.',
  // Pregunta 11
  '¿Que es Kazam?',
  'Kazam es una aplicación que te permite tener todos los programas de lealtad de nuestras marcas afiliadas en tu celular. Deja de preocuparte por cargar, olvidar, y perder tus tarjetas de lealtad. Con Kazam puedes acumular sellos y canjearlos por premios de las marcas afiliadas.'
]
