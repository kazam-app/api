/**
 * Kazam Api
 * @name About Information
 * @file lib/about/index.js
 * @author Joel Cano
 */

'use strict'

const terms = require('./terms')
const policy = require('./policy')
const faq = require('./faq')

module.exports = { terms: terms, policy: policy, faq: faq }
