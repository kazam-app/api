/**
 * Kazam Api
 * @name Date Helpers
 * @file lib/date.js
 * @author Joel Cano
 */

'use strict'

const moment = require('moment')
const validator = require('validator')

module.exports = {
  isDate: { msg: 'invalid_date' },
  /**
   * @method validate
   * @description Validates if an incoming string is an ISO8601 date
   * @param {string} str - unknown string
   * @returns {boolean} returns a user promise with a session jwt
   *
   * @static
   */
  validate: function (str) {
    return validator.isISO8601(str)
  },
  /**
   * @method parse
   * @description Validates if an incoming string is an ISO8601 date
   * @param {string} str - unknown string
   * @returns {boolean} returns a user promise with a session jwt
   *
   * @static
   */
  parse: function (date) {
    return moment.utc(date, moment.ISO_8601, true).format()
  },
  /**
   * @method process
   * @description Validates if an incoming string is an ISO8601 date
   * @param {string} str - unknown string
   * @returns {boolean} returns a user promise with a session jwt
   *
   * @static
   */
  process: function (str) {
    let date
    if (this.validate(str)) date = this.parse(str)
    return date
  }
}
