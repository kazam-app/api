/**
 * Kazam Api
 * @name Empty String
 * @file lib/blank.js
 * @author Joel Cano
 */

'use strict'

module.exports = function (val) {
  return (val !== undefined && val === '')
}
