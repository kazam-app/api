/**
 * Kazam Api
 * @name Restify CORS
 * @file lib/cors.js
 * @author Joel Cano
 */

'use strict'

const env = process.env
const urls = {
  admin: env.CORS_ADMIN,
  merchant: env.CORS_MERCHANT,
  recovery: env.CORS_RECOVERY,
  validation: env.CORS_VALIDATION,
  verification: env.CORS_VERIFICATION
}

module.exports = function (server, log) {
  let key
  let url
  let origins = []
  const keys = Object.keys(urls)
  for (let i = 0; i < keys.length; ++i) {
    key = keys[i]
    url = urls[key]
    log.info({ provider: 'CORS' }, { key: key, url: url })
    if (url) origins.push(url)
  }
  if (origins.length === 0) {
    origins.push('*')
    log.info({ provider: 'CORS' }, { key: 'wildcard', url: '*' })
  }
  const opts = {
    origins: origins,
    preflightMaxAge: 4,
    allowHeaders: ['Authorization']
  }
  log.debug({ server: 'cors' }, { config: opts })
  const cors = require('restify-cors-middleware')(opts)
  server.pre(cors.preflight)
  server.use(cors.actual)
}
