/**
 * Kazam Api
 * @name ExpiryNotification
 * @description Expiry notification messages
 * @file lib/expiry-notification.js
 * @author Joel Cano
 */

'use strict'

module.exports = [
  // first notification
  [
    {
      headings: {
        en: '🚨¡Ojo! Tarjeta a punto de vencer. 😱'
      },
      contents: {
        en: 'Tu tarjeta de :merchant vence en menos de un mes ⏳. Asegúrate de completarla para evitar perder tus premios. 🎁'
      }
    },
    {
      headings: {
        en: '🚨¡Ojo! Tarjetas a punto de vencer. 😱'
      },
      contents: {
        en: 'Tus tarjetas de :firstMerchant y :secondMerchant vence en menos de un mes ⏳. Asegúrate de completarla para evitar perder tus premios. 🎁'
      }
    },
    {
      headings: {
        en: '🚨¡Ojo! Tarjetas a punto de vencer. 😱'
      },
      contents: {
        en: ':cardCount de tus tarjetas vencen en menos de 30 días ⏳. Asegúrate de completarlas para no perder tus premios. 🎁'
      }
    }
  ],
  // second notification
  [
    {
      headings: {
        en: '🚨¡Cuidado! Tarjeta a punto de expirar.🚨'
      },
      contents: {
        en: 'Tu tarjeta de :merchant está a punto de expirar 😱. Asegúrate de completarla para evitar perder tus premios. 🎁'
      }
    },
    {
      headings: {
        en: '🚨¡Cuidado! Tarjetas a punto de expirar.🚨'
      },
      contents: {
        en: 'Tus tarjetas de :firstMerchant y :secondMerchant expiran en menos de 15 días 😱. Complétalas y canjea tus premios. 🎁'
      }
    },
    {
      headings: {
        en: '🚨¡Cuidado! Tarjetas a punto de expirar.🚨'
      },
      contents: {
        en: '¡No pierdas tus sellos! :cardCount de tus tarjetas van a expirar muy pronto 😫, no olvides completarlas para llevarte tus premios. 🎁'
      }
    }
  ]
]
