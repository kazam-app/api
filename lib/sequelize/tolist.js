/**
 * Merchants Api
 * @name Sequelize toList Utility
 * @file tolist.js
 * @author Joel Cano
 */

'use strict'

const map = require('async/map')
const Promise = require('bluebird')
/**
 * @method toList
 * @description Returns apprpriate list data
 * @param {array} elements - Sequelize model instance array
 * @return {Promise<array>} - object array
 *
 * @static
 */
module.exports = function (elements) {
  return new Promise(function (resolve, reject) {
    map(elements,
      function (item, cbk) {
        cbk(null, item.toList())
      },
      function (err, results) {
        if (err) reject(err)
        else resolve(results)
      }
    )
  })
}
