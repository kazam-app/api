/**
 * Merchants Api
 * @name Sequelize Empty Validation
 * @file notempty.js
 * @author Joel Cano
 */
'use strict'
/**
 * @description Empty validation structure
 * @type {Object}
 *
 */
module.exports = {
  notEmpty: { msg: 'empty' }
}
