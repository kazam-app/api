/**
 * Kazam Api
 * @name Sequelize Pagination definition
 * @file lib/sequelize/page.js
 * @author Joel Cano
 */

'use strict'

const limit = 20

module.exports.total = function (sequelize) {
  return function () {
    return this.findAll({ raw: true, attributes: [[sequelize.fn('COUNT', 'id'), 'total']] })
  }
}

module.exports.paginate = function (query, params) {
  let jump = limit
  const perPage = params.per_page
  if (perPage && !isNaN(perPage)) jump = perPage
  const page = params.page
  if (page && !isNaN(page)) query.offset = page * jump
  query.limit = jump
  return query
}
