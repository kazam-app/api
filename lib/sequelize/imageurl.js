/**
 * Kazam Api
 * @name Sequelize ImageUrl definition
 * @file lib/imageurl.js
 * @author Joel Cano
 */

'use strict'

/**
 * @description Image Sequelize field structure
 * @param {object} DataTypes - DataTypes enum
 * @param {boolean} allowNull - if null values allowed
 * @param {string} field - Field name
 * @return {object} image field structure
 *
 * @static
 */
module.exports = function (DataTypes, allowNull, field) {
  if (field === undefined) field = 'image_url'
  let result = { field: field, type: DataTypes.STRING }
  if (allowNull !== undefined) result.allowNull = allowNull
  return result
}
