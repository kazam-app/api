/**
 * Kazam Api
 * @name Sequelize ToChar helper
 * @file lib/sequelize/to-char.js
 * @author Joel Cano
 */

'use strict'

/**
 * @description postgres TO_CHAR implementation
 * @param {object} sequelize - sequelize instance
 * @param {string} field - query field
 * @param {string} name - alias name
 * @param {string} format - output format
 * @return {array<sequelize.fn,string>} sequelize column structure
 */
module.exports = function (sequelize, field, name, format) {
  return [
    sequelize.fn('TO_CHAR', sequelize.col(field), format),
    name
  ]
}
