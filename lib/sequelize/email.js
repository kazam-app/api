/**
 * Kazam Api
 * @name Sequelize Email field definition
 * @file lib/sequelize/email.js
 * @author Joel Cano
 */

'use strict'

const _ = require('lodash/object')
const notEmpty = require('./notempty')

/**
 * @description String model definition structure
 * @return {object} sequelize field structure
 *
 */
module.exports = function (DataTypes, allowNull, field) {
  let result = {
    unique: true,
    type: DataTypes.STRING,
    validate: _.merge({}, notEmpty, {
      len: {
        args: [2, 255],
        msg: 'invalid_length'
      },
      isEmail: { msg: 'invalid_email' }
    })
  }
  if (allowNull !== undefined) result.allowNull = allowNull
  if (field !== undefined) result.field = field
  return result
}
