/**
 * Kazam Api
 * @name Sequelize Token fields definition and helpers
 * @file lib/sequelize/token.js
 * @author Joel Cano
 */

'use strict'

const notEmpty = require('./notempty')

/**
 * @description Token string definition
 * @type {function}
 *
 */
module.exports.token = function (DataTypes, allowNull, field) {
  let result = { unique: true, validate: notEmpty, type: DataTypes.TEXT }
  if (allowNull !== undefined) result.allowNull = allowNull
  if (field !== undefined) result.field = field
  return result
}
/**
 * @description Secret string defninition
 * @type {function}
 *
 */
module.exports.secret = function (DataTypes, allowNull, field) {
  let result = { validate: notEmpty, type: DataTypes.TEXT }
  if (allowNull !== undefined) result.allowNull = allowNull
  if (field !== undefined) result.field = field
  return result
}
/**
 * @description activeForTokenAndIdentity where query structur
 * @type {function}
 *
 */
module.exports.activeForTokenAndIdentity = function (sequelize, token, identity) {
  const Op = sequelize.Op
  return {
    [Op.or]: [
      { identity: identity, token: token, expiresAt: null },
      { identity: identity, token: token, expiresAt: { [Op.gt]: new Date() } }
    ]
  }
}

/**
 * @description expiredForTokenAndIdentity where query structur
 * @type {function}
 *
 */
module.exports.expiredForTokenAndIdentity = function (sequelize, token, identity) {
  return { identity: identity, token: token, expiresAt: { [sequelize.Op.lt]: new Date() } }
}
