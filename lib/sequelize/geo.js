/**
 * Kazam Api
 * @name Sequelize Coordinates definition
 * @file lib/sequelize/geo.js
 * @author Joel Cano
 */

'use strict'
/**
 * @description Latitude model definition structure
 * @return {object} sequelize field structure
 *
 */
module.exports.lat = function (DataTypes, allowNull) {
  let result = {
    type: DataTypes.DOUBLE,
    validate: {
      min: { args: [-90.0], msg: 'less_than' },
      max: { args: [90.0], msg: 'greater_than' }
    }
  }
  if (allowNull !== undefined) result.allowNull = allowNull
  return result
}
/**
 * @description Longitud model definition structure
 * @return {object} sequelize field structure
 *
 */
module.exports.lng = function (DataTypes, allowNull) {
  let result = {
    type: DataTypes.DOUBLE,
    validate: {
      min: { args: [-180.0], msg: 'less_than' },
      max: { args: [180.0], msg: 'greater_than' }
    }
  }
  if (allowNull !== undefined) result.allowNull = allowNull
  return result
}
