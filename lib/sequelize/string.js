/**
 * Kazam Api
 * @name Sequelize String field definition
 * @file lib/string.js
 * @author Joel Cano
 */

'use strict'

const _ = require('lodash/object')
const notEmpty = require('./notempty')

/**
 * @description String Sequelize field structure
 * @param {object} DataTypes - DataTypes enum
 * @param {boolean} allowNull - if null values allowed
 * @param {string} field - Field name
 * @return {object} string field structure
 *
 * @static
 */
module.exports = function (DataTypes, allowNull, field) {
  let result = {
    type: DataTypes.STRING,
    validate: _.merge({}, notEmpty, {
      len: {
        args: [2, 255],
        msg: 'invalid_length'
      }
    })
  }
  if (allowNull !== undefined) result.allowNull = allowNull
  if (field !== undefined) result.field = field
  return result
}
