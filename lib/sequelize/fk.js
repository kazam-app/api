/**
 * Kazam Api
 * @name Sequelize FK field definition
 * @file lib/fk.js
 * @author Joel Cano
 */

'use strict'

/**
 * @description String Sequelize field structure
 * @param {object} DataTypes - DataTypes enum
 * @param {boolean} allowNull - if null values allowed
 * @param {string} field - Field name
 * @return {object} string field structure
 *
 * @static
 */
module.exports = function (DataTypes, allowNull, field) {
  let result = { type: DataTypes.BIGINT }
  if (allowNull !== undefined) result.allowNull = allowNull
  if (field !== undefined) result.field = field
  return result
}
