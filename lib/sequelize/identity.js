/**
 * Kazam Api
 * @name Sequelize Identity definition
 * @file lib/identity.js
 * @author Joel Cano
 */

'use strict'

/**
 * @description Identity Sequelize field structure
 * @param {object} DataTypes - DataTypes enum
 * @param {integer} defaultValue - Default
 * @param {array} values - Possible value array
 * @param {string} field - Field name
 * @return {object} identity field structure
 *
 * @static
 */
module.exports = function (DataTypes, defaultValue, values, field) {
  let result = {
    allowNull: false,
    type: DataTypes.INTEGER,
    defaultValue: defaultValue,
    validate: {
      isInt: { msg: 'invalid_integer' },
      isIn: { args: [values], msg: 'invalid_value' }
    }
  }
  if (field !== undefined) result.field = field
  return result
}
