/**
 * Merchants Api
 * @name Sequelize UUID definition
 * @file uuid.js
 * @author Joel Cano
 */
'use strict'

const notEmpty = require('./notempty')

/**
 * @description UUID Sequelize model structure
 * @param {object} DataTypes - DataTypes enum
 * @return {object} uuid field structure
 *
 * @static
 */
module.exports = function (DataTypes, name) {
  let spec = {
    unique: true,
    allowNull: false,
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    validate: notEmpty
  }
  if (name) spec.field = name
  return spec
}
