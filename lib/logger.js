/**
 * Kazam Api
 * @name Logger Helper Class
 * @file lib/logger.js
 * @author Joel Cano
 */

'use strict'

const bunyan = require('bunyan')
const level = process.env.LOG_LEVEL || 'info'

module.exports = function (name) {
  return bunyan.createLogger({
    name: name,
    level: level
  })
}
