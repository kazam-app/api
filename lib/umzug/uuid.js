/**
 * Kazam Api
 * @name Sequelize UUID Umzug definition
 * @file lib/umzug/uuid.js
 * @author Joel Cano
 */
'use strict'
/**
 * @description UUID Umzug migration structure
 * @param {object} Sequelize - Sequelize instance
 * @return {object} uuid field structure
 *
 * @static
 */
module.exports = function (Sequelize) {
  return {
    unique: true,
    allowNull: false,
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4
  }
}
