/**
 * Kazam Api
 * @name Umzug Token migration definition
 * @file lib/umzug/token.js
 * @author Joel Cano
 */

'use strict'

/**
 * @description Token umzug definition structure
 * @return {object} umzug migration structure
 */
module.exports.token = function (Sequelize, allowNull) {
  let result = { unique: true, type: Sequelize.TEXT }
  if (allowNull !== undefined) result.allowNull = allowNull
  return result
}

/**
 * @description Secret umzug definition structure
 * @return {Object} umzug migration structure
 */
module.exports.secret = function (Sequelize, allowNull) {
  let result = { type: Sequelize.TEXT }
  if (allowNull !== undefined) result.allowNull = allowNull
  return result
}
