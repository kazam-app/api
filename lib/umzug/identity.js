/**
 * Kazam Api
 * @name Umzug Identity migration definition
 * @file lib/umzug/identity.js
 * @author Joel Cano
 */
'use strict'
/**
 * @description Identity Umzug migration structure
 * @param {object} Sequelize - Sequelize instance
 * @return {object} identity field structure
 *
 * @static
 */
module.exports = function (Sequelize) {
  return {
    defaultValue: 0,
    allowNull: false,
    type: Sequelize.INTEGER
  }
}
