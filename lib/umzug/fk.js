/**
 * Kazam Api
 * @name Sequelize Foreign Key Migration
 * @file lib/sequelize/fk.js
 * @author Joel Cano
 */
'use strict'
/**
 * @description Foreign key structure helper function
 * @param {object}  Sequelize - Sequelize orm instance
 * @param {string}  name - Field name
 * @param {boolean} update - CASCADE or SET NULL
 * @param {boolean} del - CASCADE or SET NULL
 * @return {object} sequelize foreign key field structure
 *
 * @static
 */
module.exports = function (Sequelize, name, update, del) {
  const onUpdate = update ? 'CASCADE' : 'SET NULL'
  const onDelete = del ? 'CASCADE' : 'SET NULL'
  return {
    allowNull: !update,
    type: Sequelize.BIGINT,
    references: {
      key: 'id',
      model: name
    },
    onUpdate: onUpdate,
    onDelete: onDelete
  }
}
