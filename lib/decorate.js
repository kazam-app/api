/**
 * Kazam Api
 * @name Decorator Helper
 * @file lib/decorate.js
 * @author Joel Cano
 */

'use strict'

const map = require('async/map')
const Promise = require('bluebird')

module.exports = function (decorator) {
  return function (elements) {
    if (Array.isArray(elements)) {
      return new Promise(function (resolve, reject) {
        map(elements,
          function (item, cbk) {
            cbk(null, item[decorator]())
          },
          function (err, results) {
            if (err) reject(err)
            else resolve(results)
          }
        )
      })
    } else return elements[decorator]()
  }
}
