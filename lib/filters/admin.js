/**
 * Merchant Api
 * @name JsonWebToken Admin Auth Method
 * @file lib/admin.js
 * @author Joel Cano
 */

'use strict'

const InvalidCredentialsError = require('restify-errors').InvalidCredentialsError

/**
 * @description Jwt Admin processing method
 * @param {object}  req - Restify request instance
 * @param {object}  res - Restify response instance
 * @param {function} next - callback
 *
 * @static
 */
module.exports = function (req, res, next) {
  req.jwtUser.getOrganization().then(function (organization) {
    if (organization || req.user.rel) {
      next(new InvalidCredentialsError(req.__('organization_auth_fail')))
    } else {
      req.urbanity = true
      next()
    }
  })
}
