/**
 * Kazam Api
 * @name JsonWebToken Organization Auth
 * @file lib/filters/organization.js
 * @author Joel Cano
 */

'use strict'

const InvalidCredentialsError = require('restify-errors').InvalidCredentialsError

/**
 * @method auth
 * @description Jwt Organization processing method
 * @param {object}  req - Restify request instance
 * @param {object}  res - Restify response instance
 * @param {function} next - callback
 * @param {integer} role - role integer value
 *
 * @static
 */
function auth (req, res, next, role) {
  req.jwtUser.getOrganization().then(function (organization) {
    if (organization && req.user.rel && role) {
      return req.models.OrganizationUserMerchant
        .findForAuth(organization.id, req.jwtUser.id, req.user.rel, role)
        .then(function (userMerchant) {
          if (userMerchant) return [userMerchant.Merchant, organization]
          else return []
        })
    } else return []
  }).spread(function (merchant, organization) {
    if (merchant && organization) {
      req.organization = organization
      req.merchant = merchant
      next()
    } else next(new InvalidCredentialsError(req.__('organization_auth_fail')))
  }).catch(next)
};

module.exports = {
  /**
   * @method read
   * @description Jwt Organization read role authentication
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  read: function (req, res, next) {
    const Role = req.models.OrganizationUserMerchant.Role
    return auth(req, res, next, [
      Role.Read,
      Role.ReadWrite
    ])
  },
  /**
   * @method readWrite
   * @description Jwt Organization readWrite role authentication
   * @param {object}  req - Restify request instance
   * @param {object}  res - Restify response instance
   * @param {function} next - callback
   *
   * @static
   */
  readWrite: function (req, res, next) {
    return auth(req, res, next, req.models.OrganizationUserMerchant.Role.ReadWrite)
  }
}
