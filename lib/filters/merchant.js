/**
 * Merchant Api
 * @name Merchant Filter
 * @file merchant.js
 * @author Joel Cano
 */

'use strict'

const BadRequestError = require('restify-errors').BadRequestError

module.exports = function (req, res, next) {
  const id = req.params.merchant_id
  const err = new BadRequestError(req.__('merchant_not_found'))
  if (isNaN(id)) next(err)
  else {
    req.models.Merchant.findById(id).then(function (merchant) {
      if (merchant) {
        req.merchant = merchant
        next()
      } else next(err)
    }).catch(next)
  }
}
