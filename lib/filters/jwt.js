/**
 * Kazam Api
 * @name JsonWebToken Secret Method
 * @file lib/filters/jwt.js
 * @author Joel Cano
 */

'use strict'

const InvalidCredentialsError =
  require('restify-errors').InvalidCredentialsError
/**
 * @method organization
 * @description Jwt secret processing method for OrganizationUsers
 * @param {object}  req - Restify request instance
 * @param {object}  payload - Jwt payload
 * @param {function} next - callback
 *
 * @static
 */
module.exports.organization = function (req, payload, next) {
  if (!payload) next(null, null)
  else {
    req.models.OrganizationUserToken.findSessionToken(payload.iss)
      .then(function (result) {
        if (result) {
          req.jwtUser = result.OrganizationUser
          next(null, result.usableSecret())
        } else next(new InvalidCredentialsError(req.__('no_active_token')))
      }).catch(next)
  }
}

/**
 * @method user
 * @description Jwt secret processing method for Users
 * @param {object}  req - Restify request instance
 * @param {object}  payload - Jwt payload
 * @param {function} next - callback
 *
 * @static
 */
module.exports.user = function (req, payload, next) {
  if (!payload) next(null, null)
  else {
    req.models.UserToken.findSessionToken(payload.iss).then(function (result) {
      if (result) {
        req.jwtUser = result.User
        req.jwtToken = result
        next(null, result.usableSecret())
      } else next(new InvalidCredentialsError(req.__('no_active_token')))
    }).catch(next)
  }
}
