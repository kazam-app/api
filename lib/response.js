/**
 * Kazam Api
 * @name Response Helper
 * @file lib/response.js
 * @author Joel Cano
 */

'use strict'

const i18n = require('i18n')
const _ = require('lodash/collection')
const errors = require('restify-errors')
const changeCase = require('change-case')

const RES_LOG = process.env.RES_LOG
const shouldLog = (RES_LOG === 'true' || RES_LOG === true)

function debug (type, msg, logger) {
  if (shouldLog) logger.debug({ response: type }, msg)
}

function warn (type, msg, logger) {
  if (shouldLog) logger.warn({ response: type }, msg)
}

function error (type, msg, logger) {
  if (shouldLog) logger.error({ response: type }, msg)
}

function parseError (msg) {
  if (typeof msg === 'object') {
    if (msg.message) msg = msg.message
    else msg = JSON.stringify(msg)
  }
  if (typeof msg === 'undefined' && !msg) msg = ''
  return i18n.__(msg)
}

module.exports = {
  /**
   * @method success
   * @description Returns custom success middleware function
   * @param {object} res - Restify Response instance
   * @return {function} Custom success middleware
   *
   * @static
   */
  success: function (res, logger) {
    return function (result) {
      if (typeof result === 'boolean') result = result.toString()
      debug('success', '', logger)
      return res.send(200, result)
    }
  },
  /**
   * @method created
   * @description Returns custom created middleware function
   * @param {object} res - Restify Response instance
   * @return {function} Custom success middleware
   *
   * @static
   */
  created: function (res, logger) {
    return function (result) {
      if (typeof result === 'boolean') result = result.toString()
      debug('created', '', logger)
      return res.send(201, result)
    }
  },
  /**
   * @method validation
   * @description Returns custom Bad Request middleware function
   *              Parses Sequelize error into a hash array
   * @param {object} res - Restify Response instance
   * @return {function} Custom Bad Request middleware
   *
   * @static
   */
  validation: function (res, logger) {
    return function (error) {
      let errors = error.errors
      warn('validation', errors, logger)
      let results = {}
      _.each(errors, function (err) {
        // Validator error
        let msg = err.message
        // Schema-level error
        if (err.type === 'notNull Violation') msg = 'empty'
        const field = changeCase.snakeCase(err.path)
        msg = i18n.__(msg)
        if (results[field]) results[field].push(msg)
        else results[field] = [msg]
      })
      // TODO: change to 422 UnprocessableEntityError
      return res.send(400, { code: 'ValidationError', message: results })
    }
  },
  /**
   * @method badRequest
   * @description Returns custom 400 middleware function
   * @param {object} res -  Restify Response instance
   * @return {function}     Custom Bad Request middleware
   *
   * @static
   */
  badRequest: function (res, logger) {
    return function (msg) {
      error('badRequest', msg, logger)
      return res.send(new errors.BadRequestError(parseError(msg)))
    }
  },
  /**
   * @method unauthorized
   * @description Returns custom 401 middleware function
   * @param {object} res  Restify Response instance
   * @return {function}   Custom success middleware
   *
   * @static
   */
  unauthorized: function (res, logger) {
    return function (msg) {
      error('unauthorized', msg, logger)
      return res.send(new errors.UnauthorizedError(msg))
    }
  },
  /**
   * @method forbidden
   * @description Returns custom 403 middleware function
   * @param {object} res  Restify Response instance
   * @return {function}   Custom success middleware
   *
   * @static
   */
  forbidden: function (res, logger) {
    return function (msg) {
      warn('forbidden', msg, logger)
      return res.send(new errors.ForbiddenError(parseError(msg)))
    }
  },
  /**
   * @method notFound
   * @description Returns custom 404 middleware function
   * @param {object} res  Restify Response instance
   * @return {function}   Custom Bad Request middleware
   *
   * @static
   */
  notFound: function (res, logger) {
    return function (msg) {
      warn('notFound', msg, logger)
      return res.send(new errors.NotFoundError(parseError(msg)))
    }
  },
  /**
   * @method gone
   * @description Returns custom 410 middleware function
   * @param {object} res  Restify Response instance
   * @return {function}   Custom success middleware
   *
   * @static
   */
  gone: function (res, logger) {
    return function (msg) {
      warn('gone', msg, logger)
      return res.send(new errors.GoneError(parseError(msg)))
    }
  },
  /**
   * @method tooMany
   * @description Request throttling response HTTP 429
   * @param {object} res  Restify Response instance
   * @return {function}   Custom Bad Request middleware
   *
   * @static
   */
  tooMany: function (res, logger) {
    return function (msg) {
      error('tooMany', msg, logger)
      return res.send(new errors.TooManyRequestsError(parseError(msg)))
    }
  },
  /**
   * @method mailerError
   * @description Returns custom mailerError middleware function
   * @param {object} user    User instance
   * @param {object} res     Restify Response instance
   * @return {function}      Custom success middleware
   *
   * @static
   */
  mailerError: function (res, logger) {
    return function (err, user) {
      error('mailer', err, logger)
      if (process.env.NODE_ENV === 'production') throw err
      else return user
    }
  }
}
