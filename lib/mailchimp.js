/**
 * Kazam Api
 * @name Mailchimp
 * @description Mailchimp Instalation and helper methods
 * @file lib/mailchimp.js
 * @author Joel Cano
 */

'use strict'

const Promise = require('bluebird')
const Mailchimp = require('mailchimp-api-v3')

const mailchimp = new Mailchimp(process.env.MAILCHIMP_KEY)
// const list = process.env.MAILCHIMP_LIST || 'e24cfef922'

module.exports = {
  api: mailchimp,
  /**
   * @method lists
   * @description Gets the all the lists related with the API Key
   * @returns {Promise} returns a user promise with a Mailchimp API Response
   *
   * @static
   */
  lists: function () {
    return mailchimp.get({ path: '/lists' })
  },
  /**
   * @method list
   * @description Gets a list data with the API Key via id
   * @param {string} list - MailChimp List id
   * @returns {Promise} returns a user promise with a Mailchimp API Response
   *
   * @static
   */
  list: function (list) {
    return mailchimp.get({ path: '/lists/' + list })
  },
  /**
   * @method groups
   * @description Retrives all list interest categories with the API Key via id
   * @param {string} list - MailChimp List id
   * @returns {Promise} returns a user promise with a Mailchimp API Response
   *
   * @static
   */
  groups: function (list) {
    return mailchimp.get({ path: '/lists/' + list + '/interest-categories' })
  },
  /**
   * @method updateMember
   * @description Retrives all list interest categories with the API Key via id
   * @param {string} list - MailChimp List id
   * @param {string} member - MailChimp List Member id
   * @param {object} params - Member updated values
   * @returns {Promise} returns a user promise with a Mailchimp API Response
   *
   * @static
   */
  updateMember: function (list, member, params) {
    console.log('updateMember', list, member, params)
    return mailchimp.patch({
      path: '/lists/' + list + '/members/' + member,
      body: params
    })
  },
  /**
   * @method subscribe
   * @description Subscribes a member to the Mailchimp list
   * @param {object} member - MailChimp List Member params
   * @returns {Promise} returns a user promise with a session jwt
   *
   * @static
   */
  subscribe: function (list, member) {
    if (process.env.MAILCHIMP_ACTIVE) {
      console.log(member)
      return mailchimp.post({
        path: '/lists/' + list + '/members',
        body: member
      }).catch(console.log)
    } else {
      return Promise.resolve({
        id: 'MAILCHIMP_NOT_ACTIVE',
        unique_email_id: 'MAILCHIMP_NOT_ACTIVE'
      })
    }
  }
}
