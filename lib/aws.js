/**
 * Kazam Api
 * @name AWS Helper Class
 * @file lib/aws.js
 * @author Joel Cano
 */

'use strict'

const Sdk = require('aws-sdk')
const uuid = require('uuid/v4')
const _ = require('lodash/object')
const Promise = require('bluebird')
const validator = require('validator')
const Buffer = require('safe-buffer').Buffer

function Aws () {
  Sdk.config.update({
    accessKeyId: process.env.AWS_KEY,
    secretAccessKey: process.env.AWS_SECRET
  })
  this.Url = process.env.AWS_S3_URL
  this.Bucket = process.env.AWS_S3_BUCKET
  this.S3 = new Sdk.S3({
    Bucket: this.Bucket
  })
  return this
}

_.assign(Aws.prototype, {
  qr: function (stream, token) {
    return new Promise((resolve, reject) => {
      if (process.env.NO_AWS) {
        resolve('no_aws.gif')
      } else {
        if (stream && !validator.isEmpty(stream)) {
          const name = 'qr-' + token + '.gif'
          const img64 = Buffer(stream.replace(/^data:image\/\w+;base64,/, ''), 'base64')
          const params = {
            Bucket: this.Bucket,
            Key: name,
            Body: img64,
            ACL: 'public-read',
            ContentType: 'image/gif',
            Tagging: 'type=qr'
          }
          this.S3.putObject(params, function (err, data) {
            if (err) reject(err)
            else resolve(name)
          })
        } else reject(new Error('stream_error'))
      }
    })
  },
  /**
   * Build url with name
   * @param  {String} name filename
   * @return {String}      Full url to asset
   */
  getUrl: function (name) {
    // https://s3-us-west-2.amazonaws.com/kzm-api/5c894420-6e34-42dc-9b14-2fe60e6b28e0.jpg
    let url
    if (name) {
      if (process.env.CDN_URL) url = process.env.CDN_URL + '/'
      else url = this.Url + this.Bucket + '/'
      url += name
    }
    return url
  },
  /**
   * Uploads images to S3 and returns a constructed url.
   * @param  {String} stream base64 image stream
   * @param  {String} folder image subfolder or nil
   * @param  {Bunyan} logger bunyan logger or nil
   * @return {Promise}       name + extension Promise
   */
  upload: function (stream, folder, logger) {
    if (!folder) folder = ''
    return new Promise((resolve, reject) => {
      if (stream && process.env.NO_AWS) {
        logger.info({ provider: 'AWS S3', no_aws: true })
        resolve('no_aws.jpg')
      } else {
        logger.info({ provider: 'AWS S3', bucket: this.Bucket })
        if (stream && !validator.isEmpty(stream)) {
          let mime = stream.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/)
          // Check mime
          if (mime && mime.length) {
            mime = mime[1]
            const img64 = Buffer(stream.replace(/^data:image\/\w+;base64,/, ''), 'base64')
            const name = uuid() + '.' + mime.split('/')[1]
            // Build params
            let params = { Key: name, ACL: 'public-read', Bucket: this.Bucket, ContentType: mime }
            // debug params
            logger.debug({ params: params, provider: 'AWS S3' })
            // add body
            params.Body = img64
            // put to S3
            this.S3.putObject(params, function (err, data) {
              if (err) {
                logger.error({ provider: 'AWS S3' }, { message: err.message, name: name })
                reject(err)
              } else {
                logger.info({ provider: 'AWS S3' }, { name: name })
                resolve(name)
              }
            })
          } else reject(new Error('invalid_file'))
        } else resolve()
      }
    })
  },
  /**
   * Remove an object from s3
   * @param  {String} name   Full filename
   * @param  {Bunyan} logger logger object
   * @return {Boolean}       removed Promise
   */
  remove: function (name, logger) {
    return new Promise((resolve, reject) => {
      if (process.env.NO_AWS) {
        logger.info({ provider: 'AWS S3', no_aws: true })
        resolve(true)
      } else {
        const params = { Key: name, Bucket: this.Bucket }
        logger.debug({ params: params, provider: 'AWS S3' })
        this.S3.deleteObject(params, function (err, data) {
          if (err) {
            logger.error({ provider: 'AWS S3', message: err.message, delete: name })
            reject(err)
          } else {
            logger.info({ provider: 'AWS S3', delete: name })
            resolve(true)
          }
        })
      }
    })
  }
})

module.exports = new Aws()
